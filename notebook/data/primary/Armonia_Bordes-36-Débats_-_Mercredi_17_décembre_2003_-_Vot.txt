Armonia Bordes # Débats - Mercredi 17 décembre 2003 - Votes
  Bordes et Cauquil (GUE/NGL
),
par écrit
. 
- Si on ne peut qu’être d’accord avec la condamnation du concept d’"ivoirité" et des assassinats commis en son nom, nous n’avons pas voté ce rapport.
D’une part, il dissimule la responsabilité des gouvernements français successifs qui, après une longue période d’oppression coloniale, ont soutenu la dictature de Houphouët-Boigny, puis le régime autoritaire de son successeur Bédié, qui a inventé la démagogie sur l’"ivoirité" dans sa lutte contre ses rivaux pour le pouvoir.
Et, aujourd’hui encore, l’armée française présente sur le terrain protège les intérêts des capitaux français en Côte d’Ivoire, et nullement la population.
Par ailleurs, que signifie "la restauration de l’autorité de l’État" sans offrir des moyens pour mettre fin �  la "pauvreté, la répartition inégale des richesses, l’injustice sociale, les violations des droits humains, l’oppression des minorités…", 
dans lesquelles le rapport lui-même voit autant de facteurs de déstabilisation qui ont conduit aux conflits actuels?
Quant aux prétendues forces de maintien de la paix, comme celles de la CEDEAO, les précédents du Sierra� Leone et du Liberia ont montré qu’elles ne faisaient en général qu’ajouter une bande armée de plus �  celles qui s’affrontent déj�  sur le dos de la population. Nous n’avons pas cautionné, par notre vote, un texte hypocrite par lequel les parlementaires européens déchargeraient seulement leur conscience. 
