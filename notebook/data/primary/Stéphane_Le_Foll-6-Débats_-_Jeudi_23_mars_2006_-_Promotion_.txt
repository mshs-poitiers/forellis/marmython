Stéphane Le Foll # Débats - Jeudi 23 mars 2006 - Promotion de cultures à des fins non alimentaires (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Stéphane Le Foll, 
au nom du groupe PSE
. - Monsieur le Président, Madame la Commissaire, mes chers collègues, je voudrais d’abord, comme tous ceux qui viennent de s’exprimer, saluer le travail qui a été fait par M. Parish et vous dire que je partage totalement les objectifs qui sont affichés dans ce rapport.
Premièrement, prendre en compte, de manière globale, la question environnementale: bioénergie, biomasse et matières biodégradables. Deuxièmement, diversifier nos sources d’énergie quand on sait que nous devons anticiper la fin des énergies fossiles à 40/50 ans et qu’il faut lutter de toutes nos forces et par tous les moyens contre l’effet de serre. Troisièmement, et surtout, puisqu’il s’agit d’agriculture, offrir de nouveaux débouchés à notre agriculture européenne.
Cela dit, je voudrais ajouter que cela ne peut se faire qu’à deux conditions, Madame la Commissaire. D’abord, les biocarburants ne résument pas à eux seuls l’ensemble de la politique agricole et il est nécessaire de conserver, dans le cadre de cette politique agricole, deux piliers: celui lié à la production et celui lié à la cohésion territoriale et sociale. Ensuite, la nouvelle politique visant les bioénergies ne peut, en particulier, avoir de sens que si elle s’inscrit pleinement dans une logique et une volonté politique de faire du modèle agricole un modèle de développement durable. Il me semble qu’à ce titre, il faut aussi que nous engagions une réflexion sur les nouveaux modèles qui permettent de diversifier les productions agricoles en même temps que de garantir un bon équilibre écologique à l’ensemble de nos territoires. 
