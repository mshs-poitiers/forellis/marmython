Jean-Louis Bernié # Débats - Mercredi 20 novembre 2002 - Votes
  Bernié (EDD
),
par écrit
. - La publicité des produits du tabac - comme la protection de la nature et le bien-être animal - génère toujours un débat polémique et passionnel. Je félicite le rapporteur pour la qualité de ses amendements et pour sa tentative de calmer le jeu. Nous soutenons son rapport qui vise à concilier la prévention du tabagisme, mesure prioritaire au titre de la santé publique comme le préconise l’OMS, la possibilité pour les États membres de maintenir leurs dispositions sur la publicité, ce sans entraver le fonctionnement du marché intérieur et tout en préservant quelques perspectives à la tabaculture, filière à l’avenir incertain, qui permet le maintien de nombreuses petites exploitations à l’équilibre fragile.
Cinq cents producteurs européens se sont réunis, ici-même à Strasbourg, du 5 au 7 novembre, pour exprimer leurs inquiétudes sur l'avenir de l'OCM mise en place en 1999 mais aussi pour présenter les progrès réalisés.
Ils ont en effet consenti de gros efforts pour améliorer la qualité d’une production qui ne représente que 6 % de la production mondiale en procédant la réduction du goudron dans la plante, (notons que le tabac de Virginie, de plus en plus cultivé, ne rejette pratiquement pas de nitrates). Soutenons les efforts des tabaculteurs ! 
