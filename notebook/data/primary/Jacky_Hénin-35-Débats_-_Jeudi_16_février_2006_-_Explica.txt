Jacky Hénin # Débats - Jeudi 16 février 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Gebhardt (A6-0409/2005
) 
  Jacky Henin (GUE/NGL
), 
par écrit
. - Tout se résume à deux chiffres: aujourd’hui, dans l’Union, les services représentent 70% de la valeur ajoutée mais ils ne pèsent que 20% dans les échanges intracommunautaires.
Alors, faute de pouvoir délocaliser la majorité des entreprises de services, on délocalise les salaires de misère et pour ce faire, on a inventé cette directive Bolkestein. Son but: casser les salaires, tirer vers le bas les protections sociales, limiter les droits des consommateurs, démanteler les services publics. Ce n’est même plus de l’ultralibéralisme, c’est du total libéralisme.
Le compromis entre le PPE et le PSE, loin de régler le problème du principe du pays d’origine, loin de préserver les services publics de la loi impitoyable du marché, ne fait que placer la Commission et la Cour de justice en position d’arbitre suprême. Vu l’orientation ultralibérale de la Commission et de la Cour de justice, cela revient à confier la garde d’un poulailler à des renards.
Accepter la directive sur les services reviendrait à signer l’arrêt de mort, en Europe, de la protection des salariés et des consommateurs, celui des services publics.
Les peuples d’Europe n’ont pas besoin d’une directive qui mette en concurrence les choix de politique sociale et fiscale qu’ils ont faits démocratiquement.
(Explication de vote écourtée en application de l’article 163, paragraphe 1, du règlement)

