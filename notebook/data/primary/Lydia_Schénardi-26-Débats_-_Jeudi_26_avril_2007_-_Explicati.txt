Lydia Schénardi # Débats - Jeudi 26 avril 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Proposition de résolution: B6-0155/2007

  Lydia Schenardi (ITS
), 
par écrit
. - Nous avons voté pour la résolution sur le projet de navigation par satellite Galileo.
Nous avons eu l’occasion à plusieurs reprises d’exprimer ici notre soutien à un projet industriel ambitieux, pour lequel, pour une fois, la dimension européenne avait toute sa place, même si nous en avons également souligné les insuffisances ou les défauts, tels que sa dimension purement civile ou encore les transferts de technologies à des pays non européens tels que la Chine, dictature communiste et concurrent commercial déloyal.
Aujourd’hui, les négociations concernant le déploiement et l’exploitation du système sont bloquées. Et il me vient à l’esprit que la volonté politique d’États agissant en coopération de manière très pragmatique, avait su mettre tous les moyens, techniques, humains et financiers, au service de la construction d’une industrie aéronautique européenne et fait naître Airbus. Dans le cas de Galileo, l’Eurocratie ayant étendu ses pouvoirs, c’est la Commission de Bruxelles qui négocie laborieusement avec des partenaires privés et les discussions achoppent sur des problèmes d’argent.
Visiblement, la première méthode était de loin la meilleure. 
