Patrick Louis # Débats - Mardi 26 septembre 2006 - Livre blanc de la Commission sur les services d’intérêt général (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Patrick Louis (IND/DEM
). 
 - Monsieur le Président Barroso, Monsieur le Président, mes chers collègues, ce rapport invoque, à juste titre, le principe de la subsidiarité comme base juridique des questions relatives aux services d’intérêt général. Sa définition est floue. D’un principe de suppléance, nous sommes passés à un principe de délégation. De ce fait, la définition des services d’intérêt général ne peut s’établir qu’aux dépens des singularités et des nécessités nationales.
Une fois encore, nous devons déplorer que l’avertissement sans frais que vous ont adressé les peuples français et néerlandais soit traité avec autant de mépris. Je rappellerai que les services d’intérêt général ne concernent l’Union européenne que sous l’angle, très contestable, de la concurrence. On porte ici gravement atteinte à la liberté des États de définir les missions qu’ils entendent confier au service public, conformément aux vœux de leurs peuples.
Non, il n’appartient pas à des pays qui n’ont pas, historiquement, de culture des services publics d’empêcher ceux qui en ont une de disposer de tels services lorsqu’ils l’ont estimé justifié. Si vous respectiez le principe de subsidiarité dans son vrai sens, vous admettriez que les services publics, essentiels à la vie de nos sociétés, doivent être définis, organisés et gérés au plus près des citoyens dans le cadre national.
Nous pensons que seule une puissance publique forte est capable d’assurer un avenir, qui ne se conçoive pas à l’aune de la seule rentabilité trimestrielle des actions, mais sur des décennies en matière d’éducation, de santé, de transport, d’énergie, d’écologie.
(Le Président invite l’orateur à conclure)

La seule règle dont nous ayons besoin, c’est celle de la souveraineté, ce qui signifie que chaque État devrait être libre de décider de ses services publics et que les obligations de service public priment sur le droit communautaire de concurrence. 
