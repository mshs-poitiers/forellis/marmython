Christine de Veyrac # Débats - Mardi 11 décembre 2007 - Agence européenne de la sécurité aérienne (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Christine De Veyrac  (PPE-DE
), 
par écrit
. – (FR)
 Je me réjouis que nous soyons arrivés à un accord équilibré avec le Conseil. Cela devrait nous permettre de mettre en œuvre rapidement ce texte qui est un progrès certain pour l'Union européenne et la sécurité des passagers.
Désormais, chaque aéronef des pays tiers sera inspecté et devra recevoir une certification pour être autorisé à transporter des voyageurs dans l'Union, c'est aussi une avancée pour l'harmonisation de la protection des citoyens européens. Cela prolonge le processus amorcé avec la liste noire européenne, qui permet à chaque passager d'être protégé de la même façon quel que soit l'aéroport qu'il emprunte sur le territoire communautaire.
Avant de conclure, je voudrais évoquer un autre point qui me semble important. Dans le financement de Galileo adopté récemment, il est prévu que 50 millions soient prélevés sur le budget de plusieurs agences européennes, dont l'AESA. J'ai toujours soutenu le projet Galileo qui revêt une importance particulière pour ma région et je suis très heureuse qu'on soit enfin parvenu à un accord sur son financement. Mais je voudrais souligner qu'on ne peut pas à la fois augmenter les compétences d'une agence et diminuer son budget. L'AESA ne doit pas être concernée par cette ponction. 
