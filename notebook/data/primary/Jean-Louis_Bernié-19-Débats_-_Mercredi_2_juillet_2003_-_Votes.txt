Jean-Louis Bernié # Débats - Mercredi 2 juillet 2003 - Votes
  Bernié, Butel et Esclopé (EDD
),
par écrit
. - Sur l’indication des ingrédients présents dans les denrées alimentaires, la position commune est satisfaisante à une exception près.
Sur les auxiliaires technologiques, elle doit être révisée. La position commune est en effet tout à fait disproportionnée en demandant d’indiquer sur les étiquettes toutes les substances utilisées dans le processus de fabrication. Surtout lorsque ces substances ne laissent pas de traces dans le produit fini.
C’est le cas de boissons renommées qui font vivre des régions entières, comme la bière et le vin, qui pourraient ainsi se voir étiquetées comme contenant des dérivés d'œuf et de poisson. De quoi ruiner la réputation de ces boissons de qualité, consommées sans risque depuis des siècles.
Les amendements déposés, qui vont dans le même sens que ceux que nous avions déposés pour le vote en commission de l’environnement, demandent que ces auxiliaires technologiques fassent l’objet de tests scientifiques précis prouvant leurs effets allergènes.
Pour l’instant, rien n’est prouvé, tout est supposé. Attendons donc les premiers résultats avant de rendre obligatoire l’étiquetage de ces auxiliaires technologiques. Oui à la transparence pour informer les consommateurs sur la composition des produits tout en s’opposant au sur-étiquetage, désastreux pour des filières agro-alimentaires vitales pour la France comme de nombreux autres pays. 
