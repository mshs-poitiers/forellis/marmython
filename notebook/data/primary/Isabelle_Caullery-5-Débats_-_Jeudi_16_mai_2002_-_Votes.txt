Isabelle Caullery # Débats - Jeudi 16 mai 2002 - Votes
  Caullery (UEN
),
par écrit
. - 
La délégation française de mon groupe ne peut aucunement souscrire aux conclusions de ce rapport d’initiative.
Certes, le système actuel de répartition des compétences est d’une effroyable complexité. Nous approuvons le rapporteur lorsqu’il indique que les États membres "doivent disposer d’une compétence de droit commun et que l’Union ne doit bénéficier que de compétences d’attribution". Enfin, il est indéniable qu’il existe un décalage croissant entre les attentes des citoyens à l’égard de l’Europe et les problèmes effectivement traités par celle-ci.
Cependant, rien ne justifie que les règles clarifiant la ventilation des compétences soient introduites dans une "Constitution européenne", pour l’élaboration de laquelle ni notre Parlement ni la Convention n’ont reçu de mandat des peuples.
S’il est évidemment nécessaire de définir une hiérarchie des normes communautaires, cela ne doit pas déboucher sur une augmentation des compétences législatives du Parlement. La légitimité démocratique de l’Union découle uniquement du consentement et de la participation des peuples au processus en cours. Elle réside donc au sein du Conseil européen, du Conseil des ministres et des parlements nationaux. En définitive, nous refusons ce rapport parce que, sous prétexte de mieux répartir les compétences, le rapporteur ne cherche qu’à accroître celles de l'Union et du Parlement. ...
(Explication de vote écourtée en application de l'article 137, paragraphe 1, du règlement)

