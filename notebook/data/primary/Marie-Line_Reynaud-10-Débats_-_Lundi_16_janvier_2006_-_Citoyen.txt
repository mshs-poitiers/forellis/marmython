Marie-Line Reynaud # Débats - Lundi 16 janvier 2006 - Citoyenneté de l’Union (4e rapport) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie-Line Reynaud, 
au nom du groupe PSE
. - Monsieur le Président, en tant que rapporteur fictif pour le parti socialiste de la commission libertés civiles, justice et affaires intérieures, je suis très satisfaite du texte qui nous est proposé aujourd’hui et je n’ai d’ailleurs déposé aucun amendement pour le vote en plénière.
Je tiens tout d’abord à féliciter M. Cattania qui a fourni un travail remarquable dans le cadre de la commission LIBE et qui est parvenu à rassembler une majorité de députés autour de son projet de rapport. Grâce à des amendements de compromis élaborés conjointement par la GUE, les Verts, le PSE et les libéraux, et également à des amendements individuels déposés par des membres de ces groupes, nous avons obtenu, il me semble, un certain nombre d’avancées importantes. J’en citerai cinq:
Le Parlement européen invite les États membres à réfléchir à l’établissement d’un lien plus fort entre la résidence légale permanente pendant une période de temps raisonnable et l’obtention de la citoyenneté nationale et donc européenne. Il affirme son souhait d’une coordination accrue des critères généraux et des procédures d’acquisition de la nationalité dans les États membres.
Deuxièmement, il demande aux États membres de débattre de la possibilité de créer une carte européenne d’électeur, commune à l’ensemble des États de l’Union.
Troisièmement, il invite la Commission à rédiger un livre blanc sur les évolutions possibles de la citoyenneté européenne.
Quatrièmement, il invite les États membres à étendre le droit de vote aux élections municipales et européennes aux ressortissants de pays tiers et aux apatrides résidant de façon permanente dans l’Union depuis plus de cinq ans, ainsi que le droit de circuler librement et d’obtenir un permis de séjour permanent dans n’importe quel État membre.
Enfin, le Parlement demande aux États membres de débattre au plus vite de la possibilité de reconnaître aux citoyens de l’Union le droit de voter et de se présenter aux élections municipales, cantonales et régionales de l’État membre dans lequel ils résident, de même que de leur octroyer le choix, non cumulable, de voter et de se présenter aux élections nationales soit dans le pays dans lequel ils résident, soit dans leur pays d’origine.
À travers ces demandes et ces souhaits exprimés par le Parlement européen, il s’agit bien de faire évoluer l’Union vers une véritable communauté politique. La citoyenneté politique et la participation démocratique sont les clés de l’avenir même du projet européen que nous désirons. 
