Françoise Castex # Débats - Mardi 26 septembre 2006 - Livre blanc de la Commission sur les services d’intérêt général (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Françoise Castex (PSE
), 
rapporteur pour avis de la commission du commerce international
. - Madame la Présidente, Monsieur le Président, mes chers collègues, vous l’avez rappelé, chaque pays de l’Union a une histoire et une conception propre des services publics. La construction européenne doit effectivement se poursuivre dans le respect de cette diversité, mais en prenant appui sur un ensemble de valeurs communes tenant à la justice sociale, à l’égalité, à la solidarité. Nous pouvons dégager un cadre commun pour un service public européen. Celui-ci ne peut être envisagé sous l’angle de la concurrence car nous savons que les bénéfices que tire la société des services publics se comptent en termes d’éducation, de santé, de sécurité, de cohésion entre les États membres et leurs concitoyens.
Nous allons prochainement, comme cela a été rappelé, adopter une directive ouvrant le marché des services au niveau communautaire. Nous connaissons les enjeux de ce marché des services dans les négociations de l’OMC. Au niveau communautaire comme au niveau mondial, il est impératif d’être capable juridiquement de distinguer entre services marchands et services non marchands et d’établir la libre circulation des uns et la réglementation des autres. La santé, l’éducation, le logement social mais aussi la distribution de l’eau doivent-ils être régis par la seule loi du profit? Bien sûr que non! Il n’y a qu’un seul impératif qui vaille, celui de l’intérêt général et du progrès social pour tous. Et seule l’autorité publique, quel que soit son niveau, est la garante de cet intérêt général.
Nos concitoyens craignent, à juste titre, que le projet européen se dilue dans la libéralisation et dans la mondialisation. Nous devons répondre à leurs exigences de qualité, d’accessibilité, de responsabilité sociale et de respect de l’environnement. Quelle meilleure garantie, Monsieur le Commissaire, offrir à nos concitoyens inquiets qu’un cadre juridique européen pour les services publics? 
