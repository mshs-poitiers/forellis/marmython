Stéphane Le Foll # Débats - Mardi 5 mai 2009 - Vins rosés et pratiques œnologiques autorisées (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Stéphane Le Foll  (PSE
), 
par écrit
. – 
Les producteurs de vins de rosé européens sont aujourd’hui préoccupés par le projet d’application de la Commission, relatif la levée de l’interdiction de coupage de vins blancs et rouges de table pour élaborer des vins rosés. 
Cette nouvelle pratique nuit à une production de qualité et ignore les efforts entrepris par les producteurs, depuis plusieurs années pour élaborer un produit de rosé longtemps déconsidéré, mais qui a aujourd’hui trouvé une vraie place sur le marché et dans les habitudes de nombreux consommateurs, d’autant que cette pratique risque de tromper le consommateur. 
Si dans les prochaines semaines le projet de coupage de la Commission devait être confirmé par les États membres, nous souhaitons avec mon ami Gilles Savary qu’un étiquetage soit rendu obligatoire, permettant de distinguer le vrai vin rosé, d’un nouveau produit issu de coupage qui ne pourrait alors pas être appelé rosé.
