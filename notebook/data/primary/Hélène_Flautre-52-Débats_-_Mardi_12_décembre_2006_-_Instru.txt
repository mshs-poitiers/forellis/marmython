Hélène Flautre # Débats - Mardi 12 décembre 2006 - Instrument financier européen pour la promotion de la démocratie et des droits de l’homme dans le monde (vote) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Hélène Flautre (Verts/ALE
), 
rapporteur
. - Monsieur le Président, je veux dire, brièvement, que j’espère que nous nous apprêtons à adopter, massivement, cet instrument européen pour la démocratie et les droits de l’homme parce que c’est le seul instrument qui permet de soutenir les projets de promotion de la démocratie dans les pays tiers, sans l’aval des gouvernements, le seul instrument, donc, qui permet de répondre aux questions qui viennent de nous être posées par M. Milinkievitch: comment soutenons-nous des médias libres, comment soutenons-nous la société civile indépendante, comment défendons-nous et protégeons-nous les défenseurs des droits de l’homme dans les pays tiers, sans l’aval des gouvernements? La réponse est dans cet instrument.
(Applaudissements)

