Bruno Gollnisch # Débats - Jeudi 17 mai 2001 - VOTES
  Gollnisch (TDI
). 
 - Merci, Madame la Présidente, je voulais simplement dire, conformément aux usages de cette maison et aux recommandations des questeurs, que, dans la ligne de la position juridique exprimée tout à l'heure, mes collègues et moi ne participerions pas au vote final du rapport Schleicher. Voilà, c'était bien simple, c'était bien peu de choses et j'ajouterai juste un petit commentaire, Madame la Présidente : n'ayez pas peur de nous, n'ayez pas peur de nous donner la parole, nous ne la prenons que pour renvoyer au règlement et nous conformer aux usages de cette maison. Nous sommes, à tout prendre, beaucoup moins méchants qu'on ne le décrit habituellement. 
