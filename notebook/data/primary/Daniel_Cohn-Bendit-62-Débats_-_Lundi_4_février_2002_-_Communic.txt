Daniel Cohn-Bendit # Débats - Lundi 4 février 2002 - Communication du Président
  Cohn-Bendit (Verts/ALE
). 
 - Monsieur le Président, vous savez que j'ai défendu une position contraire et je reste persuadé que le fait de ne pas porter plainte est une erreur. Je voulais simplement le dire aussi en plénière, car si nous acceptons que le Conseil puisse faire passer quelque chose en codécision, en nous enlevant la codécision, on fait le jeu du Conseil. Malheureusement, je ne crois pas que le Conseil l'interprétera de façon positive, mais plutôt comme quelque chose qu'il pourra répéter. Je voulais donc seulement dire que je crois que c'est une erreur de ne pas porter plainte devant la Cour de justice. 
