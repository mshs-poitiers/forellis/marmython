Gérard Caudron # Débats - Mercredi 24 septembre 2003 - Votes
  Caudron (GUE/NGL
),
par écrit
. - Ma position d’aujourd’hui ne préjuge en rien de ma position sur la future Constitution européenne.
Par principe, je suis pour une Constitution mais je ne peux, à ce stade, m’associer à ceux qui déclarent que la Conférence intergouvernementale ne doit pas changer notablement le projet issu de la Convention.
Le projet de Valéry Giscard d’Estaing est en effet notoirement insuffisant sur le plan social et dans sa dimension citoyenne.
Une Constitution n’a pas, par ailleurs, à figer le cadre économique et politique de l’Union et à interdire ainsi toute possibilité réelle pour les citoyens de changer de société par leur vote démocratique.
J’espère donc de la CIG des changements profonds qui me permettraient de voter, le moment venu, pour la Constitution. 
