Robert Navarro # Débats - Mercredi 10 octobre 2007 - Une politique maritime pour l’Union européenne
  Robert Navarro  (PSE
), 
par écrit
. – 
(FR)
 Avec ce 'Livre Bleu', l'Union européenne fait un pas en avant. Même s'il faut déplorer que sur certains points - comme la question des garde-côtes européens ou celle d'un pavillon européen - la Commission européenne ait dû revoir ses ambitions à la baisse, faute de soutien de la part de certains Etats-membres, ce document est une bonne base de départ. J'espère donc qu'il tiendra toutes ses promesses. La question des financements sera néanmoins déterminante, chose que la Commission elle-même reconnaît. Nos ministres assumeront-ils leurs responsabilités?
Pour le reste, je me réjouis tout particulièrement de la décision de la Commission de s'attaquer à la révision de la législation sociale dans le secteur maritime, où souvent le droit du travail classique et ses protections ne s'appliquent pas. Il était grand temps! Ce facteur déterminant pour l'attractivité des carrières maritimes devrait donc aider l'Europe à préserver ses savoir-faire maritimes. 
