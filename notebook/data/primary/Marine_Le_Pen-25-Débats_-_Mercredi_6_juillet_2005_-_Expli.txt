Marine Le Pen # Débats - Mercredi 6 juillet 2005 - Explications de vote
  Marine Le Pen (NI
),
par écrit. -
 Ce rapport, ou plutôt ce réquisitoire, devrait suffire à prouver à tous les partisans d’une Europe sans frontière, sans culture et sans passé, que la Turquie n’est pas un pays européen et que notre modèle démocratique occidental n’est pas forcément partagé par une nation qui est sensible à d’autres héritages.
Ce rapport dresse la liste de toutes les atteintes graves en matière de droits de la femme commises par l’État Turc, qu’il s’agisse des violences diverses, de l’accès des femmes au marché du travail ou de leur absence dans les organes de représentation politique. Ce rapport met aussi en évidence le système discriminatoire qui, dès le plus jeune âge, cantonne la femme dans un rôle accessoire et soumis. Enfin, il apparaît que la Turquie maintienne un système d’oppression des minorités, notamment des communautés Kurdes.
Les avancées modestes décidées par le Gouvernement Turc en la matière et les difficultés rencontrées sur le terrain pour les faire appliquer, ne permettent pas de remplir les conditions par ailleurs insuffisantes pour adhérer à l’Union européenne.
Dans ces conditions et après avoir pris acte des résultats instructifs des référendums français et néerlandais, nous demandons l’arrêt immédiat des négociations d’adhésion avec la Turquie. 
