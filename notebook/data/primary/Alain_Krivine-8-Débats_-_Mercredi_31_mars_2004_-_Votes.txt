Alain Krivine # Débats - Mercredi 31 mars 2004 - Votes
  Krivine et Vachetta (GUE/NGL
),
par écrit
. 
- Nous avons rejeté le texte d’initiative de la République italienne sur les "charters européens", condamnés par la Convention européenne des droits de l’homme et appelés pudiquement vols communs. Nous sommes scandalisés par l’acharnement dont fait preuve l’Union européenne (UE) en général et certains de ses États membres, comme la France, l’Allemagne et l’Italie, à vouloir mettre en place à tout prix un dispositif visant à rationaliser, par l’organisation de vols communs, les opérations d’éloignement des ressortissants. Nous trouvons scandaleux l’idée de financer ce type d’opération à hauteur de 30 millions d’euros, comme le proposent la Commission et le Conseil. Cette politique de l’Europe forteresse ne mène qu’à la honte.
Nous aimerions plutôt que le même acharnement, que les mêmes efforts soient consacrés à la lutte contre le chômage, les inégalités et la pauvreté dans l’UE. Mais cela ne semble pas être la priorité des Quinze. Ils préfèrent flatter les tendances populistes et xénophobes de l’électorat en désignant les immigrés comme le problème numéro 1 de nos sociétés.
Au contraire, dans ce contexte, nous continuons à réaffirmer avec les associations de défense des droits de l’homme et de lutte contre le racisme la nécessité d’une autre politique d’immigration en Europe, une politique basée sur l’ouverture des frontières, le respect du droit d’asile et l’égalité des droits. 
