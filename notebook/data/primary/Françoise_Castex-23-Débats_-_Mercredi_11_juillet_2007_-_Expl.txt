Françoise Castex # Débats - Mercredi 11 juillet 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Ferber (A6-0246/2007
) 
  Françoise Castex (PSE
), 
par écrit.
 -
 J’ai voté contre la libéralisation des services postaux. En outre, j’ai soutenu la demande de rejet de cette directive au motif que les motivations de cette proposition sont contradictoires et non réalisables en ce qui concerne la garantie de l’universalité du service pour lequel les moyens de financement ne sauvegardent pas le droit à la collecte et à l’expédition quotidiennes du courrier pour tous les citoyens européens.
Enfin, je considère que la fixation a priori d’un terme d’expiration de la directive 97/67 ne s’adapte pas aux résultats de la consultation des acteurs sociaux concernés et des États membres qui ont manifesté la nécessité de maintenir d’une manière uniforme, proportionnelle et équitable les garanties du service universel.
Pour moi, cette proposition n’offre aucune garantie pour régler sérieusement les aspects sociaux de cette libéralisation afin d’éviter la concurrence sur le marché de l’emploi et le dumping social. 
