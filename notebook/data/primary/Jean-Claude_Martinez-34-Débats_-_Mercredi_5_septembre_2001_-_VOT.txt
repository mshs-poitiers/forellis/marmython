Jean-Claude Martinez # Débats - Mercredi 5 septembre 2001 - VOTES
  Martinez (TDI
),
par écrit
. -
 C'est sans doute une très grande aventure que l'élargissement. Mais elle est rabaissée jusqu'ici au rang d'une OPA de type commercial ou d'une fusion de sociétés, au lieu d'être l'événement historique majeur de retrouvailles des peuples chrétiens qui se réunissent, au moment où l'adversaire séculaire revient menacer le sud. A cette aune-là du camp de la liberté et de la lumière contre le camp de l'obscurantisme intégriste, l'Ukraine et la Russie, sentinelle avancée de l'Occident, auraient eu d'ailleurs leur place.
De cet enlisement technique dérisoire d'une question politique au sommet, l'expression "d'acquis communautaire" est le résumé. En une trentaine de chapitres, de l'agriculture à la fiscalité, en passant par l'environnement, les PME, l'éducation ou les transports, les législations des pays candidats comme leurs administrations, leurs sociologies et même leurs morales sont passées au crible d'un regard policier qui doit rappeler à ces anciens pays de la morale prolétarienne des pratiques pesantes non encore oubliées. Tel État se voit ainsi reprocher, comme obstacle à son adhésion, son cadre pénal réprimant telle pratique sexuelle. Beaucoup sont sermonnés pour leurs fraudes et actes de corruption, comme si la TVA européenne n'était pas, depuis 1993, un facteur d'une fraude fiscale massive. Et Malte se voit même reprocher d'attirer trop de sociétés délocalisées grâce à un régime fiscal privilégié. Comme si l'Irlande ne l'avait pas fait et comme si les États-Unis ne pratiquaient pas ce système à une échelle énorme avec les FSC ; condamnées par l'OMC, mais tolérées par l'Europe qui descend tous les "échelons" de sa soumission à l'Empire.
À aucun moment, l'élargissement n'est l'occasion d'un dialogue des expériences et d'un échange des réussites. La Pologne ainsi, à forte raison, fait observer que son acquis agricole vaut bien l'acquis d'une Europe aux 8 millions d'animaux abattus pour fièvre aphteuse ou ESB et aux 136 000 personnes qui incuberaient la maladie mortelle du Creutzfeld-Jakob atypique.
Au lieu de s'enrichir et de préserver le patrimoine de biodiversité végétale et animale de ces pays de l'Est, sans parler de leur patrimoine de liens sociaux non encore brisés par la sauvagerie ultracapitaliste, la Commission européenne avilit ce rendez-vous historique à une annexion sans condition. Un Anschluss en somme. Ce qui permet de comprendre pourquoi le dossier a été confié à un commissaire allemand et le rapport du Parlement européen à un député, comme par hasard, tout aussi allemand.
Les choses sont, il est vrai, ainsi plus claires. Il y a bien élargissement, mais comme en 1990, il est encore allemand. La parenthèse de 1945 se referme. L'Empire se reconstitue. La Mitteleuropa est là. Éternelle dans sa géographie et son histoire. 
