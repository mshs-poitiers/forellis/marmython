Ambroise Guellec # Débats - Mardi 14 novembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Lienemann (A6-0373/2006
) 
  Ambroise Guellec (PPE-DE
), 
par écrit
. - Je salue l’adoption en première lecture du rapport de Mme Lienemann sur la directive «Stratégie pour le milieu marin», directive essentielle qui s’inscrit dans la continuité de la directive cadre sur l’eau (DCE). Je souhaite cependant exprimer mon scepticisme quant à la faisabilité du calendrier qui vient d’être adopté. En effet, la connaissance du milieu marin n’est pas encore suffisante pour parvenir, dans des délais raccourcis, à une évaluation efficace de la situation environnementale des mers européennes. On le voit actuellement pour la mise en œuvre de la DCE, la phase de préparation/ état des lieux est toujours difficile et plus longue que prévue. De plus, le milieu marin a une inertie particulièrement importante. Il me semble donc que le calendrier proposé par la Commission est déjà suffisamment ambitieux. 
