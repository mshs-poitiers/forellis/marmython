Jacques Toubon # Débats - Jeudi 18 décembre 2008 - L’acte authentique européen - E-Justice - Protection juridique des adultes: implications transfrontalières (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jacques Toubon  (PPE-DE
). 
– 
Monsieur le Président, chers collègues, Madame la Ministre de la justice, c’est justement en cette qualité que je voulais m’adresser à vous à la fin de ce débat. En effet, avec la Présidence portugaise, la Présidence slovène, et aujourd’hui la Présidence française, c’est-à-dire Rachida Dati comme garde des sceaux, je crois vraiment – et je le dis avec mon expérience d’ancien ministre de la justice – que nous avons franchi, en Europe, un pas, une étape, et que nous ne reviendrons pas en arrière.
Aujourd’hui il est en train de se créer, entre les systèmes juridiques et judiciaires, qui sont pourtant par nature méfiants les uns par rapport aux autres, une entreprise de rapprochement, de reconnaissance et d’harmonisation. Il y a même, sur certains points, une démarche visant à entreprendre des législations qui soient des législations communautaires, totales ou partielles, comme par exemple sur les obligations alimentaires. De cette façon, au-delà même de ce que nous avons fait sous la pression de la nécessité, par exemple en matière de sécurité, de droit pénal et de lutte contre le terrorisme, nous nous adressons aujourd’hui à ceux qui ont besoin de règles et de règlement des litiges qui s’appliquent partout en Europe, parce qu’ils vivent, par définition, dans leur pays mais aussi partout ailleurs, qu’ils travaillent et qu’ils ont des relations avec tout le monde dans l’ensemble de l’Europe.
Et cela, Monsieur le Président, il faut le souligner. C’est la marque, incontestablement, de la Présidence française. Tous mes collègues l’ont dit, mais il faut souligner que c’est un pas historique qui est franchi, incontestablement, dans la justice en matière de coopération et de législation. L’Europe, je pense, ne sera plus pareille si, véritablement, les gens ont le sentiment que l’espace judiciaire européen c’est quelque chose qui n’est pas seulement pour les discours dominicaux. 
