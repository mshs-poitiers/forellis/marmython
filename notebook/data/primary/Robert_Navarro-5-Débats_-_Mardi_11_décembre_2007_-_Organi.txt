Robert Navarro # Débats - Mardi 11 décembre 2007 - Organisation commune du marché vitivinicole (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Robert Navarro  (PSE
), 
par écrit
. – 
(FR)
 La Commission européenne a décidément des difficultés à sortir des approches stéréotypées qu'elle applique à tous les secteurs sans distinction. Le vin ne peut pas être traité comme les pièces détachées automobiles ou je ne sais quel autre type de produit manufacturier industriel. C'est une production qui façonne les régions, les cultures, les modes de vie.
Concernant la réforme de l'OCM, si elle est nécessaire, elle doit permettre de préserver la viticulture européenne, pas la faire disparaître ou la défigurer. Elle doit soutenir l'adaptation des producteurs - notamment, la restructuration des filières d'aval - et doit avoir pour objectif de leur permettre de reconquérir le marché intérieur. D'autre part, les restrictions en matière de droits de plantation ne devraient pas être levées. Elles sont la garantie d'un contrôle de la production et d'un maintien de sa qualité. Enfin, concernant les outils de gestion de crise, si le Parlement améliore la situation par rapport à la proposition initiale de la Commission, je ne peux que regretter que la distillation de crise - qui, rendue obligatoire, ne donnerait plus lieu aux excès que nous avons connus - ne figure plus dans la palette des instruments possibles pour gérer les crises conjoncturelles. 
