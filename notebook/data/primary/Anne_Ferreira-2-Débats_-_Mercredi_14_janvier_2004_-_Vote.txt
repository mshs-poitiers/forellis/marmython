Anne Ferreira # Débats - Mercredi 14 janvier 2004 - Votes
  Ferreira (PSE
),
par écrit
. 
- J’ai voté contre la proposition de résolution sur les services d’intérêt général car les orientations votées par une majorité de députés européens ne donnent pas une image objective des conséquences des libéralisations menées jusqu’à présent. En outre, elles ouvrent la voie à la libéralisation d’autres secteurs.
Aujourd’hui, il est impératif, comme nous l’avons déjà demandé par le passé, de réaliser une évaluation sérieuse, pluraliste et contradictoire de la politique de libéralisation et d’en rendre publiques les conclusions.
Il est nécessaire de revenir sur les libéralisations entreprises, afin également de donner toute sa portée à l’adoption d’une directive-cadre. Par ailleurs, il est largement temps d’exclure définitivement des secteurs de la politique de concurrence qui prévaut au niveau communautaire. Il en va ainsi notamment de la santé, de l’éducation et des services sociaux, y compris le logement social. Enfin, il convient de garantir l’exercice de la subsidiarité et des pouvoirs locaux ou territoriaux.
Cette résolution ne consacre pas les services d’intérêt général européen comme une valeur ajoutée de la construction européenne et comme moyen de réaliser les objectifs de cohésion territoriale, économique, écologique et sociale de l’Union. Les services d’intérêt général sont un élément essentiel de notre modèle social qu’il convient de défendre et de promouvoir. 
