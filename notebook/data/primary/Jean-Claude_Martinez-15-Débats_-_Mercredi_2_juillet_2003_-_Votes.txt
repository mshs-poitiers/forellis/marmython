Jean-Claude Martinez # Débats - Mercredi 2 juillet 2003 - Votes
  Martinez (NI
),
par écrit
. - Tout un chacun est favorable à l’étiquetage des denrées alimentaires contenant des OGM, afin d’informer le consommateur et assurer la traçabilité de ces produits le long de la chaîne de production et de distribution. Pourtant, ces mesures jettent surtout de la poudre aux yeux, parce que les étiquettes, les contrôles et autres inspections pourront difficilement éviter la propagation des pollens, l’interpénétration des deux circuits alimentaires, à savoir avec OGM et sans OGM, et enfin la victoire des biotechnologies de la multinationale Monsanto et des autres grandes sociétés semencières américaines.
Ce pharisaïsme des dirigeants européens, qui feignent d’encadrer le phénomène OGM pour des raisons sanitaires aussi incertaines que les apports positifs que ces produits génétiquement manipulés amèneraient à l’agriculture mondiale, se double d’une ironie de calendrier. En effet, c’est au moment où le dirigeant agricole français, José Bové, symbole mondial de l’alerte sur les dangers réels ou surévalués des OGM, est incarcéré dans l’excès et la disproportion des moyens que le Parlement européen se prononce sur ce sujet.
Étiquetage pour les OGM, bouclage pour l’anti-OGM, voici résumée l’ambiguïté de ces biotechnologies et de la position européenne. 
