Ambroise Guellec # Débats - Mercredi 15 novembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Gebhardt (A6-0375/2006
) 
  Ambroise Guellec (PPE-DE
), 
par écrit
. - La directive sur les services vient d’être adoptée, à une nette majorité. C’est à l’honneur du Parlement européen. C’est une victoire de l’Union européenne. Pourquoi? Les services représentent plus de la moitié de l’économie européenne, et sont le principal gisement d’emplois. La libre prestation de services figure parmi les principes fondateurs de l’Union, et la création d’un véritable marché intérieur des services est indispensable à la croissance économique en Europe, et donc à la création d’emplois.
Le texte voté aujourd’hui à Strasbourg nous engage dans cette voie, tout en apportant les garanties nécessaires sur la sauvegarde des services publics d’intérêt général et sur la pérennité de nos acquis sociaux et de notre droit du travail. Par l’exclusion du principe du pays d’origine, il interdit la compétition sociale et constitue une barrière efficace au dumping social. Il supprime les obstacles protectionnistes injustifiés, tout en permettant aux États membres d’appliquer leurs règles nationales quand l’intérêt public le justifie. Le champ d’application du texte a aussi été réduit, avec l’exclusion de secteurs sensibles comme l’audiovisuel, la santé, une partie des services sociaux, les jeux ou encore le notariat. C’est un bon compromis qui fera progresser l’Europe dans le sens des intérêts de sa population. 
