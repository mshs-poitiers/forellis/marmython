Nicole Thomas-Mauro # Débats - Jeudi 18 mai 2000 - VOTES
  Thomas-Mauro (UEN
),
par écrit
. 
- Le groupe UEN a choisi de voter contre le rapport Gröner-Smet pour plusieurs raisons. L’une d’elles est celle de l’idéologie qui motive Pékin plus 5.
Nous ne souhaitons pas rejeter le contenu de ce rapport en bloc mais nous refusons la vision idéologique de la femme que l’on veut imposer au monde.
Cette vision de la femme que nous rejetons comprend, en effet, des présupposés empiriques : tout d’abord nous ne sommes pas d’accord avec le principe de la femme facteur de paix, cette image de la colombe qui se superpose avec celle de la femme pour ne faire qu’un. L’homme serait donc cet être hirsute et belliqueux, source de tous les désordres du monde.
D’autre part, on ne peut considérer que la femme, par nature, puisse être un facteur de développement économique. Certes, elle peut y contribuer et il faut lui en donner les moyens. Cependant nous sommes d’avis que, dans le cadre de ce débat, la priorité absolue est à donner à l’éducation.
Nous ne pouvons pas non plus accepter la segmentation des droits de l’homme que l’on retrouve dans ce rapport.
Le groupe UEN, attaché à la tradition européenne intégrale des droits de l’homme, ne peut accepter une segmentation de ces droits alors qu’ils ne représentent que des besoins spécifiques auxquels des réponses adaptées doivent être apportées.
On ne peut décidément pas fonder un instrument international contraignant sur des présupposés erronés. 
