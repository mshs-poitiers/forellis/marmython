Jean-Louis Bourlanges # Débats - Jeudi 29 novembre 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Martine Roure (A6-0444/2007
) 
  Jean-Louis Bourlanges  (ALDE
). – 
(FR) 
Monsieur le Président, comme l'ensemble de mes collègues, j'ai voté le rapport de Mme Roure relatif à la lutte contre le racisme et la xénophobie au moyen du droit pénal. Je l'ai fait parce que j'en approuve les orientations, mais aussi parce que le rapporteur et la commission des libertés nous ont proposé un texte soucieux de protéger la liberté de penser, de recherche et d'expression. Toute décision de cet ordre comporte en effet un risque: celui de donner à une autorité politique le droit de dire aux gens ce qui est licite ou illicite de penser, de dire ou d'écrire.
Dans mon pays, on a vu des lois inopportunes: M. Gollnisch souhaitait l'inscription dans les programmes scolaires de mentions flatteuses sur l'œuvre coloniale de la France. Plus subtilement, des lois dont l'orientation n'était pas douteuse, comme celle de Mme Taubira, ont été interprétées ou utilisées abusivement pour engager des actions judiciaires injustifiées contre des travaux historiques dont l'objectivité, la rigueur intellectuelle et plus généralement la qualité scientifique avaient été unanimement saluées par la communauté des historiens.
L'avis que nous avons voté me paraît de nature à prévenir de telles dérives. D'abord, parce que la liberté d'expression y est solennellement reconnue comme un droit fondamental et, ensuite, parce que la décision-cadre ne vise pas à sanctionner les observations, les analyses ou les opinions, mais, ce qui est bien différent, les incitations à la haine. Puisse l'ensemble des États membres et des formations politiques faire preuve du même souci d'équilibre et de sagesse que notre rapporteur. 
