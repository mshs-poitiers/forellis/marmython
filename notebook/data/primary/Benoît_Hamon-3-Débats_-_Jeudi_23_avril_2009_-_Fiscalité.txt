Benoît Hamon # Débats - Jeudi 23 avril 2009 - Fiscalité des revenus de l’épargne sous forme de paiement d’intérêts - Système commun de TVA en ce qui concerne la fraude fiscale liée aux importations et autres opérations transfrontalières (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Benoît Hamon, 
rapporteur
. − 
Madame la Présidente, je serai très bref, puisque j’ai déjà beaucoup parlé tout à l’heure.
Je voudrais d’abord remercier Mme 
Raeva et Mme 
Siitonen, ainsi que les collègues, pour leurs interventions et leurs contributions à ce texte, et dire à M. Kovács qu’à mon sens, j’ai entendu ce qu’il a dit.
Mais je pense qu’un signal fort donné demain par le Parlement européen à la fois sur la question du champ d’application, sur la question du secret bancaire et sur la question de la liste des paradis fiscaux lui sera d’une aide précieuse au Conseil, surtout si demain nous devons négocier de nouveaux accords avec les États tiers.
Et puis, je voudrais terminer par ce que Mme 
Lulling m’a reproché et a reproché à M. Kovács – gentiment d’ailleurs – de «mélanger les torchons et les serviettes», mais c’était très gentiment fait. Je voudrais lui dire que je pense que, demain, ce Parlement, s’il mélange parfois les torchons et les serviettes, saura faire la différence entre l’intérêt général et les intérêts particuliers, et j’espère que nous aurons ainsi contribué mieux à lutter contre la fraude fiscale. 
