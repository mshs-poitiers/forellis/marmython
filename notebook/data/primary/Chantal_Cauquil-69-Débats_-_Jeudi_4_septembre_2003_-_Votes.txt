Chantal Cauquil # Débats - Jeudi 4 septembre 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. - Ce que les autorités européennes nomment pompeusement droits fondamentaux 
n’est, au mieux, qu’une notion des plus vagues dont le contenu se réduit à peu de choses pour les travailleurs et les plus vulnérables des habitants de cette Europe. Et, à quelques rares exceptions près, le rapport relève non pas des avancées, mais des reculs en la matière, à cause, le plus souvent, des politiques voulues et appliquées par des États dont on voudrait nous faire croire qu’ils sont les garants de droits réels pour les populations d’Europe.
Ceux auxquels cette société dénie les droits individuels, économiques, sociaux ou politiques les plus élémentaires sont d’abord les "petits", et ceux envers lesquels elle est la plus dure sont les femmes, les enfants, les handicapés, les personnes âgées, les homosexuels, les demandeurs d’asile, les immigrés, les membres de minorités nationales, etc. Pour des millions de travailleurs, les perspectives sont le chômage, des salaires insuffisants, l’arbitraire du patronat, l’exploitation, la mortalité au travail. Pour eux, o sont les droits fondamentaux sur lesquels pérore l’Union européenne?
Notre vote en faveur du rapport a été motivé par la justesse de la description critique de l’état réel des droits 
de 
l’homme en Europe, mais il n’implique pas notre partage des illusions de son auteur quant à la capacité des institutions européennes à changer fondamentalement les choses. 
