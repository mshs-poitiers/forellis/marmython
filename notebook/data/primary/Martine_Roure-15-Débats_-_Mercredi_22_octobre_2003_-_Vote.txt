Martine Roure # Débats - Mercredi 22 octobre 2003 - Votes
  Roure (PSE
),
par écrit
. - La Convention de La Haye est entrée en vigueur le premier janvier 2002. Elle concerne la compétence, la loi applicable, la reconnaissance ainsi que l’exécution et la coopération en matière de responsabilité parentale et de mesures de protection des enfants. Il est donc désormais essentiel que les États membres fassent le nécessaire pour que l’Union adhère à la Convention.
L’Union européenne œuvre en faveur de l’établissement d’un espace judiciaire commun fondé sur le principe de la reconnaissance mutuelle des décisions judiciaires. Nous nous devons de veiller à ce que toute décision soit prise dans l’intérêt supérieur de l’enfant.
Notre devoir est de protéger nos enfants qui sont les adultes citoyens de demain. Notre société ne pourra progresser pour le bien de tous que si nous sommes capables de veiller à l’éducation de nos enfants, si nous sommes capables de veiller à leur équilibre physique et psychique. Œuvrer pour les droits de l’enfant, c’est œuvrer pour les droits de la personne; les protéger contre les violences, notamment les violences institutionnelles, c’est les préparer à un monde de paix. 
