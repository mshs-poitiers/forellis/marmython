Philippe de Villiers # Débats - Mercredi 28 septembre 2005 - Explications de vote
  Paul Marie Coûteaux et Philippe de Villiers (IND/DEM
), 
par écrit
. - En ouvrant formellement les négociations d’adhésion avec la Turquie, les chefs d’États et de gouvernements européens vont prendre une décision dont ils devront répondre devant les peuples et l’histoire.
Les autorités de mon pays sont pourtant dépositaires d’un mandat clair du peuple qu’elles sont sensées représenter: Le 29 mai dernier, les Français ont dit non à la Constitution européenne et Non au projet d’élargissement à la Turquie.
Aujourd’hui, les représentants de l’U.M.P. dans ce Parlement s’émeuvent des dangers d’une Europe «autiste», du danger d’un divorce entre le peuple et le projet européen. Quelle découverte! Se souviennent-ils qu’ils ont régulièrement voté les crédits de préadhésion en faveur de la Turquie? Se souviennent-ils qu’ils siègent, au sein du Parti Populaire européen aux côtés de l’AKP, parti islamiste de M. Erdogan? Savent-ils seulement que celui qui détient la clé de cette affaire est le premier d’entre eux, Jacques Chirac?
Ce mensonge collectif, qui consiste à dire que la Turquie doit rentrer dans l’Union, est indigne car chacun, en silence, espère que l’autre dira non et prendra la responsabilité de la rupture.
C’est pourquoi, au nom du peuple français, nous nous adressons solennellement au Président de la République, et lui demandons d’opposer son veto à l’ouverture de ces négociations. 
