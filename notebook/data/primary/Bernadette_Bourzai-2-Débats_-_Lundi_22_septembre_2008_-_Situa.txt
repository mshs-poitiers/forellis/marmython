Bernadette Bourzai # Débats - Lundi 22 septembre 2008 - Situation et perspectives de l’agriculture dans les régions montagneuses (brève présentation)
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bernadette Bourzai  (PSE
),
 par écrit. 
– L’agriculture en zone de montagne constitue un enjeu tout à fait central, à la fois pour le développement économique local et pour la protection de l’environnement (lutte contre le changement climatique, biodiversité, fourniture d’espaces de détente et de tourisme ouverts et sécurisés...).
Les désavantages spécifiques liés à l’altitude, à la topographie, au climat auxquels sont confrontés les agriculteurs de montagne, justifient le versement d’une indemnité compensatoire de handicap naturel et la perception d’une aide directe au revenu visant à les indemniser des surcoûts de production et aussi du rôle croissant de gestionnaire de l’espace qu’ils occupent.
Selon moi, les questions majeures sont les suivantes: éviter le découplage total des aides agricoles notamment dans le secteur de l’élevage car il risque de conduire à des pertes d’emplois, aider le secteur laitier qui joue un rôle central dans les zones défavorisées, renforcer le soutien à l’installation des jeunes agriculteurs et encourager une solidarité entre l’aval et l’amont concernant notamment la gestion de l’eau.
Par ailleurs, je soutiens la mise en place d’une véritable stratégie européenne intégrée en faveur de la montagne qui devrait se fonder sur l’intégration des spécificités des massifs montagneux dans l’élaboration des différentes politiques européennes.
