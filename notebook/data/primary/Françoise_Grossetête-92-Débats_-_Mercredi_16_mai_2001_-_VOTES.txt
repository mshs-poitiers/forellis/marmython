Françoise Grossetête # Débats - Mercredi 16 mai 2001 - VOTES
  Grossetête (PPE-DE
),
par écrit
. 
- Il nous appartient, en tant que politique responsable, de nous préoccuper de la santé des européens, et notamment des plus jeunes.
La recrudescence de la consommation d'alcool chez les enfants et les adolescents appelle donc toute notre vigilance. C'est pour cela que je me suis prononcée en faveur de cette recommandation du Parlement européen qui incite les États membres à prendre des mesures de protection envers la jeunesse.
Il est ainsi souhaitable que des actions de sensibilisation aux dangers d'une consommation excessive d'alcool soient menées au sein des pays de la Communauté européenne pour responsabiliser les enfants, les adolescents, mais également les parents.
Les enfants et les adolescents sont particulièrement réceptifs aux messages qui leur sont transmis, et une pédagogie adaptée peut donc conduire à des résultats concrets.
Rappelons que les jeunes sont les premières victimes de ces accidents de la route meurtriers venant d'une consommation abusive d'alcool. Aussi, la diffusion de messages pertinents, les responsabilisant dès leur plus jeune âge face aux dangers liés à l'excès d'alcool, pourrait ainsi éviter certains drames que nous connaissons.
Pour autant, nous devons différencier les alcools. Nous savons, par exemple, que la consommation modérée de vin peut avoir des effets bénéfiques sur la santé. C'est pour cela que nous ne souhaitons, en aucun cas, condamner les producteurs. Notre objectif est de juguler la consommation excessive de boissons alcoolisées chez les jeunes.
Les producteurs ont cependant un rôle à jouer. Aussi, ces derniers ne devraient pas prendre les adolescents comme cible privilégiée de leurs campagnes commerciales et publicitaires. Nous savons l'impact que celles-ci peuvent avoir sur les jeunes, et la tentation doit être régulée au profit de la prévention.
Enfin, une réflexion sur les mises en garde à apposer sur les produits, à l'instar de celle qui existe en France en matière publicitaire, devrait avoir lieu pour juger de son éventuelle efficacité. 
