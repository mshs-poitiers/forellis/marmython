Dominique Vlasto # Débats - Mercredi 27 septembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Eurlings (A6-0269/2006
) 
  Dominique Vlasto (PPE-DE
), 
par écrit
. - J’ai décidé de m’abstenir sur le rapport Eurlings, même si c’est un rapport dur.
Certes, il marque un tournant dans la prise de conscience de la réalité des relations UE/Turquie, mais en sous-entendant et en confirmant, après le rejet de certains amendements, que l’adhésion est une fin en soi, il reste trop unilatéral. L’indéniable ralentissement des réformes en Turquie, malgré l’ouverture par la Commission des négociations d’adhésion, devrait au contraire renforcer l’option d’un partenariat privilégié. Le fait que l’ouverture de ces négociations n’ait pas accéléré les réformes est inquiétant et notre réponse devrait traduire une exigence de résultats et non l’idée que l’adhésion se fera quoi qu’il arrive.
C’est pourquoi il était nécessaire de rappeler que la normalisation des relations avec Chypre doit être un préalable inconditionnel à une éventuelle adhésion. Le fait que la Turquie n’ait toujours pas ratifié et mis en œuvre le protocole d’Ankara, qui est une forme juridique de reconnaissance minimale et tout juste acceptable, est inadmissible. Enfin, j’ai soutenu l’amendement sur la reconnaissance du génocide arménien comme préalable à cette éventuelle adhésion, car il ne s’agit pas d’un point symbolique mais d’un devoir moral et d’une exigence historique qui incombent aux autorités turques. 
