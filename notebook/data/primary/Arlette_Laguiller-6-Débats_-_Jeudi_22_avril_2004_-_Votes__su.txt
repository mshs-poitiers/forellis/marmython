Arlette Laguiller # Débats - Jeudi 22 avril 2004 - Votes (suite) 
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. 
- Sans partager toutes les formulations fort édulcorées de ce rapport quant à la mainmise des grands capitaux sur les moyens d’expression, vidant partout la liberté d’expression d’une grande part de son contenu, nous l’avons cependant voté.
Berlusconi, qui cumule le poste de Premier ministre et celui d’un des plus grands patrons de presse d’Italie, est la personnification quasi caricaturale de cette mainmise du capital sur les médias. 
