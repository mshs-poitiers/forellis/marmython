Jean-Claude Martinez # Débats - Lundi 18 février 2008 - Interventions d’une minute (Article 144 du Règlement PE) 
  Jean-Claude Martinez  (NI
). 
 – (FR) 
Monsieur le Président, en droit international, c’est très clair. À propos du Kosovo, il y a un État lorsqu’il y a: premièrement une population, deuxièmement un territoire, troisièmement des services publics. Il n’y a pas de services publics au Kosovo, la preuve c’est que la Commission européenne doit y aller. Le seul service qui fonctionne est celui de la mafia.
Un État est souverain lorsque sa compétence est plénière. La preuve, le Président du Parlement nous a dit, tout à l’heure, que la compétence du Kosovo était en souveraineté surveillée.
Un État est souverain lorsque sa compétence est autonome, autonomos
 en grec, c’est-à-dire qu’il décide lui-même de ce qu’il fait. Or là, c’est l’OTAN, ce sont les États-Unis qui décident.
Un État est souverain lorsque sa compétence est exclusive. Or, sur le territoire du Kosovo, il y a différentes forces, il n’y a pas une seule force.
Un État est souverain lorsqu’il respecte le principe de l’uti possidetis
, de l’intangibilité des frontières.
Autrement dit, aucun des caractères du droit international pour être souverain n’existe au Kosovo. Alors pourquoi? Parce qu’après les États voyous, après les États mafieux, on a inventé les États laboratoires, où la Commission européenne teste et invente le fédéralisme par désagrégation. 
