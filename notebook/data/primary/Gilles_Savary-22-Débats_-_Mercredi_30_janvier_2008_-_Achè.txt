Gilles Savary # Débats - Mercredi 30 janvier 2008 - Achèvement du marché intérieur des services postaux de la Communauté (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Gilles Savary  (PSE
). – 
(FR) 
Madame la Présidente, je crois que ce que l'on va voter demain est historique puisque, depuis le fond des temps, en particulier depuis les monarchies, la poste a toujours été publique. Elle l'a été parce que la distribution du courrier est stratégique. Elle l'a été aussi pour assurer une desserte universelle et rapide.
Nous venons de mettre fin, ou nous allons mettre fin demain, au contrôle public des postes au profit d'un marché postal largement dérégulé. Ce que nous propose cette directive sera d'abord un formidable marché pour les avocats et les hommes de loi, puisque ce n'est pas une harmonisation: chaque État membre pourra choisir son mode de financement, et il y en a quatre différents! Ce que nous propose cette directive, c'est aussi de compenser le financement du service universel, paradoxe absolu, par des aides d'État, là où, dans certains pays, la péréquation faisait en sorte que là où c'était rentable, on finançait ce qui ne l'était pas.
Je pense que nous faisons une faute. L'avenir le dira, mais nous avons déjà, aujourd'hui, des éléments qui nous permettent d'en juger. Plus de 880 millions d'euros injectés dans la poste britannique; la poste espagnole qui, sous la pression de la concurrence, vient de décréter qu'elle ne desservira plus les zones rurales par desserte postale directe; les Allemands, qui ont des difficultés à rendre le salaire minimum compatible avec le marché postal. Je crois que nous servons aujourd'hui des compagnies -  nous allons leur offrir un écrémage du marché - , mais nous ne servons pas l'intérêt général postal et la compétitivité extérieure de l'Union européenne. Tel est mon sentiment.  
