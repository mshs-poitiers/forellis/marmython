Arlette Laguiller # Débats - Mardi 16 décembre 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. 
- Le rapport n’est préoccupé que par les distorsions de concurrence susceptibles de nuire aux entreprises privées qui envahissent le secteur postal, et pas du tout des intérêts des usagers ni de ceux du personnel de la poste.
Depuis plus d’un siècle, les États européens ont mis en place des services publics postaux qui ont fonctionné à peu près convenablement jusqu’à ce qu’on commence à remplacer les critères de service public par des critères de rentabilité. Le bon fonctionnement des services publics, notamment postaux, a été, pendant longtemps, un des indices les plus sûrs du degré de développement, et je dirais même du degré de civilisation, d’un pays.
C’est précisément parce que les services postaux ont été mis dans une certaine mesure à l’abri de la concurrence et de la course au profit qu’ils ont pu remplir leur fonction. C’est parce que leur raison d’être n’était pas de produire du profit mais de rendre service à tous qu’ils desservaient les villages les plus reculés et quadrillaient les pays d’un réseau dense de bureaux de poste. C’est ce progrès que les États nationaux comme les institutions européennes sont en train de démolir systématiquement.
Nous sommes absolument opposées à cette entreprise de démolition des services publics qui constitue une régression et une injustice sociale.
À cette raison, fondamentale, de voter contre ce rapport s’en ajoute une autre: nous sommes contre la TVA et, donc, contre son application et contre le renchérissement des prix qui en résultera et dont les usagers aux revenus les plus modestes seront les principales victimes. 
