Claire Gibault # Débats - Mercredi 25 mars 2009 - Renforcement de la sécurité et des libertés fondamentales sur Internet (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Claire Gibault  (ALDE
). - 
 Monsieur le Président, Monsieur le Commissaire, chers collègues, en ma qualité d’artiste, je suis désolée, je suis très choquée par le peu d’intérêt que le rapport de M. Lambrinidis porte au secteur culturel.
Je souligne qu’il convient de sauvegarder et de protéger en toute occasion l’ensemble des droits des individus, conformément à la charte des droits fondamentaux de l’Union européenne, et d’assurer les droits et les libertés de toutes les parties concernées. La société de l’information forme un secteur économique d’importance croissante, mais aussi une source d’innovation et de créativité majeure, à la base de l’économie moderne.
Cela implique notamment d’assurer l’accès de tous à une culture diversifiée et à l’éducation dans un cadre respectueux du droit communautaire ainsi que la juste reconnaissance de la valeur du travail créatif des auteurs et des artistes interprètes, y compris dans l’économie numérique. Or, cette reconnaissance implique une rémunération de leur contribution créative pour tous les types d’utilisation afin qu’ils puissent vivre de leur profession et s’y consacrer en toute indépendance.
Dans ce contexte, les droits de propriété intellectuelle ne doivent pas être vus comme un obstacle mais bien comme un moteur des activités créatives, notamment dans le contexte du développement de nouveaux services en ligne.
D’autre part, je considère que même sur Internet, des discours racistes, haineux ou négationnistes doivent pouvoir faire l’objet de poursuites pénales. La liberté d’expression doit être exercée de façon responsable. Il faut trouver un juste équilibre entre le libre accès à Internet, le respect de la vie privée et la protection de la propriété intellectuelle. Je vous demande donc, mes chers collègues, de soutenir mes amendements 2 à 6. 
