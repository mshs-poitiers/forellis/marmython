Francis Wurtz # Débats - Jeudi 5 juin 2008 - Processus de Barcelone: Union pour la Méditerranée (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Francis Wurtz, 
au nom du groupe GUE/NGL
. –
 Madame la Présidente, Madame la Commissaire, les enjeux de la relation de l'Europe avec les peuples de la rive sud de la Méditerranée dépassent de loin ceux d'un simple voisinage.
C'est peut-être l'avenir de la paix qui se joue dans cette zone de fracture par excellence. Les raisons de cette situation sautent aux yeux. D'abord, le déséquilibre économique. Treize ans après le lancement du processus de Barcelone, non seulement la prospérité partagée promise n'est pas au rendez-vous, mais les écarts se sont accentués. De fait, l'obsession du libre-échange a prévalu sur l'objectif du développement. Pour réussir demain ce qui a échoué hier, un changement d'orientation s'impose. Je ne le vois pas.
Deuxième problème: le traitement humiliant des migrants. La population de ces nations est très jeune. Elle veut vivre et se voit sans avenir. Tout en étant profondément attachés à leur terre, à leur culture, à l'histoire de leur civilisation aux apports si prestigieux – n'en déplaise à M. Berlusconi – beaucoup tournent leur regard vers l'Europe et ils voient leurs frères émigrés subir les affronts que l'on sait: de la chasse au faciès aux discriminations, des centres de rétention au refoulement.
À cet égard aussi, le fossé s'est dramatiquement creusé. Parler de dialogue des cultures et de rapprochement des peuples sans rupture avec ces pratiques n'a tout simplement aucune crédibilité.
Enfin, il y a l'inertie de l'Europe vis-à-vis du problème palestinien. On me rétorquera que l'Union et ses États membres sont les premiers donateurs pour la Palestine; c'est vrai, et c'est bien. Mais tout observateur confirmera, comme vient de le faire l'ensemble de la délégation de notre Parlement qui rentre tout juste du Proche-Orient, que sans un engagement européen résolu sur le terrain politique, cette aide ne résoudra rien sur le fond.
Autrement dit, ce qu'on attend de l'Europe au sud de la Méditerranée à ce propos, c'est qu'elle surmonte enfin ce que l'ancien représentant du Secrétaire général des Nations unies, M. de Soto, a fort justement appelé l'autocensure à l'égard d'Israël concernant la violation permanente du droit international comme de ses propres engagements.
Israël doit comprendre – et il est du devoir de l'Union d'y contribuer - que la normalisation de ses relations avec toute la région a un prix, et ce prix n'est ni plus ni moins que ce que rappellent l'initiative de paix de la Ligue arabe, la Road Map du Quartet 
ou la déclaration d'Annapolis, à savoir la fin de l'occupation et de la barbarie qu'elle entraîne, et la reconnaissance de l'État palestinien dans les frontières de 1967.
L'attitude européenne sur cette question, voilà peut-être le critère décisif du succès ou de l'échec de toute tentative de relance du partenariat euro-méditerranéen.
Ce serait donc bien que le Conseil d'association Union européenne/Israël du 16 juin prochain prenne ce fait crucial sérieusement en considération lorsqu'il examinera la demande israélienne d'un rehaussement (upgrading
) du statut de son partenariat avec l'Union. 
