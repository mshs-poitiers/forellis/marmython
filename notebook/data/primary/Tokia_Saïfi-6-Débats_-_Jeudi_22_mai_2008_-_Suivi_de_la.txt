Tokia Saïfi # Débats - Jeudi 22 mai 2008 - Suivi de la déclaration de Paris de 2005 sur l'efficacité de l'aide au développement (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Tokia Saïfi  (PPE-DE
)
, par écrit
. – 
(FR) 
En 2005, la Déclaration de Paris établissait des engagements spécifiques afin de promouvoir une plus grande efficacité de l’aide au développement, sur la base d’un dialogue et d’une responsabilité mutuelle. Cet encadrement international est essentiel pour l'Union européenne qui fournit plus de 55 % du total mondial de l’aide au développement.
Afin d'améliorer encore la qualité et l'efficacité de cette aide, le Parlement européen préconise aussi de simplifier les procédures, de lutter contre la corruption et de débourser l'aide selon les propres priorités des partenaires. Ces mesures sont plus que nécessaires alors même que l'aide au développement des États membres s'est quelque peu réduite passant, entre 2006 et 2007, de 0,41 % à 0,38 % du RNB de l'Union européenne.
A l'heure où la crise alimentaire touche les plus fragiles des PVD et que l'accès à l'eau et à l'éducation reste restreint, on comprend que L'Union européenne doit redoubler d'efforts pour atteindre l'objectif fixé par les ODM de consacrer 0,7 % de son RNB à l'aide au développement d'ici 2015. De même pour que l'aide puisse prendre la forme d'un financement à long terme et prévisible, il est vital de dégager des ressources additionnelles par des financements innovants et mobiliser les fonds souverains vers des investissements productifs. 
