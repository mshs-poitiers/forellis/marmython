Danielle Darras # Débats - Mercredi 3 mai 2000 - VOTES
  Darras (PSE
),
par écrit
. -
 Cette question des subventions communautaires en faveur de la distribution du lait dans les écoles date de 1997 et suscite un intérêt grandissant dans chacun de nos États membres, d’autant plus qu’elle touche à des questions de santé publique et d’économie, proches donc des citoyens européens. Pour s’en convaincre, il n’y a qu’à relire les nombreux courriers qui nous ont été adressés lorsqu’il est apparu que la Commission européenne voulait interrompre ce programme !
C’est pourquoi je ne peux que vous inciter à suivre l’avis de notre rapporteur qui souhaite non seulement poursuivre un tel programme, mais en outre s’élève contre la proposition de cofinancement (50-50) que suggère la Commission européenne. Un tel cofinancement si brutal risquerait en effet de voir certains États membres renoncer à financer leur part nationale et entraînerait ainsi une discrimination sur le territoire de l’Union.
Sans nier la nécessité d’améliorer la gestion de cette ligne budgétaire, je crois néanmoins qu’une amélioration du système peut et doit se faire en douceur, et je vous incite donc à soutenir la commission de l’agriculture et du développement rural du Parlement européen et son rapporteur en adoptant ce rapport. 
