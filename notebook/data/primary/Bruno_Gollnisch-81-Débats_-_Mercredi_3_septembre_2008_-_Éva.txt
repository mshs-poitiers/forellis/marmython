Bruno Gollnisch # Débats - Mercredi 3 septembre 2008 - Évaluation des sanctions communautaires prévues dans le cadre des actions et politiques de l'UE dans le domaine des droits de l'homme (débat) 
  Bruno Gollnisch  (NI
), 
par écrit
. – 
(FR)
 Jouant à l'ONU, le Parlement européen prétend établir le règne des droits de l'homme partout dans le monde. Il ferait mieux de balayer devant sa porte.
En France, en Belgique, en Allemagne, en Autriche, des milliers de citoyens, y compris des universitaires, des publicistes, des élus mandatés par leurs compatriotes, sont poursuivis, condamnés, ruinés, privés de leur emploi, emprisonnés, accusés de racisme pour avoir critiqué l'immigration, de xénophobie pour avoir défendu la légitime préférence nationale, de négationnisme pour avoir critiqué les "vérités officielles" mais changeantes de l'histoire contemporaine, d'homophobie pour avoir exprimé une légitime préférence pour la famille naturelle, la seule à même de transmettre la vie.
Ces persécutions politico-judiciaires s'étendent même aux avocats. En Allemagne, Me Sylvia Stolz est arrêtée pour avoir défendu à la barre du tribunal les positions de son client. En France, le Conseil de l'Ordre des avocats de Paris, en refusant l'honorariat à l'avocat en retraite Éric Delcroix au lieu de prendre sa défense, se déshonore lui-même.
Des magistrats comme Estoup à Versailles, Schir à Lyon, Cotte à Paris rivalisent pour faire de ces lois arbitraires des applications extensives au mépris de tous les principes protecteurs des libertés. Mais surtout, plusieurs responsables de ces législations liberticides siègent ici-même. C'est à eux d'abord que devrait s'adresser notre indignation. 
