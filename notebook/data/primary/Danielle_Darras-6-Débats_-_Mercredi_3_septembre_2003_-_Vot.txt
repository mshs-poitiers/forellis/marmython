Danielle Darras # Débats - Mercredi 3 septembre 2003 - Votes
  Darras (PSE
),
par écrit.
 - Le naufrage du Tricolor
 ou encore celui du Prestige ont cruellement rappelé combien il est urgent et nécessaire de faire de la sécurité maritime un enjeu majeur, combien donc il importe d’assurer un niveau élevé de formation des gens de mer sur les bâtiments européens, y compris lorsque les marins sont originaires de pays tiers.
Cette possibilité de faire venir dans l’Union des gens de mer issus de pays tiers présente d’ailleurs un intérêt réel pour la flotte européenne. Ainsi sur les quelque 140 000 marins servant sur des bateaux battant pavillon d’un État membre de l’Union européenne, environ un tiers (c’est-à-dire 47 000) proviennent d’un État tiers. De plus, dans de nombreux États membres, les armateurs trouvent difficilement du personnel formé au niveau national.
Cette proposition, qui vise à remplacer la procédure actuelle de reconnaissance des brevets d’aptitude par l’État membre par une procédure de reconnaissance au niveau de la Commission, avec l’assistance de l’Agence européenne pour la sécurité maritime, est donc une avancée conséquente pour garantir le plus haut niveau de sécurité possible. Lors du vote, je ferai particulièrement attention aux amendements insistant sur les aptitudes des gens de mer, l’accès à la formation, le rapport d’évaluation et la lutte contre les pratiques frauduleuses. 
