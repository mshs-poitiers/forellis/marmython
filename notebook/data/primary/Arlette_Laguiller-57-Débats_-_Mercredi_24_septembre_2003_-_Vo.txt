Arlette Laguiller # Débats - Mercredi 24 septembre 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. - Nous avons voté contre le projet de Constitution européenne car, par-delà les détails de formulation, cette Constitution est destinée à devenir un des fondements juridiques des lois conçues pour préserver l’ordre social existant et assurer les privilèges des possédants.
Notre rejet de la Constitution européenne n’est en rien un acte de repli sur la Constitution nationale, tout autant conçue pour défendre la propriété bourgeoise et l’exploitation. Partisans d’une Europe totalement unifiée, d’un bout à l’autre du continent, mais débarrassée de la domination de la grande bourgeoisie et de la dictature des groupes financiers sur l’économie, nous n’avons aucune complaisance pour le souverainisme anachronique.
Nous nous sommes abstenues sur la plupart des amendements car même les mieux intentionnés se proposent d’améliorer une Constitution que nous rejetons.
Nous nous sommes abstenues sur les amendements proposant d’organiser des référendums sur le projet de Constitution. Outre le fait que les questions soient toujours biaisées par les autorités qui les posent, nous ne considérons pas les référendums comme l’expression de la volonté populaire dans une organisation sociale où les moyens d’information et les médias sont monopolisés par les riches. 
