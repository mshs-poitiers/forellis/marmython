Véronique Mathieu # Débats - Mardi 24 avril 2007 - Décharges pour l’exercice 2005 (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Véronique Mathieu, 
au nom du groupe PPE-DE
. - Monsieur le Président, chers collègues, en ce qui concerne la décharge des agences, je souhaite tout d’abord faire différents constats. Le premier, c’est que le nombre d’agences est en augmentation constante et que cette progression est inquiétante car elle semble se réaliser hors de tout cadre d’orientation global. Cette situation pose différents problèmes: celui du contrôle financier de ces agences et celui des faiblesses constatées dans leur organisation, qui ne sont pas toujours résolus.
Concernant le premier aspect, il me semble primordial d’établir des règles claires dans la procédure de décharge. En effet, sur 35 agences, 16 font aujourd’hui l’objet d’une décharge individuelle du Parlement. Pour éviter les incohérences actuelles dans l’application de l’article 185 du règlement financier, puisque de nombreuses agences font toujours l’objet d’une décharge globale au titre du budget général de la Commission européenne, il serait opportun qu’à l’avenir, toutes les agences, quel que soit leur statut, fassent l’objet d’une décharge distincte du Parlement européen.
Le deuxième aspect que je souhaite évoquer concerne le fonctionnement efficace des agences. Bien que des progrès dans ce domaine aient été constatés, ils demeurent néanmoins insuffisants. Il faut en priorité renforcer les systèmes comptables, garantir la bonne application des procédures d’appel d’offres et des règles de contrôle interne et enfin rendre les procédures de recrutement plus transparentes. Parallèlement, la Commission doit poursuivre ses efforts, à la fois pour renforcer la visibilité des agences, en élaborant une stratégie de communication efficace, pour développer les synergies, et enfin pour mettre à leur disposition davantage de services techniques et administratifs adaptés à leurs besoins.
Par ailleurs, je soutiens les deux propositions visant à rendre obligatoire la publication d’une étude coûts-bénéfices avant la création de toute agence, et demandant la préparation tous les cinq ans d’un rapport d’évaluation sur la valeur ajoutée générée par chaque agence dans son domaine d’activité. Ces deux propositions sont essentielles pour s’assurer que les activités développées par les agences répondent aux besoins spécifiques des différents secteurs.
En conclusion, je voudrais souligner que, même si des réserves ont été émises par la Cour des comptes pour trois d’entre elles, je me félicite que les agences aient dans leur ensemble enregistré des progrès par rapport à l’exercice 2004 et je les invite à poursuivre leurs efforts dans ce sens. 
