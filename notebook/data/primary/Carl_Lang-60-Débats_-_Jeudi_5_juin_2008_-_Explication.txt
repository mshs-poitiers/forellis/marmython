Carl Lang # Débats - Jeudi 5 juin 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Proposition de résolution: Processus de Barcelone: Union pour la Méditerranée (RC-B6-0281/2008
) 
  Carl Lang  (NI
), 
par écrit
. –
 Le titre même de la résolution souligne l'échec de M. Sarkozy. Non content d'être le président de la République française, il prétendait devenir le concepteur, puis le président, d'une Union euro-méditerranéenne, ouverte uniquement aux pays riverains de la Méditerranée.
Avant même que commence sa présidence européenne, son château de cartes s'écroule. L'Allemagne a imposé ses vues: tous les États de l'Union européenne participeront à ce projet, conçu comme un simple développement du processus de Barcelone, dirigé par l'eurocratie bruxelloise.
Ce fiasco illustre l'abaissement de notre pays dans les institutions européennes. L'Europe de Bruxelles, loin de renforcer la France, comme le prétendent l'UMP et le PS, l'affaiblit dans tous les domaines: économique, avec la destruction de notre paysannerie, la ruine et la délocalisation de nos industries; militaire, avec le démantèlement de notre armée, appelée à se dissoudre dans un eurocorps; démographique, avec une immigration remettant en cause notre identité nationale.
Seule une nouvelle Europe, l'Europe des patries, fondée sur la souveraineté des États et sur les valeurs fondatrices de notre civilisation européenne et chrétienne, permettra à la France de redevenir une puissance de premier plan et de conduire en Méditerranée une grande politique de coopération visant, entre autres, au retour dans leurs pays des populations immigrées. 
