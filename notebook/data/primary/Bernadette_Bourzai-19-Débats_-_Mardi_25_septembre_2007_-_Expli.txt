Bernadette Bourzai # Débats - Mardi 25 septembre 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
– Rapport: Thomsen (A6-0287/2007
) 
  Bernadette Bourzai  (PSE
), 
par écrit
. –
 On doit soutenir l'engagement pris par l'Union européenne d'atteindre, d'ici 2020, un objectif ambitieux de 20 % d'énergies renouvelables dans la part totale d'énergie consommée en Europe. Les avantages que l'Europe peut tirer des énergies renouvelables sont multiples: réduction des émissions de CO2, réduction de la dépendance européenne et contribution à la création d'emplois et à la croissance.
Il faut toutefois veiller au développement de toutes les énergies renouvelables dans une perspective durable et en fonction, bien sûr, des capacités des États membres et aussi réaliser des économies d'énergie - mode de consommation et efficacité énergétique.
Je regrette que la directive sur l'électricité produite à partir de sources d'énergie renouvelables n'impose pas la cogénération et aboutisse à la mise en place de centrales de production de biomasse, de grande échelle et sans cogénération, qui ne sont pas performantes au plan énergétique et déstabilisent le marché du bois d'industrie et du bois énergie. J'en ai des exemples dans ma région, le Limousin.
Je suis donc satisfaite que le Parlement européen demande qu'une proposition sur les secteurs du chauffage et de la réfrigération soit au cœur de la future directive-cadre sur les énergies renouvelables et j'ai donc voté pour le rapport Thomsen. 
