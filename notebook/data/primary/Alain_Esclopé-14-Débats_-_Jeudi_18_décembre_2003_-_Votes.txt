Alain Esclopé # Débats - Jeudi 18 décembre 2003 - Votes
  Esclopé (EDD
),
par écrit
. 
- J’ai voté contre le rapport Sommer car la proposition de mettre en place un système européen de télépéage va trop loin. L’objectif, inscrit dans le livre blanc, de lutter contre la congestion du trafic routier pourrait être atteint par d’autres moyens. Le télépéage européen ne constitue pas une priorité dans ce domaine. À titre d’exemple, il serait plus judicieux de réduire le trafic des poids lourds en encourageant la pratique du ferroutage.
L’adaptation des infrastructures des États membres pour la mise en place d’un tel système engendrerait des coûts disproportionnés qui se répercuteraient encore une fois sur les usagers. Les systèmes actuels de péage fonctionnent relativement bien et il n’y a pas d’urgence à mettre en place un tel système.
Au nom du principe de subsidiarité, le choix d’opter pour tel ou tel système de péage et pour la technologie adéquate devrait être laissé à l’appréciation des États membres. 
