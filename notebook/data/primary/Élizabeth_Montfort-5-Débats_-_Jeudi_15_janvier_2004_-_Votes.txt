Élizabeth Montfort # Débats - Jeudi 15 janvier 2004 - Votes
  Montfort (PPE-DE
),
par écrit
. 
- J’ai approuvé le projet de résolution législative du Parlement visant à proroger la faculté d’autoriser les États membres à appliquer des taux réduits de TVA pour certains services à forte intensité de main-d’œuvre, spécialement le bâtiment.
Ce texte est très loin d’être satisfaisant. Il n’est qu’un pis-aller au regard de l’avis rendu par le Parlement le 4 décembre dernier et qui demandait de rendre permanente l’application de ces taux réduits et de l’étendre à la restauration et aux produits culturels.
Mais il permettra au moins aux États membres concernés de prolonger une expérience qui a démontré ses effets positifs sur l’emploi et la santé des PME et des artisans visés par ces mesures. La Commission, en dépit des études nationales, n’est pas convaincue par les trois premières années d’expérience. Cette prolongation devrait avoir le mérite de la convaincre.
Néanmoins nous ne pouvons nous contenter de combler des vides juridiques au coup par coup. Je souhaite que le Conseil prenne conscience de l’impact de ces mesures sur l’économie et l’aménagement du territoire et adopte la directive amendée par le Parlement le 4 décembre dernier et ouvre, au libre choix des États, l’application des taux réduits de TVA à la restauration. 
