Catherine Lalumière # Débats - Mardi 2 septembre 2003 - Révision à mi-parcours du quatrième protocole en matière de pêche entre l’UE et le Groenland
  La Présidente.  
- L’ordre du jour appelle le rapport (A5-0228/2003
) de Mme Miguélez Ramos, au nom de la commission de la pêche, sur une communication de la Commission au Conseil et au Parlement européen intitulée: "Révision à mi-parcours du quatrième protocole en matière de pêche entre l’Union européenne et le Groenland" [COM(2002) 697
 - 2003/2035(INI)]. 
