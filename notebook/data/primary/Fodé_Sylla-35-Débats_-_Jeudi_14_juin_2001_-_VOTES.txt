Fodé Sylla # Débats - Jeudi 14 juin 2001 - VOTES
  Sylla (GUE/NGL
). 
 - Monsieur le Président, ce n'est pas pour m'opposer que j'interviens, c'est juste pour dire que je trouvais que les modifications qui étaient proposées par Mme Maes étaient de bon sens mais qu'en même temps il se trouve que le débat que nous avons eu n'est pas tout à fait conforme au texte sur lequel nous devons voter.
Alors personnellement, je ne prendrai pas part à ce vote parce que je crois que la dimension de la responsabilité des pays occidentaux, et particulièrement de la France, dans le cadre précis du Tchad n'a pas été suffisamment révélée dans le texte. J'accepte les modifications de Mme Maes, mais personnellement, je ne prends pas part au vote sur le Tchad parce que je pense que cela ne va pas assez loin. 
