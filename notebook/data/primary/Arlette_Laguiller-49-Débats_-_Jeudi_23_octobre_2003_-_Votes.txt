Arlette Laguiller # Débats - Jeudi 23 octobre 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. - La résolution prétend soutenir le régime démocratique en Bolivie alors que, dans ses propres considérants, elle affirme que, dans ce pays, le plus pauvre d’Amérique du sud, les paysans, les ouvriers et les sans-emploi sont largement exclus de la vie politique et que, par ailleurs, ce prétendu régime démocratique vient de massacrer les pauvres qui manifestaient contre le président de la République, alors en exercice.
Eh bien, pour nous, la seule démocratie qui a existé pendant quelques jours en Bolivie est la démocratie directe exercée par le peuple, laquelle a fait la démonstration que, malgré l’armée, malgré la répression, il est capable de chasser un président de la République dont la majorité ne veut pas.
La Bolivie est un exemple, non pas par son régime, mais par la réaction de son peuple. 
