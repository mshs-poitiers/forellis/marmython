Anne Ferreira # Débats - Lundi 16 juin 2008 - Révision de la directive-cadre sur les déchets (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Anne Ferreira  (PSE
).  
– (FR) 
Madame la Présidente, Monsieur le Commissaire, chers collègues, pour ma part, je ne suis pas satisfaite de ce texte de compromis, ni sur la forme, ni sur le fond.
Sur la forme, j'estime que travailler pendant deux ans à l'élaboration d'un texte pour que les ultimes décisions – qui ne sont pas que des ajustements – soient établies en catimini bien en–deçà de ce qui avait été adopté en commission de l'environnement et n'arrivent sur la table des députés que quelques heures avant le vote, cela ne nous permet pas de travailler dans de bonnes conditions.
Sur le fond, ce texte de compromis est un texte de renoncement qui abandonne une définition claire de la valorisation, qui abandonne la volonté de stabiliser le volume de déchets, qui abandonne des objectifs de recyclage ambitieux et qui témoigne malheureusement de l'incapacité de la Commission et du Conseil à traduire en actes les grandes déclarations ambitieuses qui sont faites sur la scène européenne et sur la scène internationale.
Non, je ne suis pas satisfaite, et je suis même inquiète de notre incapacité politique à véritablement agir sur notre environnement ainsi que pour notre santé et son amélioration, et c'est aussi la raison pour laquelle j'ai voté non. 
