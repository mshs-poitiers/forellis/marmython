Jacques Toubon # Débats - Mardi 5 mai 2009 - Réseaux et services de communications électroniques, protection de la vie privée et protection des consommateurs - Réseaux et services de communications électroniques - Organe des régulateurs européens des communications électroniques (ORECE) et Office - Bandes de fréquence à réserver pour les communications mobiles (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jacques Toubon  (PPE-DE
). 
 – Madame la Présidente, je voudrais d’abord remercier Mme
 Reding, notre commissaire, la présidence française, qui a obtenu, au mois de décembre, une position commune très intelligente, puis la présidence tchèque qui vient d’arriver à ses fins, et je voudrais remercier et féliciter principalement nos trois rapporteurs, Malcom Harbour, Catherine Trautmann et Pilar del Castillo, qui ont fait un travail magnifique.
Le paquet que nous votons aujourd’hui comporte des dispositions qui vont tout à fait dans le sens de ce qu’on peut souhaiter pour le développement du marché des télécommunications, dans l’ordre et au bénéfice de tous: une régulation européenne confiée à un BEREC avec un financement mixte, sans veto, un équilibre entre la concurrence et la nécessité des nouveaux investissements – la séparation fonctionnelle est limitée à des cas exceptionnels -, un souci de porter attention aux services publics dans la gestion du spectre, les nouveaux services, les nouveaux accès, libérés, et le droit des consommateurs - grâce à Malcolm Harbour en particulier - considérablement augmenté. J’ai un regret simplement pour le rejet du «must carry»
.
Pour la France, ce texte est parfaitement équilibré, c’est un bon compromis final, et je veux dire à cet égard que, ce qui a été proposé à la fin, à propos des fameux amendements controversés, me paraît intelligent, parce qu’il donne la possibilité de mettre en œuvre la propriété intellectuelle sur l’internet sans en compromettre la liberté d’accès. Internet doit appliquer la loi dans ce domaine comme dans les autres. Le monde virtuel n’est pas un monde sans loi et il ne doit pas se soumettre seulement aux pouvoirs de la publicité engrangée par les opérateurs et les FAI. C’est pourquoi je partage le point de vue de Catherine Trautmann. Le compromis met sur un pied d’égalité droit des salariés, droit des artistes, droit des internautes, ce qui est bien. 
