Alain Lipietz # Débats - Mercredi 11 mars 2009 - Plan européen de relance économique (A6-0063/2009, Elisa Ferreira) (vote) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Alain Lipietz  (Verts/ALE
). 
 – Madame la Présidente, il s’agit d’un amendement purement technique. Il y a une faute dans l’édition de notre amendement. Il y a un «indent» qui était rédigé ainsi: «intensify the elimination of barriers» 
et nous avons remplacé par «remove injustified barriers», mais malheureusement, l’ancien paragraphe, l’ancien indent est resté dans le texte de l’amendement. C’est donc bien le troisième indent que nous avons légèrement modifié et il n’y a pas lieu de maintenir l’ancienne version. 
