Tokia Saïfi # Débats - Jeudi 13 décembre 2007 - Textiles (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Tokia Saïfi  (PPE-DE
), 
auteur
. – 
(FR)
 Monsieur le Président, Monsieur le Commissaire, il y a deux ans, suite à la levée des quotas, le secteur du textile connaissait un véritable big bang. Pour tenter de remédier à cette crise, vous prôniez, Monsieur le Commissaire européen, des mesures transitoires mais palliatives, avec l'ambition de contrôler et de limiter les importations de certains textiles chinois. Dans quelques jours, ces garde-fous n'existeront plus et les échanges commerciaux textiles seront soumis à la surveillance d'un regard croisé Union européenne-Chine, que j'espère attentif et vigilant.
En effet, aujourd'hui, notre inquiétude repose sur la façon dont sera mis en place ce système de surveillance conjointe. Quels sont les gages dont nous disposons pour nous assurer que ce double contrôle sera adéquat et efficace? Le textile est un secteur qui a été de tout temps mondialiste tant en termes de production que de consommation, mais qui a fait les frais d'une certaine mondialisation.
Or, cette mondialisation peut s'anticiper et se réguler. Pour cela, il faut avoir la volonté politique de créer un cadre compétitif pour nos industries textiles européennes. Nous devons progresser vers des conditions d'accès au marché équitables et réciproques. Nous devons continuer à utiliser sans crainte les instruments de défense commerciale dont l'Union européenne dispose, car être protecteur n'est pas être protectionniste. Nous devons faire de la lutte contre la contrefaçon notre priorité. L'Europe de demain n'aura plus d'industrie si on ne défend pas ses droits de propriété intellectuelle et son savoir-faire. Une application des mêmes règles du jeu par tous et pour tous est la seule manière d'offrir à toutes les parties concernées un scénario gagnant-gagnant.
Je fais donc le vœu pour 2008, Monsieur Mandelson, que nous puissions envisager ensemble et dans un climat plus serein l'avenir prometteur du secteur "textiles".
