Carl Lang # Débats - Mercredi 14 février 2001 - VOTES
  Lang (TDI
),
par écrit
. -
 Une fois n'est pas coutume, nous sommes d'accord avec une proposition de directive de la Commission. Ce n'est pas tant que nous voyions d'un bon œil une intervention de sa part - vous savez ce que nous pensons de la construction européenne dans son ensemble et de cette institution en particulier -, mais il faut reconnaître que, pour une fois, la Commission a fait des efforts : consultation préalable des usagers et des industriels, étude coûts-avantages, efforts de réalisme économique et technique... voilà une démarche que l'on ne rencontre que trop rarement dans la multitude de dossiers que nous traitons ici.
En revanche, nous ne suivrons pas le rapporteur, dont on ne sait pas au juste s'il fait le plus possible de démagogie ou s'il est stipendié par nos concurrents pour couler les constructeurs européens. Au point où il en est, il pourrait tout aussi bien demander aux motards d'arrêter de respirer pour ne plus rejeter de CO2 dans l'atmosphère.
Alors, oui à des motos plus propres, oui à de nouveaux protocoles d'essai plus conformes à l'usage réel des véhicules, oui au calendrier que les industriels ont jugé réaliste. Mais non à des normes d'émissions irréalistes, non à des dispositifs supplémentaires que l'on n'exige pas d'autres modes de transport, non, en un mot, aux amendements de M. Lange. 
