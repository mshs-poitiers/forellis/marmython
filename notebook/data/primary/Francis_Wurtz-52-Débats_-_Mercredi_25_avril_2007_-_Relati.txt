Francis Wurtz # Débats - Mercredi 25 avril 2007 - Relations transatlantiques (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Francis Wurtz, 
au nom du groupe GUE/NGL
. - Monsieur le Président, que nous réserve au juste le partenariat économique transatlantique?
Selon Mme Merkel, il ne s’agirait ni de libre-échange, ni de marché commun, mais de régulation des marchés, de protection des brevets, d’harmonisation des règles et de coopération pour améliorer la gouvernance économique mondiale. Son secrétaire d’État, M. Würmeling, a été plus direct en déclarant que l’objectif était de progresser vers un marché transatlantique sans barrières. La chancelière avait, au demeurant, elle-même laissé entendre que l’expérience du marché unique européen pourrait servir de modèle à ce nouvel espace.
Or, est-il besoin de rappeler la définition que donnait de ce marché unique le commissaire chargé de son suivi, M. McCreevy? Comme il l’a souligné, le marché unique «est, de loin, l’exercice de déréglementation le plus poussé de l’histoire récente de l’Europe». Est-ce donc bien cette expérience qu’il convient de généraliser à l’échelle transatlantique?
La question mérite d’autant plus d’être posée que ce projet a déjà une histoire tumultueuse. C’est en mars 1998, que le commissaire Leon Brittan, alors figure de proue de l’Europe libérale, avait lancé le projet du New Transatlantic Market
 calqué sur le modèle de l’accord de libre-échange nord-américain, L’ALENA. Parallèlement se négociait en grand secret, à l’OCDE le projet d’Accord multilatéral sur l’investissement, l’AMI, qui visait déjà à faire la chasse à toute législation perçue par les investisseurs comme une entrave à leurs opérations financières de plus en plus tentaculaires.
Ces deux projets suscitèrent dans l’opinion européenne une telle levée de boucliers qu’ils durent être abandonnés. Mais depuis, des lobbies, tel le Transatlantic Business Dialogue, 
n’ont de cesse de remettre sur le tapis ce projet stratégique sous une forme nouvelle. L’adoption, l’an dernier, des normes comptables américaines, et, plus récemment, le rachat des bourses européennes d’Euronex par la place de New York s’inscrivent dans cette tendance pesante.
Loin de l’image de la coopération constructive qu’on voudrait nous vendre, il s’agit bel et bien d’un front majeur de la bataille sur la conception de l’avenir de l’Europe. Sont en jeu à la fois son modèle de société et son identité démocratique. Je rappelle que le rapport adopté à ce propos, en juin dernier, par notre Parlement regrettait, «que les liens entre l’Union et les États-Unis soient grevés par des conflits d’ordre politique et assez fréquemment caractérisés par de grandes déclarations».
Allons-nous devoir, au nom des valeurs communes du Transatlantic Business Dialogue 
nous taire sur la guerre en Irak ou Guantanamo? Sur la peine de mort ou la Cour pénale internationale? Sur Kyoto ou les OGM? Sur les données personnelles, l’affaire SWIFT, ou les vols de la CIA? Alors qu’est engagé le processus devant conduire à un nouveau traité européen, la nature des relations entre l’Union européenne et les États-Unis constitue un enjeu crucial dont il faudra traiter en toute clarté. 
