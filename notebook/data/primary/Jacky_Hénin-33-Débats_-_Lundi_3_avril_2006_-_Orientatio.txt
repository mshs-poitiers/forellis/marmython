Jacky Hénin # Débats - Lundi 3 avril 2006 - Orientations relatives aux réseaux transeuropéens d’énergie (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jacky Henin (GUE/NGL
). 
 - Monsieur le Président, cela ne s’est pas passé en Europe mais ça aurait pu être le cas. Non seulement aucune leçon n’a été tirée des pannes d’électricité de 2000 en Californie, mais vous créez les conditions pour que l’Europe connaisse à son tour une catastrophe énergétique de grande ampleur.
Vos propositions conduiront à une énergie plus chère et moins fiable. La séparation du réseau de transport et des unités de production de l’énergie électrique est une aberration économique, écologique et industrielle. Une fois de plus, les institutions européennes prennent le parti des intérêts financiers au détriment de l’intérêt général. Le marché capitaliste est incapable d’assumer les investissements à long terme dans le domaine de l’énergie. La satisfaction des intérêts des actionnaires entraîne le sacrifice de la recherche, du développement durable et de la sécurité. Le choix de créer artificiellement un grand marché intérieur de l’énergie amènera également la suppression de nombreux emplois et la précarisation de tant d’autres.
Or, notre Union a besoin d’un pôle public européen fort, financé par le public pour satisfaire les besoins des citoyens européens. 
