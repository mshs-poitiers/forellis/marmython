Bruno Gollnisch # Débats - Mardi 10 mars 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Herczog (A6-0074/2009
) 
  Bruno Gollnisch  (NI
), 
par écrit
. – Monsieur le Président, mes chers collègues, nous avons approuvé ce rapport qui énumère, sous forme de vœux qui resteront sans doute virtuels un certain temps, les moyens et méthodes pour faciliter la vie des petites entreprises dans l’Union européenne.
Quelques remarques cependant.
La demande, discrète il est vrai, d’une sorte de discrimination positive à l’égard des PME, je cite, «détenues par des minorités ethniques sous-représentées» est inutile, incompréhensible et totalement idéologique.
L’accès des PME, et notamment des PME locales, aux marchés publics, que le rapporteur entend favoriser désormais, a été entravé par les textes votés il y a quinze ans dans cette assemblée même, malgré les mises en gardes contre leurs effets pervers. Ces textes promouvaient de fait l’accès des grandes entreprises, étrangères notamment, aux marchés publics, entreprises disposant de l’information et des moyens administratifs et juridiques de postuler à ces marchés, quand les PME locales ne les avaient pas.
L’accès des PME aux aides nationales et européennes existantes est d’une extraordinaire complexité, du fait des exigences des lois européennes elles-mêmes.
Bref, une fois de plus, on a l’impression qu’il faut régler par des textes européens des problèmes prévisibles nés d’autres textes européens. 
