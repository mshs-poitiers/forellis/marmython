Catherine Lalumière # Débats - Lundi 8 mars 2004 - Agence européenne pour la gestion de la coopération opérationnelle aux frontières extérieures
  La Présidente.  
- L’ordre du jour appelle le rapport (A5-0093/2004
) de M. von Boetticher, au nom de la commission des libertés et des droits des citoyens, de la justice et des affaires intérieures, sur la proposition de règlement du Conseil portant création d’une Agence européenne pour la gestion de la coopération opérationnelle aux frontières extérieures. 
