Tokia Saïfi # Débats - Mercredi 13 décembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Rapport Bachelot-Narquin (A6-0385/2006
) 
  Tokia Saïfi (PPE-DE
), 
par écrit
. - J’ai voté en faveur du compromis régissant la création d’un Fonds européen d’ajustement à la mondialisation (FEM). La mondialisation est une réalité qui peut être bénéfique lorsqu’elle est juste et équitable.
Or, lorsque la mondialisation a des effets négatifs sur les travailleurs, l’Union européenne se doit, dans un souci de solidarité, de corriger les conséquences des modifications de la structure du commerce mondial. Ce fonds, qui s’élève à 500 millions d’euros, ne servira pas à financer la restructuration d’entreprises mais à aider les travailleurs licenciés, notamment dans leur démarche de réinsertion sur le marché du travail.
Le FEM est un instrument important car il permet de montrer que l’Union européenne prend pleinement en compte l’impact social de la mondialisation et qu’elle ne se désintéresse pas du sort des salariés. C’est en 2005, en voyant l’impact de la libéralisation et de la concurrence sur le secteur du textile et de l’habillement, et plus particulièrement sur ses salariés, que l’idée d’un tel fonds est née. C’est en 2007, soit seulement deux ans après, que ce fonds devient effectif et qu’il répond de façon concrète aux attentes des travailleurs licenciés. Oui l’Europe peut être proche des citoyens, elle le prouve aujourd’hui! 
