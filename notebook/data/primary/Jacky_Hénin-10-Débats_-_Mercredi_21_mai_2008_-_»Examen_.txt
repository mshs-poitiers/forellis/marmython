Jacky Hénin # Débats - Mercredi 21 mai 2008 - »Examen à mi-parcours de la politique industrielle - contribution à la stratégie pour la croissance et l'emploi de l'Union européenne (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jacky Hénin, 
au nom du groupe GUE/NGL
. – 
(FR) 
Monsieur le Président, chers collègues, la rédaction de ce rapport et la discussion de ce jour n'ont d'autre but que de masquer une réalité criante au sein de l'Union: l'industrie est en crise et les dégâts qu'elle engendre sont conséquents. Certes, les chiffres mis en avant font rêver: 80%, 73%. On aurait fort bien pu les remplacer par des milliards d'euros qui en auraient mis plein à la vue à tout un chacun. Mais personne n'est dupe! Ils ne servent qu'à masquer l'état réel des choses.
L'un des soucis majeurs que rencontre le secteur industriel dans sa globalité est que le seul élément qui compte est le taux de profit. En voie de disparition, la race des capitaines d'industrie remplacés par de sombres machines à sous parlantes! Peu importe les décisions prises et leurs conséquences sur les femmes et les hommes qui peuplent notre territoire, le profit doit être maximum et immédiat.
Évidemment, certains vont me rétorquer que jamais les masses investies, produites, échangées, gagnées par l'industrie n'ont été aussi importantes. C'est vrai, mais que signifie une masse financière énorme profitant à quelques-uns quand la majorité de nos peuples souffrent et voient s'envoler leurs rêves d'un peu de bonheur?
Force est de constater à ce niveau que la question de l'emploi est absente de ce rapport, comme sont absentes les notions d'aménagement du territoire, de besoins des populations, de recettes fiscales et sociales. Pour cause, les succès commerciaux des grands groupes industriels européens contribuent maintenant de manière de plus en plus faible au développement dans l'Union. Les grands groupes européens délocalisent et externalisent vers les pays pratiquant le dumping salarial, social, sanitaire, fiscal et écologique et, pendant ce temps, l'Union européenne se refuse à toute politique industrielle.
Cette politique asphyxie également les PME et les pousse à faire de même. Il est une évidence, ce ne sont pas les …
(Le président retire la parole à l’orateur) 
