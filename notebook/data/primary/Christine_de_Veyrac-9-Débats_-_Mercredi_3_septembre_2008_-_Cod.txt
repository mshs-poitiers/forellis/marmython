Christine de Veyrac # Débats - Mercredi 3 septembre 2008 - Code de conduite pour l'utilisation de systèmes informatisés de réservation (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Christine De Veyrac  (PPE-DE
), 
par écrit
. – (FR)
 Chers collègues, le texte dont nous débattons ce soir a fait l'objet de discussions animées au sein de la commission des transports et ce n'est pas sans raison, car il s'agit d'une législation importante pour l'industrie du tourisme et du transport aérien, mais aussi pour les citoyens.
Et il n'était pas aisé de trouver le bon équilibre entre le maintien de la concurrence entre les compagnies aériennes et la garantie de l'indépendance des agences de voyages, tout en permettant aux voyageurs de recevoir une information utile et neutre.
Le texte sur lequel le rapporteur et la Présidence française se sont mis d'accord respecte ces exigences et je m'en réjouis.
Une définition équilibrée de la notion de transporteur associé a pu être dégagée et cela est essentiel pour assurer une concurrence saine entre les différents systèmes informatisés de réservation.
J'espère que le vote de demain entérinera l'accord en première lecture afin que ce texte puisse être appliqué rapidement.
Je vous remercie de votre attention. 
