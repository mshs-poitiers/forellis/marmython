Marie-Line Reynaud # Débats - Mardi 14 mars 2006 - Institut européen pour l’égalité entre les hommes et les femmes (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie-Line Reynaud (PSE
). 
 - Je tiens à féliciter les deux corapporteurs, M. Gröner et Mme Sartori, pour l’excellent travail qu’elles ont accompli. Le rapport apporte en effet, au texte initial de la Commission, un certain nombre d’améliorations que j’avais également suggérées dans mon projet d’avis en commission des libertés civiles, de la justice et des affaires intérieures.
J’en citerai quatre. D’abord le rôle proactif reconnu a l’institut, en particulier à travers ses missions d’analyse et d’expertise et la possibilité qui lui est accordée de soumettre des recommandations et des orientations aux institutions communautaires. Ensuite, l’importance de la coopération avec l’agence des droits fondamentaux. Ensuite encore, la nécessité d’une présence équilibrée des hommes et des femmes au sein du conseil d’administration et, enfin, le rôle du Parlement européen, notamment concernant la nomination du directeur de l’institut et des membres du conseil d’administration et le suivi de leur travail.
Cet institut du genre est indispensable pour une vraie Europe des citoyens et des citoyennes et j’ai été extrêmement déçue que mon projet d’avis ait été rejeté en commission des libertés civiles, de la justice et des affaires intérieures, dix-huit voix contre dix-huit, en raison de la volonté d’une partie du PPE et des libéraux d’empêcher cet institut de voir le jour. Cela dit, je suis heureuse également de voir et de constater que les corapporteurs ont intégré l’essentiel de mes préoccupations dans leur rapport. 
