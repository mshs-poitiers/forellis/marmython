Dominique Souchet # Débats - Mercredi 14 janvier 2004 - Votes
  Souchet (NI
),
par écrit
. 
- Tout devrait sourire à la pêche et à l’industrie thonière communautaires. Elles disposent de professionnels entreprenants, de grande qualité, notamment en France et en Espagne, et proposent un aliment sain et naturel, qui fait l’objet d’une demande forte et croissante. Pourtant, la compétitivité européenne est fragilisée du fait même de la Commission, bien que celle-ci prétende incarner seule le "bien commun européen".
Les contraintes draconiennes et sans cesse croissantes de la législation communautaire touchant notamment la sécurité, le contrôle, les conditions sanitaires de production, la protection de l’environnement et la protection sociale entraînent pour la flotte et les transformateurs communautaires des coûts très supérieurs à ceux de leurs concurrents.
L’idéologie de l’ouverture systématique et intégrale qui anime la Commission s’applique aux produits de la mer, avec l’extension permanente de la liste des pays bénéficiaires de préférences tarifaires, jouissant d’un système de certificats d’origine particulièrement opaque, ainsi que le contingent filets de thon et le contingent conserves de thon ouverts à la Thaïlande, aux Philippines et à l’Indonésie. "L’ouverture du marché communautaire, relève notre rapporteur, à des produits étrangers soumis à de moindres exigences et à des contrôles insuffisants mine la compétitivité du secteur".
Il est temps de faire cesser ces discriminations imposées par la Commission au détriment de nos économies et de nos emplois. 
