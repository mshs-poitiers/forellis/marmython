Bernard Lehideux # Débats - Mercredi 25 mars 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport David Martin (A6-0117/2009
) 
  Bernard Lehideux  (ALDE
), 
par écrit
. – 
Notre position sur les accords de partenariat économique est toujours critique. Nous pensons que les négociations ont très mal débuté, en ne respectant pas les spécificités de nos partenaires. Nous sommes également toujours persuadés qu’il ne faut pas précipiter les négociations avec eux, et, surtout, ne pas leur imposer des réformes brutales qui pourraient se révéler désastreuses pour leur cohésion sociale et leurs économies.
Mais notre vote tient compte des prises de position très encourageantes de la commissaire Ashton en séance plénière lundi 23 mars dernier. C’est pourquoi nous nous sommes abstenus, et n’avons pas voté contre, comme nous l’aurions certainement fait il y a encore quelques semaines.
Pour autant, notre abstention est un avertissement: nous tenons à juger l’action de la Commission sur pièces, et ne souhaitons pas lui donner un blanc seing pour l’avenir. 
