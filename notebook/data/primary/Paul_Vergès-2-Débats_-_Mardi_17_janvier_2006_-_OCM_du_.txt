Paul Vergès # Débats - Mardi 17 janvier 2006 - OCM du sucre - Régimes de soutien en faveur des agriculteurs (sucre) - Restructuration de l’industrie sucrière
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Paul Verges, 
au nom du groupe GUE/NGL
. - Madame la Présidente, la réforme de l’organisation commune du marché du sucre soulève une vive inquiétude tant dans les pays ACP que dans les régions ultrapériphériques. Cette inquiétude est particulièrement vive à l’île de la Réunion, où la canne à sucre demeure la culture principale. La mobilisation des acteurs de la filière canne à sucre n’a pas été vaine. Elle a conduit à des modifications sensibles du projet initial de réforme. Nous prenons acte de ces modifications positives et saluons le travail accompli par la commission de l’agriculture et son rapporteur.
Pour autant, nous devons rester pleinement lucides. Les mesures d’atténuation des effets de cette réforme sont limitées dans le temps et apparaissent alors comme autant de mesures palliatives qui n’ont d’autre objectif que de faire accepter une réforme qui risque d’avoir à terme des effets catastrophiques. En ce sens, le sort des régions ultrapériphériques rejoint celui des pays ACP. Il nous est impossible de laisser croire que l’avenir est assuré. Il est d’autant moins garanti que les incertitudes sont nombreuses sur l’après 2013. En effet, quelle sera, au-delà de la compensation communautaire insuffisante, la part de la compensation nationale et surtout qu’en est-il de sa pérennisation au-delà de 2013?
Cette inquiétude est d’autant plus légitime que le sommet de Hong Kong a laissé prévoir un nouveau débat à partir de 2009 avec des incidences sur le budget européen, y compris la politique agricole commune. Lorsque l’on sait qu’une souche de canne à sucre représente en moyenne sept ans de récolte et qu’elle doit être suivie par une nouvelle replantation on imagine fort bien que ces incertitudes risquent d’altérer la confiance nécessaire pour atteindre les objectifs de replantation jusqu’à maintenant soutenus par l’Union européenne. C’est en définitive la question de la sauvegarde même d’une filière de canne à sucre qui est posée.
Madame la Présidente, je conclus en vous disant que, devant cette situation où l’avenir de nos planteurs n’est pas assuré au-delà de la durée d’une souche de canne, il nous est impossible d’approuver le rapport qui nous est présenté. 
