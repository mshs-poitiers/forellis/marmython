Bruno Gollnisch # Débats - Jeudi 12 mars 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Proposition de résolution (B6-0105/2009
) 
  Bruno Gollnisch  (NI
). - 
 Monsieur le Président, errare humanum est, perseverare diabolicum
. Jamais cette maxime latine n’a été autant appropriée que dans la lamentable saga des négociations de l’adhésion avec la Turquie.
Depuis 2005, vous nous répétez les mêmes constats négatifs en matière de droits de l’homme, en matière de respect des minorités, en matière d’engagements pris envers l’Union, tout en maintenant intact l’objectif d’adhésion.
Or, en réalité, le problème n’est pas là. Le fond du problème réside dans la volonté des Européens de ne plus assumer les conséquences d’une liberté d’établissement qui découlerait nécessairement de l’adhésion.
Il réside aussi dans le fait que la Turquie appartient géographiquement, culturellement, linguistiquement et spirituellement à une zone différente de l’Europe. Il faut par conséquent abandonner cette fiction, il faut abandonner cette mascarade de l’adhésion et il faut entamer immédiatement des discussions pour le concret, c’est-à-dire pour un partenariat reposant sur nos intérêts mutuels et réciproques. Il faut abandonner cette procédure d’adhésion. 
