Françoise Grossetête # Débats - Mardi 16 décembre 2008 - Stockage géologique du dioxyde de carbone (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Françoise Grossetête, 
rapporteure pour avis de la commission de l’industrie, de la recherche et de l’énergie
. − Monsieur le Président, au nom de la commission industrie, je me réjouis du compromis qui a été obtenu et qui pose les bases du développement de technologies de capture et de stockage de CO2
, en définissant d’ailleurs un nouveau cadre réglementaire qui va fixer les conditions juridiques pour le stockage permanent et sûr de CO2
 en sous-sol.
Nous sommes dans le domaine de l’expérimentation. Nous devons donc mettre toutes les chances de notre côté pour expérimenter cette technologie et en démontrer sa fiabilité. Nous avons réussi à apporter les moyens financiers qui faisaient défaut pour la construction de douze projets de démonstration à travers l’Europe.
À ce titre, je salue l’accord sur les 300 millions de droits d’émission qui ont été obtenus et je voudrais en profiter pour féliciter les deux rapporteurs, M. Davies et Mme
 Doyle. La bonne coordination établie entre eux a permis d’obtenir un tel résultat.
Si la technique du CSC devient commercialement viable, elle pourra être proposée à des pays tiers aussi, comme la Chine, l’Inde, etc., et cette technologie devrait permettre également à l’Union européenne d’occuper un rôle de chef de file au niveau mondial dans l’éventail diversifié des technologies énergétiques propres, efficaces et à faible émission de carbone.
Une fois que nous aurons pris le recul nécessaire en matière de recherche expérimentale, nous pourrons alors donner un caractère contraignant à cette technologie pour un certain nombre de centrales.
