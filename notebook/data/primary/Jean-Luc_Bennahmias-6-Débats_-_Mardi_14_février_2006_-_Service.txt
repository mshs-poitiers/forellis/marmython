Jean-Luc Bennahmias # Débats - Mardi 14 février 2006 - Services dans le marché intérieur (suite du débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Luc Bennahmias (Verts/ALE
). 
 - Monsieur le Président, chers collègues, Monsieur le Commissaire, je demande, au nom du groupe des Verts, de prévoir jeudi, lors du vote, une interruption de séance juste avant le vote final.
Le Parlement et la Commission devraient ériger une statue à M. Bolkestein, l’Européen le plus connu des années 2005 et 2006, pour que tous se rappellent que nous ne voulons plus d’une proposition de ce type due à l’initiative de la Commission européenne.
Certes, aujourd’hui, nous n’en sommes plus à la directive Bolkestein initiale: nos commissions parlementaires ont travaillé et bien travaillé. Mais est-ce suffisant pour accepter ce compromis? Sincèrement, je ne le pense pas, nous ne le pensons pas. Il existe encore trop de zones d’ombre dans ce texte, notamment sur les possibilités de contrôle en ce qui concerne le droit du travail, le droit de l’environnement et le droit du consommateur. Il n’est pas possible d’accepter que des services économiques d’intérêt général comme les services sociaux ou le logement social soient concernés par cette directive.
Si l’on veut redonner confiance à l’ensemble de nos concitoyens, qui doutent de plus en plus du rôle de la construction européenne dans l’amélioration de leur quotidien, nous devons passer rapidement à une véritable harmonisation sociale par le haut, notamment en élaborant en priorité une directive qui définisse les notions de service public européen. 
