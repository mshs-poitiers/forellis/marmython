Pierre Moscovici # Débats - Mercredi 28 mars 2007 - Interventions d’une minute sur des questions politiques importantes
  Le Président. 
 - Ce n’est pas une dictature, au contraire, c’est une démocratie. Nous avons un règlement et il convient de le respecter. Tout parlementaire ne peut pas tenir, ici, des propos généraux sur ce qu’il veut.
Monsieur Stoyanov, j’avais cru comprendre que vous vouliez intervenir pour un fait personnel. Ce n’est pas le cas et je ne peux pas vous laisser poursuivre. 
