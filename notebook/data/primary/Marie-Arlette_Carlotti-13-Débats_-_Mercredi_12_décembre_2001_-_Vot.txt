Marie-Arlette Carlotti # Débats - Mercredi 12 décembre 2001 - Votes
  Carlotti (PSE
),
par écrit
. 
- Depuis plus de 30 ans, l'Europe a fait le choix de construire un modèle original de partenariat avec les pays d'Afrique, des Caraïbes et du Pacifique.
C'est dans le cadre de cette démarche d'ouverture et de solidarité que l'organisation commune du marché de la banane a été mise en place pour garantir l'accès au marché européen d'une partie des productions de ces pays ACP, dont certains comptent parmi les plus pauvres du monde.
Ce dispositif a depuis son origine été combattu par les grandes multinationales du secteur de la banane dont les productions sont très compétitives... mais souvent au prix d'une exploitation proche de l'esclavage !
De pressions politiques en sanctions économiques et de compromis en concessions, c'est tout le dispositif de protection quantitative qui doit être démantelé dès 2006 au profit d'un système de protection uniquement tarifaire dont nul ne connaît à l'heure actuelle l'efficacité pour sauvegarder les intérêts des producteurs traditionnels de bananes ACP.
Certes, le compromis conclu en avril 2001 a permis de mettre fin à la guerre commerciale qui a empoisonné les relations transatlantiques depuis près de dix ans. Mais certaines de ses dispositions constituent pour les pays ACP une véritable "potion amère" : fin de la protection contingentaire qui permettait de garantir un accès au marché européen pour ses producteurs traditionnels en 2006, réduction dès janvier 2002 de 100 000 tonnes de la quantité de production garantie, sans compter les dysfonctionnements et retards du dispositif d'assistance technique et financière censé aider les producteurs ACP à s'adapter aux nouvelles conditions du marché, mais dont les versements se font toujours attendre.
Le rapport de Michel Dary sur lequel nous devons nous prononcer aujourd'hui propose que pendant la période transitoire qui précède le plongeon dans l'inconnu que représente la fin de l'OMC bananes, plusieurs "filets de sécurité" soient accordés aux pays ACP.
(Intervention écourtée en application de l'article 137 du règlement)

