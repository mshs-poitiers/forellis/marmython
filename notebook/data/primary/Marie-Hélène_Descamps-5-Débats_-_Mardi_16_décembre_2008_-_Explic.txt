Marie-Hélène Descamps # Débats - Mardi 16 décembre 2008 - Explications de vote 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
– Rapport: Christa Prets (A6-0461/2008
) 
  Marie-Hélène Descamps  (PPE-DE
), 
par écrit
. – Qu’ils soient traditionnels ou nouveaux, les médias occupent une place importante dans notre quotidien. Face à ce constat, il convient de permettre aux citoyens européens de comprendre, d’analyser et d’évaluer l’afflux d’informations et d’images qu’ils reçoivent afin qu’ils puissent les exploiter au mieux. Ces compétences sont d’autant plus nécessaires aujourd’hui que l’usage de l’internet s’est généralisé et que le consommateur n’est plus simple spectateur mais devient de plus en plus acteur de ce processus.
Le rapport qui nous est soumis, et que personnellement je soutiens, s’inscrit dans ce contexte. Il traduit la volonté politique d’agir pour sauvegarder les droits et libertés de tout un chacun évoluant dans l’environnement numérique. 
Incluant tous les citoyens, en particulier les plus jeunes, ce rapport plaide pour la mise en place d’un niveau élevé d’éducation aux médias. Il vise une formation adaptée à chaque type de média tout en réaffirmant le droit d’accès pour tous aux technologies de l’information et de la communication. Il encourage une formation de qualité qui privilégie une attitude responsable et respectueuse vis-à-vis des droits de propriété intellectuelle. Contribuant par ailleurs à la réalisation des objectifs de Lisbonne, cette éducation aux médias constitue un atout indispensable pour le développement d’une citoyenneté consciente et active.
