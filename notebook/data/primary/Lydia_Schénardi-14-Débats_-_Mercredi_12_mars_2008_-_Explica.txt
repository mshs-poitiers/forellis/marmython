Lydia Schénardi # Débats - Mercredi 12 mars 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Christa Klaß (A6-0031/2008
) 
  Lydia Schenardi  (NI
), 
par écrit
. – 
(FR)
 La question de la condition de la femme dans les zones rurales a été abordée de multiples fois lors de nombreuses Conférences mondiales sur les femmes (1975, 1980, 1985 et 1995), sur la réforme agraire et le développement rural (1979) ou sur la population (1994), mais aussi par la Commission et le Parlement européens. Mais les textes ont beau s'accumuler, les constats sont toujours les mêmes: augmentation de la "masculinisation" de la population rurale et aucune amélioration relative au statut du conjoint-aidant sur une exploitation agricole.
Il est plus que temps de développer des stratégies visant à freiner l'exode rural des femmes, en particulier des diplômées. De prendre en considération l'évolution des grandes orientations sur le plan mondial et notamment la libération des échanges et des finances, ainsi que la privatisation de l'agriculture en un secteur commercial strictement réglementé et quotingenté.
Les temps changent, mais ce n'est pas le cas des mentalités et des coutumes qui veulent que le rôle des femmes dans le secteur agricole se résume bien trop souvent à être coexploitantes ou ouvrières non rémunérées dans des exploitations bien souvent uniquement familiales.
Ce rapport propose des modifications de ce statut ? Nous voterons pour. 
