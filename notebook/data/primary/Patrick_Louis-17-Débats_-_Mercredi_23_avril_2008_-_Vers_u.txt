Patrick Louis # Débats - Mercredi 23 avril 2008 - Vers une réforme de l'Organisation mondiale du commerce (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Patrick Louis  (IND/DEM
). – 
(FR)
 Madame la Présidente, chers collègues, l'échange entre les nations est une bonne chose. L'échange économique libre est souhaitable, mais aujourd'hui le monde a changé et les règles de l'OMC sont inadaptées et, souvent, doivent être modifiées.
Effectivement, la nature des échanges internationaux a évolué. Hier, l'échange était celui des complémentarités, on cherchait ce que l'on n'avait pas, on exportait les excédents. Cet ordre a engendré la prospérité des nations. Mais aujourd'hui, le dumping social prime. On abandonne ce que l'on sait faire pour importer ce que l'autre fait moins cher, non pas parce qu'il est plus productif mais parce qu'il est moins chargé, moins fiscalisé, moins contraint socialement.
Cet ordre de l'OMC permet aux pays pauvres, aux pauvres des pays riches, d'enrichir les riches des pays pauvres. Il est de moins en moins celui des solidarités, des mutualisations organisées dans les nations, mais un ordre bouleversant les nations et créant un conflit entre la caste des gagnants et celle des perdants.
Ainsi, les règles de l'OMC doivent être changées. Elles doivent réhabiliter la préférence communautaire, il faut renouer avec l'esprit du traité de Rome qui instituait le tarif extérieur commun. Ce n'était pas une protection pour frileux, mais une juste compensation face au dumping social. Les pères fondateurs n'ont pas toujours eu tort. L'OMC doit intégrer l'évolution erratique des taux de change dans son évaluation des contraintes commerciales. Il est inadmissible que le yuan reste bas malgré un commerce extérieur très excédentaire. Il est scandaleux qu'EADS perde un milliard chaque fois que le dollar baisse de 10 centimes face à un euro idéologique.
En conclusion, l'avenir d'un commerce vraiment libre nous rappelle qu'avant de laisser faire, il faut beaucoup faire. Il faut, d'une part, réhabiliter la frontière comme condition de la politique et donc de la liberté des peuples et, d'autre part, subordonner l'économie monétaire et financière à l'économie réelle, l'économie productive qui, elle seule, permet aux peuples de vivre ici, maintenant et libres. 
