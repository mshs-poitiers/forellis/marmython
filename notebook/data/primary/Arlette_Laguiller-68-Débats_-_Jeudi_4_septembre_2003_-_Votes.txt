Arlette Laguiller # Débats - Jeudi 4 septembre 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. - Nous n’avons pas voté ce rapport car nous ne voulons pas soutenir les investisseurs de capitaux, que ce soit dans le secteur culturel ou dans d’autres secteurs. Ce sont d’ailleurs souvent les mêmes: bien des capitalistes qui tirent profit d’investissements dans la télévision privée, dans le cinéma ou dans l’édition, tirent l’essentiel de leurs profits des travaux publics, du monopole de la distribution de l’eau, voire de la fabrication d’armes.
Nous voulons, en revanche, protéger ceux grâce à qui cette "industrie de la culture" fonctionne. En conséquence, nous exprimons notre entier soutien à ces intermittents du spectacle qui, en France, luttent depuis plusieurs mois pour défendre leurs conditions d’existence. Le gouvernement français qui fait si grand cas de "l’exception culturelle" ne fait pas d’exception pour les travailleurs du spectacle: sur les recommandations directes du grand patronat, il vient d’attaquer leur protection sociale, comme il se prépare à aggraver les attaques contre la Sécurité sociale de tous les salariés. 
