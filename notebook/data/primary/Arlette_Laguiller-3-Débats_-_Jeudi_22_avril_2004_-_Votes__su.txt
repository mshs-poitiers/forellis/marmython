Arlette Laguiller # Débats - Jeudi 22 avril 2004 - Votes (suite) 
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. 
- En appuyant par notre vote ce que souhaitent les syndicats, nous avons voté ce rapport, bien qu’il refuse même le droit de vote aux représentants syndicaux qui participent aux conseils d’administration, ce qui devrait être un minimum.
Cela dit, étant donné que ce rapport fait partie d’un ensemble qui vise à la "libéralisation" du système ferroviaire, nous tenons à réitérer notre opposition absolue à toute forme de privatisation des chemins de fer, lesquels devraient être un service public à l’échelle de l’ensemble de l’Europe. 
