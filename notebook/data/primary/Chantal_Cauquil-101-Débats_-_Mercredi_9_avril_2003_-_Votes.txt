Chantal Cauquil # Débats - Mercredi 9 avril 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. - Nous sommes bien entendu contre la polygamie et l'oppression qu'elle représente pour les femmes.
Nous n'acceptons cependant pas que, sous prétexte qu'une des conjointes vit déjà dans le pays d'immigration, on interdise aux autres d'y entrer et d'y séjourner.
Sous des dehors moralisateurs, il s'agit seulement de dresser un obstacle de plus à l'immigration en brisant des liens personnels.
Tout en refusant le principe même d'une telle limitation, nous avons voté l'amendement qui au moins envisage des cas d'exception lorsque "l'intérêt de l'enfant mineur l'exige".
Par ailleurs, nous avons rejeté les amendements vexatoires comme l'utilisation des tests ADN contre le migrant ou encore l'argument de santé publique qui procède de l'attitude égoïste consistant à refuser les soins que les pays européens sont capables d'offrir à des populations qui en sont dépourvues.
Et, une fois de plus, les propositions qui représentent un progrès ne sont pas contraignantes et l'ensemble, au lieu de rapprocher les législations nationales par le haut, les rapproche par le bas.
