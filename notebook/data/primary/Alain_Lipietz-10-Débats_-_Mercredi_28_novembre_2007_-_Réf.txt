Alain Lipietz # Débats - Mercredi 28 novembre 2007 - Référendum au Venezuela (débat) 
  Alain Lipietz, 
au nom du groupe Verts/ALE
. 
– (FR) 
Je parle en tant que président de la délégation pour la communauté andine et, à ce titre, je me rends depuis plusieurs années une ou plusieurs fois par an au Venezuela.
Depuis que je vais dans ce pays, c'est-à-dire depuis l'époque du coup d'État militaire fomenté contre le président Chavez, j'ai toujours entendu les médias vénézuéliens hurler contre le président Chavez, hurler à la dictature. Dans les hôtels où je me rendais, qui étaient pourtant des hôtels trois, quatre, cinq étoiles, on n'avait pas le droit de capter la télévision publique et on n'a toujours pas le droit de le faire. Elle est diffusée en général sous une forme neigeuse. Les généraux qui ont commis le coup d'État militaire campent depuis ce coup d'État sur la plus grande place de Caracas sans que jamais le président Chavez, légalement élu, réélu, re-réélu, n'ait jamais levé le petit doigt contre eux.
Le Venezuela est un des pays qui essaient de gérer de la façon la plus pacifique possible les conflits dont toute l'Amérique latine est le siège. Je ne suis pas absolument fanatique de toutes les modifications que le président Chavez a proposé d'intégrer dans la constitution bolivarienne. Cela dit, comme vient de le dire M. Matsakis, c'est le peuple vénézuélien qui tranchera.
Alors on peut effectivement s'excuser sur la façon dont on a donné l'impression de soutenir le coup d'État militaire. Oui, cela a beaucoup aidé à une radicalisation de ce régime. Mais je crois qu'il faut d'abord et avant tout respecter la décision du peuple vénézuélien. 
