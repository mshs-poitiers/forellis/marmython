Jean-Louis Bourlanges # Débats - Jeudi 16 mars 2000 - VOTES
  Bourlanges (PPE-DE
). 
 - Monsieur le Président, excusez-moi, j'essaie d'attirer votre attention, mais vous allez très vite, ce dont je vous félicite. Je voudrais simplement, à propos du rapport de Mme Jackson, dire que je n'ai pas pris part au vote pour des raisons voisines de celles de Mme Lienemann. Je considère que le respect du droit communautaire doit être général et ne saurait faire l'objet d'interprétations sélectives nécessairement arbitraires. Je voudrais qu'il soit porté au procès-verbal que je n'ai pas pris part au vote.
***
