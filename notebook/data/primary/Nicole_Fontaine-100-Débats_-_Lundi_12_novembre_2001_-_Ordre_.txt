Nicole Fontaine # Débats - Lundi 12 novembre 2001 - Ordre des travaux
  La Présidente.  
- 
Monsieur Tannock, vous pouvez imaginer que j'ai été extrêmement attentive à l'information à laquelle vous avez fait allusion. On m'a affirmé que les plans que ces terroristes avaient sur eux ne concernaient que la cathédrale de Strasbourg et le marché de Noël. Mais, bien évidemment, cela nous incite à une prudence encore plus grande, et je vais tout de suite donner la parole à M. Balfe, qui va d'ailleurs pouvoir vous expliquer ce que nous avons fait pour renforcer la sécurité. 
