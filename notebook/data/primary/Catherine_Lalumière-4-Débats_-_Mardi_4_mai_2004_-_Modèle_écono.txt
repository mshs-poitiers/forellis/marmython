Catherine Lalumière # Débats - Mardi 4 mai 2004 - Modèle économique et social européen
  La Présidente.  
- Monsieur Lepper, on est obligé de vous couper le micro. Je suis désolée. Je dois rappeler que les orateurs qui n’auront pas eu le temps de parole qu’ils espéraient avoir peuvent déposer par écrit l’intervention qu’ils auraient souhaité prononcer. Le document sera à leur disposition pour qu’ils le fassent connaître comme une intervention faite à la session plénière du Parlement européen. 
