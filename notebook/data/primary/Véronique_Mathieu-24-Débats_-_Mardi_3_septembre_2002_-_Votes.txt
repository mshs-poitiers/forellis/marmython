Véronique Mathieu # Débats - Mardi 3 septembre 2002 - Votes
  Mathieu (EDD
), 
par écrit.
 
- Alors que l'UE se trouve dans une situation de grande dépendance à l'égard de quelques fournisseurs pour son approvisionnement en protéines végétales, la Commission propose de continuer à recourir aux importations, préférant une opportunité de marché à la mise en cohérence de ses diverses politiques. Ce faisant, elle ignore l'expérience difficile que les agriculteurs ont déjà faite de ce type de situation et prive l'agriculture de nouvelles perspectives.
Le développement des oléoprotéagineux permettrait de répondre au souci de développer une agriculture diversifiée et durable, à la demande des consommateurs pour des produits tracés et à leur refus des OGM. Elle permettrait aussi de donner de nouvelles perspectives aux agriculteurs par le développement de débouchés non alimentaires dont les biocarburants. Enfin, ces cultures ont un rôle essentiel sur le plan agronomique et environnemental.
Ne bradons pas ce secteur de l'agriculture. Au contraire, donnons-lui les moyens de ses ambitions en établissant des conditions de rentabilité comparables à celles des céréales, en favorisant la diversité des espèces, et plus particulièrement de celles qui ne sont pas concernées par l'accord de Blair House.
Mais ces efforts seront vains si nos négociateurs ne s'engagent pas à défendre avec conviction notre production agricole sur la scène internationale. 
