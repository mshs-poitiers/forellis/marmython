Élizabeth Montfort # Débats - Mardi 20 avril 2004 - Votes
  Montfort (PPE-DE
),
par écrit
. 
- Le rapport de Monsieur Mantovani se penche sur l’égalité des chances pour les personnes handicapées, ce qui est incontestablement une bonne chose.
Trop souvent il est vrai, on oublie qu’avant d’être en présence de personnes handicapées, nous sommes face à des personnes tout simplement, qui méritent tout autant le respect de leur dignité et de leurs droits: droit à la libre circulation, droit de participer à la vie de la société en général, droit à l’égalité des chances et droit d’être respectées en tant que personne humaine. En un mot, les droits reconnus à tous les êtres humains.
Une société digne se reconnaît à la place qu’elle réserve à ceux qui sont les plus fragiles. Malheureusement, aujourd’hui, dans les pays que l’on dit développés, les personnes handicapées sont encore trop souvent victimes de discriminations.
C’est pourquoi, je soutiens le rapporteur lorsqu’il demande que toute action politique se fonde sur les valeurs universelles reconnues pour tous. Puisse ce rapport nous rendre enfin attentifs à toutes les personnes handicapées. C’est la raison pour laquelle j’ai voté en sa faveur. 
