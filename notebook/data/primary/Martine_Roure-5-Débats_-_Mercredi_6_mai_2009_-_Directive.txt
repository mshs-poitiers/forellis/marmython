Martine Roure # Débats - Mercredi 6 mai 2009 - Directives (2006/48/CE et 2006/49/CE) sur les exigences de fonds propres - Programme communautaire de soutien à des activités spécifiques dans le domaine des services financiers, de l’information financière et du contrôle des comptes
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  La Présidente. - 
 L’ordre du jour appelle la discussion commune sur:
– le rapport de Othmar Karas, au nom de la commission des affaires économiques et monétaires, sur la proposition de directive du Parlement européen et du Conseil modifiant les directives 2006/48/CE et 2006/49/CE en ce qui concerne les banques affiliées à des institutions centrales, certains éléments des fonds propres, les grands risques, les dispositions en matière de surveillance et la gestion des crises (COM(2008)0602
 - C6-0339/2008
 - 2008/0191(COD)
) (A6-0139/2009
), et
– le rapport de Karsten Friedrich Hoppenstedt, au nom de la commission des affaires économiques et monétaires, sur la proposition de décision du Parlement européen et du Conseil établissant un programme communautaire de soutien à des activités spécifiques dans le domaine des services financiers, de l’information financière et du contrôle des comptes (COM(2009)0014
 - C6-0031/2009
 - 2009/0001(COD)
) (A6-0246/2009
).
