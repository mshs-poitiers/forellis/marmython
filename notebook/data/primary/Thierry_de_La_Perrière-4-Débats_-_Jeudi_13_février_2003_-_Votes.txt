Thierry de La Perrière # Débats - Jeudi 13 février 2003 - Votes
  de La Perriere (NI
),
par écrit
. - C'est une bonne chose que l'Union européenne aide les pays en développement à prendre soin de leur santé. Cependant, qui dit aide ne dit pas placage de nos modèles européens à des pays de cultures et traditions différentes. Il est moralement inacceptable de conditionner notre aide à la négation de l'identité des peuples et à l'adoption obligatoire de notre conception du planning familial.
Par ailleurs, il est inconcevable d'ériger l'avortement en nouveau mode de contraception. C'est pourtant ce à quoi conduit ce rapport, même si Mme Sandbæk s'en défend. L'avortement est un crime qui tue l'enfant et détruit la mère. En offrant aux femmes comme unique choix l'avortement, au lieu de les aider, on ajoute à leurs difficultés un nouveau traumatisme. C'est pourquoi je soutiens les amendements visant à développer une politique d'accueil et d'accompagnement des futures mères en détresse, afin de les soutenir dans leur choix de maternité. C'est comme cela que l'on pourra leur procurer l'aide morale et matérielle dont elles ont besoin.
C'est aussi pourquoi je voterai contre ce rapport qui, loin d'aider les populations en difficulté, ne pourra qu'ajouter à leur désarroi. 
