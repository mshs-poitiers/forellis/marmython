Georges Berthu # Débats - Mardi 9 mars 2004 - Votes
  Berthu (NI
),
par écrit
. 
- Le règlement sur le statut et le financement des partis politiques au niveau européen viole les principes fondamentaux que j’ai énumérés hier au cours du débat, et aussi un certain nombre de dispositions de procédure du traité.
À ce dernier titre, on peut soutenir qu’il viole l’article 202 TCE qui prévoit que des compétences d’exécution peuvent être données à la Commission, mais non au Parlement européen. Il viole aussi l’article 198 TCE qui dispose que "sauf dispositions contraires du présent traité, le Parlement européen statue à la majorité absolue des suffrages exprimés". Or le règlement, à son article 5, décide que le Parlement européen vérifiera le respect des conditions de financement "à la majorité de ses membres", alors que l’article 191 du traité ne lui a donné aucune autorisation en ce sens. À l’évidence, c’est illégal.
Le rapport Dimitrakopoulos essaie de corriger cette irrégularité en réintroduisant, au niveau du règlement du Parlement européen, l’exigence d’un vote "à la majorité des suffrages exprimés". Mais il ne fait qu’accroître la confusion, en multipliant les contradictions.
Toutes ces anomalies, souhaitons-le, fourniront matière la Cour de Justice pour déclarer non conforme au traité ce règlement inopportun et mal bâti. 
