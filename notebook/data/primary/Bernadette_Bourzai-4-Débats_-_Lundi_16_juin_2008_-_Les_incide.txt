Bernadette Bourzai # Débats - Lundi 16 juin 2008 - Les incidences des politiques de cohésion sur l'insertion des populations et des catégories vulnérables (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bernadette Bourzai  (PSE
).  
– (FR) 
Monsieur le Président, Madame la Commissaire, chers collègues, je tiens d'abord à féliciter notre collègue Harangozó pour l'excellent travail qu'il a réalisé et pour la réflexion qu'il a lancée sur ce sujet complexe et délicat. J'ai tenu à participer activement à l'avis qu'a rendu la commission de l'agriculture et du développement rural sur ce rapport, car les personnes les plus vulnérables se concentrent souvent dans les zones rurales les moins développées et les zones à handicap naturel permanent et, donc, l'agriculture et la politique de développement rural ont un grand rôle à jouer.
La politique de cohésion doit, selon moi, s'employer à maintenir des activités, agricoles ou non agricoles, rémunératrices dans les zones rurales, et ce afin de maintenir des populations souvent tentées par l'exode rural, mais aussi d'accueillir de nouvelles populations. Il est important de favoriser une agriculture familiale, créatrice d'emplois, ainsi qu'un égal accès aux services publics et à la présence de services au public afin de répondre aux besoins des familles, des communautés et des groupes défavorisés. Bref, il faut rendre le milieu rural attractif et vivable. Or, pour atteindre cet objectif, il est nécessaire d'assurer une synergie entre les différentes politiques menées sur ces territoires et à destination de ces populations fragiles et, donc, d'exploiter davantage les complémentarités des divers instruments financiers disponibles. 
