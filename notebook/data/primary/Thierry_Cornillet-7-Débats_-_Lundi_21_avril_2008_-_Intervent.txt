Thierry Cornillet # Débats - Lundi 21 avril 2008 - Interventions d’une minute sur des questions politiques importantes
  Thierry Cornillet  (ALDE
). – 
(FR) 
Monsieur le Président, je voulais simplement rappeler à nos collègues que, mercredi, c'est pour la première année la Journée mondiale du paludisme. Jusqu'à présent, c'était African Malaria Day.
 Pour la première année, c'est une Journée mondiale du paludisme.
Je voulais rappeler que ce fléau mondial, lié naturellement à la pauvreté, affecte 107 pays dans le monde et que, dans 90 de ces pays, c'est un problème majeur de santé publique, puisque 40 % de la population mondiale vit dans des régions affectées par le paludisme. Le nombre de cas de paludisme rencontrés dans le monde se situe entre 350 et 500 millions par année, dont plus de 60 % des cas en Afrique, qui causent 90 % des décès. Toutes les trente secondes – c'est-à-dire qu'à la fin de mon intervention, Monsieur le Président, deux enfants seront morts du paludisme –, toutes les trente secondes, un enfant meurt en Afrique.
On estime en plus que c'est un ralentisseur de croissance, puisque le paludisme coûte au PIB douze milliards de dollars par an. Il y a, mercredi, un déjeuner de travail au salon C 2.1, à 13 heures, sur ce sujet. 
