Philippe Morillon # Débats - Mercredi 9 juillet 2008 - Décision sur l’urgence
  Philippe Morillon, 
Président de la commission PECH
. –
 Mes chers collègues, nous avons effectivement été saisis de cette demande d'urgence, et celle-ci a pu être examinée par la commission de la pêche à l'occasion de la réunion exceptionnelle qui nous a rassemblés ici–même à 10 heures. Je signale que la commission de la pêche s'est prononcée à l'unanimité en faveur de l'adoption de cette procédure d'urgence, et je la remercie d'ailleurs pour la diligence avec laquelle elle a pris en compte les problèmes. 
