Jean-Paul Gauzès # Débats - Mercredi 25 mars 2009 - Renforcement de la sécurité et des libertés fondamentales sur Internet (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Paul Gauzès  (PPE-DE
). - 
 Monsieur le Président, Monsieur le Commissaire, mes chers collègues, j’interviens au nom de notre collègue, Jacques Toubon.
Je souhaiterais d’abord remercier notre collègue Vlad Popa qui a fait un excellent travail pour tenter de trouver des compromis acceptables par tous, malgré les positions extrêmes prises par certains collègues du PSE et des Verts sur cette question.
Le rapport soulève la question importante de l’équilibre à trouver entre la sécurité et les libertés fondamentales sur Internet. En effet, si cette nouvelle technologie est pour beaucoup synonyme de progrès et d’opportunités, elle n’est pas sans danger. Il est, par exemple, essentiel d’assurer la liberté d’expression et d’information sur ce nouveau média, tout en veillant à ce que cela se fasse en harmonie avec le respect d’autres libertés fondamentales, telles que la protection de la vie privée, des données personnelles ou encore le respect de la propriété intellectuelle.
Le rapporteur, M. Lambrinidis, qui a fait un excellent travail, a ainsi pris en compte les nouvelles formes de crime sur Internet et les dangers qu’il représente, notamment pour les mineurs. Le rapport reste malheureusement plus ambigu et même dangereux sur d’autres points.
Les amendements déposés par nos collègues Hieronymi, Mavrommatis ou encore Jacques Toubon visent à préciser le fait que les atteintes à des libertés fondamentales ne devraient pas être cautionnées au nom de la liberté d’expression et d’information.
Les États membres et les acteurs de l’Internet devraient conserver une certaine marge de manœuvre pour trouver les meilleures solutions afin que les droits des uns n’empêchent pas radicalement l’exercice de ceux des autres. Les lois doivent s’appliquer sur Internet comme partout ailleurs. Internet ne peut pas être un territoire virtuel où un acte qui constitue une infraction légale dans le monde réel serait considéré comme autorisé, et même protégé par le simple effet de la technologie et de l’usage qui en est fait. Il en va de l’État de droit dans nos sociétés démocratiques. 
