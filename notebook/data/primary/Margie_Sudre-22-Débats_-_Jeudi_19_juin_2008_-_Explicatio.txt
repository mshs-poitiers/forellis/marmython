Margie Sudre # Débats - Jeudi 19 juin 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Résolution commune - Crise du secteur de la pêche due à l’augmentation du prix du gazole (RC-B6-0305/2008
) 
  Margie Sudre  (PPE-DE
), 
par écrit
. – 
(FR) 
L'augmentation dramatique du prix du gazole, 300% depuis 2003, affecte de plein fouet le secteur de la pêche. De fait, l'aggravation continue de la crise a placé de nombreuses entreprises de pêche dans une situation de grande fragilité financière.
Or, la filière pêche constitue un pilier particulièrement important du développement économique et social des régions ultrapériphériques. De plus, le renchérissement du prix du carburant est d'autant plus accentué du fait de leur situation géographique éloignée. Cette situation est inadmissible. Il est du devoir de l'Union européenne de dégager un compromis.
Le Parlement européen vient d'envoyer un signal fort aux pêcheurs. En demandant un relèvement du plafond des aides publiques à 100 000 euros par navire et non par entreprise, en appelant à l'application immédiate des mesures d'urgence et d'accompagnement social, enfin, en exigeant une réorganisation des dépenses du Fonds européen pour la pêche, les eurodéputés ont fait comprendre aux pêcheurs que leur détresse a été entendue.
Ces propositions seront discutées par le Conseil des ministres européens de la pêche, les 23 et 24 juin prochains. L'Union européenne n'a pas d'autre choix que de rétablir la confiance avec les marins pêcheurs, colmatant ainsi le décalage abyssal qui s'est dernièrement fait jour. 
