Jacky Hénin # Débats - Mardi 16 mai 2006 - Amélioration des performances environnementales du système de transport de marchandises («Marco Polo II») (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jacky Henin (GUE/NGL
). 
 - Monsieur le Président, face aux dangers pour la sécurité et pour l’environnement, face aux pratiques d’une profession qui, trop souvent, transforme ses salariés en esclaves des temps modernes, face à l’augmentation importante des flux annoncés, la Commission semble prendre conscience que le problème est grave et qu’il faudrait s’en préoccuper. La concurrence libre et non faussée nous conduit à la catastrophe. On nous propose de développer les autoroutes de la mer, le rail, l’intermodalité et d’aider les PME et les PMI. Cependant, la Commission poursuit parallèlement sa croisade pour privatiser les chemins de fer, sans même dresser le bilan de dix années catastrophiques. L’orientation est bonne mais n’est pas crédible, tant les moyens font défaut.
C’est pourquoi mon groupe demande un vote séparé sur le rapport de notre collègue Rack et sur l’avis de la commission des budgets. Nous voterons contre cet avis car il se résume ainsi: «Dis-moi ce qui te fait défaut, je t’expliquerai comment t’en passer».
