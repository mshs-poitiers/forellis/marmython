Didier-Claude Rod # Débats - Mercredi 14 mars 2001 - Travaux de l'Assemblée ACP/UE (2000) 
  Rod (Verts/ALE
). 
 - Monsieur le Président, l'accord de Cotonou consacre la dimension parlementaire de l'Assemblée paritaire. Cela implique que seuls des représentants démocratiquement élus, sauf exception, ont le droit de participer, de s'exprimer, au sein de l'Assemblée parlementaire paritaire. Mais un seul représentant par pays ACP empêche l'accès et l'expression des représentants de l'opposition et occulte ainsi l'expression pluraliste politique dans ces pays.
En outre, ce principe d'une voix par pays dans le groupe ACP contre une voix par personne du côté européen n'est pas équitable. La structure même de cette Assemblée est donc déséquilibrée. Le Parlement européen est une institution en tant que telle, le groupe ACP n'a pas d'existence en dehors de son appartenance à l'Assemblée parlementaire paritaire et n'a pas d'organisation sous forme de courants politiques.
Deuxième remarque : l'Assemblée parlementaire paritaire se voit reconnaître un nouveau rôle, mais il n'y a pas que
 le pouvoir d'avis. Elle n'a pas le droit de vote sur l'utilisation du Fonds européen de développement, elle n'intervient pas dans la procédure de ratification de l'accord de Cotonou.
Nous voterons donc avec conviction le rapport de M. Martínez Martínez, car il met ces lacunes en évidence. Mais l'Assemblée doit continuer à réfléchir sur sa composition, son fonctionnement et ses pouvoirs, afin de devenir en 2001 une Assemblée parlementaire paritaire et égalitaire. 
