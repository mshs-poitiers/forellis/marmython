Margie Sudre # Débats - Mardi 13 novembre 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Peter Liese (A6-0402/2007
) 
  Margie Sudre  (PPE-DE
), 
par écrit
. –
 Les régions ultrapériphériques (RUP) contribuent solidairement à la lutte contre le changement climatique et manifestent régulièrement leur engagement de continuer sur cette lancée, en devenant de véritables pôles d'innovation en la matière.
Je suis d'accord avec le but poursuivi par l'inclusion de l'aviation dans le système d'échange de quotas d'émission.
Cependant, les RUP ne pourraient supporter en l'état la mise en œuvre de la proposition de la Commission, faute de quoi les compagnies aériennes desservant ces régions n'auraient d'autre recours que d'imputer ces nouveaux surcoûts directement sur le prix des billets au détriment des passagers d'outre-mer et des touristes déjà pénalisés par la cherté des voyages, et sur le prix du transport aérien de marchandises, entraînant une hausse des prix aux dépens des consommateurs et des entreprises ultrapériphériques.
Je remercie le rapporteur d'avoir entendu mes revendications à ce sujet, et le Parlement européen d'avoir finalement entériné le principe d'un traitement particulier pour les RUP dans ce dossier.
Je me félicite également que notre Parlement ait accepté la possibilité qu'une partie des fonds issus des mises aux enchères de quotas puisse être affectée à la limitation de l'impact négatif du projet en termes d'accessibilité et de compétitivité pour les RUP. 
