Patrick Gaubert # Débats - Mercredi 22 octobre 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Proposition de résolution: Conseil européen (B6-0543/2008
) 
  Patrick Gaubert  (PPE-DE
), 
par écrit
. –
 Je me réjouis du soutien apporté par le Parlement au pacte européen pour l’immigration et l’asile dans le cadre du vote de la résolution sur le Conseil européen des 15 et 16 octobre derniers pour laquelle j’ai voté.
Les députés ont salué cette initiative de la Présidence française de l’UE qui propose une approche cohérente et équilibrée en matière d’immigration et qui réaffirme le choix responsable de l’Union européenne de promouvoir une immigration légale et de lutter fermement contre l’immigration illégale.
Ce succès qui concerne le cadre d’action globale est consolidé par les efforts de la présidence française en vue d’aboutir à une adoption rapide des propositions de directives en cours de négociation, traduisant ainsi ces déclarations ambitieuses en actions concrètes. Il s’agit notamment de la directive sur la procédure unique et le socle commun des droits, de la directive dite «carte bleue» sur les conditions d’admission des ressortissants hautement qualifiés et de la directive sur les sanctions contre les employeurs qui embauchent des ressortissants illégaux.
Ce pacte s’inscrit pleinement dans la voie d’une véritable politique commune de l’immigration et de l’asile respectueuse des droits fondamentaux et de la dignité humaine, telle que défendue par le Parlement européen. 
