Marie-Hélène Aubert # Débats - Mercredi 5 avril 2006 - Journée mondiale de la santé (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie-Hélène Aubert, 
au nom du groupe Verts/ALE
. - Monsieur le Président, Madame le Commissaire, chers collègues, si l’on peut s’interroger sur la portée souvent éphémère et sur l’efficacité de ces journées mondiales consacrées à telle ou telle grande cause, il reste que c’est pour nous l’occasion, d’une part, de mettre en lumière une situation dramatique, la pénurie des personnels de santé au Sud à laquelle on porte peu attention habituellement et, d’autre part, de formuler des propositions à mettre en œuvre sur le court, le moyen et le long terme. D’où d’ailleurs la nécessité d’inscrire ces objectifs en matière de santé dans le cadre de programmations financières pluriannuelles, ce qui permettrait d’accroître la prévisibilité des fonds de l’Union européenne qui fait tant défaut aujourd’hui et de soutenir plus concrètement les stratégies nationales en matière de renforcement des personnels de santé.
Il faut tout de même rappeler que ce problème que nous dénonçons aujourd’hui n’est pas un mauvais coup du sort supplémentaire dû à la fatalité. Cette pénurie de personnel de santé est en effet la conséquence, notamment, de programmes dits «d’ajustement structurel» menés de façon brutale par les institutions financières internationales et qui ont conduit à l’écroulement des services publics de santé et d’éducation dans bon nombre de pays. Il y a donc «un peu beaucoup» d’hypocrisie à proclamer en 2006 une volonté affirmée de renforcer les ressources humaines de santé pour les années à venir, alors que des politiques financières macroéconomiques conduites à un autre niveau ont mis à mal ces mêmes ressources depuis plus d’une décennie. Nous nous réjouissons néanmoins de cette prise de conscience, même tardive, de la nécessité absolue d’investir dans les ressources humaines, sans lesquelles aucune politique de développement, aussi vertueuse soit-elle, ne peut être mise en œuvre.
C’est pourquoi l’action de l’Union européenne dans ce domaine devrait s’orienter pour nous dans trois directions. D’une part, comme cela a déjà été dit, le volume consacré aux questions de santé dans le cadre de l’aide publique au développement. Aujourd’hui il s’élève à environ 5 %. C’est très notoirement insuffisant et d’ailleurs, les perspectives ne sont pas tellement encourageantes, alors qu’il faudrait au moins y consacrer 20 % notamment pour prendre en charge des coûts salariaux pour la formation et pour toutes sortes de questions dont nous avons déjà parlé. Deuxièmement, mettre un terme aux mesures de restriction budgétaire imposées par les institutions financières internationales. Ici le poids de l’Union européenne dans ces instances est important, notamment en ce qui concerne le plafond des salaires et le recrutement dans la fonction publique. Enfin, un code d’éthique qui permettrait de remédier au scandale que représente, au Nord, le recrutement à deux vitesses en termes de statut et de rémunération. Il faut lutter contre un tel recrutement, appliqué dans bon nombre de nos pays, et en finir avec le principe du pays d’origine.
Pour conclure, nous espérons que ces généreuses déclarations d’intention seront réellement suivies d’effet, grâce à un engagement très ferme des institutions de l’Union européenne et du Parlement européen en particulier, et grâce aussi à la campagne initiée par les ONG et la société civile européenne. 
