Marine Le Pen # Débats - Jeudi 8 septembre 2005 - Explications de vote
  Marine Le Pen (NI
),
par écrit
. 
- Dès 1957, l’Europe a mis en place des écoles particulières destinées à éduquer ensemble les enfants du personnel des Communautés européennes tout en acceptant, pour un nombre très limité, des enfants extérieurs. Quel était donc l’esprit de cette politique d’exception, appliquant un principe de préférence corporatiste, qui par ailleurs est condamné dans d’autres domaines par cette même Europe? Fais ce que je dis, mais surtout ne fais pas ce que je fais!
Ce système des écoles européennes promeut donc le concept de citoyenneté européenne qui est pour l’Europe ce que devrait être le concept de préférence nationale pour la France et les autres nations libres d’Europe. Mais, le développement de ces écoles est destiné, non pas à construire l’Europe des nations à laquelle nous aspirons, mais à favoriser l’intégration européenne; politique qui vient d’être condamnée par le peuple français le 29 mai 2005.
Ces écoles sont non seulement discriminatoires à l’égard des fonctionnaires non européens mais le sont aussi à l’égard de tous les enfants d’Europe. Elles créent aussi un système de privilèges exorbitants destiné pratiquement aux seuls fonctionnaires des institutions européennes pour reproduire un modèle européen unique et obligatoire: l’Europe fédérale par l’intégration. 
