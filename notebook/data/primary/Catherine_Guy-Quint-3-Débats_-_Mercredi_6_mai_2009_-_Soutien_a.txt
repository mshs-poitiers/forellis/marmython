Catherine Guy-Quint # Débats - Mercredi 6 mai 2009 - Soutien au développement rural par le Fonds européen agricole pour le développement rural (Feader) - Programme d’aide à la relance économique par l’octroi d’une assistance financière communautaire à des projets dans le domaine de l’énergie - Accord interinstitutionnel du 17 mai 2006 en ce qui concerne le cadre financier pluriannuel (modification) (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Catherine Guy-Quint  (PSE
). - 
 Madame la Présidente, en novembre 2008, la Commission a présenté un plan de relance qui n’était pas à la hauteur des enjeux, tant par son volume que par son contenu. Il faut constater, six mois après, que la mise en œuvre de ce plan de relance est quasi inexistante, et je voudrais avoir des nouvelles de ces 30 milliards d’euros de relance.
Que sont devenus les 15 milliards annoncés à travers les actions nouvelles et confiés à la BEI? Comment peut-on concilier un stimulus annoncé de 7 milliards à travers les Fonds structurels de cohésion et une annonce de sous-consommation de 10 milliards d’euros en 2009 sur les dépenses structurelles?
Enfin, en ce qui concerne les 5 milliards dont nous parlons aujourd’hui, je voudrais faire quatre constats. Malgré la pression du Parlement européen, le Conseil des ministres des finances n’a pas été en mesure de dégager les 5 milliards au titre de 2009, mais uniquement 2,6 milliards d’euros.
Nous n’avons aucune certitude quant à la capacité du Conseil de trouver les 2,4 milliards manquant au titre de 2010. Le Parlement est prêt à trouver toutes les solutions possibles d’une façon réglementaire. Mais il ne faut aucune remise en cause des autres priorités politiques. Cela ne sera pas autorisé par le Parlement. Nous ne pouvons pas accepter de redéploiement, c’est la ligne rouge à ne pas franchir.
Il sera difficile de trouver ces 2,4 milliards d’euros, parce que, avec la présentation de l’APB de la Commission, nous savons que, au grand maximum, 1,7 milliard d’euros seront disponibles. Et encore faudra-t-il que le Conseil accepte de dégager ces marges! Il faut donc, dans tous les cas, au nom d’une orthodoxie budgétaire de courte vue et d’une vision notariale de règlement budgétaire, que les nombreux États ne soient plus en capacité d’arrêter tout ce plan de relance.
Il faut conserver un budget fort pour l’avenir de l’Union, et nous voyons, en quatrième constat, que la masse et la façon dont été négociées et acceptées les dernières perspectives financières pénalisent fortement l’avenir européen.
