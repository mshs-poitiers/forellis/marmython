Gérard Onesta # Débats - Jeudi 18 janvier 2001 - VOTES
  Deprez (PPE-DE
). 
 - Pardonnez-moi, Monsieur le Président, mais dans ce cas la règle aurait dû être la même ce matin pour le rapport de M. Ferri. Le rapport de M. Ferri a été reporté ce matin alors même que cela n'aurait pas pu se faire, si je suis votre interprétation du règlement. En conséquence, puisqu'on a pu le faire pour M. Ferri, je ne vois pas pourquoi on ne pourrait pas le faire au moment où je le demande pour la bonne organisation des travaux de cet après-midi. Je ne demande pas un privilège, je demande simplement l'application de la procédure qui a eu lieu ce matin. 
