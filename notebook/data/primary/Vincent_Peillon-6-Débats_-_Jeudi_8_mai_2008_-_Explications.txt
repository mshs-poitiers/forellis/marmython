Vincent Peillon # Débats - Jeudi 8 mai 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
− (IT) Rapport: Ingo Friedrich (ex Alexander Stubb) (A6-0105/2008
) 
  Vincent Peillon  (PSE
), 
par écrit
. – 
(FR) 
J'ai voté en faveur de ce rapport proposant de mieux encadrer les activités des représentants d'intérêt auprès des institutions de l'UE.
À Bruxelles, les "lobbyistes" sont en effet légion: on estime généralement leur nombre à plus de 15 000, pour 2 500 groupes de pression. Ayant pour objectif principal d'influencer les décisions communautaires, leur activité - n'inspirant pourtant à nombre de concitoyens que méfiance et suspicion - participe pleinement de la vie démocratique. Un député, par exemple, comprendra toujours mieux les enjeux d'un projet législatif après avoir écouté l'avis des associations professionnelles, des ONG, des syndicats ou des régions.
Toutefois, le lobbying ne peut être cet outil au service de la démocratie que s'il s'effectue dans la transparence. Les députés, comme les citoyens, doivent pouvoir connaître avec précision l'identité de ces acteurs: Qui les financent? Quels intérêts défendent-ils réellement?
Contraignant les lobbyistes à s'inscrire à un registre public commun à l'ensemble des institutions communautaires et à y indiquer le détail de leurs financements, ce texte répond ainsi à cette exigence; et ce d'autant plus que ces interlocuteurs devront désormais respecter un code de conduite et encourront des sanctions en cas de manquement au respect des règles. 
