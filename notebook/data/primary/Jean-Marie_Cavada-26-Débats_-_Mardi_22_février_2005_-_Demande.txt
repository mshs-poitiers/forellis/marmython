Jean-Marie Cavada # Débats - Mardi 22 février 2005 - Demande d’urgence
  Cavada (ALDE
). 
 - Monsieur le Président, en adoptant ce rapport, la commission des libertés civiles a retenu, bien sûr, la demande d’urgence du Conseil, mais par la même occasion, elle a aussi invité cette institution, par un amendement à la résolution législative et au considérant du règlement, à donner toutes ses chances à la diplomatie avant de procéder à l’adoption finale du texte. J’invite donc, Monsieur le Président, chers collègues, la plénière à suivre la suggestion de la commission que j’ai l’honneur de présider et qui a été soutenue également à l’unanimité par la commission du développement.
Du point de vue de la procédure, et afin d’éviter aussi des contestations de la part de tiers, je vous prie d’attirer l’attention du Conseil sur la nécessité de soumettre au Parlement, lors des consultations législatives, des textes complets au niveau des annexes également. 
