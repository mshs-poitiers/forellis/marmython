Georges Berthu # Débats - Jeudi 11 mars 2004 - Votes (suite) 
  Berthu (NI
),
par écrit
. 
- La résolution du Parlement européen sur le processus constitutionnel et la préparation du Conseil des 25-26 mars 2004 exerce le chantage habituel, que nous avons enregistré à chaque traité, consistant à clamer qu’une absence d’accord aurait des conséquences négatives pour l’intégration comme l’élargissement et entraînerait "une perte dévastatrice de solidarité et de légitimité".
Nous pensons au contraire que tout est préférable à un mauvais accord qui nous lierait les mains pour longtemps. Faut-il rappeler nos avertissements sur le traité d’Amsterdam, qui notamment transférait des compétences essentielles à la Communauté sur les questions d’immigration et augmentait les pouvoirs de la Commission? Faut-il rappeler nos avertissements sur le traité de Nice, qui supprimait le deuxième commissaire de la France et réduisait notre représentation au Parlement européen de 87 membres à 78 (et bientôt 72), alors que l’Allemagne reste à 99?
Tous ces avertissements, et bien d’autres, n’ont jamais été pris en compte par les gouvernements, et aujourd’hui tout le monde se désespère d’être enfermé dans des processus qui affaiblissent la France.
L’actuel projet de Constitution européenne est inopportun, comme je l’ai expliqué encore hier dans ma déclaration annexée au débat sur l’élargissement. Si nous l’adoptions, nous nous en repentirions vite. 
