Françoise Castex # Débats - Mercredi 25 octobre 2006 - Détachement des travailleurs (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Françoise Castex (PSE
). 
 - Monsieur le Président, Monsieur le Commissaire, mes chers collègues, nous devons prendre acte de l’existence réelle d’un marché du travail européen, et ce notamment dans le domaine des services qui, plus que les autres, implique le déplacement des salariés. Nous y sommes favorables, mais pas à n’importe quelles conditions et pas au risque du dumping social. Le travail humain n’est pas une marchandise et ne peut être soumis aux règles de la libre concurrence. Nous avons exprimé cette préoccupation lors du débat sur la directive «Services» et le Parlement a rejeté - vous l’avez rappelé - les articles 24 et 25 de cette directive qui traite précisément de l’assouplissement des conditions de détachement de travailleurs. Or, il n’est pas acceptable que la Commission remette en cause cette position et ignore les critiques émises par le Parlement.
La directive sur le détachement des travailleurs est mal appliquée dans les États membres et ne remplit pas ses objectifs. Soit, mais faut-il en conséquence assouplir la réglementation ou, au contraire, renforcer la volonté de l’appliquer? Je ne crois pas que les obligations de déclarations, de contrôles efficaces et la conservation des documents entraînent une surcharge bureaucratique inutile quand il s’agit de protéger les droits sociaux des salariés détachés et locaux. Le laxisme en la matière ne profite qu’aux fraudeurs. Par ailleurs, le renforcement de la contrainte et la menace même de sanctions pécuniaires doivent exercer un effet dissuasif.
C’est pourquoi je soutiens ce rapport et appelle de toute urgence la Commission et les États membres à régler les difficultés d’application de la directive dont nous ne demandons pas, à ce stade, la modification. 
