Robert Navarro # Débats - Mardi 10 juillet 2007 - Vers une politique maritime de l’Union: une vision européenne des océans et des mers (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Robert Navarro (PSE
). 
 - Madame la Présidente, je tiens tout d’abord à remercier Willy Piecyk pour l’impressionnant travail de synthèse qu’il a fourni. C’est au final un rapport complet, équilibré et qui prend bien en compte la dimension économique, environnementale et sociale de cette future politique maritime. En matière sociale, il souligne l’importance de la formation professionnelle et des perspectives de carrière, ainsi que l’impérieuse nécessité pour les États membres de l’Union de signer et de ratifier rapidement les conventions de l’OIT relatives au travail maritime. Une telle exigence est essentielle si l’on veut que le savoir-faire européen dans ce domaine ne disparaisse pas. Pour le reste la balle est désormais dans le camp de la Commission et des États membres.
Concernant ces derniers, la réforme du budget de l’Union à partir de 2008 devrait leur donner l’occasion de montrer qu’ils prennent ces enjeux au sérieux. Quant à la Commission, elle aura pour mission la coordination de cette politique maritime. Il s’agit d’un chantier immense où l’absence d’une ligne solide pourrait vite mener au chaos. C’est pourquoi je pense que l’idée d’un commissaire en charge de la politique maritime, qui dispose d’un mandat suffisant pour arbitrer quand cela s’impose, mérite d’être examinée. Bien sûr, solidité ne saurait signifier rigidité. Aussi, pour donner la souplesse nécessaire à la mise en œuvre des initiatives qui seront décidées dans le cadre de cette future politique maritime, je crois qu’il serait pertinent d’y associer fortement les régions. 
