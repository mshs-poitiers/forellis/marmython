Anne Laperrouze # Débats - Lundi 2 février 2009 - Conséquences de la crise récente dans le domaine du gaz - Deuxième analyse stratégique de la politique énergétique - Défi de l’efficacité énergétique et technologies de l’information et de la communication (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Anne Laperrouze, 
rapporteure
. −
 Monsieur le Président, Madame la Commissaire, Monsieur le Commissaire, chers collègues, merci pour l’extraordinaire richesse de vos contributions, qui montrent combien ce domaine de l’énergie est immense et que cette énergie est vraiment un besoin vital.
Dans nos débats et dans le rapport, qui reflètent d’ailleurs les discussions que nous avons eues entre collègues des différents groupes politiques, j’ai noté un large consensus sur la nécessité de renforcer les réseaux, les interconnexions, la nécessité d’utiliser les techniques d’information et de communication pour rendre les réseaux intelligents – ce que vient d’expliquer Mme la commissaire –, la nécessité de renforcer les relations avec les pays producteurs et les pays de transit – c’était notamment l’objet de la commission des affaires étrangères avec notre rapporteur, M. Dimitrakopoulos – et un accord également sur l’efficacité énergétique, les économies d’énergie, le développement des énergies renouvelables.
En conclusion, l’amélioration de l’efficacité énergétique, le développement des énergies renouvelables, la diversification de nos sources et voies d’approvisionnement, l’approfondissement du dialogue avec les pays producteurs, mais aussi le fait que les vingt-sept États parlent d’une seule voix et, surtout, un changement de notre vie, voilà le consensus que nous avons obtenu. Ces dimensions sont autant de voies nécessaires pour garantir cette sécurité énergétique commune que nous souhaitons tous.
Les divergences, bien sûr, portent sur la composition du bouquet énergétique. Quelles sont les sources d’énergie? J’ai envie de répondre à nos collègues du groupe des Verts et puis à d’autres collègues aussi qui se sont manifestés contre le nucléaire. Je voudrais dire qu’il faut quand même faire attention.
Il y a beaucoup d’exagération dans les propos. Je pense que nous avons fixé des objectifs très ambitieux pour 2050. On parle de 80 % de réduction des émissions de CO2, on parle de 60 % d’énergies renouvelables. On a bien vu qu’une large part était accordée à toutes les sources d’énergie renouvelables. En ce qui concerne le nucléaire, on reconnaît, dans ce rapport, qu’il fait partie du mix énergétique.
Là-dessus, je voudrais simplement, en conclusion, vous rappeler les objectifs, les 450 ppm de concentration, donc de CO2, qui sont visés pour garantir cette limite de + 2 degrés. Je voudrais vous rappeler que, dans ces efforts qui sont annoncés, on parle de 9 % de part du nucléaire, on parle de 54 % d’efficacité énergétique, de 35 % de renouvelables et de 14 % de captage et de stockage géologique du carbone.
Tout cela, c’est pour 2030. Donc, le nucléaire en fait partie et le charbon aussi. Moi, je ne suis pas une fan du charbon, je ne suis pas une fan du nucléaire, mais il faut avoir le plus large faisceau possible de sources énergétiques. Je ne voudrais pas avoir à choisir pour 2050 entre le charbon et le nucléaire. 
