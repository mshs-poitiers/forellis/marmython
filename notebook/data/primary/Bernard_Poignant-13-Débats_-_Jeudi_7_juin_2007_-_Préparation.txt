Bernard Poignant # Débats - Jeudi 7 juin 2007 - Préparation du Conseil européen (21-22 juin) et état de la révision des traités (débat) 
  Bernard Poignant (PSE
). 
 - Monsieur le Président, je salue l’arrivée du plan B, B comme Brok et B comme Barón. Cela dit, nous avions un traité compliqué, nous aurons un traité simplifié, mais il ne faudrait pas qu’il soit trop simpliste.
Je prends deux exemples: les symboles et la Charte. On lit que les symboles vont disparaître: l’hymne, le drapeau, la devise. Ce n’est pas une bonne nouvelle parce que les Français vont voir que le résultat de leur vote fait disparaître le drapeau au moment où le Président de la République française le met sur son portrait officiel. Et le symbole, ça compte. C’est un sentiment d’appartenance. Essayez à tout le moins de garder le drapeau.
Quant à la Charte, j’aimerais bien que mes compatriotes disposent de trois textes: la Déclaration des droits de l’homme et du citoyen du 26 août 1789, la Déclaration universelle des droits de l’homme du 10 décembre 1948 et la Charte des droits fondamentaux du 29 octobre 2004. C’est un ensemble, c’est un tout.
Il faut viser l’appartenance et non se disputer sur les questions de souveraineté ou de perte de souveraineté, et il ne faut pas qu’il y ait trop de régression, même s’il y en aura un peu. Concernant les Français, pensez à ceux qui ont dit non et qui vont être déçus, mais pensez aussi à ceux qui ont dit oui et qui pourraient être frustrés.
(Applaudissements)

