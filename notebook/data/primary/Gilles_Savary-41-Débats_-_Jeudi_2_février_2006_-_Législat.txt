Gilles Savary # Débats - Jeudi 2 février 2006 - Législation sociale relative aux activités de transport routier - Harmonisation de dispositions en matière sociale dans le domaine des transports par route
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Gilles Savary (PSE
). 
 - Monsieur le Président, la procédure progresse beaucoup plus vite pour les textes concernant la concurrence que pour ceux concernant les normes sociales, nous en avons l’habitude. Le texte dont nous sommes aujourd’hui saisis est engagé depuis 2001. C’est donc l’aboutissement d’une longue procédure législative qui s’est terminée par une conciliation difficile et improbable tellement il y avait de résistance au niveau des États membres.
Je voterai en faveur de ce texte qui met en place des normes plancher dont il faut préciser qu’elles constituent une avancée considérable puisque, dans certains États membres, les travailleurs du transport routier conduisaient jusqu’à 70 heures, 74 heures et que la durée sera désormais limitée à 56 heures; puisque c’est un texte de convergence sociale qui n’empêche pas de faire mieux dans chacun des États membres; et puisque c’est un texte qui a vocation, je l’espère, à s’améliorer.
Néanmoins, je regrette beaucoup ce qu’a dit M. Jarzembowski à propos de M. Piecyk. Il a voulu traiter cela d’une façon idéologique très déplaisante. Je le regrette parce que M. Piecyk a mis le doigt sur un problème: l’absence de contrôle simultané du temps de conduite et du temps de travail. Ainsi, on pourra faire rouler en continu un poids lourd avec trois chauffeurs sous-payés: l’un conduit, le deuxième prend son temps de pause sur le siège d’à côté dans la cabine et le troisième prend son temps de repos hebdomadaire sur la couchette arrière, derrière le chauffeur. C’est cela la réalité des choses et je crois que nous sommes passés à côté d’un grand texte social. Je le regrette un peu, mais je voterai quand même positivement. 
