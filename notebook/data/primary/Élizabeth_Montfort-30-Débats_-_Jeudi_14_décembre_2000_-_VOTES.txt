Élizabeth Montfort # Débats - Jeudi 14 décembre 2000 - VOTES
  Montfort (UEN
),
par écrit
. - Contribuer à la mise en place d'un programme de développement du contenu numérique européen sur Internet est effectivement primordial et les priorités retenues ne prêtent pas le flanc à la critique.
Mais ce que le texte a intégré au motif de la nécessaire adaptation linguistique et culturelle est quelquefois consternant : l'encouragement à mettre au point des programmes dans les langues régionales et des "minorités ethniques" représentées au sein de l'Union, est une décision dont les conséquences prévisibles sont l'effacement progressif des identités nationales.
En matière de numérique, le véhicule linguistique induit tout à la fois un avantage concurrentiel, un style de raisonnement, et une incidence juridique favorables à ceux dont il est la langue maternelle. Dans ces conditions, ce sont notamment leur cohérence et leur unité qui ont assuré aux États-Unis une prééminence sur ce marché d'avenir.
Or, qu'allons-nous faire pour contrer cette hégémonie américaine ? Nous allons "balkaniser" nos produits culturels !
En face de lui, la "tunique bleue" ne trouvera qu'une kyrielle de tribus indiennes repliées sur elles-mêmes, qui s'en remettront pour communiquer avec l'extérieur au seul dénominateur commun : la voix du maître, dont le totalitarisme insidieux donne l'illusion de s'être libéré de la tutelle la plus immédiate. On dispose déjà d'exemples parlants dans ce Parlement d'élus séparatistes qui préfèrent s'exprimer en anglais plutôt que dans la langue officielle de leur État membre.
Passe encore sur les difficultés techniques à venir lorsqu'il faudra, pour respecter le texte, trouver, par exemple, un système pour passer du kurde au breton... Mais le plus grave est que ces mesures s'attaquent à l'unité linguistique, et donc à l'unité tout court, des États membres : la citoyenneté, dans presque toutes les nations d'Europe, repose sur plusieurs piliers dont l'acceptation d'une langue officielle dans une même communauté nationale n'est pas le moindre, et certains États membres, qui pratiquent quotidiennement la partition linguistique, savent qu'elle tend naturellement vers la partition politique et la désagrégation du sentiment national.
(Intervention écourtée en application de l'article 137, paragraphe 1, du règlement)

