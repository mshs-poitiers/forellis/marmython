Margie Sudre # Débats - Mercredi 11 mars 2009 - Préparation du Conseil européen (19-20 mars 2009) - Plan européen de relance économique - Lignes directrices pour les politiques de l’emploi des États membres - Politique de cohésion: investir dans l’économie réelle (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Margie Sudre  (PPE-DE
), 
par écrit. 
– La politique régionale est la première source d’investissements européens dans l’économie réelle. Accélérer et simplifier son financement peut contribuer à la relance économique, grâce à un afflux de liquidités dans des secteurs ciblés.
Les paiements plus rapides et plus souples, forfaitaires et en un seul versement, que propose la Commission permettront la mise en œuvre sans délai de projets dans les domaines de l’infrastructure, de l’énergie ou encore de l’environnement.
Les autorités nationales et régionales doivent exploiter ces opportunités et faire un usage intensif des fonds structurels pour promouvoir l’emploi, les PME, l’esprit d’entreprise et la formation professionnelle tout en garantissant leur contribution, comme l’exigent les règles du cofinancement, afin que les fonds alloués puissent être pleinement consommés.
J’appelle les conseils régionaux et les préfectures des départements d’outre-mer (DOM), autorités de gestion des fonds structurels, à anticiper ces adaptations pour que leurs programmes régionaux se concentrent immédiatement sur les projets présentant le potentiel de croissance et d’emplois le plus important.
Face au malaise actuel des DOM, alors que le mouvement social touche dorénavant la Réunion, nous devons explorer de nouvelles pistes de développement endogène et actionner tous les leviers à notre disposition, y compris ceux que l’Union européenne nous apporte. 
