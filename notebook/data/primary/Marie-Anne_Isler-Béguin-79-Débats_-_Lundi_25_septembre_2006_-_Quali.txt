Marie-Anne Isler-Béguin # Débats - Lundi 25 septembre 2006 - Qualité de l’air ambiant et un air pur pour l’Europe - Stratégie thématique sur la pollution atmosphérique (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie Anne Isler Béguin (Verts/ALE
). 
 - Monsieur le Président, chers collègues, si nous voulons lutter efficacement contre la pollution de l’air et obtenir des résultats dans le domaine de la santé, ce sont des projets bien plus ambitieux que nous devons adopter ici. La mauvaise excuse des difficultés que certains États membres rencontrent pour mettre en œuvre la législation environnementale n’est plus acceptable, car la santé n’est plus à négocier.
Le coût des maladies dues à la pollution de l’air s’alourdit de jour en jour. Or, actuellement, c’est la collectivité qui paye la facture sanitaire, car le coût de la santé n’est toujours pas intégré dans nos calculs économiques. Du reste, des économistes estiment qu’il faudrait porter la prise du gallon de pétrole, qui est actuellement de trois dollars, à dix dollars pour intégrer le coût sanitaire et celui du changement climatique. Voilà qui changerait profondément la donne.
L’Union ne doit pas s’affaiblir, Monsieur le Commissaire, en matière de politique environnementale, ce qu’elle aurait tendance à faire par le jeu insidieux de la simplification et de la concentration des textes juridiques européens. C’est pourquoi les Verts demandent des approches plus contraignantes, notamment pour les PM 2.5, ainsi que la suppression des dérogations.
La catastrophe écologique qui nous menace, chers collègues, exige des mesures drastiques et l’exemple nous vient pour une fois d’Outre-Atlantique, où l’État de Californie incite à la réflexion en portant plainte contre les plus gros constructeurs automobiles pour atteinte à l’environnement et dommage à la santé publique.
Enfin quelques mots à l’intention de notre cher ami Jonas. Je crois que ton éclairage à la commission de l’environnement va nous manquer, mais nous te souhaitons bon vent de l’autre côté de la frontière, de l’autre côté de l’Union européenne. Porte là-bas aussi le message de la protection de la nature. Merci à toi. 
