Pervenche Berès # Débats - Mercredi 14 novembre 2007 - L'intérêt européen: réussir le défi de la mondialisation (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Pervenche Berès  (PSE
). - 
(FR)
 Madame la Présidente, Monsieur le Président de la Commission, Monsieur le Président du Conseil, le Président Barroso, tout à l'heure, nous a indiqué que l'Union européenne était sans doute le meilleur espace pour inventer une régulation à l'échelle mondiale. Il a raison. Mais pour cela, nous devons aussi faire notre travail à la maison. Dans les outils dont nous disposons à l'intérieur de l'Union européenne pour faire face à ces défis, il y en a qui s'appellent les lignes directrices, dans le domaine de la politique économique et dans le domaine de l'emploi. Je crains qu'aujourd'hui, la Commission essaie de noyer sous la globalisation ces lignes directrices, dont nous avons besoin. Elles sont utiles et nous devons les réviser.
Nous devons d'abord le faire parce qu'au Conseil européen de mars dernier, les chefs d'État et de gouvernement se sont dotés de la meilleure stratégie possible pour l'Union européenne pour faire face à la globalisation, répondre aux défis de l'énergie et du changement climatique. Si, pour satisfaire cette stratégie, nous n'utilisons pas, au sein de l'Union européenne, l'ensemble des moyens qui sont à notre disposition, y compris les lignes directrices, et peut-être d'abord les lignes directrices, nous n'irons nulle part et nous fabriquerons de la désillusion vis-à-vis de la capacité de l'Union européenne à faire face aux enjeux de la globalisation.
Nous devons aussi le faire parce que le commissaire Almunia lui-même reconnaît que la question des taux de change, la question du prix du pétrole, la question de l'impact réel sur l'économie de l'Union européenne de la crise des subprime
 aura un effet qui l'a conduit à réviser les perspectives de croissance dans l'Union européenne. On passe de 2,9 à 2,4 % pour l'Union européenne, de 2,6 à 2,2 % pour la zone euro.
Nous devons le faire aussi parce que nous devons répondre aux aspirations des peuples et, contrairement à ce que pense M. Nicolas Sarkozy, la question de l'Europe sociale est un vrai sujet et vous devez la traiter si vous ne voulez pas être désavoués demain par les citoyens européens.
Nous devons le faire enfin parce que le commissaire Almunia, pour la première fois, reconnaît qu'aujourd'hui, dans le contexte international où nous sommes, le moteur et le seul moteur de la croissance européenne sera d'abord la consommation intérieure.
Peut-on imaginer que, dans ce contexte où tout change, la seule chose qui ne changerait pas, ce serait les lignes directrices, peut-on concevoir que la seule chose dont l'Union dispose pour pouvoir vraiment orienter les politiques économiques et sociales des États membres ne changerait pas?
Monsieur le représentant de la Commission, Monsieur le Vice-président, dites au Président Barroso qu'il faut changer les lignes directrices, qu'il faut tenir compte du contexte nouveau pour que l'Union européenne se dote des outils, en interne, qui sont à sa disposition pour faire face le mieux possible aux enjeux de la globalisation. 
