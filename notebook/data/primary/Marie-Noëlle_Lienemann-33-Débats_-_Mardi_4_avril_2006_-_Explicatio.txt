Marie-Noëlle Lienemann # Débats - Mardi 4 avril 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport García-Margallo y Marfil (A6-0077/2006
) 
  Marie-Noëlle Lienemann (PSE
), 
par écrit
. - Je n’ai pas approuvé les grandes orientations des politiques économiques qui ne font qu’avaliser les orientations libérales de l’UE depuis plusieurs années et sont la cause de la faiblesse de la croissance, du chômage, de la précarisation des salariés, du recul de nos protections sociales.
Le rapport ne soutient aucune des orientations permettant une politique alternative: soutien du pouvoir d’achat, revalorisation des salaires pour relancer la consommation populaire et la croissance; soutien à d’ambitieux investissements publics pour la modernisation et l’emploi; harmonisation de la fiscalité et des standards sociaux vers le haut pour combattre le dumping; création d’un véritable gouvernement économique capable de peser face à la BCE. 
