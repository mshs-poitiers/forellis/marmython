Pervenche Berès # Débats - Mercredi 12 juin 2002 - Élargissement de l'Union
  Berès (PSE
), 
rapporteur pour avis de la commission économique et monétaire
. - Madame la Présidente, j'évoquerai, en une minute, cinq points pour rappeler la position de votre commission économique et monétaire. D'abord, notre préoccupation quant au rapport d'évaluation que nous avons demandé à la Commission sur l'état et les besoins des pays candidats en matière de services d'intérêt général. Ensuite, l'idée que, s'agissant des pays candidats, nous devons observer leur capacité économique avec une certaine flexibilité, en tenant compte de la nécessité d'y créer les conditions d'une croissance économique à la fois soutenue et durable. Puis, notre préoccupation, encore, quant à la lutte contre le blanchiment des capitaux et les paradis fiscaux, laquelle constitue selon nous un véritable préalable à l'adhésion, que d'autres orateurs ont mentionné avant moi.
Par ailleurs, la nécessité d'inciter ces pays à davantage de coopération régionale, et enfin l'idée que l'adhésion de ces pays à l'Union leur permettra plus facilement en soi de réaliser les objectifs poursuivis par l'Union européenne. 
