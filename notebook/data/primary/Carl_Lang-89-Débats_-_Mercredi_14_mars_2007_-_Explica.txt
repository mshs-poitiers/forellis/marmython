Carl Lang # Débats - Mercredi 14 mars 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Rapport (A6-0057/2007

) de M. Hasse Ferreira
  Carl Lang (ITS
), 
par écrit
. - M. Ferreira a raison quand il affirme que les services sociaux d’intérêt général constituent une composante essentielle du modèle social européen, fondé sur la protection des personnes les plus vulnérables: les enfants, les personnes âgées, les malades, les handicapés, les chômeurs...
Trois phénomènes remettent aujourd’hui en cause ce modèle social. Un, l’immigration incontrôlée qui est en train de ruiner nos systèmes de protection sociale. Ainsi, en France, l’attribution aux immigrés clandestins de l’aide médicale d’État gratuite coûte chaque année 600 millions d’euros. Deux, la destruction des frontières qui, en livrant nos entreprises à la concurrence internationale et au dumping social, notamment en Chine, détruit le soubassement économique de ce modèle social. Trois, la suppression progressive des services publics décidée en 2000 lors du sommet européen de Lisbonne.
Le rapport de notre collègue Ferreira ne propose aucune solution car ces phénomènes sont le résultat de l’idéologie ultralibérale mise en œuvre par l’Europe de Bruxelles.
Seule une autre Europe, l’Europe des patries, fondée sur le respect des souverainetés nationales, sur la préférence communautaire et sur des frontières la protégeant d’une immigration sans frein et d’une concurrence internationale déloyale, permettra à nos nations de reconstruire un modèle social européen. 
