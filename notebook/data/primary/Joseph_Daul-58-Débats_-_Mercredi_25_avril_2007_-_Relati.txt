Joseph Daul # Débats - Mercredi 25 avril 2007 - Relations transatlantiques (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Joseph Daul, 
au nom du groupe PPE-DE
. - Monsieur le Président, Monsieur le Commissaire, Monsieur le Ministre et représentant de la Présidence en exercice du Conseil, chers collègues, né il y a soixante ans en Alsace, je fais partie d’une génération qui peut témoigner, pour l’avoir vécu, à quel point les Européens sont redevables aux Américains.
Les liens transatlantiques très forts qui unissent nos deux continents reposent sur des millions d’histoires personnelles comparables à la mienne. Elles ont contribué à façonner notre histoire et nos valeurs communes.
Lors de la célébration récente du cinquantième anniversaire des traités de Rome, le groupe PPE-DE a salué le rôle clé de l’Amérique dans la mise en place de ce qui allait devenir l’Union européenne. Car, sans le soutien du plan Marshall, sans le rôle décisif joué par les États-Unis et le Canada dans le cadre de l’OTAN, la reconstruction de l’Europe n’aurait sans doute pas été possible. Même dans les périodes difficiles, nous avons toujours cru à l’importance vitale du partenariat transatlantique, un partenariat fondé sur le dialogue et le respect.
Au sein du Parlement européen, notre groupe est le partisan le plus déterminé de relations transatlantiques étroites. C’est pourquoi j’ai souhaité que le premier déplacement hors de l’Union soit pour Washington. Le Parlement européen doit développer des liens plus forts avec le Congrès et l’Administration des États-Unis afin de coopérer plus en amont sur les sujets d’intérêt commun. Et je voudrais proposer, ici, que le Président du Parlement invite la nouvelle présidente de la Chambre des représentants à intervenir en session plénière.
J’ai été heureux d’apprendre que, comme nous, le Congrès américain a constitué une commission temporaire sur le climat. Je souhaite que ces deux commissions puissent coopérer étroitement.
Chers collègues, la création d’un marché commun transatlantique d’ici à 2015 est l’une de nos priorités. Nous devons réduire le fardeau des réglementations, encourager la concurrence et harmoniser les normes techniques des deux côtés de l’Atlantique. Dotons-nous d’une feuille de route contraignante, assortie d’un calendrier précis avec 2015 comme date butoir pour le lancement d’un marché transatlantique sans barrières.
Le Parlement européen doit être largement associé à ce processus, mais entre amis, nous avons aussi le devoir de nous parler en toute franchise, voire d’exprimer des critiques.
Comme l’a observé le Président Kennedy en 1963, ne méconnaissons pas nos différences, mais intéressons-nous aussi aux moyens de résoudre nos différends. Je souhaite également souligner ma préoccupation quant aux risques que des contrôles douaniers américains plus stricts ne se transforment en barrières de commerce déguisées.
Nous devons maintenir notre vigilance sans porter atteinte à la loyauté des échanges commerciaux. De la même façon, la législation américaine sur la protection des données personnelles laisse encore planer des doutes sur le respect total de la protection de la vie privée et des libertés civiles.
L’Europe est déterminée à lutter contre le terrorisme et le crime organisé, mais ce combat doit être fondé sur les bases juridiques appropriées. Le respect des droits fondamentaux ne fera que renforcer notre action et notre influence dans le monde.
Nous partageons aussi l’engagement visant à créer les conditions de la stabilité, de la paix, de la prospérité dans le voisinage de l’Union européenne. Nous avons déjà coopéré de façon positive en Biélorussie, en Ukraine et au Kosovo. Mais nous devons aussi agir en Afrique. C’est un devoir moral et historique que de redonner de l’espoir aux plus pauvres de la planète.
Le génocide du Darfour ou la tyrannie au Zimbabwe montre que nous ne sommes pas à la hauteur des enjeux. Nous devons aussi persuader d’autres nations telles que la Chine, l’Inde, le Brésil ou l’Afrique du Sud d’accompagner nos efforts dans les pays en développement.
Par ailleurs, il faut faire aboutir l’accord de Doha parce qu’il est le cycle du développement pour les pays les plus pauvres. L’Europe et les États-Unis doivent garantir un accord global au plus vite.
Enfin, notre groupe croit aux chances d’un monde plus sûr. La prolifération nucléaire a rendu le monde plus dangereux. Nous appuyons une solution négociée sur le programme nucléaire iranien. Européens et Américains partagent des racines communes qui ont largement façonné notre monde. Nous devons tenir notre rang dans un monde devenu multipolaire, et comme l’a dit Jean Monnet, ensemble, les Américains et les Européens défendent une civilisation commune.
(Applaudissements)

