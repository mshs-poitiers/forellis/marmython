Marie-Anne Isler-Béguin # Débats - Mardi 13 juin 2006 - Heure des questions (questions à la Commission) 
  Marie Anne Isler Béguin (Verts/ALE
). 
 - Monsieur le Commissaire, étant donné que vous allez discuter avec nos homologues russes, je souhaiterais poser une question complémentaire et qui va dans le même sens, à propos cette fois de la Géorgie.
Vous savez que la Russie a instauré un embargo identique à l’égard du vin géorgien. Étant donné que la Géorgie est couverte par l’initiative «nouveau voisinage» et qu’un plan d’action a été mis en place en sa faveur, êtes-vous au fait de cette question et, dans la négative, pouvez-vous l’examiner en vue de savoir quelle réponse la Russie entend donner à la Géorgie? 
