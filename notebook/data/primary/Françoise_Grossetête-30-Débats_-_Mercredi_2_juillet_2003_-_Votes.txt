Françoise Grossetête # Débats - Mercredi 2 juillet 2003 - Votes
  Grossetête (PPE-DE
),
par écrit
. - J’ai voté en faveur de ce texte.
Je ne suis pas particulièrement favorable à ce système mondial de vente de "droits à polluer". En effet, des expériences concrètes que j’ai pu mener avec les industriels de mon secteur montrent que des efforts conséquents peuvent être entrepris par ces acteurs économiques pour réduire l’émission de gaz à effet de serre.
Pour autant, j’ai souhaité apporter des modifications à la proposition initiale présentée par la Commission européenne.
En effet, il paraissait très important d’étendre le champ d’application de la directive aux secteurs des produits chimiques et de l’aluminium. Par le biais de mes amendements, le Parlement a rejoint ma position. Aussi, je regrette vivement que, sur ce point, le Conseil des ministres n’ait pas accepté la position adoptée par mes collègues, et retarde ainsi l’inclusion des secteurs de la chimie et de l’aluminium. 
