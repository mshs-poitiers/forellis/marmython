Carl Lang # Débats - Jeudi 15 janvier 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Véronique Mathieu (A6-0488/2008
) 
  Carl Lang  (NI
), 
par écrit
. – 
Le rapport sur le contrôle budgétaire des fonds de l’Union Européenne en Afghanistan, de par les constats qui y sont faits, démontre une fois de plus que l’interventionnisme occidental dans ces régions n’aura rien changé. Notre présence ne fait que prolonger la guerre et les souffrances qui en résultent plutôt que d’y remédier.
Parler de démocratie et d’égalité des sexes dans un pays où les coutumes confinent parfois à la barbarie, résume toute l’illusion béate des européistes préférant toujours s’occuper de l’international plutôt que de régler les problèmes de l’Europe.
Les ethnies composant l’Afghanistan, terre belliqueuse depuis des siècles, n’accepteront jamais aucune occupation étrangère, fût-elle «humanitaire». Elle ne fait que renforcer les positions des talibans ou d’autres factions extrémistes plutôt que de permettre l’émergence d’un pouvoir légitime fort et capable de stabiliser le pays.
Les Européens doivent se retirer au plus vite du guêpier afghan. 
