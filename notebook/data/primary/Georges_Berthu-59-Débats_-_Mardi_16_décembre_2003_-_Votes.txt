Georges Berthu # Débats - Mardi 16 décembre 2003 - Votes
  Berthu (NI
),
par écrit
. 
- Le vote de la décharge sur le budget de la Convention est l’occasion de rappeler que, malgré les louanges que lui tressent les fédéralistes, cette instance est en grande partie responsable du blocage final du Conseil de Bruxelles sur la Constitution européenne.
Tout d’abord, sa composition ne reflétait pas du tout l’opinion publique européenne, puisque les souverainistes n’étaient pratiquement pas représentés. Gisela Stuart, représentante britannique qui pourtant faisait partie de son Praesidium, a pu dire récemment qu’il s’agissait d’une "élite autosélectionnée".
Ses conclusions n’ont pas du tout été inspirées par un consensus. Au contraire, elles ont été téléguidées par les institutions européennes. Les pays ou les individualités qui n’étaient pas d’accord se trouvaient marginalisés, en application de la formule de Valéry Giscard d’Estaing: "Le consensus, c’est moins que l’unanimité, mais plus que la majorité".
Dans cette enceinte, il s’est créé un microclimat, une sorte d’euro-enthousiasme communicatif, piloté par les fédéralistes, qui, pour beaucoup de membres, a fait perdre de vue les positions et les intérêts nationaux, qui se sont vengés ensuite.
Enfin, l’ambition de réécrire complètement les traités était démesurée, de sorte qu’il apparaissait au dernier moment que bon nombre de problèmes avaient été techniquement bâclés. 
