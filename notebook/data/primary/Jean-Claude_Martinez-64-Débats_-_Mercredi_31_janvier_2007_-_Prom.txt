Jean-Claude Martinez # Débats - Mercredi 31 janvier 2007 - Promouvoir une alimentation saine et l’activité physique: une dimension européenne pour la prévention des surcharges pondérales, de l’obésité et des maladies chroniques (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Claude Martinez, 
au nom du groupe ITS
. - Monsieur le Président, Monsieur le Commissaire, l’Europe compte près de 100 millions de gros, à la différence du Darfour qui n’a que des maigres. Bien sûr, il y a 800 millions d’enfants qui peuvent mourir de faim dans le monde, mais nous, nous avons 14 millions d’enfants obèses et ça va en augmentant.
Alors, que faire, non pas pour leur éviter de ressembler aux personnages de Botero, mais pour économiser chaque année 7 % de nos dépenses de santé?
Il y aurait d’abord les régimes alimentaires. Outre le régime Biafra, trop dangereux; il y a le régime crétois, mais il faut de l’huile d’olive, des fruits, des légumes, des viandes blanches et, malheureusement, la refonte de la PAC détruit nos oliviers, nos maraîchers, nos arboriculteurs, nos éleveurs de poulets.
Il y a aussi les régimes financiers, qui sont très efficaces. L’obésité, en effet, diminue au fur et à mesure que l’on monte dans l’échelle des revenus. Au sommet, les riches sont tous maigres. Il faut donc enrichir les pauvres pour les faire maigrir. Mais pour cela, il faudrait revenir sur le pacte d’austérité de Maastricht, qui fabrique des exclus et donc des gros.
Alors moi, je n’ai qu’une seule espérance, que le réchauffement climatique continue, ainsi les petits Européens iraient à l’école au milieu des dunes, sur leurs petits chameaux, feraient de l’exercice. Autre souhait: j’espère qu’aucun de ces petits enfants, qui ont les yeux enfoncés au fond des orbites et le ventre gonflé par la faim, j’espère qu’aucun de ces gosses ne lira le rapport de Mme Ries, parce qu’il y a deux façons de vomir: de trop manger ou de lire un rapport scandaleux! 
