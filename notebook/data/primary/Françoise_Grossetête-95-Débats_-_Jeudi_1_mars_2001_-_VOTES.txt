Françoise Grossetête # Débats - Jeudi 1 mars 2001 - VOTES
  Grossetête (PPE-DE
),
par écrit
. - J'ai voté en faveur de cette proposition de résolution.
C'est un fait caractérisé : les institutions publiques et technocratiques ont été défaillantes dans la gestion de la crise de l'ESB.
Or, les élus, ceux qui rencontrent les agriculteurs sur le terrain, connaissent les attentes de ces populations et les difficultés auxquelles elles sont confrontées.
C'est pour cela que le Parlement européen doit faire entendre sa voix en relayant auprès des institutions les souhaits et les propositions du secteur agricole.
Aussi, j'espère que le Parlement européen pourra, dans l'avenir, intervenir avec davantage de pouvoir, et le transfert des dépenses de marché vers la procédure de codécision serait, pour ce processus indispensable, une des étapes importantes.
Depuis des années, je me bats pour que soit mise en place une réforme complète de la politique agricole commune. Nous n'avons que trop attendu, et les crises que nous connaissons à l'heure actuelle mettent une nouvelle fois en avant les limites observées depuis des années par ce dispositif.
Jusqu'ici, la Commission s'est montrée particulièrement frileuse dans ses propositions, et je le déplore fortement.
En effet, il s'agit aujourd'hui d'aider les agriculteurs non pas à produire plus, mais à produire mieux. Les consommateurs ont perdu confiance, et celle-ci ne sera rétablie que par le développement d'une agriculture durable.
La PAC doit faire de la santé publique son objectif prioritaire. Cette attente est non seulement partagée par les consommateurs, mais également soutenue par l'ensemble des agriculteurs. 
