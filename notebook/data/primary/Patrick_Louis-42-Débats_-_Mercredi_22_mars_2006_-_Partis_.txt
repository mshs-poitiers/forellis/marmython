Patrick Louis # Débats - Mercredi 22 mars 2006 - Partis politiques européens (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Patrick Louis (IND/DEM
). 
 - Monsieur le Président, puisque le cadre national est le seul lieu naturel du débat politique et démocratique, des partis cohérents dotés d’un programme global n’ont de place qu’à l’intérieur des États. Le déficit démocratique de l’Union ne pourra être comblé qu’à travers la représentation de partis nationaux ayant leur identité propre, partis responsables devant des citoyens qui les connaissent et les comprennent.
Les partis politiques européens, quasi financés par l’Union, doivent pouvoir préserver leur indépendance et ne pas devenir un nième moyen de propagande et de communication européennes auprès des électeurs. Ainsi les partis politiques européens doivent rester de simples instruments de coopération entre partis politiques nationaux, un lieu d’échange ouvert dans le respect de chaque membre. En aucun cas, ils ne reflètent ou ne sont le vecteur d’une prétendue opinion publique européenne qui n’existe pas et qui n’existera pas, car la diversité des langues est un fait et l’Union est un moyen et non pas une fin en soi. 
