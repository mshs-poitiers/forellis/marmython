Lydia Schénardi # Débats - Lundi 12 février 2007 - Le rôle des femmes en Turquie dans la vie sociale, économique et politique (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Lydia Schenardi (ITS
). 
 - Monsieur le Président, chers collègues, ce rapport de ma collègue, Mme Bozkurt, a, au-delà de la clarté, le grand mérite de dresser le bilan de la triste réalité des droits des femmes en Turquie.
En effet, malgré toutes les déclarations d’intention des autorités turques et les pseudo-réformes entreprises en matière de droits de l’homme, la réalité est que les violences domestiques, les crimes d’honneur et les mariages forcés vont en augmentant, et que les discriminations à l’emploi et à l’enseignement pour les femmes perdurent.
Toutefois, ce constat ne doit pas faire oublier que, s’il est vrai que ces atteintes aux droits de la femme sont incompatibles avec la conception des droits de l’homme, tels que fixés notamment dans la charte des droits fondamentaux, il n’en reste pas moins que la Turquie n’est pas européenne et qu’elle n’a pas vocation à entrer dans l’Union européenne.
En effet, quand bien même il s’avérerait que tous les critères économiques, juridiques et sociaux définis au sommet de Copenhague sont remplis, la Turquie, composée de 99% de musulmans et dont 94% du territoire se situent en Asie, ne partagerait toujours pas, pour autant, nos valeurs, empreintes de christianisme et d’humanisme.
La Turquie est un beau pays, un grand pays, où vit un peuple courageux, fier et hospitalier. Il convient bien sûr de préserver les relations privilégiées que nous entretenons déjà avec elle dans le cadre de l’Union douanière, mais en aucun cas, elle ne peut ni ne doit adhérer à l’Union européenne. 
