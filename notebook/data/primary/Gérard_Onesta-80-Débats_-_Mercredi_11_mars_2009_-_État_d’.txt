Gérard Onesta # Débats - Mercredi 11 mars 2009 - État d’avancement du projet SIS II (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Le Président.  
– L’ordre du jour appelle la discussion commune sur:
-	la question orale au Conseil sur l’état d’avancement du projet SIS II de Carlos Coehlo, au nom du groupe PPE-DE, Martine Roure, au nom du groupe PSE, et Henrik Lax, au nom du groupe ALDE (O-0005/2009
 - B6-0010/2009
), et
-	la question orale à la Commission sur l’état d’avancement du projet SIS II de Carlos Coehlo, au nom du groupe PPE-DE, Martine Roure, au nom du groupe PSE, et Henrik Lax, au nom du groupe ALDE (O-0006/2009
 - B6-0011/2009
). 
