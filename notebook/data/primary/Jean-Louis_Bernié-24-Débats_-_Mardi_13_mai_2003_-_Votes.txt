Jean-Louis Bernié # Débats - Mardi 13 mai 2003 - Votes
  Bernié (EDD
),
par écrit
. - C’est avec un certain nombre de réserves que nous avons voté en faveur du rapport Sacconi sur les accords environnementaux conclus au niveau communautaire, qui prévoit un nouvel instrument pour simplifier et accélérer le travail législatif au niveau européen: les accords volontaires.
Ces accords, inscrits dans le cadre du Sixième programme d’action communautaire pour l’environnement, pourront prendre deux formes: l’autorégulation et la corégulation. Il ne nous paraît pas opportun de privilégier la corégulation, comme le préconise le rapporteur. Elle n’a pas une valeur supérieure à l’autorégulation. La Commission n’a pas à s’ériger en juge et partie des conditions préalables à la mise en place de ces accords.
De plus, les accords volontaires ne doivent pas être considérés comme des instruments complétant l’arsenal législatif communautaire actuel. Les accords volontaires sont des instruments souples ayant toute leur place au rayon des instruments législatifs. Nous nous félicitons que l’initiative de la Commission ait pris la forme d’une communication non contraignante. Nous souhaitons que la Commission poursuive sur cette voie. 
