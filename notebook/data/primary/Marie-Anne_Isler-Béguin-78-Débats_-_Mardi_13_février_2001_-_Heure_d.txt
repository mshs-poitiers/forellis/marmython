Marie-Anne Isler-Béguin # Débats - Mardi 13 février 2001 - Heure des questions (Commission) 
  Isler Béguin (Verts/ALE
). 
 - Monsieur le Président, Monsieur le Commissaire, hier à la réunion des commissions de l'environnement et des affaires étrangères, nous avons justement entendu M. Haavisto qui a fait son rapport, concernant les résultats des experts. Par contre, s'il a parlé de l'uranium appauvri, il n'a absolument pas parlé de l'annonce qu'a faite l'OTAN le 18 janvier, dans laquelle elle reconnaissait qu'en plus de l'uranium appauvri, du plutonium avait également été utilisé.
Envisagez-vous de faire des recherches de plutonium dans ces zones et comptez-vous mettre à disposition des fonds européens pour la décontamination ? 
