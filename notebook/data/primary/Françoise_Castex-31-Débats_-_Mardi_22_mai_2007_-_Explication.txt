Françoise Castex # Débats - Mardi 22 mai 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Caspary (A6-0149/2007
) 
  Françoise Castex (PSE
), 
 par écrit. - 
Ce rapport engage l’Union européenne et ses partenaires commerciaux dans la voie d’un libéralisme conquérant, à contre-courant des fondements de la politique commerciale européenne.
Je regrette que les parlementaires européens renoncent à ce qu’ils ont défendu jusque là: une politique commerciale qui consistait à subordonner les accords commerciaux à des exigences sociales et environnementales en respectant la souveraineté des pays en développement dans la gestion d’un certain nombre de secteurs vitaux pour leur développement (services publics, investissements, marchés publics et règles de concurrence). Ce vote a remplacé cette politique par une stratégie de large libéralisation des services et des investissements dans les pays en développement, qui répondra aux attentes des industriels européens au détriment des besoins économiques pour le développement. Le principe de libre-échange doit être un outil au service du développement et non un objectif en soi.
Je déplore que les sujets de Singapour, qui avaient été exclus des négociations multilatérales de Doha, aient été réintroduits par les eurodéputés dans ce rapport comme des priorités dans de futures négociations bilatérales. 
