Anne Ferreira # Débats - Mardi 5 mai 2009 - Préparation du Sommet de l’Emploi - Fonds européen d’ajustement à la mondialisation - Agenda social renouvelé - Inclusion active des personnes exclues du marché du travail (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Anne Ferreira, 
rapporteure pour avis de la commission de l’environnement, de la santé publique et de la sécurité alimentaire
. −
 Monsieur le Président, chers collègues, en tant que rapporteure pour avis, je remercie M. Peneda d’avoir intégré dans son rapport le fait que la Commission ne proposait pas de mesures concrètes pour maîtriser les conséquences sociales et sanitaires des crises écologique et climatique. Je le remercie aussi d’avoir mentionné l’économie sociale, bien que je regrette que ne soit pas rappelé son rôle en matière de cohésion et de création d’emplois de qualité et faiblement délocalisables.
À la veille des élections européennes, ce rapport serait plutôt le bienvenu si certaines formulations ne souffraient pas d’un manque d’ambition avéré. Pouvons-nous nous contenter de flexisécurité, de normes minimales en matière de droit du travail? Non. Et pourtant, nous pouvons craindre que demain, la droite rejette ces normes minimales comme elle refuse, depuis cinq ans, une directive sur les services d’intérêt général.
Approuverons-nous enfin demain la demande d’un salaire minimum? Les citoyens européens réclament depuis des années une Europe sociale forte. La prochaine Assemblée doit pouvoir concrétiser les différents progrès sociaux proposés par le rapport. Cela pourrait contribuer à mobiliser chacun le 7 juin. 
