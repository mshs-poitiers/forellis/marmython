Jean-Paul Gauzès # Débats - Lundi 22 septembre 2008 - Fonds alternatifs et fonds de capital-investissement - Transparence des investisseurs institutionnels (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Paul Gauzès  (PPE-DE
). 
 - Monsieur le Président, Monsieur le Commissaire, chers collègues, si les banques européennes résistent assez bien à la crise actuelle, c’est certainement parce que – même si elle est encore à parfaire – la supervision en Europe est sérieuse. Le travail en cours sur CRD et sur Solvency II fournit l’occasion de prendre des dispositions utiles d’amélioration de la sécurité financière. Cela étant, le métier de banquier est un métier à risque, mais ce risque doit être maîtrisé. Pour un banquier, l’important est de toujours connaître la contrepartie de chaque risque. Cette règle simple a été perdue de vue par certains intervenants non régulés. Pour eux, l’autorégulation n’est plus suffisante. Les marchés de produits dérivés sont devenus de plus en plus opaques, les intervenants opérationnels sur les marchés ont agi dans des conditions de risque qui n’étaient plus maîtrisées par les directions générales des établissements qui sont aujourd’hui les plus exposés.
Pour sortir de la crise, il faut construire un système de contrôle qui inspire et rétablisse la confiance. Aujourd’hui, des pans entiers de la finance échappent aux autorités de contrôle. Qui contrôlait les courtiers qui ont prêté à livre ouvert à des ménages insolvables? Qui contrôlait les banques d’investissement qui ont relayé la crise en transformant les mauvais crédits en actifs financiers pour les vendre à toute la planète? Ce vide réglementaire touche également les réhausseurs de crédits, les agences de notation et les hedge funds. L’Europe ne peut pas subir périodiquement les conséquences du système lacunaire financier américain.
Pour ce qui est des hedge funds, les gendarmes des marchés britannique et américain viennent d’interdire temporairement la spéculation à la baisse sur les valeurs financières, c’est une bonne chose! La déconfiture de certains de ces acteurs trop opaques accélérerait la crise du système financier dérégulé. Tous les fonds ne sont pas nuisibles, certains sont nécessaires, mais il faut éviter que ne perdurent des trous noirs financiers. Réfléchir est utile et indispensable mais aujourd’hui, il est plus que temps d’agir! C’est le sens des deux rapports dont nous débattons. 
