Jean Marie Beaupuy # Débats - Mercredi 17 décembre 2008 - Projet de budget général 2009, modifié par le Conseil (toutes sections) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean Marie Beaupuy  (ALDE
). 
 – Monsieur le Président, Madame la Commissaire, nombreux sont mes collègues qui viennent de rappeler, encore à l’instant, l’insuffisante consommation des budgets, notamment au niveau des Fonds structurels. Nous devons donc impérativement corriger cette situation en mettant en place des actions pertinentes et concrètes. Aussi, je me réjouis de voir inscrit dans ce budget un montant de 2 millions d’euros pour le projet pilote Erasmus des élus locaux et régionaux, projet que j’ai personnellement proposé il y a quelques mois.
En effet, cette inscription budgétaire fait suite aux propositions concrètes de mon rapport sur la gouvernance qui a été adopté en octobre dernier, et ce à une très large majorité de notre Assemblée.
En effet, pour mettre en œuvre avec efficacité nos politiques de développement régional, il ne suffit pas de voter des règlements et des budgets, il est essentiel que les élus porteurs des projets locaux et régionaux puissent devenir de véritables locomotives des objectifs de Lisbonne et de Göteborg, et cela grâce à leur savoir-faire. Avec cet Erasmus des élus locaux et régionaux, nous pourrons tout à la fois renforcer les liens humains, mais aussi et surtout, donner les moyens d’un usage plus rapide et plus efficace des Fonds structurels.
De nombreuses associations d’élus m’ont déjà fait connaître leur enthousiasme vis-à-vis de cet Erasmus des élus locaux. Aussi, avec l’appui de la DG REGIO, nous allons pouvoir lancer ce nouveau dispositif et ainsi appliquer la formule: «Think global, act local».

