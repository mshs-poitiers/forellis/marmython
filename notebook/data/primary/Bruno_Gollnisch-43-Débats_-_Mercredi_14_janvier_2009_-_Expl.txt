Bruno Gollnisch # Débats - Mercredi 14 janvier 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Alexander Graf Lambsdorff (A6-0415/2008
) 
  Bruno Gollnisch  (NI
), 
par écrit
. – 
Le renforcement de la compétitivité de l’industrie européenne de l’armement, supposée être pénalisée par des marchés européens trop étroits et trop fermés sur eux-mêmes, a servi de prétexte à cette directive sur l’ouverture à la concurrence des marchés publics dans ce secteur.
Certes, le texte qui nous est proposé aujourd’hui a tenu compte d’un certain nombre de problèmes soulevés par le texte initial de la Commission, comme son champ d’application, la non-application de l’accord de l’OMC sur les marchés publics, les seuils financiers ou la confidentialité.
Mais il répond à la logique de Bruxelles, selon laquelle aucun secteur, même stratégique et vital, ne doit échapper à sa supervision, à la libéralisation et aux privatisations. Il ne garantit pas le respect de la souveraineté des États membres, pourtant légalement seuls responsables de leur sécurité nationale. Il ne favorise pas l’existence de marchés d’envergure en Europe, où les budgets de défense des États se réduisent de manière drastique. Il n’instaure aucune préférence communautaire qui seule permettrait l’émergence naturelle d’un véritable marché européen. Il renforce la dichotomie civil/militaire, si spécifique à l’Europe, et qui nous a déjà tant coûté. Et surtout, il fait passer les considérations économiques et de marché avant toute autre. Ces graves défauts sur des points essentiels ont motivé notre opposition. 
