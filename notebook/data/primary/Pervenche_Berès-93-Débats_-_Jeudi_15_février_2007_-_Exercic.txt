Pervenche Berès # Débats - Jeudi 15 février 2007 - Exercice des droits de vote des actionnaires (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Pervenche Berès (PSE
). 
 - Avant que nous commencions ce débat, je voudrais dire ceci: je sais qu’il n’y a pas de règles qui définissent l’ordre d’intervention des collègues. Pour autant, je voudrais attirer l’attention de la plénière sur une situation un peu cocasse qui fait que le rapporteur s’est fait attribuer la totalité de son temps de parole pour répondre, en conclusion, à l’ensemble des orateurs qui se seront exprimés. Notre pratique, bien établie, veut que les orateurs intervenant au nom des groupes le fassent dans un ordre proportionnel au poids politique de leur groupe, et non en conclusion du débat, lorsqu’il s’agit d’un texte législatif.
La réponse appartient à la Commission, qui est ici très bien représentée, je m’étonne de la pratique suivie aujourd’hui: s’il s’agit d’une innovation, il faudra nous l’expliquer. 
