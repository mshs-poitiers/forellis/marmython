Lydia Schénardi # Débats - Mercredi 18 juin 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
– Rapport: Eluned Morgan (A6-0191/2008
) 
  Lydia Schenardi  (NI
), 
par écrit. – (FR) 
À l'évidence, le véritable objectif de ce troisième paquet énergie n'est pas la sécurité des approvisionnements, la qualité des services fournis, les prix abordables ou le réel choix de leur fournisseur par les consommateurs, mais bien le démantèlement définitif de ce qui reste des anciens monopoles publics de l'électricité.
L'acharnement de la Commission et de beaucoup de parlementaires à vouloir imposer la "séparation patrimoniale", c'est-à-dire la renonciation forcée de la propriété de leur réseau aux opérateurs "historiques" comme EDF, est inacceptable. Ce dont on les accuse - restriction d'accès au réseau pour leurs concurrents, limitation volontaire des investissements dans les infrastructures - n'est absolument pas prouvé. On ne voit pas non plus en quoi confier la gestion du réseau à un opérateur unique différent du fournisseur d'électricité peut garantir la pertinence et l'adéquation des investissements, le non-abus d'une position cruciale ou la meilleure gestion des congestions.
C’est pourquoi, bien que nous ayons toujours défendu ici les prérogatives exclusives des États en matière énergétique, domaine stratégique d’une trop grande importance pour être laissé aux eurocrates, nous soutiendrons comme un moindre mal la solution de "séparation effective" proposée entre autres par la France et l’Allemagne. Et nous voterons contre des textes dont nous désapprouvons fondamentalement la philosophie. 
