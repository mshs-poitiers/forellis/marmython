Catherine Guy-Quint # Débats - Mercredi 6 mai 2009 - Le rôle nouveau et les responsabilités nouvelles du Parlement en vertu du traité de Lisbonne - Incidence du traité de Lisbonne sur le développement de l’équilibre institutionnel de l’UE - Évolution des relations entre le Parlement européen et les parlements nationaux en vertu du traité de Lisbonne - Aspects financiers du traité de Lisbonne - Mise en œuvre de l’initiative citoyenne (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Catherine Guy-Quint, 
rapporteure
. − 
Monsieur le Président, ce débat a été tout à fait intéressant, passionnant. Mais permettez-moi de relever avec humour ce qu’a dit M. Kamall tout à l’heure, parce que nous traiter de vieux - donc de fossiles - au moment où nous prenons notre retraite, pour laisser la place aux jeunes, c’est absolument exquis.
Mais ce que je voulais dire à tous ceux qui ont vilipendé ce projet de traité, c’est: ne confondez pas démocratie et démagogie! Voyez-vous, depuis huit ans dans ce Parlement, dans l’Europe entière, nous vivons non pas un psychodrame, mais un drame politique où l’Europe s’est enlisée, et nous voyons bien que nous sommes en train de passer à côté des problématiques actuelles.
Ce débat me renforce dans la conviction que ce traité doit être mis en œuvre, malgré toutes les difficultés qui ont été soulevées, parce que, par son contenu, il va apporter de la transparence. Il va apporter de la démocratie, et ce choc démocratique, nous en avons tous besoin pour recentrer le projet européen sur de la politique, et de la politique du XXIe
 siècle s’inscrivant dans le monde d’aujourd’hui.
Le budget, à ce titre, n’est qu’un outil, mais il permettra de veiller au rééquilibrage des institutions et, par cette transparence, nous pourrons connaître l’attitude du Parlement, de la Commission et, surtout, du Conseil. Cette volonté politique est indispensable. Cette transparence est indispensable pour lutter contre le cancer de l’égoïsme national, qui ronge le projet politique européen depuis de si nombreuses années.
Cette transparence, j’espère qu’elle va redonner de la conviction à tous les citoyens européens, qu’elle va permettre que nous développions mieux l’information, parce que la chose est très difficile. Madame Wallström, cela fait des années que vous vous y employez, que vous commencez à percer, et il faudra persévérer.
Tout cela nécessite de la conviction, du temps, et surtout un courage politique qui fait défaut. Il faut que nous retrouvions le courage politique, et aussi le sens de l’utopie, de l’utopie des pères fondateurs de l’Union européenne, de ceux qui ont cru que de la guerre pouvait naître la paix. À notre façon, aujourd’hui, au XXIe
 siècle, nous devons relever ce défi, et un des outils qui nous y aidera est le traité de Lisbonne. Reprenons l’utopie, et l’utopie pour la paix!
