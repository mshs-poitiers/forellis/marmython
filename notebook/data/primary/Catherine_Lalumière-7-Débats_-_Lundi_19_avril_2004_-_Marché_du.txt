Catherine Lalumière # Débats - Lundi 19 avril 2004 - Marché du gaz
  La Présidente.  
- Merci, Madame la Commissaire. Vous avez bien voulu vous féliciter de l’excellente collaboration avec le Parlement européen et en particulier avec les membres de la commission de l’industrie, du commerce extérieur, de la recherche et de l’énergie. Je crois pouvoir, au nom des membres de cette commission et au nom de tout le Parlement européen, vous dire à quel point nous avons apprécié aussi de travailler avec vous dans le domaine de l’énergie, ainsi que dans d’autres. Les sentiments que vous éprouvez, je crois, sont éprouvés réciproquement par les membres de cette Assemblée.
Le débat est clos.
Le vote aura lieu demain à 12 heures. 
