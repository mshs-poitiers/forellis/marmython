Bernadette Vergnaud # Débats - Mercredi 22 octobre 2008 - Protection des consommateurs en ce qui concerne certains aspects de l’utilisation des biens à temps partagé (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bernadette Vergnaud  (PSE
). -  
Madame la Présidente, Madame la Commissaire, chers collègues, nous allons voter le dépoussiérage nécessaire d’une directive vieille de quatorze ans, qui concerne des millions de personnes en Europe - professionnels du tourisme comme consommateurs de vacances à temps partagé ou en club.
Ce secteur pesant plus de deux milliards d’euros par an et employant 200 000 personnes est un vecteur important du marché intérieur, dans le cadre de la stratégie de Lisbonne, d’autant plus que les prévisions annoncent une croissance rapide de ce type de services. La logique économique, certes non négligeable, ne doit pas empêcher de protéger au mieux les nombreux utilisateurs de ces services, qui disposent souvent de budgets vacances moyens et qui ont besoin de protection et de plus de clarté juridique.
L’harmonisation des conditions de rétractation tout comme l’interdiction de pratiques commerciales déloyales telles que le versement d’acomptes durant le délai de réflexion et l’obligation de fournir un contrat précis, clair et lisible dans la langue choisie par l’acheteur constituent ainsi un progrès important dans la protection et la responsabilisation des consommateurs. Ce texte va enfin permettre de mettre un terme à des pratiques inadmissibles rendues possibles par les insuffisances de la directive actuelle et va redonner crédit à un secteur plombé par une image négative. On peut donc espérer, d’une part, un regain de dynamisme positif pour des prestataires de services débarrassés de concurrents malhonnêtes et, d’autre part, une confiance retrouvée chez des consommateurs rassurés.
Je tiens donc à féliciter le rapporteur, M .Manders, et les rapporteurs fictifs, notamment mon ami, Joel Hasse Ferreira, pour leur travail fructueux qui va aboutir, en première lecture, à un accord qui maintient les nombreuses avancées voulues par le Parlement mais refusées par le Conseil. 
