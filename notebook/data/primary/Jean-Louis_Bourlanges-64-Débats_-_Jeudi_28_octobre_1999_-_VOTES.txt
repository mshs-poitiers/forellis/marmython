Jean-Louis Bourlanges # Débats - Jeudi 28 octobre 1999 - VOTES
  Bourlanges (PPE
),
 rapporteur général
. - Madame la Présidente, je vous remercie de vos félicitations. C’est un travail assez considérable que nous avons fait avec les rapporteurs, avec les membres de la commission des budgets, avec vous tous, et c’est évidemment avec une certaine émotion que j’accueille ces votes.
Je voudrais, mes chers collègues, remercier tous ceux qui ont voté pour ce budget, remercier aussi ceux qui ont voté contre et qui contribuent à l’expression démocratique. Je voudrais surtout remercier, avec un soin particulier, le secrétariat de la commission des budgets qui a fait un travail absolument considérable.
(Applaudissements
)
Il faut être rapporteur pour savoir à quel point la vie générale de l’ensemble des collaborateurs du secrétariat de la commission des budgets est perturbée par le rythme infernal de cette procédure, et je crois qu’il faut en être conscient.
À part cela, je voudrais dire deux mots, si vous m’y autorisez, au Conseil et à la Commission. Monsieur le Représentant du Conseil, Madame la Commissaire, je pense que vous avez pu constater à quel point ce Parlement était rassemblé, uni pour combattre, non seulement pour ses droits, mais pour un certain nombre de changements fondamentaux.
L’année que nous sommes en train de vivre a été caractérisée par deux crises fondamentales : la crise internationale dans les Balkans, et la crise qui a affecté nos institutions avec le départ de la Commission. Sur ces deux points, les votes que vous avez émis, mes chers collègues, marquent la détermination de notre Parlement à aller de l’avant et à apporter à ces deux problèmes les solutions qui s’imposent. Demain, nous serons ouverts à la discussion avec le Conseil et avec la Commission, mais ne doutez pas, chers amis, du dialogue interinstitutionnel, ne doutez pas de la détermination de cette Assemblée !
(Vifs applaudissements
) 
