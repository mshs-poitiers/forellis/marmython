Chantal Cauquil # Débats - Mercredi 11 février 2004 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. 
- Qu’une grande partie de la recherche agronomique ait été, d’une façon ou d’une autre, orientée jusqu’à présent vers des domaines dont les capitalistes de l’agroalimentaire espéraient un maximum de retombées pour leurs profits est une évidence. Ce rapport préconise de réorienter, même partiellement, cette recherche vers des secteurs actuellement délaissés. Ce serait effectivement une bonne chose. Cela, même si nous ne pensons pas, à la différence du rapporteur, que le développement de l’agriculture dite durable et des productions agricoles dites biologiques, qu’il soutient, soit forcément synonyme d’un avantage réel tant pour les consommateurs, du moins dans leur grande majorité, que pour les petites exploitations familiales.
Nous ne nous opposons bien sûr pas à une telle réorientation de la recherche agronomique, ni à ce qu’elle soit financée sur des fonds publics, comme le demande le rapporteur. Cette dernière demande souligne à quel point l’économie de marché est incapable d’assurer le financement des recherches dès lors que leurs résultats ne débouchent pas sur du profit immédiat. 
