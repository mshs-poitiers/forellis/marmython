Bruno Gollnisch # Débats - Mardi 10 mars 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Petre (A6-0088/2009
) 
  Bruno Gollnisch  (NI
), 
par écrit
. – On ne peut qu’approuver les intentions du rapport : veiller à la qualité des produits alimentaires européens, à la compétitivité des producteurs, à une information simple mais complète du consommateur sur l’origine des produits, au respect des appellations d’origine et des labels de qualité, à une meilleure définition des produits traditionnels ou bio...
Et le rapporteur à raison de souligner qu’il est nécessaire de prévoir que les importations de produits agricoles et alimentaires en Europe doivent répondre aux mêmes normes que celles imposées aux producteurs européens, ce qui n’est hélas pas toujours le cas. Il a raison de vouloir mettre en œuvre un accès conditionnel à nos marchés...
Il reste cependant quelques problèmes à régler : celui de la concurrence intra-communautaire déloyale, lorsqu’un État membre impose des normes plus strictes que celles prévues au niveau communautaire, notamment pour des raisons de santé publique ou de protection de l’environnement. Cet État devrait pouvoir, ne vous en déplaise, appliquer les mêmes règles que celles que vous demandez au niveau de l’OMC.
Pose également problème la cohérence avec les préoccupations environnementales de ce Parlement, qui devrait se préoccuper de promouvoir les circuits courts (manger des produits de saison produits localement), plutôt qu’une adaptation forcément imparfaite au marché mondial. 
