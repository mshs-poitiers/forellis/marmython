Yves Butel # Débats - Jeudi 10 octobre 2002 - Votes
  Butel (EDD
),
par écrit
. 
- Le rapport de Michael John Holmes souligne à juste titre le manque de connaissances - scientifiques - des stocks de poissons en eaux profondes. Cependant, on sait que les poissons en eau profonde tel que l'hoplostète, appelé couramment Empereur, ont une durée de vie longue et a fortiori une maturité tardive qui entraîne naturellement une régénération très lente du stock.
Actuellement, on constate effectivement sur le terrain une diminution des captures en eaux profondes confirmée par la décision d'armateurs industriels qui ont stoppé d'eux-mêmes leurs activités devenues non rentables.
La limitation d'accès aux pêcheries des stocks d'eau profonde est donc justifiée mais pourra être réexaminée à la lumière de nouvelles données scientifiques objectives. 
