Béatrice Patrie # Débats - Mercredi 21 mai 2008 - Rapport 2007 sur les progrès accomplis par la Turquie (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Béatrice Patrie  (PSE
). - 
(FR) 
Madame la Présidente, chers collègues, je me réjouis de l'équilibre de ce rapport qui adresse un signal positif à la Turquie. Les socialistes seront attentifs à ce que les négociations d'adhésion se poursuivent positivement sous présidence française. C'est justement parce que nous souhaitons l'adhésion qu'il ne doit demeurer aucune zone d'ombre sur des évènements qui touchent à nos valeurs démocratiques communes.
Il n'est pas acceptable que des intellectuels, tel le journaliste d'origine arménienne Hrant Dink, risquent leur vie dès lors qu'ils parlent de certaines périodes de l'histoire turque. Il n'est pas non plus acceptable d'entendre perdurer une thèse officielle qui banalise le génocide arménien sous le terme de grande tragédie et qui balaie du revers de la main la souffrance d'un peuple dont le nombre des déportés est assimilé à celui des victimes britanniques de la grippe.
Avec le philosophe Bernard-Henri Lévy, je pense que la négation du génocide est partie constitutive du génocide lui-même. Aussi, j'exhorte les autorités turques à s'engager dans la voie raisonnable de la vérité contribuant à la réhabilitation de toutes les minorités nationales. 
