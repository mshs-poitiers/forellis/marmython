Armonia Bordes # Débats - Jeudi 15 janvier 2004 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. 
- Nous sommes pour la suppression de la taxe à la valeur ajoutée car c’est un impôt qui est particulièrement injuste et nous sommes pour son remplacement par un impôt fortement progressif sur les revenus et par un fort impôt sur les profits.
Tout en étant contre les impôts, nous ne voulons pas faire obstacle à ce que la réduction du taux de TVA pour certains secteurs soit prorogée, bien que le choix des secteurs bénéficiant de cette mesure soit arbitraire et limitatif.
Nous avons voté ce rapport pour cette unique raison, mais ce vote n’implique aucune caution pour le système d’impôt indirect en général et pour la TVA en particulier. 
