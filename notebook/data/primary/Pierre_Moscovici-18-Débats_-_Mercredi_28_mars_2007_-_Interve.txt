Pierre Moscovici # Débats - Mercredi 28 mars 2007 - Interventions d’une minute sur des questions politiques importantes
  Le Président. 
 - Je suis désolé, mais nous devons passer à la suite de l’ordre du jour. Je vous communique néanmoins que tous ceux qui ont demandé la parole aujourd’hui seront prioritaires pour la session de Strasbourg, en avril, étant entendu qu’ils devront s’inscrire à nouveau, pour la forme. La présidence prévue à ce moment-là en sera informée et ils seront placés en haut de la liste des orateurs. 
