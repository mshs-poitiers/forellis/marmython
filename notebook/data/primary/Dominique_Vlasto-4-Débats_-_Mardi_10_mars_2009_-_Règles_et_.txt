Dominique Vlasto # Débats - Mardi 10 mars 2009 - Règles et normes communes concernant les organismes habilités à effectuer l’inspection et la visite des navires ainsi que les activités pertinentes des administrations maritimes (refonte) - Règles et normes communes concernant les organismes habilités à effectuer l’inspection et la visite des navires (refonte) - Contrôle par l’État du port (refonte) - Système communautaire de suivi du trafic des navires et d’information - Enquêtes sur les accidents dans le secteur des transports maritimes - Responsabilité des transporteurs de passagers par mer en cas d’accident - Assurance des propriétaires de navires pour les créances maritimes - Respect des obligations des États du pavillon (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Dominique Vlasto, 
rapporteure
. − Monsieur le Président, pour conclure, je voudrais dire que le travail qui a été réalisé par l’ensemble des collègues nous permet aujourd’hui, si le Parlement bien entendu adopte ce paquet, d’être beaucoup plus sereins en ce qui concerne la protection de nos mers, de nos côtes et de nos concitoyens.
L’important maintenant, c’est la mise en place des dispositions préconisées pour la prévention et la répression, qui doivent être vraiment effectives dans l’ensemble de nos pays. Nous aurons ainsi contribué à la sauvegarde de notre patrimoine maritime.
Je remercie M. le Commissaire, qui nous a montré sa détermination pour faire en sorte que la directive européenne soit mise en œuvre et ne reste pas à l’état de discours. 
