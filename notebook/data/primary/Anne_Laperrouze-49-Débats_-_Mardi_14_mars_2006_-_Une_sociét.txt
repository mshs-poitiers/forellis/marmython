Anne Laperrouze # Débats - Mardi 14 mars 2006 - Une société de l’information pour la croissance et l’emploi (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Anne Laperrouze, 
au nom du groupe ALDE
. - Monsieur le Président, l’innovation dans le domaine des TIC est tellement rapide que nous pouvons craindre que l’Union européenne, qui n’investit que 80 euros par habitant, contre l’équivalent de 350 euros pour le Japon et de 400 euros pour les États-Unis, ne soit rapidement dépassée. L’UE doit donc accroître ses investissements dans la recherche et exhorter les États membres à en faire de même.
D’autre part, devant cette innovation galopante, nous pouvons craindre un élargissement de la fracture numérique et, par conséquent, des écarts sociaux, au détriment de la cohésion sociale et territoriale, que nous recherchons. Il est donc essentiel de bâtir une société de l’information fondée sur l’inclusion, sur un large usage des technologies de l’information et de la communication dans les services publics, les PME et les ménages.
Le succès de la stratégie «i 2010» exige de la part de la Commission européenne des propositions pour rendre accessibles à tous les citoyens les technologies, en tenant compte du rôle crucial dévolu aux régions, pour garantir les principes de liberté et de pluralisme des médias, pour définir des actions claires en matière de protection contre les contenus illicites et nuisibles, de protection des mineurs et de dignité de l’homme, tout en veillant à protéger la vie privée. La Commission devra aussi mettre l’accent sur le bon usage des TIC dans les services publics, notamment dans la santé et dans l’éducation.
Enfin, si j’approuve, à moyen terme, l’ouverture des marchés après une période de transition vers la mise en œuvre des règles générales de concurrence, je rappelle que les traités définissent les règles de la libre concurrence, tout en appelant à la cohésion économique et sociale, à la cohésion territoriale.
La libre concurrence dans le domaine des TIC ne doit pas avoir pour conséquence le refus, de la part du privé, d’investir dans des infrastructures non rentables. Le rôle des États et des régions sera alors déterminant pour encourager les infrastructures nécessaires.
Il nous appartient à tous de faire des TIC de véritables outils pour le développement économique et pour la cohésion sociale et territoriale de l’Union européenne. Je remercie M. Paasilinna pour la qualité de son travail, ainsi que mes collègues pour leur contribution au débat. 
