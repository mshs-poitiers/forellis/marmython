Pierre Schapira # Débats - Mercredi 23 avril 2008 - Situation en Birmanie (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Pierre Schapira  (PSE
). – 
(FR) 
Monsieur le Président, il y a déjà six mois, des manifestations à Rangoun ont révélé à la face du monde les graves violations des droits de l'homme qui ont lieu régulièrement en Birmanie.
Il semble, malheureusement, que l'opinion internationale ait déjà détourné son regard de ce pays en crise. La réalité est que notre soutien à la population de ce pays devrait être sans faille et suivre une stratégie cohérente sur un long terme, pour que la démocratie et la liberté de la presse, de culte, d'expression et d'association, soient enfin respectées.
Malgré les pressions diplomatiques, malgré l'action exemplaire d'Aung San Suu Kyi qui, je le rappelle, a reçu le prix Sakharov en 1990, malgré la mobilisation de la société civile internationale, la situation ne s'est pas améliorée. 400 000 moines bouddhistes du pays, parce qu'ils n'ont pas le droit de vote, ne pourront participer à ce référendum.
Cette situation, inacceptable, est la preuve que les pressions n'ont pas été suffisantes. Les sanctions à l'égard du régime birman doivent être élargies, tout en visant les élites politiques plutôt que la population.
Mais je souhaite surtout que l'action de l'UE s'en trouve renforcée. Et pour alerter l'opinion, je demande, nous demandons, que Aung San Suu Kyi, qui incarne cette lutte, soit faite citoyenne d'honneur de toutes les capitales européennes. Nous montrerons ainsi notre détermination à promouvoir effectivement les droits de l'homme et la liberté en Birmanie. 
