Margie Sudre # Débats - Lundi 17 novembre 2008 - Fonds de solidarité de l’Union européenne: obstacles à sa réforme (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Margie Sudre  (PPE-DE
), 
par écrit.
 – Le Parlement et Commission sont parvenus à un accord, depuis plus de deux ans, pour élargir le champ d’application du Fonds de solidarité, afin qu’il couvre les catastrophes naturelles, mais aussi les accidents industriels, les attentats terroristes et les crises majeures de santé publique.
L’accord précise qu’une attention particulière sera accordée aux RUP même si elles ne remplissent pas totalement les critères d’éligibilité, afin qu’elles bénéficient d’aides d’urgence en cas d’aléas. 
Pourtant, cette réforme n’est toujours pas entrée en vigueur, le Conseil ne parvenant pas à statuer, repoussant toujours plus loin l’adoption d’une position commune.
Même si ce Fonds a récemment été mobilisé en faveur de la Réunion suite au cyclone Gamède, et pour la Martinique et la Guadeloupe, victimes du cyclone Dean, une incertitude pèse toujours sur la recevabilité de chaque demande, faute d’une décision rapide du Conseil sur cette réforme.
La Commission doit également revoir ses propositions en vue de renforcer la capacité de l’Union en matière de protection civile, pour exploiter l’expertise et la localisation géographique des RUP et des PTOM qui souhaitent constituer des points d’appui en cas d’intervention hors d’Europe. 
Sur ces deux dossiers, les ultramarins attendent une réponse ambitieuse de l’UE pour garantir leur sécurité. 
