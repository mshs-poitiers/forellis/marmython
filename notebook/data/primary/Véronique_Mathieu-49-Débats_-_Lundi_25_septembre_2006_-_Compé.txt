Véronique Mathieu # Débats - Lundi 25 septembre 2006 - Compétences clés pour l’éducation et la formation tout au long de la vie (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Véronique Mathieu (PPE-DE
). 
 - Dans le contexte actuel de la mondialisation, l’univers professionnel, devenu complexe et multiforme, soumis à des changements ultrarapides est facteur d’insécurité et d’anxiété. Les nouvelles manières de travailler, de produire, d’innover, d’échanger obligent les travailleurs à une remise en cause permanente des connaissances, et savoir-faire.
Pour réconcilier ces objectifs économiques de plus en plus durs avec notre vision humaniste du travail, la formation tout au long de la vie, principe affirmé dans la plupart de nos droits nationaux, est l’objet d’un large consensus.
Les injonctions officielles, y compris européennes, sont déjà nombreuses. Mais ne nous contentons pas du slogan!
Nous sommes encore loin de passer au niveau de la pratique, de sa systématisation. La formation tout au long de la vie est trop souvent soit une obligation légale à laquelle se livrent les entreprises sans réelle réflexion sur la plus-value de ces actions, et sans réelle politique de long terme; soit une incantation évoquée en cas d’urgence afin de répondre à des difficultés sociales.
Ainsi, un travail doit à présent être fait au niveau national et local, pour développer d’une part des structures d’apprentissage efficientes, d’autre part un engagement réel de la part des entreprises et des salariés. 
