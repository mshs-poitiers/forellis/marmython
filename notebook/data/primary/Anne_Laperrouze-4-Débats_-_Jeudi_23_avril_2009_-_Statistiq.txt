Anne Laperrouze # Débats - Jeudi 23 avril 2009 - Statistiques sur les produits phytopharmaceutiques (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Anne Laperrouze, 
au nom du groupe ALDE
. – 
Madame la Présidente, Monsieur le Commissaire, chers collègues, lors de la session plénière du mois de janvier 2009, nous avons adopté deux textes législatifs relatifs à l’utilisation durable des pesticides et aux procédures de mise sur le marché des produits phytopharmaceutiques. Les négociations de ces textes avaient été passionnelles et ont permis de véritables avancées pour la protection de la santé humaine et de l’environnement.
Ces deux textes ne seraient toutefois rien sans celui-ci. Ce règlement est, en effet, le bras armé des deux autres. Vous le rappeliez, Monsieur le Commissaire, ces statistiques sont nécessaires pour connaître l’évolution des mises sur le marché des produits, mais aussi de leur utilisation, et surtout pour calculer les indicateurs de risques définis dans le rapport sur l’utilisation durable des pesticides.
Je souligne également qu’à mon sens, nous avons atteint un équilibre entre nécessaire transmission des données et confidentialité de celles-ci, mais aussi charges administratives proportionnées puisqu’il s’agit là d’une inquiétude pour les utilisateurs.
Concernant les biocides, il est important de souligner que ce texte devrait, à terme, les couvrir en fonction des résultats d’une étude d’impact.
Nous nous sommes interrogés sur les utilisations commerciales non agricoles des pesticides. Pour l’instant, toute appréciation sur leur volume ne peut être qu’intuitive. Pour cette raison, les études pilotes devant être conduites par la Commission européenne se révèleront très éclairantes.
Je tiens enfin à remercier notre rapporteur qui nous a associés tout au long des négociations, et a fortement contribué à ce que nous parvenions à un accord. 
