Paul-Marie Coûteaux # Débats - Mercredi 6 septembre 2006 - Situation au Moyen-Orient (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Paul Marie Coûteaux (IND/DEM
). 
 - Monsieur le Président, puisqu’il est revenu dans l’hémicycle, je dirai à Daniel Cohn-Bendit que je le félicite pour son intervention assez chaleureuse que nous avons entendue à l’instant. Je suis d’accord avec lui sur presque tous les points et, en tout cas, sur l’essentiel, au risque de l’étonner et de le chagriner. C’est l’évidence même: il n’y a pas d’autre solution que politique. Et cette solution politique, elle est possible, au-delà des désespoirs des uns et des autres, des désespoirs des extrémistes des deux camps. Il y a du désespoir dans la politique israélienne actuelle et il faut protéger l’État d’Israël contre lui-même. Je pense que cette politique de coup de force que mène Israël est une politique de fuite en avant qui met son existence en danger à terme et qui préoccupe tous ceux qui sont attachés à sa sécurité, comme nous le sommes nous-mêmes.
Simplement, nous divergeons sur deux points: non, ce n’est pas l’Europe qui imposera cette solution politique car elle n’est pas unanime à la vouloir; il a fallu plus de trois semaines - Daniel Cohn-Bendit, vous le savez - pour que les ministres des Affaires étrangères se réunissent après le début des bombardements, et ce fut pour constater leurs désaccords. Cessons de rêver d’une politique fondée sur une énième armée européenne, sur une marine européenne, ou que sais-je encore. Nous ne sommes pas d’accord politiquement; il n’y aura donc pas de choix politique européen propre en tant que tel. Tout au moins, tant que nous suivrons la politique de Washington - et c’est l’autre point sur lequel je diverge avec vous - qui consiste au fond à favoriser les extrêmes des deux camps et à détruire l’équilibre géopolitique de la région. En détruisant l’Irak, en stigmatisant la Syrie, en forçant la Syrie à se retirer du Liban, ce qui fragilise celui-ci - les chrétiens eux-mêmes s’en rendent compte, même Aoun -, nous avons créé les conditions d’un déséquilibre et nous récoltons toujours le fruit de ce déséquilibre, qui est la guerre, qui est la parole donnée aux extrêmes, des deux camps, d’ailleurs, au dominant comme au dominé.
Je crois, hélas, que ce n’est pas l’Europe qui peut imposer cette solution politique; je ne vois pas d’autre puissance politique que la France pour le faire. 
