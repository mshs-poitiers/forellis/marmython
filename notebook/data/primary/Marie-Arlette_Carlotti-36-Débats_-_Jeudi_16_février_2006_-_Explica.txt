Marie-Arlette Carlotti # Débats - Jeudi 16 février 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Nouveau mécanisme de financement pour le développement dans le cadre des objectifs du Millénaire (RC-B6-0119/2006
) 
  Marie-Arlette Carlotti (PSE
), 
par écrit
. - L’UE a une responsabilité et une opportunité uniques en la matière. Elle seule peut réunir les deux conditions de mise en place de ces nouveaux mécanismes: taille critique et volonté politique. Le texte qui nous est soumis n’est hélas pas à la hauteur de cette attente.
Certes, cette résolution apporte un soutien de principe aux nouvelles sources de financement du développement en insistant pour que ces fonds viennent s’ajouter à l’aide publique traditionnelle et non la remplacer. Mais cette position du Parlement est bien timide.
Le soutien est exprimé du bout des lèvres et le concept central dans ce débat, un «impôt mondial» pour le développement, est passé sous silence. Les différents projets qui sont aujourd’hui sur la table (taxation des transactions financières, des émanations de CO2, des ventes d’armement...) ne sont pas évoqués. Aucune référence n’est faite aux «Biens publics mondiaux», que ces mécanismes devraient financer en priorité.
Parce qu’elle est un premier pas dans la bonne direction, je voterai pour ce projet de résolution. Mais je veux y voir un encouragement pour aller plus vite et plus loin vers une mise en œuvre concrète de ces nouveaux outils à l’échelle européenne. 
