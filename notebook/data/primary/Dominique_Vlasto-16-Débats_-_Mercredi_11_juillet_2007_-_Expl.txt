Dominique Vlasto # Débats - Mercredi 11 juillet 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Ferber (A6-0246/2007
) 
  Dominique Vlasto (PPE-DE
), 
par écrit.
 - 
J’ai souhaité voter en faveur du compromis élaboré par Markus Ferber car il améliore sur certains points la proposition initiale de la Commission européenne.
Il donne en effet un délai supplémentaire aux postes européennes pour se préparer à l’ouverture à la concurrence, repoussée au premier janvier 2011 au lieu de 2009 comme initialement prévu par la Commission.
J’ai également soutenu le compromis car il protègera les termes et conditions de travail des salariés du secteur postal. La libéralisation des marchés ne peut se faire aux dépens des salariés quand bien même elle se ferait au profit des consommateurs.
Notre vote apporte en outre des sécurités en matière de cohésion territoriale. L’ensemble des usagers est en effet assuré de pouvoir disposer sur tout le territoire de l’Union européenne d’une levée et d’une distribution du courrier au moins 5 jours par semaine.
Néanmoins il subsiste une difficulté majeure qui constituera l’enjeu de la seconde lecture. Je veux parler du financement du service universel. Il nous faut disposer de mécanismes de financement qui soient juridiquement sécurisés et pérennes. C’est un préalable indispensable pour les opérateurs économiques du secteur, préalable sans lequel je ne pourrai m’engager en faveur de la libéralisation totale du secteur. 
