Gilles Savary # Débats - Mercredi 27 septembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Rapkay (A6-0275/2006
) 
  Gilles Savary (PSE
). 
 - Monsieur le Président, je voudrais expliquer pourquoi, après des débats intéressants hier et les votes d’aujourd’hui, j’ai voté contre le rapport Rapkay. Tout simplement parce que je suis obligé de constater que c’est une victoire éclatante des libéraux.
Nous avons voté pour la dérégulation des services publics, nous avons voté contre une directive-cadre, nous avons voté contre la distinction entre les services d’intérêt économique général et les services d’intérêt général, nous avons voté pour l’application du droit de la concurrence à tous les services d’intérêt général et leur définition précise, nous avons voté contre la définition du in-house
, autrement dit les régies publiques, nous avons voté contre les précisions des critères Altmark. Dans ces conditions, nous sommes en recul considérable par rapport aux précédentes résolutions, celle de M. Herzog et celle de M. Langen en 2001, et la Commission nous propose en revanche une nouvelle communication à la fin de l’année.
C’est dire que nous laissons aujourd’hui exposés des pans entiers du droit communautaire et des services publics locaux aux incertitudes de la Cour. Qu’entend-on par régie directe? Qu’entend-on par structure intercommunale? Qu’entend-on par société d’économie mixte? Que sont des concessions face au droit du marché et de la concurrence? Nous ne le savons toujours pas et c’est la raison pour laquelle je continuerai à militer pour des textes transversaux permettant de sécuriser la subsidiarité. Je crois que, hélas, on est loin du compte et que, aujourd’hui, c’est une bataille de perdue. J’espère que ce n’est pas une défaite définitive. 
