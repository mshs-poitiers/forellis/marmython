Catherine Lalumière # Débats - Mercredi 4 juin 2003 - Méthode ouverte de coordination
  La Présidente.  
- Merci, Madame Stauner.
J’ai reçu, en conclusion de ce débat, une proposition de résolution, au nom de la commission de la culture, de la jeunesse, de l’éducation, des médias et des sports, conformément à l’article 42 du Règlement.(1)

Le vote aura lieu demain, à midi. 

(1) Cf. procès-verbal.

