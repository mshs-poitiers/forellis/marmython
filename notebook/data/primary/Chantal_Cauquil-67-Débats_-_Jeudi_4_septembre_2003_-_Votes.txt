Chantal Cauquil # Débats - Jeudi 4 septembre 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. - Nous pourrions souscrire à certaines des exhortations exprimées dans la résolution commune et nous sommes bien entendu pour la collaboration entre pays face aux catastrophes, en particulier pour aider des pays comme le Portugal. Mais l’ensemble du texte n’est destiné qu’à masquer la responsabilité des États dans leur incapacité à faire face aux conséquences de la canicule et des feux de forêt.
La canicule elle-même n’est évidemment pas de la responsabilité des États et les feux de forêt, en général non plus. Mais pourquoi n’y avait-il pas, dans les maisons de retraite, dans les hôpitaux, du personnel en nombre suffisant pour faire face aux conséquences de la canicule sur les personnes les plus âgées? Pourquoi n’y avait-il pas assez de pompiers, de matériel, pour combattre les feux de forêt?
Parce que tous les États, même les plus riches, rognent depuis des années sur les budgets des services publics indispensables. On diminue les crédits qui leur sont accordés, on n’embauche pas de personnel en nombre suffisant. Même, en temps ordinaire, les services d’urgence des hôpitaux et les services de lutte contre les incendies sont à la limite de leurs possibilités et ne font face à leurs obligations qu’en imposant des conditions de travail insupportables à leur personnel. Que valent dans ces conditions les promesses, d’ailleurs extrêmement vagues, que cela ne se reproduira pas?
(Explication de vote écourtée en application de l’article 137, paragraphe 1, du règlement).

