Tokia Saïfi # Débats - Mercredi 23 avril 2008 - Vers une réforme de l'Organisation mondiale du commerce (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Tokia Saïfi  (PPE-DE
). – 
(FR)
 Madame la Présidente, à l'heure où les négociations menées dans le cadre du cycle de Doha se désarticulent et s'enlisent, se pose aujourd'hui, plus que jamais, la question du fonctionnement de l'OMC.
N'est-il pas possible de sortir de l'impasse en réformant l'OMC? À défaut d'accord sur le fond, peut-on favoriser le processus menant à un tel accord? Cette relance de l'OMC semble possible et passe par le renforcement de son efficacité fonctionnelle et décisionnelle.
Deux niveaux de réformes sont effet envisageables: celles qui visent à améliorer la procédure de négociation et celles qui tendent à renforcer la légitimité de l'Organisation, facteur clé de son organisation. Pour cela, il faut insister sur la dimension parlementaire au sein de l'OMC et rendre grâce aux représentants légitimes des citoyens que nous sommes, et rendre plus transparents et démocratiques les enjeux de la mondialisation.
Un grand chantier de cohérence entre les politiques internationales est également à mettre en œuvre. Il ne sert à rien, en effet, d'éliminer les obstacles aux frontières si, derrière celles-ci, demeurent des obstacles à l'investissement, des droits sociaux bafoués et des normes environnementales ignorées. Une OMC efficace est donc primordiale pour garantir l'objectif de multiplication des échanges et d'ouverture régulée par les marchés. Et qui dit règle ne signifie pas mesures protectionnistes. En effet, la libéralisation sans garde-fou n'est pas la solution à tous les maux, en particulier à ceux que nous vivons aujourd'hui, à travers la hausse des prix des matières premières agricoles et les émeutes de la faim qui en découlent.
La proposition Falconer d'abaissement des droits de douane est en ce sens inacceptable pour notre agriculture européenne et aurait des conséquences que l'on sous-estime sur la production agricole des pays les plus pauvres.
Aussi, pour conclure le cycle de Doha, il faut rééquilibrer les négociations et parvenir à une véritable réciprocité des engagements d'accès au marché. Nous ne sommes pas encore prêts à sacrifier notre agriculture et notre rôle de contributeurs à la sécurité alimentaire, pour obtenir, en peau de chagrin, quelques diminutions de tarifs industriels. 
