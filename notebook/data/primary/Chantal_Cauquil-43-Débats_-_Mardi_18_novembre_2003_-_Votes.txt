Chantal Cauquil # Débats - Mardi 18 novembre 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. 
- Nous sommes pour le développement de la recherche et donc pour son financement. Dans les dépenses des institutions européennes, ce ne sont certainement pas les dépenses en faveur de la recherche et du développement qui nous gênent, pour autant qu’elles ne servent pas seulement de prétexte pour des subventions à des entreprises privées. Nous constatons pourtant que les dépenses de la recherche sont, avec les dépenses sociales, celles que l’on rogne, en tout cas en France, le plus volontiers pour augmenter aides et subventions au grand patronat.
Il reste que toute la démarche exposée dans ce rapport consiste à n’envisager la recherche que comme un élément de la concurrence, entre l’Europe et les États-Unis ou entre les différentes entreprises. Or, le rapport lui-même est obligé de constater que les entreprises privées, essentiellement préoccupées par leur profit, voire par leur profit à court terme, négligent les investissements dans la recherche.
En réalité, il y a contradiction entre la recherche du profit et la recherche scientifique au service des intérêts de l’ensemble de la société. Nous n’avons pas voté contre ce rapport parce que nous sommes pour que la recherche publique bénéficie de bien plus de moyens qu’actuellement, mais nous n’avons pas voulu, non plus, cautionner cette subordination de la recherche à la course au profit. 
