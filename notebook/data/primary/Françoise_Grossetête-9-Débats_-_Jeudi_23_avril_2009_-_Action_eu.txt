Françoise Grossetête # Débats - Jeudi 23 avril 2009 - Action européenne dans le domaine des maladies rares (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Françoise Grossetête, 
rapporteure pour avis de la commission de l’industrie, de la recherche et de l’énergie
. − 
Monsieur le Président, les patients qui sont atteints de maladies rares souffrent d’errance diagnostique et, souvent, ne bénéficient d’aucun traitement.
La rareté de ces maladies génère des défis tant d’un point de vue scientifique qu’économique. Or, ayant été rapporteure du règlement sur les médicaments orphelins, il y a dix ans, je sais combien ces malades sont trop peu nombreux pour être un enjeu local ou régional, et combien les maladies sont, elles, par contre, trop nombreuses pour être enseignées aux professionnels de la santé. Les expertises sont donc rares.
La réponse passe nécessairement par l’Europe, et notre commission ITRE soutient M. Trakatellis dans sa volonté de renforcer la recherche et la prévention. Comment priver, par exemple, un couple dont deux enfants seraient atteints de mucoviscidose et qui souhaiterait avoir un troisième enfant, des avancées de la recherche pour que celui-ci ne soit pas atteint de cette même maladie? C’est pour cela qu’il faut plus de coordination, plus de sécurité, plus de clarté pour les patients. Ce sont des sujets essentiels qui répondent aux attentes des citoyens européens pour une Europe de la santé. 
