Marie-Anne Isler-Béguin # Débats - Mardi 3 février 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Rapport: Anne Laperrouze (A6-0013/2009
) 
  Marie Anne Isler Béguin  (Verts/ALE
), 
par écrit
. – 
Le rapport propose à nouveau l’option nucléaire alors que cette énergie n’est pas compétitive que l’uranium s’obtient dans des conditions dangereuses, de discriminations ethniques et avec un impact sanitaire inacceptables.
Le charbon, au regard du problème du réchauffement climatique, ne doit pas être considéré comme une «composante transitoire».
Je pense que «la diversification des ressources énergétiques de l’UE» est liée à la mise en production de ressources fossiles en Mer Caspienne. Les gisements de pétrole et de gaz (région du Kashagan) mettent sous pression les populations et leurs ressources environnementales : l’extraction du pétrole chargé en sulfures met en péril la santé des populations et la biodiversité.
La diversification de l’approvisionnement énergétique implique des gazoducs et oléoducs pour acheminer les ressources jusqu’à l’UE. Les projets du TBC ou celui du Nabucco pèsent sur la stabilité politique de nos voisins et nous oblige à ne pas la remettre en question par nos besoins énergétiques. Les populations du Caucase du sud doivent trouver un avantage économique et social à l’acheminement de l’énergie à partir de leurs territoires.
En Afrique, la production d’énergie solaire destinée à nos besoins, doit être convenablement rétribuée.
Pourquoi ne pas dire dans le rapport que les énergies renouvelables et les économies d’énergie constituent la solution d’avenir? En l’état actuel, je vote contre ce rapport. 
