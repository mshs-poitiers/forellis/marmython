Lydia Schénardi # Débats - Mardi 13 juin 2006 - Activités de recherche, de développement technologique et de démonstration (2007-2013, FP7) ***I - Activités de recherche et de formation en matière nucléaire (2007-2011) * (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Lydia Schenardi (NI
). 
 - Monsieur le Président, nous approuvons le programme de recherche qui nous est proposé parce qu’il met l’accent sur la recherche en matière d’énergie de fusion ainsi que sur la sécurité et la gestion des déchets. Toutefois, des inquiétudes persistent.
Les centrales dites «de quatrième génération» ne nous apparaissent pas comme résolvant totalement les problèmes actuels de sécurité et d’environnement et posent en outre celui de la circulation à grande échelle de matières radioactives. Premièrement, les concepts retenus tels que les SFR semblent répondre avant tout aux intérêts industriels. Ensuite, en ce qui concerne l’information du grand public, je cite, «les bienfaits d’un usage sûr de l’énergie atomique» ne doivent pas, compte tenu des intérêts en présence, déboucher sur une désinformation, voire une intoxication du public. Enfin, l’enthousiasme pour la coopération scientifique internationale qui se met en place, tant pour la quatrième génération que pour ITER, ne doit pas nous faire perdre de vue que cette coopération n’est pas synonyme d’une absence de compétition et que cette dernière ne reposera pas sur les principes de la libre concurrence, mais sur une lutte stratégique.
Nous devons nous y préparer en définissant des stratégies claires et en y allouant les moyens nécessaires afin de ne pas troquer notre dépendance vis-à-vis du pétrole contre une autre dépendance. 
