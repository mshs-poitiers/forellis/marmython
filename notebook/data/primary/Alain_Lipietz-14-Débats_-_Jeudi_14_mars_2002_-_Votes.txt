Alain Lipietz # Débats - Jeudi 14 mars 2002 - Votes
  Lipietz (Verts/ALE
), 
rapporteur
. - Encore une fois, mes chers collègues, le passage de 10 à 20 % est une question éminemment complexe qui ne peut pas être déposée à l'instant même qui précède le vote, alors que l'amendement de compromis qui avait été remis à 17 h à M. Radwan, qu'il avait approuvé sur le coup, me paraîtrait tout à fait à sa place avant le 48. Je pourrais jouer à l'imbécile qui dit : puisque M. Radwan utilise une manœuvre déloyale de l'Assemblée, je demande à mes amis de se lever également pour refuser son amendement oral. Sauf que moi, dans l'intérêt de la stabilité des marchés financiers en Europe, je le trouve beau, son amendement. Donc, je l'approuve et je vous demande d'accepter de le voter, et ensuite de l'adopter. Mais je vous fais observer que les manœuvres totalement déloyales de M. Radwan ont également fait tomber des amendements approuvés par le Conseil et la Commission, ainsi qu'une grande partie d'entre vous, et je trouve que les petites manœuvres de ce genre n'honorent pas notre Assemblée.
(Applaudissements)

