Christine de Veyrac # Débats - Jeudi 30 novembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport van Orden (A6-0420/2006
) 
  Christine De Veyrac (PPE-DE
), 
par écrit
. - Favorable au principe de l’adhésion de la Bulgarie dans l’Union, je me suis néanmoins abstenue sur ce rapport. De par ses règles actuelles, l’Union européenne rencontre à ce jour de véritables difficultés à décider et à s’accorder; dans ces conditions, faire entrer de nouveaux participants autour de la table est prématuré. Il convient au préalable de réformer nos institutions communes, mais aussi de prévoir de nouvelles sources de financement, afin de pouvoir procéder correctement à l’intégration dans l’Union de nouveaux États membres. Une pause dans les élargissements est à ce stade nécessaire. En tout état de cause, si l’adhésion de la Bulgarie devait se confirmer au 1er janvier 2007, je souhaite néanmoins que des clauses de sauvegarde soient appliquées notamment sur la libre circulation des travailleurs. 
