Marie-Arlette Carlotti # Débats - Mercredi 15 septembre 1999 - VOTES
  Carlotti (PSE
),
par écrit
. - À l’image de mes camarades du parti socialiste européen particulièrement attentifs et assidus à l’occasion des auditions des commissaires candidats, je me félicite de cette procédure riche d’enseignements sur la personnalité et les intentions de ceux qui seront nos interlocuteurs privilégiés.
Cet "examen de passage" du futur exécutif collégial devant les représentants élus des citoyens européens constitue une étape importante de la démocratisation des institutions européennes dont les socialistes font leur cheval de bataille.
Les prestations en demi-teinte de certains des candidats, apparemment toujours réticents dès lors qu’il s’agit d’évoquer l’Europe sociale ou l’emploi, voire l’audition très peu convaincante de Mme Palacio, ne sauraient occulter une impression d’ensemble globalement satisfaisante.
Contrairement à la droite européenne qui a manifestement cherché à masquer ses divisions par la virulence de ses attaques, parfois personnelles et injurieuses, à l’encontre de certains commissaires, j’entends privilégier la voie de la concertation et de la collaboration avec la future Commission.
Il s’agit surtout, à mes yeux, de retrouver un interlocuteur crédible, de mettre fin à une situation de transition interminable et de permettre à l’exécutif européen de reprendre sa lourde tâche et d’avancer enfin, en étroite concertation avec les parlementaires européens, sur les questions déterminantes qui concernent l’Europe et ses citoyens.
C’est animée de cet état d’esprit que je vote en faveur de l’investiture de la nouvelle Commission, sans pour autant signer "un chèque en blanc". J’entends m’accorder la possibilité de juger sur "pièces" la qualité du travail accompli. 
