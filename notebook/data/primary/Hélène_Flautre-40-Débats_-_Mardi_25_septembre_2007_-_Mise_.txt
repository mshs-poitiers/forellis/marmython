Hélène Flautre # Débats - Mardi 25 septembre 2007 - Mise en œuvre de la décision du Conseil relative au moratoire sur la peine de mort (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Hélène Flautre  (Verts/ALE
). – 
 Madame la Présidente, le nombre de pays abolitionnistes, ou appliquant un moratoire, augmente chaque année. Les certitudes vacillent, même dans des États rétentionnistes, comme les États-Unis, et des coalitions s'organisent dans les pays musulmans.
Pour autant, chaque jour, des démarches sont entreprises pour éviter une exécution capitale. L'homosexualité reste toujours passible de la peine de mort dans plusieurs pays, et, malheureusement, le contexte européen actuel rappelle que ce combat reste très difficile.
Sur le moratoire et l'Assemblée générale des Nations unies, le Parlement vous le demande, Monsieur le Président en exercice du Conseil, pour la troisième fois cette année, et de la manière la plus solennelle: tout doit être entrepris pour obtenir, dans les plus brefs délais, d'ici à la fin de l'année, une résolution à l'Assemblée générale des Nations unies en vue d'un moratoire, comme stratégie sur la voie de l'abolition. Je crois que nous disons ici tous la même chose: c'est à vous de jouer et de mobiliser.
Sur la Journée européenne contre la peine de mort, comment expliquer que nous n'ayez pas dit un mot sur la situation qui est celle de l'Europe aujourd'hui à ce sujet? Comment est-il pensable qu'un pays puisse instrumentaliser un tel enjeu à des fins de politique intérieure, en faisant en sorte, dès lors, de se mettre en porte-à-faux vis-à-vis du projet européen, de l'article 2 de la Charte des droits fondamentaux, en porte-à-faux vis-à-vis des objectifs communs, peut-être les plus anciens, de son action extérieure? Les institutions de l'Union européenne et vous, comme président en exercice du Conseil, vous ne pouvez tolérer cette situation!
Le 10 octobre sera la Journée mondiale contre la peine de mort. Nous serons, en ce qui nous concerne, Parlement européen, réunis en mini session à Bruxelles. Il est crucial que le Parlement marque cet événement aussi solennellement et aussi fermement que possible. 
