Kader Arif # Débats - Lundi 2 février 2009 - L’impact des accords de partenariat économique (APE) sur le développement (brève présentation) 
  Kader Arif  (PSE
), 
par écrit
. – 
Le Parlement se prononcera jeudi sur le rapport de M. Schröder sur les Accords de partenariat économique (APE). Je serais extrêmement déçu si la première expression de notre institution sur ce sujet à la fois très technique et hautement politique (puisque tout l’avenir de nos relations avec les pays ACP est en jeu) devait se solder par l’adoption du rapport de M. Schröder. Le PSE ne votera pas ce texte, car il ne reflète en rien les préoccupations tant européennes que de nos partenaires ACP sur les APE et sur la manière dont ils sont négociés.
À l’inverse de la position du rapporteur, le PSE a déposé et votera une résolution qui replace le développement au cœur des priorités des APE, qui refuse la libéralisation des services publics ainsi que toute négociation sur les sujets de Singapour ou sur les services contre la volonté des pays ACP, qui favorise l’intégration régionale, qui demande un soutien financier massif pour mettre à niveau les économies des pays ACP et qui prenne en compte les spécificités et fragilités de ces pays, qu’ils soient PMA ou pas.
Voilà les conditions qui feraient des APE des accords acceptables. On en est malheureusement encore très loin. 
