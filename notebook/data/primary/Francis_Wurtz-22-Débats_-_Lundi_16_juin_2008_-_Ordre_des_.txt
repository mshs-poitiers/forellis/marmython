Francis Wurtz # Débats - Lundi 16 juin 2008 - Ordre des travaux
  Francis Wurtz, 
au nom du groupe GUE/NGL
. 
– (FR)
 Monsieur le Président, est–ce qu'il ne serait pas possible de poser deux questions?
La première, c'est celle posée par M. Swoboda, qui demande qu'on ait plus de temps pour le débat sur le Conseil européen et la question du référendum.
La deuxième est de savoir si on doit séparer le débat sur le prix du pétrole et le débat sur la pêche ou s'il faut en faire un seul.
Ce sont deux questions séparées. Je vous suggère de poser d'abord la question du temps de parole sur le débat sur le Conseil européen et ensuite seulement la question sur la pêche. 
