Margie Sudre # Débats - Jeudi 19 juin 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Résolution - Capacité de réaction de l’Union en cas de catastrophes (B6-0303/2008
) 
  Margie Sudre  (PPE-DE
), 
par écrit
. –
 Je salue l'initiative de la Commission de produire enfin un document relatif au renforcement de la cohérence, de l'efficacité et de la visibilité de la réaction de l'UE en cas de catastrophes naturelles ou causées par l'homme.
Je regrette néanmoins qu'il ne s'agisse toujours pas d'une véritable proposition concrète susceptible de doper les capacités européennes dans le domaine de la protection civile, incluant prévention et réaction rapide, lors de crises majeures survenant à l'intérieur ou à l'extérieur de l'Union.
Je remercie les membres du Parlement européen d'avoir apporté leur soutien à mon amendement visant à exploiter "l'expertise conjuguée à la localisation géographique des régions ultrapériphériques (RUP) et des pays et territoires d'outre-mer (PTOM)".
En effet, grâce à ces collectivités ultramarines, l'Europe est présente au large des côtes africaines (Canaries, Madère), dans l'océan Indien (Réunion), à proximité du continent américain (Guyane, Guadeloupe, Martinique, Açores), sans oublier les PTOM du Pacifique (Polynésie française, Nouvelle Calédonie).
Les territoires d'outre-mer de l'Europe sont prêts à devenir des points d'appui pour faciliter le pré-positionnement de produits essentiels et de logistique. La projection de moyens humains et matériels européens en serait facilitée, en cas d'intervention d'urgence hors de l'Union. 
