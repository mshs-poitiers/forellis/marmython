Jacques Toubon # Débats - Mardi 26 septembre 2006 - Progrès accomplis par la Turquie sur la voie de l’adhésion (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jacques Toubon (PPE-DE
). 
 - Monsieur le Président, mes chers collègues, ce rapport est excellent. Nous le soutenons, car il marque un tournant dans la prise de conscience par le Parlement européen de la réalité des relations entre la Turquie et l’Union européenne.
Il est le plus critique de tous ceux qui ont été produits dans cette enceinte depuis des décennies. En particulier, il comporte trois points déterminants pour nous: la mention de la capacité d’intégration de l’Union européenne comme critère, la nécessité absolue de normaliser l’attitude de la Turquie à l’égard de Chypre, membre à part entière de l’Union européenne, et la reconnaissance du génocide arménien comme condition préalable à l’adhésion. Je me permets de rappeler à mes collègues socialistes français que cela faisait partie de leur programme pour les élections européennes de 2004.
L’adoption de ce rapport, que nous souhaitons, doit avoir des conséquences politiques. Je demande tout d’abord à la Commission de ne plus jouer à cache-cache et de produire, le 8 novembre, un rapport véridique et non pas un conte pour enfants dont elle est coutumière dans ce domaine. Je demande au Conseil des ministres de regarder la situation avec lucidité et courage et d’envisager de marquer un arrêt dans des négociations dont le sens échappe aujourd’hui au commun des mortels.
Au-delà du cas de la Turquie, c’est l’ensemble du processus d’élargissement qui est en cause. Il ne faut entreprendre désormais aucun nouvel élargissement tant que nous n’aurons pas donné à l’Union européenne des mécanismes de décision efficaces et un budget suffisant. Poursuivre dans l’illusion et l’hypocrisie compromettrait le projet européen, c’est-à-dire la construction de l’union politique, et creuserait encore plus le fossé entre le bon sens des peuples et l’aveuglement des dirigeants.
(Applaudissements
) 
