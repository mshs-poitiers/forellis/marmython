Françoise Grossetête # Débats - Mercredi 17 décembre 2003 - Votes
  Grossetête (PPE-DE
). 
 - Monsieur le Président, merci de me donner la parole maintenant, parce que, en réalité, les trois rapports sur lesquels nous allons voter constituent un paquet. Avec ma collègue, Mme Müller, nous sommes arrivées �  obtenir de très bons compromis grâce auxquels nous opérons des avancées très importantes au niveau de la législation pharmaceutique.
Je voudrais dire, comme vient de l’exprimer Mme Müller, qu’il est essentiel que nous votions les compromis tels qu’ils sont présentés, et que nous ne votions pas en faveur des amendements n’ayant pas fait l’objet de ces compromis. Tout vote d’un amendement qui ne fait pas partie de ces compromis remettrait en cause les compromis auxquels nous sommes parvenus avec la présidence italienne. Je voudrais vraiment attirer l’attention de nos collègues: vous votez les compromis, vous ne votez pas les autres amendements qui n’en font pas partie. 
