Joseph Daul # Débats - Mercredi 9 mai 2007 - Ordre du jour
  Joseph Daul, 
au nom du groupe PPE-DE
. - Notre collègue Elmar Brok a subi une opération. Il pensait être présent cette semaine, mais ce ne sera pas le cas. Cela dit, je vous rassure, il est en bonne santé - comme on le connaît, il prend soin de sa santé - et il sera parmi nous pour la session à Strasbourg. C’est pour cette raison que je vous demande, chers collègues, de reporter le débat sur ce rapport.
(Le Parlement marque son accord)

