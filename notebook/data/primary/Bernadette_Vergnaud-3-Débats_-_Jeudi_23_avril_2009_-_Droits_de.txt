Bernadette Vergnaud # Débats - Jeudi 23 avril 2009 - Droits des patients en matière de soins de santé transfrontaliers (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bernadette Vergnaud, 
rapporteure pour avis de la commission du marché intérieur et de la protection des consommateurs
. − 
Monsieur le Président, Madame la Commissaire, chers collègues, nous allons nous prononcer sur un texte que j’ai longtemps appelé de mes vœux, notamment dans le cadre de mon rapport sur l’impact de l’exclusion des services de santé de la directive «services».
Néanmoins, je crains que le vote de tout à l’heure ne me laisse un goût amer. Le rapport, tel que voté en commission de l’environnement, avec l’appui de la plupart des groupes politiques à l’exception des socialistes, n’est en effet, avec quelques améliorations, qu’une réponse aux arrêts de la Cour de justice. Non seulement il ne répond pas aux défis majeurs des politiques de santé dans l’Union, mais il ne résout pas l’insécurité juridique pour les patients et consacre une vision marchande des soins de santé.
À propos de l’insécurité juridique, il me paraît évident que le flou artistique qui règne entre les conditions d’application respectives de cette directive ou du règlement (CEE) n° 1408/1971, et bientôt du règlement (CE) n° 883/2004 adopté hier, ne fera qu’amener la Cour de justice à devoir se prononcer à nouveau.
Quant à la vision marchande, l’esprit de ce rapport se trouve déjà résumé dans sa base juridique, à savoir le seul article 95 régissant les règles du marché intérieur. La santé ne serait donc qu’une marchandise comme une autre, soumise aux mêmes règles d’offre et de demande.
Cela ne peut que conduire à un accès inégal aux soins entre des citoyens aisés et bien informés, pouvant choisir les meilleurs soins disponibles dans l’UE, et les autres devant se contenter de services déjà fragilisés dans de nombreux États membres, et que cette directive ne vise en aucun cas à améliorer.
Dans le même esprit, l’amendement 67 revient à mettre en concurrence les systèmes de santé sociale nationaux, chacun étant libre, à condition de payer bien sûr, de s’affilier au système de son choix dans l’UE.
Je voudrais enfin évoquer la question de l’autorisation préalable pour les soins de santé hospitaliers, dont la mise en place est soumise à toute une série de contraintes pour les États membres, alors même que ce principe permet à la fois de contrôler l’équilibre financier des systèmes sociaux et de garantir aux patients des conditions de remboursement.
Pour toutes ces raisons, et parce que je me fais peu d’illusions sur l’issue du vote d’aujourd’hui, au vu de la belle unanimité...
(Le président retire la parole à l’orateur)

