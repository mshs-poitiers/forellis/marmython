Charles Pasqua # Débats - Mercredi 10 mars 2004 - Votes
  Pasqua (UEN
),
par écrit
. 
- Il n’y a guère que ce Parlement pour s’intéresser aussi passionnément à la situation des prisonniers de Guantanamo, et pour manifester tant de compassion à leur égard.
Je ne partage pas l’opinion dominante au sein cette Assemblée. Non seulement je ne crois pas qu’il s’agisse là d’une question prioritaire, mais, pour tout dire, la condition de ces personnes m’inspire peu de commisération.
À l’élite éclairée de vertueux défenseurs des droits fondamentaux qui ne voit à Guantanamo que de malheureux détenus victimes de l’arbitraire des États-Unis, je tiens à rappeler que nous parlons d’individus suspectés d’appartenir à la plus dangereuse organisation criminelle jamais identifiée, d’activistes ayant participé à des degrés divers à l’un des attentats les plus sanglants de l’Histoire, de terroristes islamistes ayant déclaré une guerre totale à l’Occident et à ses valeurs.
Une situation d’exception, comme l’est la lutte contre le terrorisme international, peut justifier des mesures exceptionnelles.
La situation des détenus de Guantanamo, question qui relève d’ailleurs de la stricte compétence des États-Unis, m’apparaît moins digne d’intérêt que celle des familles des victimes dont on se préoccupe moins. 
