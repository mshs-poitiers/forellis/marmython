Georges Berthu # Débats - Jeudi 26 février 2004 - Votes (suite) 
  Berthu (NI
),
par écrit
. 
- Je me suis abstenu sur la résolution proposée par le condominium des grands groupes sur la préparation du sommet de printemps 2004 car, quoique pétrie de bonnes intentions, elle passe à côté de l’essentiel du sujet. On dirait même que ses rédacteurs ont cherché à illustrer les défauts que je dénonçais au cours du débat d’hier: toujours plus de réglementation (voir par exemple paragraphe 18), de coordinations (paragraphe 2), et même de "synchronisation renforcée des processus de coordination" (paragraphe 15); rien sur les problèmes des prélèvements obligatoires à réduire, des créateurs de richesses à défendre, des préférences communautaires à réhabiliter.
Mais à vrai dire, pouvait-on attendre quelque chose de fort d’une résolution qui, comme beaucoup d’autres dans cette Assemblée, résulte d’un compromis entre la droite et les socialistes?
Les principaux pays d’Europe s’enfoncent dans un marasme prolongé, une paralysie toujours plus grande, une fuite des élites, car ils semblent croire que les procédures administratives créent la richesse, alors qu’au mieux elles ne font qu’offrir un cadre favorable aux créateurs. C’est eux qu’il faut valoriser en priorité. Mais cela implique une révolution des mentalités que la droite ne réussira pas en s’alliant avec des socialistes. 
