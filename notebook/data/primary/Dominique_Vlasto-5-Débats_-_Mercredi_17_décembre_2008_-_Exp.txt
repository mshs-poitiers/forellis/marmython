Dominique Vlasto # Débats - Mercredi 17 décembre 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Recommandation pour la deuxième lecture Cercas (A6-0440/2008
) 
  Dominique Vlasto  (PPE-DE
), 
par écrit
. –
 Les amendements 23 et 24 au rapport Cercas concernant le travail dominical ont été jugés irrecevables en raison du respect du principe de subsidiarité: c’est une excellente décision que j’approuve pleinement. 
Il me semblait inopportun que l’Union européenne légifère sur le travail le dimanche et impose une solution uniforme à ses États membres, quand la négociation au cas par cas permet de trouver des solutions volontaires et acceptables. S’il est indispensable de bien encadrer les possibilités de travailler le dimanche, j’estime qu’il faut le faire au niveau des États membres, en tenant compte des spécificités sociales et de la nature des activités. Il me semble tout aussi indispensable de tenir compte du contexte économique local, pour les zones touristiques, de montagne ou thermales, où l’activité est essentiellement saisonnière: l’ouverture de magasins certains dimanche dans l’année prend ici tout son sens.
Cette approche basée sur le libre choix est celle adoptée par le gouvernement français et elle permettra de trouver au cas par cas des solutions justes et équilibrées. En réaffirmant l’application du principe de subsidiarité, le Parlement européen a décidé de ne pas entraver ces politiques qui tiennent compte de ces contextes économiques et sociaux différents. 
