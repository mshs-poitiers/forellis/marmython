Tokia Saïfi # Débats - Jeudi 4 septembre 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Syed Kamall (A6-0283/2008
) 
  Tokia Saïfi  (PPE-DE
)
, par écrit
. 
– J'ai voté en faveur du rapport sur le commerce des services afin d'inviter la Commission à promouvoir, lors des négociations commerciales, à la fois l'ouverture progressive et réciproque de l'accès au marché des services et une politique de transparence accrue. L'Union européenne, qui est le plus grand exportateur et le plus grand fournisseur de services, ne peut qu'encourager un plus large accès au marché des services, tant au niveau des pays développés qu'au niveau des PVD.
Néanmoins, cette ouverture se doit d'être progressive et réciproque en tenant compte des intérêts différents des États. En ce sens, j'ai voté en faveur de l'amendement 2 qui rappelle la nécessité de faire la distinction entre services commerciaux et services non commerciaux et d'avoir une approche différenciée dans l'ouverture des marchés des services d'intérêt général. De même, j'ai voté en faveur de l'amendement 5 qui, dans le cadre des APE, demande que des services publics universels, accessibles et durables, puissent être garantis pour tous.
Enfin, en votant en faveur de l'amendement 7 qui reconnaît que certains produits, comme l'eau, devraient être considérés comme des biens publics universels, j'ai tenu à rappeler que l'ouverture du marché de ce genre de services doit être envisagée avec précaution. 
