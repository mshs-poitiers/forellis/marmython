Robert Navarro # Débats - Mardi 11 décembre 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Josu Ortuondo Larrea (A6-0345/2007
) 
  Robert Navarro  (PSE
), 
par écrit
. – 
(FR) 
J'ai voté en faveur du rapport Ortuondo Larrea sur la refonte de la directive sur l'interopérabilité ferroviaire, car l'interopérabilité est la clé du renouveau du système ferroviaire européen. En effet, le rail ne pourra redevenir compétitif – face à la route, notamment – que s'il passe à l'échelle continentale, ce qui dépend essentiellement de la capacité des trains à franchir les frontières. Or si les frontières administratives ont considérablement été abaissées, les frontières techniques, elles, sont encore des obstacles bien réels. Je me réjouis donc que ce texte ait fait l'objet d'un accord dès la première lecture, car il est urgent de progresser concrètement dans ce domaine. Avec ce texte, qui fait désormais clairement de l'intéropérabilité ferroviaire une priorité politique, l'espace européen du rail devrait faire un grand pas en avant. 
