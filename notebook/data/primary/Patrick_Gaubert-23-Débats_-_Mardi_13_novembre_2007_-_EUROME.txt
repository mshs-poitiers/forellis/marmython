Patrick Gaubert # Débats - Mardi 13 novembre 2007 - EUROMED (débat) 
  Patrick Gaubert  (PPE-DE
). 
 – (FR) 
Monsieur le Président, mes chers collègues, je voudrais d'abord prendre quelques secondes pour exprimer mon soutien, notre soutien, au commissaire Frattini, et lui dire avec force que les attaques portées contre lui sur la question des Roms en Italie sont intolérables.
Nous connaissons tous les valeurs que défend le commissaire, les valeurs de défense des droits de l'homme, les valeurs qui sont celles de l'Union européenne et, sur ces valeurs, vous êtes un homme inattaquable.
Pour en revenir à Euromed, je me félicite de l'organisation de la conférence ministérielle euroméditerranéenne sur les migrations qui se tiendra en Algarve et qui sera la première du genre, uniquement consacrée à la question des flux migratoires dans la région euroméditerranéenne.
La question des flux migratoires est un point clé qui doit être au centre de notre dialogue, de notre partenariat et de notre coopération avec les pays de la zone euroméditerranéenne, dans la continuité des conférences de Rabat et de Tripoli.
Dans ce domaine, des États membres de l'Union européenne ne peuvent agir seuls. Nous avons besoin du soutien et de la coopération de ces pays partenaires qui sont souvent des pays sources ou des pays de transit, pour rendre notre politique migratoire plus efficace.
Nous devons trouver avec eux un consensus sur la politique du retour, multiplier les accords de réadmission, mener ensemble une politique de codéveloppement intelligente pour que les ressortissants de ces États restent dans leur pays afin de contribuer à sa croissance.
La question de la gestion des flux a une dimension sécuritaire, mais elle a également une dimension humaine qu'il conviendrait de ne pas négliger. C'est pourquoi je souhaite, dans le respect des droits fondamentaux, qu'elle soit l'un des thèmes de cette conférence. Cette réunion, je l'espère, sera l'occasion d'échanges ouverts et réalistes, afin de trouver ensemble les solutions les plus appropriées pour les uns, et pour les autres. 
