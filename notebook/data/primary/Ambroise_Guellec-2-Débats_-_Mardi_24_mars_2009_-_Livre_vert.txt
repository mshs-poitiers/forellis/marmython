Ambroise Guellec # Débats - Mardi 24 mars 2009 - Livre vert sur la cohésion territoriale et état d’avancement du débat sur la future réforme de la politique de cohésion - Meilleures pratiques dans le domaine de la politique régionale et obstacles à l’utilisation des Fonds structurels - Dimension urbaine de la politique de cohésion dans la nouvelle période de programmation - Complémentarité et coordination de la politique de cohésion et des mesures de développement rural - Mise en œuvre des règles de Fonds structurels 2007-2013: résultats des négociations sur les stratégies nationales de cohésion et les programmes opérationnels - Initiative européenne pour un développement du microcrédit en faveur de la croissance et de l’emploi (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Ambroise Guellec  (PPE-DE
). - 
 Madame la Présidente, comme tous mes collègues, je me réjouis de la tenue de ce débat ici aujourd’hui et je voudrais axer mon propos sur la cohésion territoriale. Je dirais qu’il n’est jamais trop tard pour bien faire, mais tout de même, que de temps perdu pour faire de la cohésion territoriale un objectif politique majeur de l’Union européenne. Il y a eu bien sûr les problèmes institutionnels, bientôt levés, je l’espère, et aussi, pardonnez-moi, Madame la Commissaire, l’extrême prudence de la Commission en la matière. Pourtant, il faut rappeler que le Parlement a essayé de pousser les feux constamment pendant tout ce mandat, depuis 2004-2005, jusqu’à maintenant, car ce principe d’équité dans le traitement de tous les citoyens de l’Union européenne, quel que soit le lieu où ils vivent, nous apparaît extrêmement important et nécessite que nous avancions ensemble.
Enfin, le Livre vert est arrivé, nous en sommes heureux. Il me semble quelque peu manquer d’ambition, on aurait aimé que la Commission donnât une définition et des objectifs clairs plutôt que d’interroger de façon très ouverte sur le sujet, mais nous avançons même si, je crois, nous serons à nouveau quelque peu gênés par le lien excessif fait avec la stratégie de Lisbonne, lors de la mise en œuvre de la précédente génération des fonds structurels.
Maintenant la consultation est en cours et j’espère qu’elle aboutira sur le fait qu’il faut accroître les moyens, perfectionner les outils – tout ceci après 2013, on a le temps, mais ça viendra vite –, augmenter les moyens financiers, développer la coopération aux différents niveaux, avoir une vision intégrée du développement, notamment ce problème des politiques sectorielles qui nous pose question, et coordonner la politique agricole commune et le développement régional etc. Et il nous faut le Livre blanc le plus rapidement possible, Madame la Commissaire.
Je voudrais, en conclusion, dire qu’il est urgent de promouvoir la cohésion territoriale dans l’ensemble des régions d’Europe, parce que l’équité spatiale est indispensable pour la sortie de crise et la reprise économique, et aussi et surtout pour donner envie à tous nos concitoyens d’adhérer aux projets européens. 
