Chantal Cauquil # Débats - Mardi 23 septembre 2003 - Votes (suite) 
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. -
 Notre vote négatif concernant "la nomination de Jean-Claude Trichet à la fonction de président de la Banque centrale européenne" s’adresse surtout à l’institution mais aussi à l’homme.
La Banque centrale européenne est chargée d’appliquer en matière de monnaie et de crédit une politique entièrement favorable au grand patronat de l’Union européenne et, par conséquent, entièrement défavorable aux classes populaires. Nous sommes contre la fonction même de cette institution quelle que soit la personne qui la dirige.
Mais nous n’avons pas voulu, non plus, cautionner l’homme. Avant d’être nommé à sa future place, Jean-Claude Trichet a été pendant dix ans gouverneur de la Banque de France. En tant que tel, il porte la responsabilité d’un plan de "restructuration" en cours à la Banque de France, un plan qui se traduit par la fermeture de la moitié des succursales et la suppression de plus de 2 000 emplois, soit plus du quart des effectifs. Un homme assumant de telles besognes ne mérite pas plus le soutien que l’institution dont il s’apprête à prendre la direction. 
