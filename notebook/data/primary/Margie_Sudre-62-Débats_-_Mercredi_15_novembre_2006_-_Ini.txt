Margie Sudre # Débats - Mercredi 15 novembre 2006 - Initiative européenne dans le domaine de la protection civile (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Margie Sudre (PPE-DE
). 
 - La stratégie développée dans l’excellent rapport Barnier pour optimiser les efforts très importants consentis par les Européens en matière d’aide d’urgence pourrait compléter le mécanisme communautaire de protection civile institué en 2001, que la crise soit interne ou externe à l’Union.
Je retiens particulièrement la proposition visant à «appuyer cette force sur les sept régions ultrapériphériques de l’Union», grâce auxquelles l’Europe est humainement et territorialement présente au large des côtes africaines - Canaries, Madère -, dans l’océan Indien - la Réunion -, à proximité du continent américain - Guyane, Martinique, Guadeloupe, Açores -, sans parler des territoires d’outre-mer dans le Pacifique - Polynésie française, Nouvelle-Calédonie.
Il nous faut suivre l’exemple de la Croix-Rouge française qui a créé des équipes de réponse aux urgences humanitaires installées outre-mer, capables de se projeter sur zone en moins de vingt-quatre heures, aptes à fournir hôpitaux de secours, tentes, médicaments, approvisionnement en eau, télécommunications et logistique.
Pour concrétiser l’«Europe des projets», l’Union européenne doit prendre des initiatives portées par une volonté politique claire, compréhensible et forte.
Une protection civile européenne renforcée comporterait une dimension fédératrice permettant une forte identification de l’action de l’Europe, et représenterait une source de fierté pour nos concitoyens, conformément au principe de solidarité cher à notre Communauté. 
