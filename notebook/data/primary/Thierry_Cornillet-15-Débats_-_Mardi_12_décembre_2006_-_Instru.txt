Thierry Cornillet # Débats - Mardi 12 décembre 2006 - Instrument de financement de la coopération au développement - Un instrument de financement de la coopération avec les pays et territoires industrialisés et les autres pays et territoires à revenu élevé(débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Thierry Cornillet (ALDE
). 
 - Monsieur le Président, il est toujours agréable de saluer un consensus et un bon résultat: je me joins donc en ceci à mes collègues. Enfin, la pyramide a été remise sur ses bases: nous avons un instrument de droit commun qui a une fin, 2013, et nous avons une revue à mi-parcours. Nous avons enfin un instrument adéquat. Nous avons évité la confusion des genres: on ne parle pas des pays industrialisés, on ne parle pas des droits de l’homme, on ne parle que de développement. Nous pouvons fixer des objectifs clairs et rappeler nos priorités politiques.
Nous avons, enfin, un instrument juridiquement calé sur, les collègues l’ont rappelé, l’article 179, qui rétablit la procédure de codécision: il ne manquerait plus que le Parlement européen ne puisse pas codécider sur une politique de développement!
Nous avons, enfin, obtenu que les enveloppes financières aillent par programme, de façon à affiner notre contrôle. Je voudrais donc souligner le travail de notre rapporteur Mitchell et l’écoute de la Commission et du Conseil.
Nous avons pris bonne note, Madame, des engagements de la Commission au sujet de notre rôle au moment de la révision à mi-parcours prévoyant le calage financier conséquent, le dialogue pour les documents de stratégie et, je voudrais rappeler nos priorités, la santé et l’éducation.
Pour terminer, je pense que notre Parlement adoptera sans problèmes et sans amendements ce rapport. Par charité d’âme, je n’évoquerai pas ces amendements, certainement téléguidés et à tout coup passéistes et surréalistes. 
