Yves Butel # Débats - Jeudi 26 février 2004 - Votes (suite) 
  Butel (EDD
),
par écrit
. 
- Le groupe EDD déplore avec la majorité que la Commission ne présente pas, malgré des demandes répétées, un exposé clair, pertinent et significatif de l’application des principes de subsidiarité et de proportionnalité. De même, nous approuvons la demande faite à la Commission de s’abstenir d’intervenir dans des domaines où les problèmes, traités au niveau gouvernemental plus proche des citoyens trouveront des solutions mieux appropriées.
Le groupe EDD est également partisan d’un contrôle de la subsidiarité qui associe les parlements nationaux à un stade précoce de la procédure législative. Mais dans cette éventualité, les élus CPNT du groupe EDD ne peuvent accepter que le destin d’une objection que les Parlements nationaux formuleraient officiellement contre une proposition de la Commission soit soumis au bon vouloir de cette dernière pour maintenir, modifier ou retirer le texte incriminé. C’est pourtant la solution préconisée par ce rapport qui s’appuie sur le protocole annexé au projet de constitution. Ce projet a failli sur la délimitation des compétences et n’a cherché, au contraire, qu’à augmenter le champ d’intervention des institutions communautaires. Les élus CPNT s’y opposeront s’il devait voir le jour comme ils s’y opposent aujourd’hui dans le cadre de ce rapport. 
