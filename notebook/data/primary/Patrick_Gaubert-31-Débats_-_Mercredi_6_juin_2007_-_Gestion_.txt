Patrick Gaubert # Débats - Mercredi 6 juin 2007 - Gestion des frontières maritimes de l'Europe - Solidarité européenne et protection des droits des migrants (débat) 
  Patrick Gaubert (PPE-DE
), 
par écrit
. - Combien de cadavres devrons-nous encore repêcher en mer Méditerranée avant que des solutions efficaces soient mises en place?
Arrêtons de pointer du doigt des petits pays comme Malte qui n'arrivent plus à gérer les flux incessants d'immigrés s'échouant quotidiennement sur leur plage. Ce n'est pas ainsi que nous combattrons l'immigration clandestine. Aucun fil barbelé, aucune barrière n'empêchera des hommes et des femmes, prêts à tout pour subvenir aux besoins vitaux de leur famille, de traverser les mers.
En septembre 2006, le Parlement avait déjà lancé un appel aux États membres et à la Commission dans une résolution adoptée par tous les groupes politiques. Nous insistions sur la nécessité de partenariats et dialogues renforcés avec les pays d'origine. Nous avions également exigé la révision du règlement de Dublin II qui impose une charge trop lourde aux pays du Sud et de l'Est de L'Union. Il faut de toute urgence instaurer un mécanisme équitable de solidarité et de partage des responsabilités entre tous les États membres.
De plus, nous ne pouvons que constater le manque de résultats de Frontex. Une réelle volonté politique est fondamentale afin que les États donnent des moyens suffisants à cette agence pour fonctionner efficacement à l'avenir. 
