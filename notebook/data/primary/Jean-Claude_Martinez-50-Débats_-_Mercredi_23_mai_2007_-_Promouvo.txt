Jean-Claude Martinez # Débats - Mercredi 23 mai 2007 - Promouvoir un travail décent pour tous (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Claude Martinez (ITS
). 
 - Madame la Présidente, du travail pour tous, un travail décent qui plus est! Mais c’est quoi? C’est le travail des jeunes, des femmes, des enfants, en Grande-Bretagne ou au Portugal, c’est le travail des migrants, des esclaves dans les missions diplomatiques, notamment du Moyen-Orient, c’est le travail des salariés en France, ce sont les suicides au travail, chez Renault par exemple, ce sont les salaires indécents - 1 000 euros par mois pour les caissières, les maçons, les ouvriers - qui permettent juste de reconstituer la force de travail et, au terme d’une vie d’exploitation, ce sont les retraites de la honte: 130 euros pour le conjoint de l’agricultrice. À 10 euros pour mettre un chien à la fourrière, on ne peut même pas mettre l’agricultrice à la retraite à la fourrière!
Quelles sont les causes? Eh bien les causes, ce sont les nouvelles formes du capitalisme planétaire, qui n’est pas un capitalisme industriel, mais un capitalisme financier, à la recherche d’une rentabilité de 15 %. Pour obtenir un tel profit, le capitalisme des fonds de pension, des fonds spéculatifs, des hedge funds
, exerce trois pressions: sur le salaire, sur les salariés - qui travaillent à flux tendus, sont stressés, d’où les suicides - et sur le nombre de salariés. Une autre cause, c’est l’immigration des travailleurs d’Amérique latine, d’Afrique, à El Ejido en Andalousie, dans les ateliers, dans les restaurants de Barcelone, dans les chantiers. C’est la mondialisation, où le travailleur chinois à 25 centimes d’euro l’heure devient le modèle du travailleur planétaire.
Que faire? Quatre choses: les luttes sociales, les luttes juridiques, à l’OIT, à l’OMC, en faisant preuve d’imagination, notamment avec des droits de douane déductibles, les luttes politiques et, surtout, de la lucidité, appeler les choses par leur nom: le marché dérégulé, c’est le capitalisme et la mondialisation, c’est aussi le capitalisme financier planétaire. 
