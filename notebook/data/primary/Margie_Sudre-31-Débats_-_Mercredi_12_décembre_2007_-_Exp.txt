Margie Sudre # Débats - Mercredi 12 décembre 2007 - Explications de vote 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Proposition de résolution: accords de partenariat économique (B6-0497/2007
) 
  Margie Sudre  (PPE-DE
), 
par écrit
. – 
(FR) 
Les APE ne doivent pas se résumer à de simples accords de libre-échange au sens de l'OMC, et ne doivent surtout pas mettre en difficulté les économies déjà fragiles des collectivités ultramarines.
Ces accords doivent représenter un véritable partenariat permettant d'aménager un nouveau cadre économique et commercial, favorable au développement de l'ensemble des territoires concernés.
Je remercie les membres du Parlement européen d'avoir adopté mon amendement rappelant que les collectivités d'outre-mer sont au cœur de ces accords préférentiels et réciproques avec les pays ACP. La situation particulière des RUP doit impérativement être prise en compte de façon plus cohérente dans le cadre de cette négociation, sur la base de l'article 299, paragraphe 2 du traité. En outre, les PTOM avoisinant des pays ACP doivent également faire l'objet d'une attention spéciale, dans le respect des accords d'association qui les lient déjà à l'Union, au titre de l'article 299, paragraphe 3 du traité.
Même si les discussions actuelles sont difficiles, notamment pour ce qui concerne la protection des marchés locaux et la liste des produits sensibles, je demande à la Commission de trouver des compromis respectueux des intérêts spécifiques des RUP et des PTOM concernés. 
