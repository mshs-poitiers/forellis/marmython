Marie-Hélène Descamps # Débats - Mercredi 31 mai 2006 - Accord de partenariat transatlantique UE/États-Unis d’Amérique - Relations économiques transatlantiques UE/États-Unis d’Amérique (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie-Hélène Descamps (PPE-DE
), 
rapporteur pour avis de la commission de la culture et de l’éducation
. - Monsieur le Président, dans le cadre des relations économiques transatlantiques, il importe de reconnaître le rôle et les spécificités des secteurs de la culture et de l’éducation. C’est donc avec satisfaction que nous constatons la prise en compte, par le rapporteur, de l’ensemble de l’avis de la commission de la culture et de l’éducation.
La «clé culturelle» peut, en effet, contribuer à renforcer nos relations et à favoriser la compréhension mutuelle entre Européens et Américains. Dès lors, l’instauration d’un dialogue consacré à la culture et à l’éducation favoriserait l’échange régulier de bonnes pratiques et d’expériences dans des domaines comme la lutte contre la piraterie, la mobilité des acteurs de la culture, ou encore le développement du tourisme culturel.
Concernant l’éducation, ce dialogue se concentrerait sur le renforcement de la reconnaissance mutuelle des qualifications professionnelles, mais aussi sur les échanges de chercheurs, de professeurs d’université et d’étudiants. Signalons, à ce titre, que le renouvellement prochain du programme de coopération entre nos deux continents dans les domaines de l’enseignement supérieur et de la formation professionnelle fera, lui aussi, beaucoup de bien. En revanche, la spécificité du secteur audiovisuel implique que des échanges transatlantiques aient lieu dans le respect de la diversité culturelle et linguistique de l’Europe.
Sur ce point et pour conclure, regrettons néanmoins que les États-Unis n’aient pas souhaité suivre l’action de l’Union en faveur de la Convention de l’Unesco.
Pour terminer, je féliciterai Erika Mann pour la qualité de son travail et de son écoute. 
