Georges Berthu # Débats - Jeudi 18 décembre 2003 - Votes
  Berthu (NI
),
par écrit
. 
- Je soutiens entièrement les conclusions du rapport Graefe zu Baringdorf, ainsi que celles de mon collègue, M. Souchet, dans son intervention d’hier, sur la coexistence des cultures OGM et non OGM. C’est un problème majeur, qu’on n’a pas le droit de laisser de côté en donnant immédiatement des autorisations de culture et de commercialisation d’OGM, comme l’aurait voulu la Commission.
Il faudrait d’abord étudier soigneusement les risques de contamination des cultures traditionnelles, que 70% des consommateurs veulent au moins préserver. Il faudrait ensuite établir (si c’est possible) des règles de coexistence prudentes, et faire peser les responsabilités et les coûts d’introduction des OGM sur les entreprises bénéficiaires d’autorisations. Enfin, il faudrait prévoir l’existence de garanties financières pour couvrir ces responsabilités.
Or rien de tout cela n’est éclairci aujourd’hui. C’est pourquoi il est révoltant de voir la Commission prête à donner des autorisations, en prenant prétexte de la subsidiarité pour rejeter sur les États la tâche quasi-impossible d’établir les règles de coexistence, et tout en interdisant à ces mêmes États de déclarer certaines zones régionales exemptes d’OGM (décision du 2 septembre 2003 de la Commission à l’encontre de l’Autriche). Ce dossier est traité avec un intolérable mépris des citoyens. 
