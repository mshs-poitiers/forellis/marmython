Arlette Laguiller # Débats - Mercredi 9 avril 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. - Le rapport Turco, comme les initiatives des gouvernements danois et grec qu'il propose de rejeter, soulève la question du contrôle sur Europol. Ce qui en ressort, c'est qu'il n'y a aucun contrôle démocratique sur Europol. Mais comment pourrait-il en être autrement, alors qu'il n'y a aucun contrôle démocratique sur aucune police nationale ?
Dans ce domaine comme dans bien d'autres, l'Union européenne n'est qu'une juxtaposition d'États. Les institutions européennes ne font que reproduire l'absence de contrôle démocratique sur les appareils d'État. On élit des représentants, mais quel est l'État de l'Union européenne où les institutions élues et, à plus forte raison, l'ensemble de la population exercent un contrôle réel sur la police et sur la hiérarchie de l'armée ?
Le Parlement européen en est réduit dans ces conditions, malgré quelques simulacres de propositions qui ne changent rien à rien, à constater son impuissance. Nous nous sommes par conséquent abstenues sur ce rapport. 
