Marie-Noëlle Lienemann # Débats - Mercredi 28 septembre 2005 - Explications de vote
  Marie-Noëlle Lienemann (PSE
), 
par écrit
. - Je voterai contre le rapport de Mr Jarzembowski qui consacre une nouvelle étape de la dérégulation du transport par rail . Déjà la « libéralisation» du fret constituait une menace pour les services publics du chemin de fer mais le passage à celle des voyageurs est inacceptable et lourde de conséquences pour les usagers et l’aménagement du territoire européen.
Au motif d’assurer une meilleure organisation du trafic international, la Commission et le rapport ouvre la concurrence sur les lignes les plus rentables des chemins de fer dans chaque pays privant ainsi les services publics intérieurs des États membres des ressources indispensables à la péréquation tarifaire et à l’aménagement du territoire (lignes moins rentables.... etc.)
Alors que dans de nombreux pays de l’Union européenne, le fonctionnement des lignes voyageurs est de qualité, cette décision va fragiliser leur avenir, réduire les capacités publiques d’investissement. 
