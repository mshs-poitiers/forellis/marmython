Hélène Flautre # Débats - Mercredi 25 mars 2009 - Accord commercial intérimaire avec le Turkménistan - Accord commercial intérimaire avec le Turkménistan (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Hélène Flautre, 
au nom du groupe Verts/ALE
. –
 Monsieur le Président, je crois qu’il faut essayer de ne pas se raconter d’histoire et savoir que quand Valery Pal est libéré, le gouvernement turkmène continue à faire disparaître, emprisonner et torturer, pour délit d’opinion, d’autres personnes au Turkménistan.
Quand le rapporteur spécial des Nations unies sur la liberté de religion va au Turkménistan, neuf autres rapporteurs spéciaux attendent toujours leur autorisation, et pas des moindres: torture, défense des droits de l’homme, indépendance de la justice, éducation, santé, liberté d’expression, etc.
Plusieurs interventions ressemblent vraiment à des exercices d’autopersuasion. Nous restons en présence d’un des régimes les plus répressifs et fermés du monde, même s’il y a des avancées et que nous devons trouver la bonne stratégie pour les soutenir. Nous ne devons pas non plus être naïfs et, évidemment, exiger du Turkménistan qu’il devienne un modèle de démocratie et de droits de l’homme avant d’engager, plus avant, tout accord avec lui.
Alors entre ces deux extrêmes que faire? Je vous propose tout simplement d’avoir une vraie politique de politique étrangère, et de respecter les critères qui sont extrêmement précis, qui sont mesurables, qui sont réalistes et qui sont ceux du Parlement européen. Je pense à l’autorisation d’accès pour les ONG indépendantes et les rapporteurs spéciaux des Nations unies, pour la Croix-Rouge internationale. On sait que des négociations sont en cours mais elles ne sont pas bouclées. Je pense au réalignement du système éducatif – en cours mais encore très loin d’être satisfaisant – sur les standards internationaux à la libération de tous les prisonniers politiques et à leur liberté de mouvement, bref à du b.a.-ba des droits de l’homme. La proposition de mon groupe est à la fois ambitieuse et réaliste. Elle tient en une formule.
(Le Président interrompt l’oratrice pour lui demander de parler moins vite, à la demande des interprètes)

Nous ne pouvons pas être les saboteurs de notre propre politique en renonçant à nos valeurs. Il ne s’agit pas de prôner l’isolement du Turkménistan mais de s’engager avec lui. Alors comment le faire? Eh bien avoir deux crayons, un dans chaque main. Avec le premier crayon, nous allons signer une feuille de route, qui fixe des étapes dans la réalisation des critères qui sont définis par le Parlement. Étapes jalonnées dans le temps, avec des échéances précises, et qui seront discutées lors de la tenue des sous-comités «Droits de l’homme» avec ce pays.
Quand nous aurons signé cette feuille de route, avec l’autre main et l’autre crayon, nous pourrons signer l’accord intérimaire qui est devant nous. Je crois qu’au moment où la Commission et le Conseil discutent de l’avenir des clauses «droits de l’homme», il est absolument nécessaire que ces clauses soient systématiques et qu’elles soient systématiquement assorties d’un mécanisme de consultation qui peut, le cas échéant, conduire jusqu’à la suspension de cet accord. 
