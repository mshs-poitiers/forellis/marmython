Chantal Cauquil # Débats - Jeudi 23 octobre 2003 - Votes
  Bordes et Cauquil (GUE/NGL
),
par écrit
. - Nous nous sommes abstenues sur ce rapport, tout comme sur les deux autres qui concernent la pêche. Vouloir reconstituer les stocks de poissons, en particulier les stocks de cabillaud, qui sont menacés, comme en fait état un autre rapport débattu durant cette session, nous ne pouvons qu’y être favorables. Mais nous ne saurions faire confiance pour cela ni aux institutions européennes, ni aux États nationaux.
Car, ou bien cela se traduit par des mesures velléitaires, dont un rapport affirme qu’elles ont été sabordées par divers États membres de l’Union européenne, avec comme résultat que les ressources en poissons continuent, pour plusieurs espèces, à diminuer de façon dramatique; ou bien cela aboutit à des mesures, présentées comme indispensables pour sauver la nature, mais qui ont pour effet principal, sinon unique, de s’en prendre aux populations, dans le cas présent à celles qui vivent de la pêche. Cela, il faut le souligner, sans que les gros armements aient, eux, quoi que ce soit à craindre. Bien au contraire, comme en témoigne le bilan des programmes d’orientation européens pluriannuels pour les flottes de pêche, présenté à cette session.
Quel sens cela aurait-il d’avoir une nature préservée, si cela devait avoir pour prix le malheur des hommes, en tout cas de la majorité d’entre eux, de ceux qui vivent de leur travail?
(Explication de vote écourtée en application de l’article 137, paragraphe 1, du règlement)

