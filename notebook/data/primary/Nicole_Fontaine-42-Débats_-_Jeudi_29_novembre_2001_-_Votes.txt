Nicole Fontaine # Débats - Jeudi 29 novembre 2001 - Votes
  La Présidente.  
- 
Merci, Monsieur Fernández Martín. Il est bien évident que nous veillerons à ce que toutes les versions linguistiques soient correctement harmonisées.
(La présidente constate qu'il n'y a pas d'opposition à la prise en considération de l'amendement oral)

(Le Parlement approuve la proposition de la Commission ainsi modifiée)

***
Rapport (A5-0387/2001
) de M. Hughes Martin, au nom de la commission des affaires étrangères, des droits de l'homme, de la sécurité commune et de la politique de défense sur la proposition de décision du Conseil concernant la signature d'un accord euro-méditerranéen établissant une association entre les Communautés européennes et leurs États membres et la République arabe d'Egypte [8220/2001 - COM(2001) 184
 - C5-0381/2001
 - 2001/0092(AVC)]

(Le Parlement adopte la résolution législative)

***
Rapport (A5-0397/2001
) de M. Watson, au nom de la commission des libertés et des droits des citoyens, de la justice et des affaires intérieures,

1. sur la proposition de la Commission de décision-cadre du Conseil relative à la lutte contre le terrorisme [COM(2001) 521
 - C5-0452/2001
 - 2001/0217(CNS)]

2. sur la proposition de la Commission de décision-cadre du Conseil relative au mandat d'arrêt européen et aux procédures de remise entre États membres [COM(2001) 522
 - C5-0453/2001
 - 2001/0215(CNS)]

Au sujet de l'amendement 119

