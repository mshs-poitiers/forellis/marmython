Dominique Vlasto # Débats - Jeudi 1 décembre 2005 - Modification du système commun de TVA en ce qui concerne la durée d’application du minimum du taux normal - Modalités du remboursement de la TVA en faveur des assujettis établis dans un autre État membre - TVA sur les services à forte intensité de main-d’œuvre
  Dominique Vlasto (PPE-DE
). 
 - L’Union européenne a fait de l’emploi sa priorité et pourtant elle n’est pas capable de prendre une décision sur l’application du taux réduit de TVA dans les secteurs à forte intensité de main d’œuvre.
En raison d’un blocage persistent au Conseil, il n’est aujourd’hui pas possible de modifier dans les États la liste des secteurs pouvant en bénéficier. Reconduire d’année en année, à la va-vite, une décision du Conseil n’est pas une attitude responsable. L’insécurité, juridique et économique, qui plane sur ces secteurs d’activité n’est plus acceptable.
Dans le BTP, le taux réduit sur la rénovation a permis à des milliers de petites entreprises, premières concernées par cette mesure, d’augmenter leur activité et d’embaucher.
Dans les services à domicile, ce taux réduit a permis de diminuer le travail clandestin et d’améliorer les conditions d’embauche.
Pour la restauration, qui est un important vivier d’emploi en Europe, il est certain que l’application du taux réduit de TVA produira les mêmes effets bénéfiques dans les États qui souhaiteraient appliquer cette mesure.
Je demande donc au Conseil de prendre ses responsabilités et de trouver un accord qui permette de libérer le potentiel d’emploi des secteurs à forte intensité de main d’œuvre. 
