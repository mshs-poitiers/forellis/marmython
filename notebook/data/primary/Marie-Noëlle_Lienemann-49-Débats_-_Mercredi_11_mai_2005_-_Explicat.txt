Marie-Noëlle Lienemann # Débats - Mercredi 11 mai 2005 - Explications de vote
  Lienemann (PSE
),
par écrit
. 
- J’ai voté l’amendement 37 demandant le rejet de ce projet de directive qui conserve les points négatifs de la précédente législation comme l’opt-out
 et engage l’annulation du temps de travail.
Le rapporteur Monsieur Cercas cherche cependant à manifester l’intention du Parlement Européen d’obtenir l’abandon de l’opt-out
 d’ici trois ans, véritable poison pour l’avenir du droit social européen. Je crois nécessaire de le soutenir dans ce but. Je ne peux accepter néanmoins qu’en contre partie il permette le calcul des 48 heures maximum sur la base de 12 mois et permette le calcul et l’annualisation du temps de travail, tant exigée par le patronat ainsi qu’une réduction de la prise en compte du temps de garde.
Ce compromis n’est pas satisfaisant et ne constitue en rien une étape de progrès que nous serions en droit de l’Union Européenne. Il consacre même des reculs.
Nous sommes au début de la procédure législative et le vote en faveur du rapport Cercas n’est qu’un soutien à la démarche engagée pour l’abandon de l’opt-out
. Néanmoins, à la fin de l’étape de codécision, je n’approuverai pas un texte de travail sur une base de 48 heures. 
