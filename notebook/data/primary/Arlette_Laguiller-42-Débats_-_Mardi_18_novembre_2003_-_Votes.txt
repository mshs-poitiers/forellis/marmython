Arlette Laguiller # Débats - Mardi 18 novembre 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. 
- 
Nous avons évidemment voté contre ce rapport dont la seule raison d’être est d’admonester les gouvernements qui n’appliquent pas assez vite leurs propres décisions de liquider les services publics de télécommunications afin d’ouvrir toujours plus ce secteur à la concurrence et à la course au profit.
Le rapporteur s’extasie devant l’entrée des bienfaits de la concurrence dans le secteur concerné. Ces soi-disant bienfaits, ce sont les suppressions d’emplois qui ont déjà eu lieu ou celles à venir, la menace pour les salaires et les conditions de travail, la généralisation de la précarisation, dans les entreprises en voie d’être privatisées - comme France Télécom en France - et le risque de licenciements massifs dans les entreprises privées qui se livrent une guerre commerciale sans merci.
Pour les usagers, c’est un choix peut-être plus diversifié, mais seulement en direction d’une clientèle capable de payer. On supprime des cabines téléphoniques dans bien des endroits où elles ne sont pas rentables mais plus nécessaires qu’ailleurs. Le téléphone devient moins accessible ou alors plus cher, ce qui pénalise ceux qui, dans les catégories sociales aux revenus les plus modestes, ont le plus besoin de ce service, notamment les personnes âgées.
Étant opposées à la privatisation des services publics de télécommunications, nous rejetons ce rapport et la régression sociale à laquelle il contribue. 
