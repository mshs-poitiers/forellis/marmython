Françoise Grossetête # Débats - Jeudi 14 mars 2002 - Votes
  Grossetête (PPE-DE
),
par écrit
. - J'ai voté en faveur de ce rapport.
Force est de constater que le rythme de diminution et de perte de la diversité biologique en Europe et dans le monde ne cesse de s'accélérer. Les mesures en vigueur semblent insuffisantes pour inverser les tendances actuelles, notamment la diminution du nombre des espèces et de leur habitat, des écosystèmes et des gènes.
Il appartient à l'Europe de jouer un rôle particulier d'initiative et d'animation dans ce domaine et de ne pas se contenter d'émettre des vœux pieux qui ne seront pas suivis d'effets.
Dans un premier temps, l'Europe doit garantir l'application de la législation environnementale. En effet, trop d'États membres font, pour des raisons de politique nationale, de la résistance pour transposer rapidement et efficacement certaines directives (notamment les directives "Oiseaux" et "Habitats").
Des actions dans le domaine de l'agriculture et de la pêche sont également à mener. L'agriculture doit jouer un rôle central, et la nouvelle PAC ne pourra faire l'économie de réformes prenant en considération les exigences de développement durable.
Quant à la pêche, il est essentiel de prévoir des mesures cohérentes à court et moyen terme afin de préserver ou de restaurer la diversité biologique là où elle est menacée.
Enfin, préserver la diversité biologique ne signifie pas stopper l'effort de recherche et le développement des connaissances. Aussi, dans le domaine des OGM par exemple, si l'application du principe de précaution est une donnée à ne pas négliger, il convient de soutenir la validation scientifique objective, étape par étape, de ces expériences. 
