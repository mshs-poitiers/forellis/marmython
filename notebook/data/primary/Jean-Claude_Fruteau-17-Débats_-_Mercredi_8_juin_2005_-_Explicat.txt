Jean-Claude Fruteau # Débats - Mercredi 8 juin 2005 - Explications de vote
  Jean-Claude Fruteau (PSE
),
par écrit
. 
- Le rapport de M. Böge représente le mandat de négociation du Parlement dans la perspective des prochaines perspectives financières 2007-2013.
J’ai voté contre ce rapport, pour les raisons suivantes:
1. Le processus de ratification du traité constitutionnel a montré les doutes d’une partie des peuples sur l’efficacité des politiques socio-économiques européennes. Je suis convaincu que l’union européenne doit avoir un budget ambitieux, donnant les moyens aux instances communautaires de remplir cette mission. Le rapport Böge, qui fixe à 1,07% du RNB les crédits de paiement, ne répond pas à ce défi.
2. La logique de restriction budgétaire menace les engagements européens vis-à-vis des agriculteurs. Elle menace également la viabilité de l’OCM Sucre, dont la réforme ne pourra être financée. Je n’accepte pas que le monde agricole devienne la victime des égoïsmes nationaux, que traduit le rapport Böge.
3. L’introduction du cofinancement, qui ouvre la voie à la renationalisation de la PAC, est à mon sens inacceptable. Elle marque l’abandon de la seule politique publique véritablement européenne, à un moment de notre histoire où l’Europe doit se renforcer en mutualisant ses forces. Succomber aux replis nationaux continuerait de creuser le fossé entre les peuples et l’Europe. 
