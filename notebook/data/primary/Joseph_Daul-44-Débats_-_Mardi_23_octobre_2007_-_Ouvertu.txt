Joseph Daul # Débats - Mardi 23 octobre 2007 - Ouverture de la séance
  Joseph Daul, 
au nom du Groupe PPE-DE
. –
 Monsieur le Président, effectivement, je soutiens totalement le collègue Swoboda, puisque nous n'étions effectivement pas au courant; donc nous n'avons rien préparé pour cette semaine, pour cette résolution. Notre groupe n'était pas au courant, comme le groupe du PSE, et on vous demande de reporter le vote à la prochaine session.
