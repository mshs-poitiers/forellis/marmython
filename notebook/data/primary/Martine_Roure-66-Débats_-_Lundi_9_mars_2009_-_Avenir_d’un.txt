Martine Roure # Débats - Lundi 9 mars 2009 - Avenir d’un système européen commun d’asile (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Martine Roure, 
au nom du groupe PSE
. –
 Madame la Présidente, je tenais bien sûr tout d’abord à féliciter notre rapporteur pour son excellent rapport car il pointe, à juste titre, les déséquilibres qui marquent le droit d’asile actuellement en Europe et il formule, bien sûr, un certain nombre de propositions qui permettent véritablement d’avancer dans la bonne direction.
Nous devons mettre fin aux disparités inacceptables qui existent entre les États membres. En effet, selon le pays où l’on dépose sa demande d’asile, on n’a pas la même réponse.
Nous demandons aussi, bien sûr, une amélioration substantielle des conditions d’accueil des demandeurs d’asile. Cela passe, notamment, par l’affirmation du principe selon lequel les demandeurs d’asile ne doivent pas être placés en rétention, en particulier les personnes vulnérables, les femmes, les enfants, les personnes victimes de tortures. Cela passe aussi par un accès garanti à un minimum de droits: logement, emploi, santé, éducation, soit autant de droits fondamentaux pour assurer le respect de la dignité.
Enfin, il est pour nous vital de réformer le système de Dublin II, dont on a pu constater, notamment au travers de nos visites dans les centres de rétention – vous l’avez dit, Monsieur le Vice-Président –, les dommages collatéraux d’un fonctionnement inadapté, dans la mesure où il fait peser un fardeau inacceptable pour les pays les plus directement visés par les flux migratoires aux portes de l’Europe.
Le chemin qui reste à parcourir pour achever une politique commune en matière d’asile est certes encore long. Nous ne devons pas nous bercer d’illusions, mais les nouvelles propositions de la Commission, auxquelles je souhaite que nous apportions notre contribution efficace, permettront, je l’espère, de poser ses premières pierres à un édifice encore bien fragile aujourd’hui mais que nous espérons solide dans l’avenir.
Je remercie sincèrement M. le commissaire Jacques Barrot pour la volonté tenace dont il a fait preuve en la matière, parce qu’il faut de la volonté, il en faut beaucoup. J’espère, Monsieur le Commissaire, que vous pourrez avoir du temps pour mettre en œuvre ce travail, car c’est notre devoir et un impératif moral au nom des valeurs que nous défendons ici, dans l’Union européenne. 
