Gérard Caudron # Débats - Jeudi 16 janvier 2003 - Votes
  Caudron (GUE/NGL
),
par écrit
. - 
Je tiens à féliciter mon collègue Helmuth Markov pour son excellent rapport qui a d'ailleurs été adopté à l'unanimité au sein de sa commission parlementaire. La BERD n'est pas liée à l'Union européenne. Elle n'est pas non plus une banque de développement relevant du groupe de la Banque mondiale. Ses capitaux proviennent de plus de soixante pays intéressés par l'avenir de l'Europe centrale et orientale après la chute du mur de Berlin. Au cours des douze années de transition, la BERD a dû relever de nombreux défis. Son président, Jacques de Larosière, est venu devant le groupe GUE-NGL nous rendre compte de ses activités.
À l'instar du rapporteur je pense qu'il nous faut la soutenir. En effet, même si son objectif est de développer l'économie de marché dans ces anciens pays communistes, les dimensions sociales, environnementales et de service public ne sont pas sacrifiées. La BERD n'est pas comme certains pourraient le croire l'ambassadrice de l'ultra-libéralisme. Son mandat consiste à investir là où d'autres banques n'osent pas franchir ce pas. Je me félicite que la BERD fasse porter l'essentiel de ses activités sur le développement des petites et moyennes entreprises dont on connaît le rôle-clé dans la création d'emplois ! 
