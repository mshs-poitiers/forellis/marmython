Jean Marie Beaupuy # Débats - Lundi 19 mai 2008 - Régions ultrapériphériques (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean Marie Beaupuy, 
au nom du groupe ALDE
. –
 (FR) 
Monsieur le Président, Madame la Commissaire, mes chers collègues, à l'époque où notre planète est devenue un très petit village, puisqu'on se déplace facilement d'un bout à l'autre de notre terre, il faut saisir que ces régions ultrapériphériques sont une grande chance pour l'Union européenne. Comme l'a dit notre rapporteur, Mme Sudre, dans différents paragraphes, nous bénéficions de grands atouts aux niveaux agriculture, pêche, recherche, technologie, tourisme dans nos régions ultrapériphériques. C'est là le plus grand espace maritime avec 25 millions de km2
. Il faut donc s'appuyer sur ces atouts, comme cela est indiqué dans ce rapport, pour que véritablement nos régions ultrapériphériques puissent réussir notre avenir collectif.
Je voudrais prendre comme deuxième point le cas particulier des villes, en tant que président de l'intergroupe Urban avec plusieurs de mes collègues de l'intergroupe Urban ici ce soir, car c'est bien dans les villes que se concentrent les questions de chômage qui sont trois fois plus importantes que dans notre continent. C'est sur les villes que se concentrent les migrations, c'est sur les villes que se concentrent les développements démographiques.
Madame la Commissaire, vous avez là une occasion extraordinaire d'imposer aux différents fonds européens, mais aussi aux différentes politiques, aux différents projets gouvernementaux, régionaux et locaux, l'approche intégrée. Nous n'aurons pas d'efficacité dans ces régions ultrapériphériques, nous n'aurons pas de possibilité d'utiliser au mieux les atouts dont ils disposent s'il n'y a pas le développement de cette approche intégrée.
Merci d'avance, Madame la Commissaire, d'utiliser nos règlements européens pour que l'approche intégrée soit un facteur de succès de tout ce que nous mettons en place. 
