Pervenche Berès # Débats - Mercredi 22 avril 2009 - Mécanisme de soutien financier à moyen terme des balances des paiements des États membres - Mécanisme de soutien financier à moyen terme des balances des paiements des États membres (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Pervenche Berès, 
rapporteure
. −
 Madame la Présidente, le Conseil européen a décidé de doubler la facilité «balance des paiements» en faveur des pays non membres de la zone euro et la Commission a mis sur la table, le 8 avril dernier, les dispositions concrètes pour transformer cette proposition en réalité.
Ici, au Parlement européen, nous voulons prendre nos responsabilités et permettre au Conseil Ecofin d'agir avec célérité car, pour nous, il y a dans cette proposition un signe de solidarité vis-à-vis des pays non membres de la zone euro qui est absolument indispensable au cœur de cette crise.
En novembre dernier, nous doublions déjà cette facilité « balance des paiements » puisqu'elle passait de 12 à 25 milliards d'euros, et qu'aujourd'hui, pour tenir compte à la fois de la réalité, de la sévérité de la crise que nous connaissons et qui affecte tout particulièrement les pays non membres de la zone euro, mais aussi sans doute du calendrier du Parlement européen, nous vous proposons de doubler à nouveau cette facilité pour la porter à 50 milliards, étant entendu que ce que nous avons décidé en novembre dernier a déjà permis de venir en soutien à la Hongrie, à la Lettonie et, hier, à la Roumanie.
Nous pensons que c'est nécessaire et, derrière, nous avons eu entre nous un débat, que je ne vous cache pas, pour savoir si cette appréciation des pays non membres de la zone euro devait uniquement relever d'une approche au cas par cas ou si, en réalité, il y avait au fond une situation plus globale qui était celle des pays non membres de la zone euro.
C'est pourquoi, dans notre résolution, nous proposons de poser la question des conditions de l'élargissement et de vérifier combien l'appartenance à la zone peut être un élément protecteur.
Nous demandons aussi avec beaucoup d'insistance que le Parlement européen puisse être informé de la réalité des analyses des situations de crise que connaissent ces pays car, lorsque la Commission octroie ses prêts aux pays non membres de la zone euro, il y a une lisibilité, il y a une réalité de l'information sur le comportement des banques privées, sur le comportement des acteurs, qui a conduit à cette crise dont le Parlement européen doit être informé.
Enfin, nous considérons que tous les mécanismes possibles pour faire face à cette crise doivent être exploités par la Commission pour pouvoir être mis en œuvre. C'est notamment le cas de l'article 100 du traité qui permettrait aussi de mettre en place des mécanismes exceptionnels pour les pays membres de la zone euro, que nous jugeons indispensable d'explorer du fait de la gravité de la crise.
Deux derniers éléments. Tout d'abord, pour nous, la conditionnalité de ces prêts est un élément de la discussion, nous le comprenons. Généralement, la Commission mène ce travail en harmonie avec le FMI. Ce que nous souhaitons, c'est la mise en place d'un groupe de travail pour regarder comment ces mémorandums sont élaborés et pour regarder comment ces mémorandums tiennent compte de la réalité du pays en question, mais aussi de la stratégie globale de l'Union européenne, notamment lorsqu'il s'agit de l'arbitrage entre investissements, du soutien au pouvoir d'achat, ou de la conditionnalité au regard d'une stratégie pour la croissance verte et le développement durable.
Enfin, nous identifions, dans ce mécanisme, une réalité de la capacité d'emprunt de l'Union européenne et de la Commission sur les marchés internationaux, et nous pensons donc qu'il y a là une piste utile pour fonder un débat que nous menons par ailleurs sur l'emprunt européen et sur la capacité de l'Union européenne à financer des stratégies et des investissements du futur à travers cet emprunt.
Dernier mot, la Commission a mis en place un nouveau mécanisme pour s'assurer que les remboursements permettent au budget de l'Union européenne de faire face à ces exigences. Nous soutenons cette modification de la régulation et nous espérons que ce Parlement apportera un soutien massif à cette proposition utile. 
