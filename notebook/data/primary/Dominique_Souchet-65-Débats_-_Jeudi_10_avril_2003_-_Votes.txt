Dominique Souchet # Débats - Jeudi 10 avril 2003 - Votes
  Souchet (NI
),
par écrit
. - L'examen de ce texte sur les essais comparatifs et la commercialisation des semences doit être l'occasion de rappeler quelques lignes directrices concernant les semences.
Tout doit être fait pour préserver la biodiversité des plantes cultivées. Laissée à la seule libre concurrence, la sélection génétique a tendance à se concentrer entre les mains de quelques entreprises, le nombre de variétés mises sur le marché se restreint et les variétés locales disparaissent. Les firmes semencières ont bien naturellement tendance à privilégier les variétés captives, hybrides ou génétiquement modifiées, qui leur permettent d'assurer la rentabilité de leurs obtentions. Il appartient donc aux États membres de maintenir la dimension de service public dans la sélection végétale, d'assurer la pérennité des variétés locales menacées et de permettre la multiplication à la ferme des semences.
Il faut enfin faire preuve de la plus extrême prudence envers l'enrobage des semences avec des produits phytosanitaires, technique qui peut se révéler nuisible pour l'environnement et la faune : on le voit particulièrement dans le cas des enrobages contenant de puissants insecticides systémiques, qui ont des effets désastreux sur les insectes pollinisateurs, en particulier les abeilles domestiques. 
