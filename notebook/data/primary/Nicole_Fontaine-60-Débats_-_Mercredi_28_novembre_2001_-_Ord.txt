Nicole Fontaine # Débats - Mercredi 28 novembre 2001 - Ordre du jour
  La Présidente.  
- Merci, Monsieur Souladakis.
Chers collègues, étant donné que j'ai encore à peu près une dizaine de motions de procédure, dont la plupart sur ce sujet, que cette affaire devient presque un débat à elle seule, je prends la responsabilité d'interrompre les motions de procédure pour que nous passions au débat sur la préparation du Conseil de Laeken, étant bien entendu que les interventions continueront naturellement dès que le débat sera clos. 
