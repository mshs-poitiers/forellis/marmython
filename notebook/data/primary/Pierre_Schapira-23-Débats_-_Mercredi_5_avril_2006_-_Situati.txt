Pierre Schapira # Débats - Mercredi 5 avril 2006 - Situation au Proche-Orient suite aux élections en Israël (débat) 
  Pierre Schapira (PSE
). 
 - Monsieur le Président, tout d’abord quelques constats. Premièrement, le problème démographique a été au cœur du débat politique; deuxièmement, c’est la fin de l’utopie du Grand Israël; troisièmement, la question sociale a été au cœur du débat en Israël et, quatrièmement, le Hamas n’a pas influencé les élections en Israël.
Il y a eu chez les Israéliens, inconsciemment, une prise en compte définitive de deux États. L’heure devrait être maintenant à la négociation, mais il faut agir sur le Hamas afin qu’il rende caduque sa charte et reconnaisse enfin Israël pour devenir un interlocuteur valable. Sinon, mes chers collègues, il y aura une politique unilatérale des deux côtés.
Les opinions publiques des deux pays sont pour la paix. Il faut que l’Europe profite de cette situation car son rôle est désormais primordial. L’Europe est attendue et, en ce qui concerne les aides aux Palestiniens, je réaffirme qu’il faut que l’Europe continue à verser son aide, sinon ce sera la catastrophe en Palestine. 
