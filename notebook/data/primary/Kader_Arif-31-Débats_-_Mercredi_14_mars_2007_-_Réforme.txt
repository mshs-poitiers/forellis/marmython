Kader Arif # Débats - Mercredi 14 mars 2007 - Réforme des instruments de la politique commerciale de l’UE (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Kader Arif (PSE
). 
 - Monsieur le Président, chers collègues, je tiens à remercier nos collègues Barón Crespo et Guardans Cambó d’avoir pris l’initiative de cette question orale à la Commission.
En effet, le thème de nos instruments de défense commerciale constitue un enjeu capital, non seulement pour assurer la protection effective des producteurs européens contre des formes de concurrence déloyale, mais également dans le cadre du débat plus large sur la place de l’Union européenne dans une économie mondialisée et sur les règles qu’elle souhaite promouvoir pour la réguler.
À cet égard, alors même que l’Union européenne a toujours défendu le système multilatéral de l’OMC, il semble pour le moins surprenant que la Commission lance une telle consultation publique et envisage une réforme potentiellement importante de nos instruments de défense, alors que les négociations à l’OMC sur les mesures antidumping, antisubventions et de sauvegarde n’ont pas encore abouti et que leurs résultats auront un impact sur la façon dont ces instruments seront utilisés.
Je tiens ainsi à rappeler à la Commission qu’elle a elle-même commandé une étude sur l’évaluation des instruments européens de défense commerciale et que celle-ci a conclu que le statu quo est à la fois la solution la plus raisonnable et la plus adéquate pour répondre aux préoccupations de toutes les parties. Cette étude soutient également l’idée qu’il n’y a visiblement aucun besoin urgent à l’heure actuelle de revoir ou d’altérer les instruments communautaires de défense commerciale existants.
J’aimerais donc savoir quels changements concrets la Commission envisage et comment le Parlement sera associé à tous les stades de ce processus. J’appelle donc la Commission à prendre en compte ces différents éléments, de même que l’avis des députés et les résultats de la consultation publique qu’elle a lancée pour la rédaction de ses futures propositions, dans le cadre des prochaines discussions au Conseil. 
