Kader Arif # Débats - Mercredi 8 octobre 2008 - Suspension du cycle de Doha de l’OMC (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Kader Arif  (PSE
). - 
 Madame la Présidente, chers collègues, depuis 2001, les pays en développement négocient à l’OMC un cycle annoncé comme devant être celui du développement. Affrontant aujourd’hui une crise alimentaire brutale et dévastatrice, alertant le monde de l’urgence de trouver une solution globale et équilibrée à long terme, ces pays attendent de nous des réponses claires pour garantir leur sécurité alimentaire.
Je tenais à réaffirmer que, si le cycle devait rester celui de l’accès au marché à tout prix, nous n’atteindrions pas notre objectif. De surcroît, nous savons que, plus la conclusion d’un accord en faveur du développement sera repoussée, plus la perspective d’atteindre les objectifs du Millénaire pour le développement s’éloignera et nous sommes malheureusement déjà bien en retard.
Face à cette situation de crise, nous demandons que soit trouvée au plus vite une solution politique au mécanisme de sauvegarde spécial, afin de produire un outil efficace de protection des petits agriculteurs dans les pays pauvres. C’est une étape indispensable avant de poursuivre des négociations sur les autres aspects, et j’espère que la récente reprise des discussions sur l’agriculture et les NAMA permettra une avancée en ce sens.
Je souhaiterais, pour finir, évoquer les amendements déposés au texte de la résolution commune. Le groupe PSE appellera bien entendu à voter en faveur de l’amendement 2, qui est absolument essentiel pour accroître enfin les droits du Parlement en matière de commerce international.
Nous soutiendrons également les amendements déposés par le groupe des Verts, mais nous ne pouvons accepter celui du PPE, car nous estimons qu’il n’est pas opportun, dans cette résolution qui porte sur les négociations multilatérales, d’appeler à la conclusion de nouveaux accords bilatéraux régionaux dont on sait qu’ils sont le plus souvent négociés au détriment des plus faibles. 
