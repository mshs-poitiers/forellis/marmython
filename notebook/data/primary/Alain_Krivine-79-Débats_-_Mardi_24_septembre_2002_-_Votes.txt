Alain Krivine # Débats - Mardi 24 septembre 2002 - Votes (suite) 
  Krivine et Vachetta (GUE/NGL
),
par écrit
. 
- 
Nous votons pour les amendements défendus par notre collègue Jonas Sjöstedt sur la proposition de règlement du Parlement européen et du Conseil relatif aux mouvements transfrontières des OGM. En effet, le projet présenté par la Commission est en retrait par rapport au protocole de Carthagène du 29 janvier 2000 sur la prévention des risques biotechnologiques ; il prend trop peu en compte le principe de précaution inscrit dans la déclaration de Rio sur l'environnement ainsi que les débats en cours en Europe même sur ces questions.
La Commission fait une interprétation a minima
 du protocole de Carthagène (issu d'un compromis), alors qu'il est pourtant clair que ce dernier repose avant tout sur le principe de précaution. Une telle approche ne peut que contribuer à vider l'accord de Carthagène de son contenu, au moment même où sa ratification par les États est en cours. L'affaire est sérieuse. Nous venons une nouvelle fois de voir, lors de la conférence de Johannesbourg sur le développement durable de fin août - début septembre, qu'en l'absence de volonté politique fermement affichée, les intérêts particuliers de l'agro-industrie et des multinationales, ainsi que les logiques purement commerciales, l'emportent sur l'intérêt général des populations.
L'Union européenne doit utiliser le poids international qui est le sien non pour diluer la dynamique initiée par Carthagène mais pour la renforcer. Il en va de sa responsabilité. 
