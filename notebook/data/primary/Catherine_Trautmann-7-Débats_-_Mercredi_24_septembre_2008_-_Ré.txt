Catherine Trautmann # Débats - Mercredi 24 septembre 2008 - Réseaux et services de communications électroniques (A6-0321/2008, Catherine Trautmann) (vote)
  Catherine Trautmann, 
rapporteure
. −
 Monsieur le Président, nous avons une situation relativement difficile à propos de l'amendement 138 où trois demandes de vote par division ont été déposées. J'avais personnellement déposé pour la première partie de l'amendement un amendement oral qui figure dans les listes de vote et qui permet de dire: applying the principle that no restriction may be imposed on the fundamental rights and freedoms of end-users.
 C'est donc ce premier amendement oral. Depuis, nous avons cherché une solution qui puisse être acceptée par les groupes politiques à propos de la troisième partie de cet amendement.
Je propose donc un amendement oral nouveau, en accord avec les signataires, notamment la présidente de la commission ITRE, Mme Niebler, et d'autres, qui disposeraient: save when public security is threatened where the ruling may be subsequent
. Ceci remplacerait donc la troisième partie et précise le point suivant: la troisième partie proposée par notre collègue Bono, crée une ambiguïté sur l'expression d'exception. Avec la précision qui est apportée dans l'amendement oral, nous avons donc un amendement lisible et acceptable. Il n'a d'ailleurs – je le précise, pour notre collègue Ruth Hieronymi – rien à voir avec la propriété intellectuelle dont elle vient de parler.
