Bruno Gollnisch # Débats - Jeudi 23 octobre 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
− Proposition de résolution: (RC-B6-0571/2008
) 
  Bruno Gollnisch  (NI
). 
– 
Madame la Présidente, nous avons commémoré l’Holodomor
, la destruction méthodique de la paysannerie ukrainienne par la famine, et notre Parlement a reconnu, comme vient de le faire aussi notre collègue à l’instant, que c’était un génocide.
Je voudrais juste souligner ici le fait que les auteurs de ce génocide ont siégé parmi les juges de la civilisation à Nuremberg, ce qui devrait permettre, aujourd’hui, de discuter de la composition, de la procédure et des conclusions du procès de Nuremberg. Or, les intellectuels, qui y procèdent aujourd’hui en Europe, sont arrêtés, détenus, pourchassés, ruinés, poursuivis, jetés en prison... Pire, leurs avocats, qui présentent également les mêmes conclusions, sont pourchassés de la même façon.
Dans le pays de M. Pöttering, par exemple, ils sont poursuivis et arrêtés selon des procédures qui rappellent les procès staliniens. Nous avons remis le prix Sakharov de défense de la liberté d’expression à un dissident chinois, nous aurions aussi bien pu le remettre à certains Européens tels que, par exemple, la courageuse avocate allemande, Sylvia Stolz. 
