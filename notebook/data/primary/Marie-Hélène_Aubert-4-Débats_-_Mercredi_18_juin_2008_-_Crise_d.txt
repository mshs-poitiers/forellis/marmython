Marie-Hélène Aubert # Débats - Mercredi 18 juin 2008 - Crise du secteur de la pêche due à l'augmentation du prix du gazole (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie-Hélène Aubert, 
au nom du groupe Verts/ALE. 
– 
Monsieur le Président, Monsieur le Commissaire, comme vous l'avez rappelé, cette crise structurelle est profonde et durable. Il s'agit donc de trouver des solutions durables et pas seulement pour le secteur de la pêche, mais pour l'ensemble des secteurs concernés.
Il faut dire aussi que cette crise est le résultat d'années d'aveuglement sur la dépendance, justement, des pêcheries au pétrole - et un pétrole bon marché -, et sur la fuite en avant vers ce qu'on a appelé la course aux armements, avec des bateaux de plus en plus puissants, qui vont de plus en plus loin et qui peuvent ramener de plus en plus de poisson. C'est bien ces questions-là qu'il nous faut également traiter.
Le problème du gazole cher est indissociable de toutes les autres questions qui touchent le secteur de la pêche - la gestion de la ressource halieutique, la formation des prix, le commerce mondial, la lutte contre la pêche illégale -, et il est difficile de traiter cette question isolément du reste.
Les subventions, les aides que la Commission propose et qui me paraissent aller dans le bon sens ne sont acceptables, notamment pour nos concitoyens, que si elles sont conditionnées à une réorientation en profondeur des politiques de la pêche et des pratiques de pêche. Pour notre part, nous regrettons que la résolution commune de compromis ne conditionne pas justement les aides et les subventions possibles à cette réorientation et à la nécessité de mettre un terme à la surcapacité des flottes et d'aller dans le sens d'une meilleure gestion de la ressource halieutique et de la protection des écosystèmes marins. C'est d'ailleurs la condition même de la viabilité économique, sociale, des entreprises de pêche. Enfin, nous souhaitons que les États membres assument pleinement leurs responsabilités, qu'ils cessent de faire de la démagogie en promettant de l'argent qu'ils n'ont pas sans proposer de perspectives durables aux pêcheries. 
