Patrick Louis # Débats - Mardi 16 décembre 2008 - Échange de quotas d’émission de gaz à effet de serre (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Patrick Louis  (IND/DEM
).  
– Madame la Présidente, chers collègues, respect de l’environnement et compétitivité ne doivent pas être dissociés. Ensemble, ils sont sources de performance. Malheureusement, le dispositif présenté par notre Assemblée restera un compromis peut-être inefficace car il lui manque deux choses.
Il manque la mise en place de droits compensateurs aux frontières. Sans cette clé de voûte, nos exigences ne sont pas compensées au niveau mondial. Elles deviennent des coûts de production supplémentaires, des freins à l’expansion, des accélérateurs de fuite de carbone, des importations détruisant nos emplois. Sans droits compensateurs aux frontières de l’Union, nos qualités risquent de devenir des défauts.
Il manque également une politique audacieuse d’affectation des droits carbone à la forêt et à la filière bois. Cette prise en compte aurait valorisé les forêts des pays en voie de développement, limité les émissions de CO2
 et promu le plus élémentaire des puits à carbone.
Ces deux remarques de bon sens font que ce rapport plein de bonnes intentions pourrait n’être qu’un grand gaspillage d’énergie.
