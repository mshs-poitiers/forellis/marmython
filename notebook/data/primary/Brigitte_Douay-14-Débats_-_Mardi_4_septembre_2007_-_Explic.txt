Brigitte Douay # Débats - Mardi 4 septembre 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Toubon (A6-0295/2007
) 
  Brigitte Douay  (PSE
), 
par écrit
. – 
Les socialistes français ont voté en faveur du rapport Toubon sur le réexamen du marché unique et son adaptation au XXIe siècle, car il rappelle des éléments importants, parfois mis de côté lorsque l'on évoque la réalisation du marché intérieur. Ainsi, la cohésion sociale et territoriale et des services d'intérêt général modernes vont de pair avec l'achèvement du marché intérieur; nous avons néanmoins voté contre le paragraphe 24 qui a trait à la libéralisation des services postaux. Le rapport insiste aussi sur la protection des consommateurs, étroitement liée à l'achèvement du marché intérieur européen, et qui doit être garantie. Enfin, la dimension environnementale doit être intégrée aux exigences du marché intérieur, qui est un atout pour l'Union européenne à condition que ces grands principes soient respectés.
Un point important du rapport porte sur les PME et la facilitation de leur accès au grand marché intérieur européen en raison de leur rôle essentiel et de celui de l'artisanat sur l'emploi et l'innovation en Europe.
Je soutiens en particulier la volonté manifestée par le rapporteur de lutter activement et efficacement contre la contrefaçon, véritable fléau pour l'économie européenne. 
