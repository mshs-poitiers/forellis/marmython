Bruno Gollnisch # Débats - Mardi 21 octobre 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
– Rapport: Georgios Papastamkos (A6-0354/2008
) 
  Bruno Gollnisch  (NI
), 
par écrit
. – 
L’Union européenne compte 29 agences, véritables micro-institutions européennes, dont le coût s’élève à plus d’un milliard d’euros et dont l’utilité est sujette à caution. Le rapporteur a donc raison de demander plus de transparence et plus de responsabilité dans la gestion de ces multiples agences, un réel contrôle politique sur leur activité, une évaluation de celles qui existent, un moratoire sur la création de nouvelles, une analyse «coût-avantage» avant toute décision.
Mais le véritable problème est l’existence même de ces agences, de ces couches supplémentaires de bureaucratie européenne dont certaines ont un pouvoir réglementaire, d’autres des fonctions exécutives qui interfèrent avec le travail des administrations nationales, quand elles ne le compliquent pas. Le véritable problème est leur prolifération, leur dispersion à travers l’Europe, leurs sièges étant distribués comme autant de cadeaux clientélistes. Le véritable problème est que 40 % d’entre elles sont fondées en vertu de l’article 308 du Traité, ce fameux article qui permet d’accroître les compétences de Bruxelles quand elles ne sont pas expressément prévues par les textes.
Parce que ce rapport ne résout rien, nous ne pouvons l’approuver. Mais parce qu’il est, quand même, une tentative de mettre un peu d’ordre dans ce fatras, nous ne pouvons le rejeter. C’est pourquoi nous nous abstiendrons. 
