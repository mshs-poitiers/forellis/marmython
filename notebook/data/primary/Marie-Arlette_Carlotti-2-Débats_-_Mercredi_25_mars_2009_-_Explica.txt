Marie-Arlette Carlotti # Débats - Mercredi 25 mars 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport David Martin (A6-0117/2009
) 
  Marie-Arlette Carlotti  (PSE
), 
par écrit
. – 
Depuis des années, aux côtés des collègues africains, les socialistes se sont mobilisés pour faire des APE de vrais instruments de développement.
Nous avons négocié avec force et détermination avec la Commission pour obtenir des accords justes, au service des Objectifs de développement du Millénaire.
En jouant la carte d’une régionalisation choisie et conduite par les ACP eux-mêmes.
En tenant nos engagements sur l’aide au commerce promise en 2005 plutôt que la poursuite du «pillage « du FED.
Aujourd’hui notre combat a porté en grande partie ses fruits puisque la commissaire au commerce s’est engagée au nom de la Commissions européenne sur:
- l’objectif essentiel de développement des accords,
- la renégociation des points litigieux des accords dans une approche ouverte et flexible,
- la sécurité alimentaire et la protection des industries fragiles des pays ACP.
Bien sûr nous aurions souhaité plus de garanties sur l’implication des Parlements nationaux et de l’Assemblée ACP-UE dans le contrôle de la mise en œuvre des accords.
Mais en quelques semaines, les avancées sont considérables.
J’en prends acte.
Mais je veux garder toute ma vigilance pour leur mise en œuvre.
Donc pas de «blanc-seing»: abstention. 
