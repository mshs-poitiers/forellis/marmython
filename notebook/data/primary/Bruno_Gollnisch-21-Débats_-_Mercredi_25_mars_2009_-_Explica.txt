Bruno Gollnisch # Débats - Mercredi 25 mars 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Proposition de résolution B6-0141/2009
 (APE - États du Cariforum) 
  Bruno Gollnisch  (NI
), 
par écrit
. – 
Monsieur le Président, mes chers collègues, nous n’avons voté aucun des textes sur les nouveaux accords de partenariat avec les pays ACP, les plus pauvres de la planète.
Notre vote n’est pas dirigé contre ces pays, qui méritent une véritable politique de coopération et de développement qui les aide à sortir leurs populations de la pauvreté, leurs territoires du sous-équipement et leurs économies des difficultés. Ils méritent aussi des relations commerciales tenant compte de leurs situations particulières, mais également de nos propres intérêts, et notamment de ceux de nos régions ultrapériphériques, oubliées dans vos politiques.
Or, ce que vous leur proposez, ce sont des accords conformes aux sacro-saintes règles de l’OMC, visant à les insérer dans la mondialisation ultralibérale. Vous les condamnez aux cultures d’exportation qui les affament et à l’exploitation de leurs richesses par des multinationales qui ne sont plus depuis longtemps au service de tel ou tel pays, mais apatrides, anonymes et mues par leurs seuls intérêts financiers.
Ces pays ont le droit de choisir le rythme d’ouverture de leurs frontières et de libéralisation de leurs économies. Et pourquoi pas, d’opter pour une autre voie, celle du protectionnisme raisonnable et des relations mutuellement profitables, car fondées sur la réciprocité. C’est la voie que nous demandons aussi pour la France et l’Europe.
