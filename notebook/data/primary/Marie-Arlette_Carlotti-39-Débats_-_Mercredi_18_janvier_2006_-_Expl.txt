Marie-Arlette Carlotti # Débats - Mercredi 18 janvier 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Résolution RC-B6-0025/2006

  Marie-Arlette Carlotti (PSE
), 
par écrit
. - Les pays européens semblent victimes d'une inquiétante contagion homophobe.
Une homophobie d'autant plus choquante qu'elle paraît «décomplexée» et sévit tous azimuts: violences verbales ou physiques, persistance de discriminations, ou introduction de nouvelles (le parlement letton a déposé un amendement à la Constitution pour interdire les mariages entre personnes du même sexe...).
Masquée ou assumée, l'homophobie n'a pas sa place dans l'Union européenne.
La non-discrimination est garantie dans les textes (traités, Convention européenne des droits de l'homme et Charte des droits fondamentaux).
Elle doit l'être dans les faits.
Et c'est tout à l'honneur de ce Parlement européen que d'avoir toujours été à l'avant-garde de ce combat. Il s'est engagé pour faire avancer les droits et reculer les discriminations (février 1994, septembre 1996, juillet 2001, juin 2005).
Il doit à nouveau le faire aujourd'hui.
C'est l'ambition de cette résolution. Elle est sans ambiguïté dans sa condamnation de la discrimination sous toutes ses formes. Elle est volontariste, pour demander de compléter l'arsenal «antidiscrimination» sur la base de l'article 13 du traité, et une communication sur les obstacles à la libre circulation des couples homosexuels bénéficiant d'une reconnaissance légale dans l'Union.
Elle est un signal politique clair et nécessaire. Je la voterai avec détermination et conviction. 
