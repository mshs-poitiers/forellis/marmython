Nathalie Griesbeck # Débats - Mercredi 1 février 2006 - Application de la directive postale
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Nathalie Griesbeck, 
au nom du groupe ALDE
. - Monsieur le Président, mes chers collègues, je tiens à m’associer d’emblée aux remerciements formulés à l’égard de notre collègue Markus Ferber pour son rapport d’initiative. Permettez-moi d’évoquer mon point de vue sous deux angles.
Tout d’abord, avec mon groupe de l’alliance des démocrates et des libéraux pour l’Europe, je me réjouis que ce rapport nous donne dès aujourd’hui la possibilité de discuter du cadre juridique que notre Union devrait mettre en œuvre d’ici à 2009 pour organiser la libéralisation des services postaux. Ensuite, je soutiens la méthode proposée qui nous permet une approche à la fois maîtrisée et équilibrée du processus de modernisation de ce secteur.
Cependant, mes chers collègues, si cette libéralisation a pour objectif de poursuivre, comme on vient de l’expliquer, la modernisation du secteur d’activité concerné et d’améliorer les services rendus aux clients, nul n’ignore l’enjeu de la réforme ni les craintes légitimes de nos concitoyens concernant le maintien d’un service de qualité et d’une couverture de l’ensemble des territoires, y compris les plus reculés. J’attends donc, avec grand intérêt, l’étude d’impact économique que la Commission européenne doit présenter au cours de cette année. Elle devrait se fonder sur des données économiques fiables et les résultats de consultations avec toutes les parties intéressées, des syndicats aux chambres de commerce et d’industrie, en passant par tous les partenaires sociaux concernés.
J’attire par ailleurs l’attention de la Commission, et aussi du Conseil, sur la nécessité de lutter contre les distorsions de concurrence de toutes sortes. Je songe notamment à l’assujettissement à la TVA et à l’harmonisation des taux.
Pour conclure, je compte sur la Commission pour proposer des mécanismes de financement du service universel pertinents et justes, sans préjuger des choix que le Parlement devra faire puisqu’en dernier ressort, mes chers collègues, c’est bien au Parlement, notre organe démocratique, qu’il appartient d’expliquer aux 450 millions d’Européens que l’Europe forte que nous voulons construire ensemble est la leur. 
