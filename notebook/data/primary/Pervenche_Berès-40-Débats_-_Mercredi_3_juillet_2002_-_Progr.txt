Pervenche Berès # Débats - Mercredi 3 juillet 2002 - Programme d'activité de la Présidence danoise
  Berès (PSE
). 
 - Monsieur le Président, la présidence espagnole avait comme priorité la lutte contre le terrorisme. Vous avez, quant à vous, l'élargissement comme priorité. Politiquement : bravo ! Cependant, gardons-nous de croire que, dans ce tourbillon semestriel, nos citoyens se laissent aveugler et ne malmenons pas ce qui, pour la majorité d'entre eux, constitue un acquis fondamental de l'Union européenne : je pense bien évidemment au passage à l'euro. De ce point de vue, il nous reste encore beaucoup à faire. Nous avons besoin, d'abord et avant tout, d'une vraie coordination des politiques économiques pour que l'euro s'accompagne de croissance et de création d'emplois.
Votre pays, Monsieur le Président, est en opting out
. Nous souhaitons bien évidemment pour le peuple danois que cette situation évolue. Mais dites-nous comment, dans ces conditions, organiser le leadership
 s'agissant de questions requérant volonté et détermination politique pour avancer ? Quelles conséquences en tirez-vous pour le fonctionnement de nos institutions, et comment comptez-vous organiser la présidence dans ce domaine ? 
