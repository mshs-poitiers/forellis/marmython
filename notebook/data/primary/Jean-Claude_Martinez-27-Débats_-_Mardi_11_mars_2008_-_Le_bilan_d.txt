Jean-Claude Martinez # Débats - Mardi 11 mars 2008 - Le bilan de santé de la PAC (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Claude Martinez  (NI
). 
 – (FR)
 Monsieur le Président, Madame la Commissaire, Monsieur Goepel, à Berlin - il y aura bientôt dix ans - le Chancelier Schmidt et le Président français Jacques Chirac avaient décidé d'accorder un sursis aux paysans jusqu'en 2013. Avec la fin des quotas laitiers, la fin d'un vrai budget agricole dans le cadre financier 2014-2019, et le départ à la retraite non remplacé d'une majorité de producteurs, la fin d'une grande PAC avait été programmée à l'horizon 2013, simplement.
Vous remettez aujourd'hui en cause cet accord et, sous le nom de «bilan de santé de la PAC», vous accélérez la marche vers l'élimination de notre agriculture au sein du premier pilier. Ce n'est pas nouveau! MacSharry, Steichen, Fischler, à coup de quotas, de QMG, de primes Hérode, de jachères, de découplages, d'arrachages, avaient, depuis trente ans déjà, pratiqué cette stratégie qui consiste à laisser le monopole de l'alimentation du monde à l'hémisphère sud et à des États du Pacifique.
Aujourd'hui, dans les débats techniques passionnants sur le découplage intégral, la modulation - dégressive, progressive -, l'éco-conditionnalité, le développement rural, la référence - historique, non historique - ne font que cacher la seule et unique vraie question, alors que la Chine et l'Inde vont lancer un appel d'offres alimentaire planétaire, alors que le matin des paysans arrive avec la montée des prix et de la demande. L'Europe veut-elle, oui ou non, être encore avec le Brésil une des grandes fermes du monde?
(Le Président retire la parole à l'orateur)

