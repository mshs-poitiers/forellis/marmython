Marielle de Sarnez # Débats - Mercredi 25 septembre 2002 - Votes
  De Sarnez (PPE-DE
),
par écrit
. - L'excellent rapport de Miet Smet est l'occasion encore une fois de déplorer le manque de statistiques européennes sur le nombre de femmes dans les organisations de partenaires sociaux.
Même si beaucoup de stratégies ont été mises en œuvre pour remédier à la sous-représentation des femmes aux échelons supérieurs de ces organisations, ainsi que dans les délégations à la concertation sociale et aux organes consultatifs, il faut, comme le préconise le rapporteur, entreprendre au niveau européen une collecte systématique de données sur le rôle que les femmes jouent dans ce processus de décision. Il est toutefois certain que l'application et l'efficacité des mesures prises demeurent entièrement tributaires de la volonté politique au sein des organisations.
L'utilité des campagnes de sensibilisation organisées à l'occasion des élections sociales et la création de réseaux mettant en contact les négociateurs féminins pour contribuer à promouvoir la représentation féminine sont avérées.
Après la lecture de cet excellent rapport, je ne peux que vous recommander de voter en sa faveur et rappeler combien il est urgent et nécessaire que la Commission entreprenne véritablement de rassembler des données en vue de la création d'une base de données sur la représentation des femmes au sein des organisations de partenaires sociaux.
(Explication de vote écourtée en application de l'article 137, paragraphe 1, du règlement)

