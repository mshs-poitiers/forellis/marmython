Georges Berthu # Débats - Jeudi 1 avril 2004 - Votes
  Berthu (NI
),
par écrit
. 
- Le rapport Oostlander qui évalue "les progrès réalisés par la Turquie sur la voie de l’adhésion" est bien obligé d’aligner les jugements négatifs sur chaque point particulier, mais ne se résout pas à donner une conclusion globale qui soit négative. Si l’on ne veut pas rediriger la Turquie vers un partenariat privilégié, comme je l’ai proposé dans mon intervention au cours du débat, où est l’issue?
Le rapport Oostlander montre timidement une direction inattendue: la réforme interne des politiques et des institutions de l’Union elle-même (voir considérant G et paragraphe 36). Cette idée mérite considération, d’autant qu’elle peut se rapprocher d’une suggestion que nous avons faite nous-mêmes par le passé: une Europe à géométrie variable accueillerait plus facilement la Turquie comme partenaire, car elle établirait des relations libres entre pays souverains, et ne conduirait pas à des situations explosives de subordination à un système de décisions supranational dans lequel la Turquie pèserait d’un poids très lourd.
Malheureusement, le rapport Oostlander, après avoir esquissé quelques pas dans cette voie, repart dans la mauvaise direction, puisqu’il propose comme solution à tous les problèmes... le renforcement du système supranational, par l’adoption de l’actuel projet de constitution européenne. Les fédéralistes tournent en rond. 
