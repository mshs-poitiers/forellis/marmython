Armonia Bordes # Débats - Mercredi 14 janvier 2004 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. 
- 
L’objet de ce rapport est de protéger la pêche et l’industrie de la transformation du thon de l’Union européenne contre la concurrence venue d’ailleurs. Cette préoccupation est d’autant plus contestable que la flotte thonière communautaire est déjà la plus importante du monde et qu’elle exploite les ressources halieutiques de la planète entière, dont celles de nombreux pays pauvres.
En outre, la concurrence vient souvent des flottes qui, pour fonctionner sous les drapeaux d’autres pays, n’en appartiennent pas moins à des investisseurs de la Communauté européenne.
Le rapport s’inquiète prudemment de l’abaissement des ressources thonières. La concurrence, source de gaspillages et de crises dans tous les secteurs de l’économie, a en effet des conséquences plus désastreuses encore lorsqu’il s’agit de ressources naturelles qu’une concurrence sauvage et irrationnelle peut complètement épuiser.
La seule conclusion logique qu’on peut tirer de ce rapport, c’est que le système basé sur la concurrence est nuisible et que la seule façon saine de gérer les ressources naturelles serait de les gérer collectivement, de façon planifiée. Cette gestion devrait s’opérer à l’échelle de la planète et non avec la seule préoccupation de protéger la production de l’Union, car les thonidés ont la fâcheuse habitude de ne pas respecter les frontières et les eaux territoriales!
Nous nous sommes abstenues. 
