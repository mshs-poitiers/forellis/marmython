Georges Berthu # Débats - Mardi 30 mars 2004 - Votes
  Berthu (NI
),
par écrit
. 
- J’ai voté pour le rapport Bösch sur la protection des intérêts financiers des Communautés, car il apporte une utile contribution à la lutte contre la fraude au niveau européen.
Il montre aussi que beaucoup reste à faire. En 1999, la Commission était entrée en fonction en promettant une politique de tolérance zéro face à la fraude et à la corruption, mais aujourd’hui elle lègue à ses successeurs des scandales à moitié étouffés (voir Eurostat) et "un fouillis sans précédent de dispositions parfois contradictoires et de services nouvellement créés, de sorte que conflits de compétences et transferts de responsabilités sont préprogrammés" (paragraphe 2).
C’est pourquoi j’ai signé la proposition de motion de censure à ce sujet qui sera, je l’espère, présentée au vote lors de la prochaine session.
Le rapport Bösch relève par ailleurs la quantité extraordinaire de bovins sur pied exportés en 2002 vers le Liban (121 027 tonnes) en bénéficiant de restitutions financières à l’exportation. Bien entendu, tout cela sent la fraude. C’est l’occasion de rappeler que les restitutions à l’exportation ont contribué à jeter un opprobre injustifié sur l’ensemble de la politique agricole commune, et qu’il faudrait couper cette branche pourrie pour sauver le reste. 
