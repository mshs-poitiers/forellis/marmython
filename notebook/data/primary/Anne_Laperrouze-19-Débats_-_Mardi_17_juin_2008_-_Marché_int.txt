Anne Laperrouze # Débats - Mardi 17 juin 2008 - Marché intérieur de l'électricité - Conditions d’accès au réseau pour les échanges transfrontaliers d’électricité - Agence de coopération des régulateurs de l'énergie - Vers une charte européenne des droits des consommateurs d’énergie (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Anne Laperrouze  (ALDE
). 
 – (FR) 
Madame la Présidente, Monsieur le Commissaire, chers collègues, j'entamerai mon intervention sur la question de la séparation patrimoniale bien que j'estime que cette question ne soit pas la plus importante. Au contraire, je pense que nous avons perdu du temps, trop de temps sur cette question.
Être ou ne pas être séparés, là n'est pas la question. Gérer un réseau nécessite un savoir-faire industriel. L'électricité ne se stocke pas, il s'agit de maintenir la fréquence et la tension du réseau en fonction des demandes du marché. Or, à travers la séparation patrimoniale, la Commission n'écarte pas les risques d'une gestion spéculative ou politique du réseau. La question de la propriété, de la gestion industrielle, de la sécurité des réseaux n'a pas été, à mon sens, suffisamment analysée.
Au-delà de cette question, nous sommes parvenus à faire progresser un acteur-clé du fonctionnement du marché intérieur, les régulateurs. Ils doivent être dotés de véritables compétences leur permettant d'assurer leurs droits et leurs devoirs. Au travers de l'agence de coopération, les régulateurs auront un rôle central sur tout ce qui concerne les questions transfrontalières et sur l'harmonisation des codes techniques commerciaux afin de garantir un meilleur fonctionnement et une meilleure sécurité des réseaux.
Mon groupe a déposé trois amendements: le premier vise à préciser que les intérêts du consommateur particulier et industriel doivent être au centre du fonctionnement du marché intérieur. Il s'agit d'une évidence biblique, mais qu'il est pourtant bon de répéter. Le second porte sur les contrats à long terme sous réserve du respect de quelques principes. Ils doivent être perçus non comme un élément de restriction de la concurrence mais comme un élément de stabilité. Il est à souligner que cette demande émane des consommateurs industriels. Enfin, le troisième est là pour stimuler la réflexion. Il s'agit en effet que la Commission, en consultation avec tous les acteurs du marché, réfléchisse à la mise en place d'un opérateur européen de réseaux de transport. Cet objectif ultime est certes lointain, mais il est intéressant que l'on y réfléchisse. L'avenir du marché intérieur de l'électricité pourrait ainsi être mieux assuré avec un réseau européen du transport de l'électricité. 
