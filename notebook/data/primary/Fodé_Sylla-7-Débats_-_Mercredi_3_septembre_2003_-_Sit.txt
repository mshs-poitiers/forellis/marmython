Fodé Sylla # Débats - Mercredi 3 septembre 2003 - Situation des droits fondamentaux dans l’Union européenne (2002) 
  Sylla (GUE/NGL
), 
rapporteur
. -
 Monsieur le Président, je voudrais simplement dire qu’on peut avoir un débat sans stigmatiser les gens, sans les insulter, sans les humilier. Je crois que la moindre des choses, c’est la tolérance et, à défaut de tolérer, on peut au moins respecter. Mais c’est trop demander à la personne concernée. Donc, je ne lui renverrai pas les noms d’oiseau et les insultes, je pense que c’est elle qui n’est pas digne de siéger dans cette enceinte. 
