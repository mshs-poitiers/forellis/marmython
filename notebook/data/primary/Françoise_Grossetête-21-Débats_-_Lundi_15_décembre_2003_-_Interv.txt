Françoise Grossetête # Débats - Lundi 15 décembre 2003 - Interventions sur des questions politiques importantes
  Grossetête (PPE-DE
). 
 - Monsieur le Président, je voulais tout de même répondre à notre collègue qui s’est permis de s’adresser au Parlement sur une question qui ne relève que de la politique intérieure française. J’aimerais souligner qu’il n’est pas convenable que des députés au Parlement européen se mêlent de ce qui concerne la politique intérieure d’un pays. Il est bon de rappeler cette règle, surtout au moment où on reproche à l’Europe en général et à notre Parlement en particulier de se mêler de tout.
(Applaudissements)

