Marie-Anne Isler-Béguin # Débats - Mardi 10 mars 2009 - Règles et normes communes concernant les organismes habilités à effectuer l’inspection et la visite des navires ainsi que les activités pertinentes des administrations maritimes (refonte) - Règles et normes communes concernant les organismes habilités à effectuer l’inspection et la visite des navires (refonte) - Contrôle par l’État du port (refonte) - Système communautaire de suivi du trafic des navires et d’information - Enquêtes sur les accidents dans le secteur des transports maritimes - Responsabilité des transporteurs de passagers par mer en cas d’accident - Assurance des propriétaires de navires pour les créances maritimes - Respect des obligations des États du pavillon (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie Anne Isler Béguin  (Verts/ALE
).  
− Monsieur le Président, chers collègues, au moment de l’Erika, en France, je faisais partie des députés qui ont demandé aux autorités françaises de fermer les vannes pour que les marais salants ne soient pas pollués.
Au moment du Prestige, j’étais, avec notre collègue Sterckx, corapporteure, puisque nous avions une coresponsabilité en matière de transports – j’avais moi-même une responsabilité en matière d’environnement – et nous avions demandé, justement, plus de sécurité au niveau du transport maritime. Je me rappelle la bataille que nous avions menée, ici même, pour qu’une commission d’enquête soit créée sur le Prestige. C’était absolument impressionnant.
Aujourd’hui, je crois donc qu’on peut tous se satisfaire du fait que, dans le cadre du paquet maritime, de nombreuses règles internationales et européennes aient progressé et espérer que ce ne sera plus qu’un mauvais souvenir.
Mais, si vous le permettez quand même, je voudrais attirer l’attention sur...
(Le président retire la parole à l’orateur)

