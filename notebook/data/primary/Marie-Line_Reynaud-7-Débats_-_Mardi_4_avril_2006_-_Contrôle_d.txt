Marie-Line Reynaud # Débats - Mardi 4 avril 2006 - Contrôle de l application du droit communautaire (2003 2004) - Mieux légiférer 2004: application du principe de subsidiarité - Mise en oeuvre conséquences et impact de la législation en vigueur sur le marché intérieur - Stratégie de simplification de l environnement réglementaire (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie-Line Reynaud (PSE
), 
rapporteur pour avis de la commission des affaires constitutionnelles
. - Madame la Présidente, je tiens à remercier Monsieur Gargani pour la clarté et l’engagement de son rapport. Je me félicite qu’une très grande partie de mon avis ait été intégré et que son rapport reprenne les deux objectifs qui ont été les miens, à savoir tout d’abord l’accent mis sur le fait que la simplification est nécessaire mais ne doit pas être réalisée de n’importe quelle manière, et en second lieu, l’affirmation de la volonté du Parlement de prendre pleinement part à la stratégie de simplification. Nous ne pouvons qu’accueillir favorablement toute initiative visant à rendre l’environnement réglementaire plus lisible et plus cohérent.
Il est en effet impossible de continuer à fonctionner convenablement avec un acquis de plus de 80000 pages. Comment, dans ces conditions, parler de manière crédible aux citoyens d’accessibilité et de transparence? C’est pourquoi la stratégie de simplification doit être soutenue dans son principe. Elle doit permettre, à terme, de disposer de normes communautaires et nationales plus faciles à appliquer et moins coûteuses. Cependant, cette simplification comporte également un certain nombre de limites, voire de dangers, et il convient dès lors de se monter vigilant. Ce rapport précise notamment que la simplification ne doit pas se traduire par un abaissement des normes, qu’il existe des problèmes liés à l’application de l’accord interinstitutionnel régissant la procédure de refonte et qu’il est donc nécessaire de clarifier les règles applicables pour éviter conflits de compétence et blocages procéduraux. Ce rapport affirme également clairement la volonté qu’a le Parlement de prendre pleinement part à la stratégie de simplification et il met l’accent tant sur la nécessité de protéger les prérogatives du Parlement que sur la question de l’adaptation de son règlement intérieur. La simplification ne peut en effet se faire en dehors de tout contrôle démocratique et, en particulier, en dehors du contrôle du Parlement.
Le Parlement doit en outre réfléchir, dans le cadre de la simplification, à l’amélioration de ses procédures et de ses techniques législatives internes. Cette question fera l’objet d’un rapport spécifique que je suis chargée de rédiger. Enfin, concernant les modes de régulation alternatifs, je me réjouis que ce rapport demande un encadrement strict du recours à la corégulation et à l’autorégulation car il est fondamental de prévoir des garde-fous en la matière. 
