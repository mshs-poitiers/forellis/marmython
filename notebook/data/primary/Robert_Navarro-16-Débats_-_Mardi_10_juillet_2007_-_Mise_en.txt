Robert Navarro # Débats - Mardi 10 juillet 2007 - Mise en œuvre du premier paquet ferroviaire (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Robert Navarro, 
au nom du groupe PSE.
 - Monsieur le Président, au moment où nous dressons le bilan du premier paquet ferroviaire, force est de constater que nous sommes encore loin du compte. Malgré les progrès accomplis, et notamment le fait d’avoir enrayé un déclin qui semblait inéluctable, la part modale du rail reste encore trop faible. C’est d’autant plus regrettable qu’en ces temps de dérèglement climatique, le choix du rail voit sa pertinence chaque jour confirmée par l’actualité.
Les raisons de ce bilan en demi-teinte, nous les connaissons: on a trop misé sur l’ouverture, dont il ne faut certes pas nier les apports, sans se préoccuper assez des obstacles techniques, en particulier le manque d’interopérabilité, sachant que celle-ci aurait dû accompagner l’ouverture au fur et à mesure, au lieu de la suivre de loin en loin.
Cette situation tient probablement au fait que libéraliser ne coûte en général pas grand chose aux États, tandis qu’harmoniser est une autre affaire. Aujourd’hui, au moment même où l’on recommence enfin à s’attaquer aux problèmes d’interopérabilité, il me paraît donc inquiétant que, sous couvert de comodalité, les objectifs de transfert modal vers le rail, notamment, passent au second plan. Au lieu de revoir les objectifs à la baisse, il aurait fallu revoir les moyens intellectuels autant que financiers à la hausse.
J’espère que ce rapport contribuera donc à rappeler le chemin qu’il nous reste encore à parcourir pour redonner au rail la place qu’il mérite. 
