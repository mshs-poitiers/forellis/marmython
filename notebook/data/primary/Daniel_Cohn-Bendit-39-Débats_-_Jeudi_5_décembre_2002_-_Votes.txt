Daniel Cohn-Bendit # Débats - Jeudi 5 décembre 2002 - Votes
  Cohn-Bendit (Verts/ALE
). 
 - Monsieur le Président, je crois que, dans cette Assemblée, on est en train de jouer à un jeu du chat et de la souris qui est exécrable pour la démocratie. Exécrable pour la démocratie !
(Applaudissements)

Tout le monde sait qu'à partir du moment où...
Écouter, et après parler : je sais que c'est dur d'écouter, parfois, quand on croit être le plus fort, mais le plus fort n'a pas toujours été le plus intelligent. Ce n'est pas parce qu'on est gros qu'on est intelligent.
(Applaudissements)

Non, et vous aussi vous vous calmez, Monsieur Schulz... 
