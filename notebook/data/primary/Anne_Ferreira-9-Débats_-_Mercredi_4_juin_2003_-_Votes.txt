Anne Ferreira # Débats - Mercredi 4 juin 2003 - Votes
  Ferreira (PSE
),
par écrit
. -
 Contrairement à mon groupe, j’ai décidé de m’abstenir sur ce rapport.
La raison principale tient à la décision d’augmenter considérablement le traitement des députés européens. Dans le contexte politique et social actuel, dans l’Europe entière et pas seulement en France, cette décision est inacceptable et injustifiable. Nous sommes avant tout des représentants du peuple et notre priorité est de promouvoir les intérêts de nos citoyens et de concrétiser leurs attentes à l’égard de l’Europe, notamment en matière de démocratisation.
J’ai décidé de m’abstenir et non de voter contre le rapport, car j’estime que l’adoption d’un statut unique des députés est un signal positif en direction d’une Union plus étroite, d’un destin réellement partagé, de la réalisation de l’objectif d’égalité.
Cependant, j’aurais préféré que cet objectif d’harmonisation des salaires - par le haut - puisse être une priorité des législateurs européens et concerne avant tout nos concitoyens grâce à l’adoption d’un salaire minimum européen. 
