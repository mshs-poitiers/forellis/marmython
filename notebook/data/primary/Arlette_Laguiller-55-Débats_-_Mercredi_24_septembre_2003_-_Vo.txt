Arlette Laguiller # Débats - Mercredi 24 septembre 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. - La commission de la culture rappelle, dans sa justification succincte, que le brevet donne à son titulaire le droit d’empêcher autrui de faire une utilisation commerciale de son invention. Chacun sait cependant que ce droit n’est utilisé que rarement par l’inventeur lui-même, mais qu’il est souvent utilisé par les grandes sociétés, qui ont les moyens financiers de racheter des brevets et de se constituer de vastes portefeuilles leur permettant de commercialiser certaines inventions qui leur rapportent, tout en bloquant le développement d’autres.
Les brevets ne servent que très peu, voire pas du tout, les intérêts des inventeurs. Mais, en revanche, ils donnent des moyens supplémentaires aux grands groupes industriels ou commerciaux. Ils ne protègent pas la "propriété intellectuelle" mais le monopole commercial.
Étant contre les brevets, et pas seulement en matière d’informatique, nous avons rejeté la proposition du rapport McCarthy. 
