Jean-Louis Bernié # Débats - Mardi 4 mai 2004 - Votes
  Bernié, Esclopé et Saint-Josse (EDD
),
par écrit
. 
- La Commission a joué la montre et le Parlement a laissé faire. Si la Commission refuse d’assumer spontanément sa responsabilité politique tant individuellement que collectivement, c’est à notre Parlement qu’il appartenait de l’y contraindre. Le débat sur la motion de censure, qui s’est tenu mercredi 21 avril en plénière, n’a pas permis d’obtenir de réponse, pas plus que le vote d’une énième résolution le 22 avril 2004. La Commission n’écoute pas et ne répond pas aux demandes du Parlement européen, voilà la réalité. Si certains se sont opposés à cette motion, c’est qu’ils se satisfont d’une Commission irresponsable. Le rejet de la motion est donc un très mauvais signal envoyé aux citoyens des vingt-cinq États membres. Une majorité de députés a sciemment renoncé à exercer le pouvoir de contrôle démocratique reconnu au Parlement européen. 
