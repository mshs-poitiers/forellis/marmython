Marine Le Pen # Débats - Mardi 10 mai 2005 - Explications de vote
   Marine Le Pen (NI
),
par écrit
. 
- Afin de mieux protéger le patrimoine culturel européen, l’Europe entend établir des mesures de conservation et de protection exceptionnelles qui permettront de préserver l’héritage cinématographique et qui favoriseront la défense des identités linguistiques, culturelles et artistiques de notre vieux continent.
L’une des recommandations entend étendre le bénéfice des aides d’État au cinéma. Il s’agit en fait de prolonger « l’exception culturelle » et de proroger ce statut d’exception, qui est la transposition exacte de ce que nous défendons pour la France, c’est-à-dire la préférence nationale! La Commission a décidé de prolonger de trois ans le régime actuel d’aides publiques aux œuvres cinématographiques et télévisées même s’il conviendrait d’évaluer et de réformer ces dispositifs qui sont loin de donner toute satisfaction.
Dans ces conditions pourquoi limiter cette protection à la culture et ne pas l’étendre au textile, à l’agriculture, à la sidérurgie, à la construction navale, etc.?
La logique mondialiste de la Commission ne condamne-t-elle pas à terme ce moratoire qui fausse la libre concurrence? Seule la défense claire de la préférence nationale et européenne préservera l’Europe de l’ultralibéralisme mondial. 
