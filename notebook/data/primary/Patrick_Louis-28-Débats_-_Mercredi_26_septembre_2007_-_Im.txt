Patrick Louis # Débats - Mercredi 26 septembre 2007 - Immigration - Immigration légale - Priorités politiques dans le cadre de la lutte contre l'immigration clandestine de ressortissants de pays tiers (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Patrick Louis  (IND/DEM
). 
 – (FR) 
Monsieur le Président, mes chers collègues, renonçons à la langue de bois! Le droit d'asile et l'immigration issue d'une même civilisation ne sont pas le problème. La question porte essentiellement sur l'immigration extra-civilisationnelle, qu'elle soit légale ou illégale, et celle-ci n'est une chance pour personne.
L'immigration de travail commet une double injustice: elle vole les compétences payées par les pays d'origine et, dans le pays d'accueil, elle tire le marché du travail vers le bas et soustrait aux chômeurs locaux l'occasion d'un emploi.
L'immigration d'allocations présente deux erreurs: elle déracine les pauvres gens, hypnotisés par le miroir aux alouettes de la ville occidentale, et, dans le pays d'accueil, elle déséquilibre les budgets sociaux, qui sont nés et ne peuvent perdurer que dans le cadre restreint et protecteur de la nation.
Ainsi, et contrairement à ce qui a été écrit sur les murs du restaurant des députés à Bruxelles il y a un mois, l'Union européenne n'a pas besoin d'immigration. Au contraire: l'Europe a besoin d'une grande politique familiale et démographique, de coopération souveraine entre les nations, de frontières et non pas de Frontex, et le monde a besoin de comprendre, d'une part, que la paix, ce n'est pas l'immigration, mais le développement autocentré et, d'autre part, que la vraie prolétarisation, c'est le déracinement culturel. 
