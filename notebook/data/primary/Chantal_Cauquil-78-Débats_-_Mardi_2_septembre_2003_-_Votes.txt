Chantal Cauquil # Débats - Mardi 2 septembre 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. - Ce rapport reprend à son compte sur le fond, sinon sur la forme, les récriminations d’employeurs néerlandais contre des dispositions européennes qui pourraient avoir pour conséquence, non d’empêcher, mais de rendre un peu moins facile le licenciement de leurs salariés résidant dans un autre État membre. Cette situation a beau résulter d’un concours de circonstances exceptionnel, patrons et autorités néerlandaises l’estiment intolérable, de même que le rapporteur qui invite la Commission "à se pencher sur 
(ce) 
problème". Comme si les autorités européennes avaient besoin de tels encouragements pour rogner les droits des travailleurs, elles dont le cœur, les préoccupations et les actes ne "penchent" déjà que trop en faveur du patronat!
Nous avons bien sûr voté contre ce rapport. 
