Guy Bono # Débats - Mercredi 11 mars 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Jaromir Kohlicek (A6-0101/2009
) 
  Guy Bono  (PSE
), 
par écrit
. – J’ai voté en faveur de ce rapport, présenté par le député tchèque de la gauche unitaire Jaromír Kohlíček, concernant les enquêtes sur les accidents dans le secteur des transports maritimes.
Ce texte met l’accent sur la nécessité de définir, à l’échelle européenne, des orientations claires et contraignantes afin d’assurer un suivi efficace des accidents en mer. Il répond aux inquiétudes qui ont suivi le naufrage du pétrolier Erika au large des côtes françaises. C’est pour éviter que des cas de mauvaise gestion comme celui-là ne se répètent que l’Union européenne a décidé d’imposer un cadre strict, qui traite de tous les aspects techniques et de toutes les démarches à suivre en cas d’accident: méthodologie des enquêtes, base de donnée européenne sur les accidents de mer, consignes de sécurité, etc.
Je partage l’idée qu’il est indispensable de faire de l’espace maritime européen l’un des plus exemplaires et sécurisés du monde. C’est ce à quoi concourt le paquet maritime, aussi appelé «Erika III», dans lequel s’intègre le présent rapport. C’est un véritable bond en avant pour le secteur maritime, et aussi pour l’environnement, souvent victime collatérale des conduites peu respectueuses en mer. 
