Nathalie Griesbeck # Débats - Mercredi 17 janvier 2007 - Programme d'action européen pour la sécurité routière - Bilan à mi-parcours (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Nathalie Griesbeck (ALDE
). 
 - Madame la Présidente, je vous félicite pour votre élection. Je vais, à mon tour, dire quelques mots sur ce rapport relatif au transport routier. Le transport routier - je le déplore, comme de nombreuses personnes ici - ne cesse de croître, ce qui amène l’Union à faire de la sécurité routière une politique prioritaire, nous sommes tous d’accord pour le reconnaître.
Ce rapport rappelle les progrès enregistrés et annonce des mesures nouvelles qui complètent les dispositions votées tout récemment sur le permis de conduire européen et qui sont dans l’ensemble satisfaisantes. Je ne les énumèrerai pas, mais elles constituent autant d’avancées. Je voudrais simplement revenir sur la question de l’allumage des feux de jour, auquel je suis tout à fait opposée. En effet, aucune étude fiable ne démontre l’efficacité d’une telle mesure. Certes, depuis l’introduction de cette mesure dans mon pays, le nombre de tués a diminué, et c’est une bonne chose, mais lorsqu’on regarde de plus près, on constate que le nombre de décès par catégorie d’usagers dits vulnérables aurait augmenté, d’après les chiffres que j’ai trouvés, de plus de 8 % pour les piétons, de plus de 0,6 % pour les vélos et de plus 3,8 % pour les cyclomotoristes, donc plus particulièrement pour ces derniers. Sans insister davantage, je voudrais attirer l’attention de nos collègues sur cet aspect là et demander un approfondissement de la question.
Je conclurai en remerciant, à mon tour, Mme la rapporteure, et en lui souhaitant bonne chance dans sa nouvelle vie. 
