Marie-Noëlle Lienemann # Débats - Jeudi 18 janvier 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Rapport: Jarzembowski (A6-0475/2006
) 
  Marie-Noëlle Lienemann (PSE
), 
par écrit
. - J’ai voté pour le retrait de ce rapport, car il est urgent de stopper la libéralisation et la dérégulation des services publics, surtout pour le transport des voyageurs. D’ailleurs, l’UE doit bien mesurer les conséquences négatives de sa politique et le rejet grandissant des citoyens puisqu’elle refuse d’établir un bilan public de ces conditions.
On ne peut accepter la moindre nouvelle ouverture à la concurrence tant qu’une directive-cadre sur les services publics ne fixe pas des garanties pour l’égalité et l’aménagement du territoire, des politiques tarifaires assurant les indispensables péréquations ou les investissements d’avenir assurant la modernisation et la qualité des infrastructures.
Tout montre que la sécurité, la desserte ferroviaire, surtout là où la rentabilité du service est faible, vont reculer en Europe, ce qui du coup va aussi aller à l’encontre des objectifs de lutte contre l’effet de serre et accroître les inégalités. 
