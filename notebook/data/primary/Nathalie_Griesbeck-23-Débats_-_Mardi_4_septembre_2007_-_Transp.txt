Nathalie Griesbeck # Débats - Mardi 4 septembre 2007 - Transport intérieur des marchandises dangereuses (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Nathalie Griesbeck  (ALDE
). –  
Monsieur le Commissaire, Monsieur le Président, mes chers collègues, à mon tour de remercier notre rapporteur pour les travaux qu'il a dirigés sur des questions vraiment très techniques relatives au transport des marchandises dangereuses, qui représentent près de 10% des marchandises à travers l'Europe. Je me réjouis bien sûr que ce texte non seulement vise à dépoussiérer une réglementation qui touche à la sécurité des Européens, mais aussi qu'il soit élargi au transport de marchandises par voies fluviales, car il s'agit d'un mode de transport qu'il convient de développer à travers l'Europe - nous sommes nombreux à le penser - comme un des modes de transport alternatif à la route. Il suscite aussi une attente très importante de la part des citoyens européens.
C'est un pas en avant vers la simplification de la législation en termes de procédures administratives tant pour l'expéditeur que pour le destinataire, mais c'est aussi une étape indispensable en matière de formation des personnes qui se trouvent impliquées dans la sécurité des marchandises dangereuses. S'il nous faut encore approfondir notre position sur certains points évoqués par les amendements, ce texte représente un progrès incontestable en lisibilité surtout et en sécurité. 
