Hélène Flautre # Débats - Lundi 2 février 2009 - Interventions d’une minute sur des questions politiques importantes
  Hélène Flautre  (Verts/ALE
). - 
 Monsieur le Président, je souhaite attirer l’attention du Parlement européen sur la dégradation – si c’était encore possible – de la situation des droits de l’homme en Tunisie.
Depuis le 11 décembre, Sihem Bensedrine défenseur des droits de l’homme bien connue, rédactrice en chef de Kalima, fait l’objet de harcèlements dans la presse et de calomnies qui sont absolument insupportables et incompatibles avec un État de droit.
Le 23 janvier, M. Amin, qui est le coordinateur de la Coordination maghrébine des organisations des droits humains, s’est vu refuser l’entrée sur le territoire tunisien.
Le 28 janvier, la radio Kalima, qui émet depuis cette date par satellite, a été totalement encerclée. Ses journalistes ont été emprisonnés et les personnes venues les soutenir ont été malmenées dans la rue. Cette radio est toujours encerclée par la police tunisienne, avec des atteintes, donc, à la liberté d’information et d’expression.
Enfin, demain, on aura le procès en appel des ouvriers de Gafsa qui se battent contre la corruption et contre leur exploitation dans ce bassin minier de Tunisie, avec un déni de justice que nous avons vu lors du premier procès.
Les chefs de mission à Tunis s’inquiètent de la situation, en ont discuté, en discutent peut-être en ce moment même. C’était aujourd’hui leur réunion.
Je vous demande, Monsieur le Président, de prendre une initiative politique majeure pour que cessent ces violations systématiques des droits de l’homme en Tunisie. 
