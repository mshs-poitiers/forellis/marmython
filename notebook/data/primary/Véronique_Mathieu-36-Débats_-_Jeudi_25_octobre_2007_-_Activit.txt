Véronique Mathieu # Débats - Jeudi 25 octobre 2007 - Activités du Médiateur européen (2006) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Véronique Mathieu  (PPE-DE
), 
par écrit. 
–
 Le rapport de Mme Sbarbati sur les activités du Médiateur européen marque son attachement à cette institution créée par le traité de Maastricht en 1992 afin de défendre les citoyens de l'Union en cas de mauvaise administration de la part des institutions communautaires. Le texte insiste à juste titre sur certaines priorités pour l'avenir de l'Ombudsman, comme l'importance de la coopération avec les médiateurs des États membres ou la proposition de mieux contrôler en interne l'administration des organismes de l'Union.
Néanmoins, le rapport soulève deux questions qu'il serait dangereux d'écarter du débat européen: d'une part, les activités du Médiateur doivent entraîner une réflexion sur sa reconnaissance et son accessibilité partout en Europe. En effet, le nombre de saisies du Médiateur a tendance à baisser (3 830 demandes en 2006, soit une diminution de 2% par rapport à 2005) alors que les plaintes proviennent souvent d'États où la culture nationale incite déjà le citoyen à saisir ses institutions.
D'autre part, le rapport s'interroge sur l'élargissement des pouvoirs du Médiateur (accessibilité aux documents et auditions sans conditions). Une modification du statut du Médiateur ne doit en aucun cas laisser oublier le rôle majeur de la commission des pétitions du Parlement, qui tient sa légitimité directement du suffrage populaire. 
