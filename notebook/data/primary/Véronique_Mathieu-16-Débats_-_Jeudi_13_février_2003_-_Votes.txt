Véronique Mathieu # Débats - Jeudi 13 février 2003 - Votes
  Bernié, Butel, Esclopé, Mathieu, Raymond et Saint-Josse (EDD
),
par écrit
. - Devenu unique en 1986, le marché commun n'est pas réalisé pour autant. L'élargissement à dix nouveaux pays nous éloignera de cet objectif et le rendez-vous pris pour 2013 sera à nouveau reporté.
Bien plus que de parvenir à adopter une directive à la majorité ou à l'unanimité, la difficulté est de produire un texte compréhensible et applicable. Sous l'effet du nombre et de l'importance des détails, les conflits d'interprétation et les procédures d'infraction se multiplient.
La stratégie doit être de réduire les conflits, et non de les résoudre par la contrainte juridique. Seules comptent la volonté et la capacité d'appliquer les textes lorsque l'on est confronté aux réalités économiques et sociales. Plutôt que d'afficher de nouvelles ambitions, il fallait rappeler à l'ordre la Commission pour sa mauvaise volonté et sa mauvaise foi lorsqu'il s'agit d'évaluer l'impact de la libéralisation des services publics.
Avec en point de mire l'uniformisation des services de santé, le rapport explique qu'une clause de sauvegarde peut porter préjudice à l'uniformité. Voilà un rappel bien inutile, si ce n'est pour démontrer l'hostilité de principe à toute prise en compte de la diversité et les dangers qui pèsent sur les cohésions existantes.
De fait, nous sommes opposés à ce rapport. 
