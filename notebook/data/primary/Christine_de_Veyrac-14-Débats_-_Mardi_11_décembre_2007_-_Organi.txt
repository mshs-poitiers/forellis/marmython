Christine de Veyrac # Débats - Mardi 11 décembre 2007 - Organisation commune du marché vitivinicole (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Christine De Veyrac  (PPE-DE
). - 
(FR)
 Madame la Présidente, Madame la Commissaire, oui, une réforme de l'OCM vin est souhaitable, mais les moyens avancés par la Commission sont-ils les plus adéquats? Vu de Bruxelles, on peut comprendre l'équation mathématique entre baisse de la production et baisse du nombre de producteurs, mais vu du Sud-ouest de la France, vu des plaines d'Italie, des côtes espagnoles ou portugaises, Madame la Commissaire, je vous l'assure, il n'en va pas de même et c'est un sujet qui ne doit pas être regardé à travers le prisme des lunettes de l'économiste. La viticulture, ce sont des hommes et des femmes qui vivent du fruit de leur travail. La vigne c'est leur vie, et on ne peut pas les pousser vers la sortie sans se soucier de ce que sera leur quotidien. D'ailleurs, quelle logique y-a-t'il à vouloir, d'un côté, arracher et, de l'autre, proposer de libéraliser totalement les droits de plantation à partir de 2013?
Deuxième interrogation: pourquoi supprimer cet outil de régulation alors même qu'il n'existe aucune assurance sur ce que sera l'évolution du marché? Si l'on veut véritablement lutter contre la surproduction viticole, je propose, pour ma part, que l'on commence par ordonner l'arrachage des plantations illégales. Or, vous le savez comme moi, il y en a quelques hectares en Europe.
Le rapport prévoit d'autre part une plus grande variété des mesures accessibles au travers des programmes de soutien nationaux. C'est une bonne chose car la proposition de la Commission sur ce point est très limitative. Celà étant, je regrette l'insuffisance de la part consacrée à la prévention des crises. Nous savons combien la production viticole est soumise à variations d'une année sur l'autre en fonction des aléas climatiques, et les mesures de prévention des crises permettront seulement d'atténuer les fluctuations.
Enfin, je conclurai en indiquant que je comprends la prudence du rapporteur sur la question de l'enrichissement, qui est sensible, mais il est regrettable que ne soit pas proposée l'autorisation de cumuler les méthodes d'enrichissement additives et soustractives, lesquelles permettraient de réduire le recours aux saccharoses.
Pour le reste, l'orientation générale du rapport me semble aller dans le bon sens et je soutiendrai l'excellent travail du rapporteur. 
