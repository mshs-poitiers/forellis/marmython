Chantal Cauquil # Débats - Jeudi 1 avril 2004 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. 
- Nous avons voté contre l’amendement 7, dont l’auteur, au nom d’idées rétrogrades et chauvines, voudrait claquer la porte de l’Union européenne à la Turquie.
Le fait qu’il déguise sa position derrière une proposition de référendum, où l’ensemble des peuples de l’Union européenne seraient invités à se prononcer sur la question, ne la rend pas meilleure. Que ne propose-t-il un référendum sur des questions qui concernent réellement toute la population, sur le fait de consacrer, par exemple, les budgets d’armement des États aux écoles, aux hôpitaux, aux transports publics de l’Union?
Nous ne sommes évidemment pas contre l’entrée de la Turquie dans l’Union européenne et nous sommes opposées à tous ceux qui invoquent des raisons religieuses ou idéologiques pour refuser qu’on abaisse une frontière sur ce continent qui n’est déjà que trop divisé.
Cela dit, nous ne partageons ni les raisons pour lesquelles la classe possédante européenne souhaite l’entrée de la Turquie dans le cadre de l’Union ni les satisfecit qui sont décernés au gouvernement turc, d’autant moins que les droits et les libertés élémentaires continuent à être foulés aux pieds dans ce pays, comme le droit collectif du peuple kurde à disposer de lui-même, et qu’il y a encore des prisonniers politiques. Nous n’entendons pas non plus cautionner les conditions imposées par les institutions européennes à ce pays, que ses dirigeants feront inévitablement payer à leurs classes populaires.
En conséquence, nous nous sommes abstenues sur ce rapport. 
