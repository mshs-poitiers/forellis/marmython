Catherine Guy-Quint # Débats - Jeudi 27 octobre 2005 - Projet de budget général de l’Union européenne - Exercice 2006
  Catherine Guy-Quint (PSE
). 
 - Monsieur le Président, ce que je vais faire n’est pas classique. À la ligne suivante, nous avons deux amendements. Un amendement qui a été voté par la commission des budgets et un autre concernant les médicaments orphelins, qui nous est envoyé par la commission de l’environnement, de la santé publique et de la sécurité alimentaire. Il nous faut voter l’amendement proposé par la commission des budgets. Quant à l’amendement suivant, il demande un million supplémentaire pour les médicaments orphelins, dont l’Agence des médicaments a besoin. Je voudrais savoir s’il nous serait possible de voter sur l’amendement 231, pour que l’Agence puisse travailler cette année avec tous les moyens nécessaires.
Vous comprenez que techniquement, ce n’est pas correct. Je suis entièrement d’accord. Mais il se fait que depuis le vote à la commission des budgets, nous avons reçu des éléments supplémentaires. S’il était possible de voter cet amendement 231, qui rajouterait un million d’euros encore disponible dans la marge, cela nous permettrait d’améliorer considérablement les conditions de travail dans ce domaine qui nous tient à cœur.
(Applaudissements)

