Jean-Marie Cavada # Débats - Mercredi 31 janvier 2007 - Nouvel accord ‘PNR’ - SWIFT (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Marie Cavada (ALDE
), 
auteur
. - Monsieur le Président, il y a une expression en français qui veut décrire l’embarras surréaliste d’une institution lorsqu’elle se trouve devant une situation inattendue, on dit qu’elle se comporte «comme une poule qui a trouvé un révolver».
Et franchement, sans vouloir vous sous-estimer, Monsieur Gloser, le moins qu’on puisse dire est que votre propos est dramatiquement décevant sur cette affaire. Je me sens donc autorisé à rappeler que vous êtes mandaté pour défendre la souveraineté européenne, acte dont je ne vois pas beaucoup de trace jusqu’à présent. Nous sommes donc, vous l’avez bien compris, déçus et surtout inquiets.
Je parlerai d’abord des données PNR. Dans quelques mois, on le sait, vous l’avez dit, M. Frattini l’a rappelé, va expirer l’accord intérimaire concernant les données relatives aux passagers aériens. Nous sommes sans informations claires sur son fonctionnement. Je tiens à rappeler d’ailleurs que si l’accord précédent a été annulé par la Cour, il se fondait sur une évaluation de l’adéquation de la protection des données aux États-Unis. En revanche, il n’en est rien pour l’accord en vigueur, alors même que cette évaluation est exigée par le protocole à la Convention 108 du Conseil de l’Europe, qui s’impose aux États membres et donc directement, ou indirectement en tout cas, aux institutions européennes. Fondé sur une base douteuse, l’accord n’a pas encore été correctement mis en œuvre à ce jour, puisque le système de transmission des données qui aurait dû être activé depuis 2004, le système «push», ne l’a pas encore été, et ce malgré les annonces qui ont été faites, notamment en septembre dernier.
Je voudrais publiquement, dans un esprit de coopération, poser quelques questions. Premièrement, je me demande si la Commission n’aurait pas dû faire davantage pression pour obtenir l’activation du système «push» de la part des compagnies aériennes et des centres de réservation électronique qui sont sous sa surveillance. Deuxièmement, je me demande si on n’aurait pas dû demander une information effective des passagers aériens quant à l’utilisation à des fins de sécurité de leurs données personnelles, de la même façon qu’ont été menées des campagnes d’information sur les droits en cas de retard des avions, d’annulation de vols, par exemple.
De notre côté, nous attendons, concernant ce dossier, des éclaircissements de la part de la Commission et, surtout, des autorités américaines sur un certain nombre de points. J’en retiens deux. Tout d’abord, nous ne comprenons pas que pour lutter contre le terrorisme et contre le crime organisé, il soit nécessaire d’avoir recours à la totalité des trente-quatre données qui sont répertoriées dans le PNR et pas seulement aux dix-neuf données que le Parlement et les autorités pour la protection des données avaient considérées comme suffisantes à cette fin.
Ensuite, c’est le deuxième point, comment peut-on expliquer que des citoyens et des passagers européens puissent être classés comme dangereux à leur insu, pendant des années, sans qu’il y ait réciprocité et sans qu’on demande rien?
J’en viens à présent au système Swift. Ce deuxième dossier est encore plus délicat et il concerne l’accès, par les autorités américaines aux données relatives aux transferts financiers. Mes collègues, tout à l’heure, ont abordé un certain nombre de points. Je vais donc me contenter de donner l’impression de la commission des libertés. Ce qui est en jeu, c’est la protection des droits fondamentaux de nos citoyens et la crédibilité de notre partenariat avec les États-Unis. En réalité, on ne comprend pas qu’une certaine réciprocité n’ait pas été mise sur la table pour entamer des négociations avec un partenaire, fût-il amical. Est-ce qu’on procédera un jour de la même manière avec un autre partenaire à l’est de nos frontières, c’est-à-dire la Russie? C’est là où je voulais dire que la souveraineté européenne n’est pas respectée.
En accord avec les groupes politiques, j’ai donc pris l’initiative d’inviter les commissions compétentes du Congrès américain et j’attends une réponse.
(Le Président invite l’orateur à conclure
)
Je comprends très bien que le timing
 d’un Parlement soit tout à fait important, et comme je fais confiance à mes collègues pour dire le reste, je m’en tiendrai là. J’ai d’ailleurs dit l’essentiel de ce que je voulais faire dire. Je croyais honnêtement disposer de cinq minutes. 
