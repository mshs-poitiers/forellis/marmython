Chantal Cauquil # Débats - Jeudi 18 décembre 2003 - Votes
  Bordes et Cauquil (GUE/NGL
),
par écrit
. 
- Cette résolution commune est un tissu de "bons sentiments", hypocrites quant aux droits de l’homme, et de l’habituel cynisme diplomatico-commercial des grandes puissances.
La Chine ne respecte pas les droits de l’homme? Non, bien sûr, comme des dizaines et des dizaines d’États et de dictatures ignobles auxquels l’Union européenne a vendu, vend et vendra des armes, pour opprimer leurs peuples ou s’en prendre à leurs voisins et pour engraisser les marchands de canons occidentaux.
De l’Irak de Saddam Hussein, armé par la France, la Grande-Bretagne, l’Allemagne, la Russie et les États-Unis, à Israël, en passant par les dictatures d’Amérique latine, d’Asie, d’Afrique et d’ailleurs, aucun embargo n’a jamais arrêté les firmes, notamment européennes, d’armement. Elles contournent d’autant plus aisément de tels embargos - en Chine comme ailleurs - qu’elles disposent pour cela de la complicité active des gouvernements européens. 
