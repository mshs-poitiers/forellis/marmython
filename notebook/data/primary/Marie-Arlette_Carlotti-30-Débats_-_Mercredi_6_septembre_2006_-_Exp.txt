Marie-Arlette Carlotti # Débats - Mercredi 6 septembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Morillon (A-0228/2006) 
  Marie-Arlette Carlotti (PSE
), 
par écrit
. - La PCP ne répond plus aux attentes des pêcheurs méditerranéens. C’est pourquoi je me réjouis de ce plan qui comporte plusieurs orientations positives. Un allègement indispensable des charges et des contraintes pour les pêcheurs. Une refonte et harmonisation des dispositions de contrôle et de surveillance, sous l’égide de l’Agence communautaire de contrôle de la pêche, particulièrement attendue par les pêcheurs de Méditerranée qui se sentent trop souvent «harcelés». Une association plus étroite des professionnels à la définition des orientations (gestion des efforts de pêche, mesures de contrôle et limitation des captures), qui rejoint une revendication forte que je porte au Parlement européen au nom des pêcheurs méditerranéens.
Je soutiens également la demande du Parlement de jouer pleinement son rôle en refusant à la Commission européenne la possibilité de décider seule des «mesures techniques». Les pêcheurs savent en effet trop bien combien ces «mesures techniques» ont des effets directs et immédiats sur leur activité.
Je voterai donc en faveur de ce rapport, même si j’aurais souhaité qu’il aille plus loin en demandant un plan spécifique pour la pêche en Méditerranée. Pour ma part, je continuerai à mener ce combat au Parlement européen, aux côtés des pêcheurs de ma région. 
