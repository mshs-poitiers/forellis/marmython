Marie-Arlette Carlotti # Débats - Jeudi 6 juillet 2006 - Mauritanie (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie-Arlette Carlotti (PSE
), 
auteur
. - Monsieur le Président, le 3 août 2005, le régime de l’ancien Président Ould Taya est renversé, un conseil militaire déclare vouloir mettre fin aux pratiques totalitaires et instaurer la démocratie. On peut alors imaginer que ce ne sont que des déclarations destinées à justifier ce que la communauté internationale a appelé un coup d’État.
Puis on se rend vite compte qu’aucune goutte de sang n’a été versée, que les autorités de transition ont entamé un dialogue avec l’ensemble des partis politiques et de la société civile, ont fixé un calendrier électoral et, chose exceptionnelle, décident de se présenter elles-mêmes aux prochaines élections. On se dit alors qu’il se passe quelque chose en Mauritanie et que c’est peut-être une occasion de sortir ce pays de la dictature.
Désormais, notre rôle consiste à appuyer le processus démocratique en cours dans le respect du calendrier que les Mauritaniens se sont eux-mêmes fixé. À cet égard, je me réjouis de la tenue du dernier scrutin constitutionnel, en appuyant la CENI qui doit cependant faire en sorte que l’ensemble des Mauritaniens puissent obtenir leurs papiers d’identité pour pouvoir voter lors des prochaines échéances électorales, et que les Mauritaniens de l’étranger puissent participer aux futurs scrutins, en soutenant aussi les partis politiques pour qu’ils aient les moyens de s’exprimer et de faire campagne autour de plates-formes électorales porteuses de propositions pour améliorer la situation du peuple mauritanien. 
