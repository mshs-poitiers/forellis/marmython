Jean-Claude Martinez # Débats - Jeudi 23 octobre 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
− Projet de budget général de l’Union européenne pour l’exercice 2009 − Rapport: Jutta Haug (A6-0398/2008
) 
  Jean-Claude Martinez  (NI
), 
par écrit. – 
Avoir à 27 pays un budget européen de l’ordre de 130 milliards d’euros, soit l’équivalent du seul budget de l’Espagne, est, en période normale, déjà une curiosité.
Mais voilà que, dans une Europe où manquent des TGV «Finlande-Espagne» ou «France-Pologne», comme des équipements et des personnels pour les universités, les centres de recherche ou les maisons de retraite d’un continent submergé par le tsunami gériatrique, la crise planétaire des liquidités interbancaires, du choc immobilier sur plusieurs économies et de la baisse de confiance des entrepreneurs et des travailleurs vient exiger un effort budgétaire sans aucune mesure avec le budget européen habituel.
Il faut donc une programmation budgétaire d’exception pour un grand plan d’infrastructures, approuvé par un grand «référendum financier européen». C’est à dire un emprunt continental, d’un montant équivalent aux 1 700 milliards d’euros mobilisés pour le secteur bancaire. 
