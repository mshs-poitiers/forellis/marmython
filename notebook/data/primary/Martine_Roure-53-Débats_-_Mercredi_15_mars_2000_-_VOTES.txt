Martine Roure # Débats - Mercredi 15 mars 2000 - VOTES
  Roure (PSE
),
par écrit
. - Cette directive modifie la définition même du chocolat qui devrait être, nous le pensons, fabriqué à partir des fèves et du beurre de cacao.
Nous n'avons pas le droit d'induire le consommateur en erreur en employant l'appellation chocolat pour désigner des produits qui ne sont pas nécessairement fabriqués à partir du cacao.
Les consommateurs exigent une information claire et nette. Ils ne souffrent plus d'être trompés. Le vote d'aujourd'hui allait bien au-delà du seul problème du chocolat. C'est la confiance de nos concitoyens qui est en jeu.
C'est pourquoi j'ai voté tous les amendements qui visaient à améliorer cette directive et qui étaient présentés par la commission de l'environnement, de la santé publique et de la politique des consommateurs. 
