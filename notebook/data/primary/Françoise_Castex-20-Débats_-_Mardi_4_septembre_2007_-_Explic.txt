Françoise Castex # Débats - Mardi 4 septembre 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Toubon (A6-0295/2007
) 
  Françoise Castex  (PSE
), 
par écrit
. –
 J'ai voté contre le rapport relatif au réexamen du marché unique: combattre les obstacles et l'inefficacité par une meilleure mise en œuvre et une meilleure application.
Je désapprouve l'approche du rapporteur, M. Toubon, selon laquelle l'ouverture à la concurrence dans le marché unique ne connaîtrait ni limite, ni régulation sociale. Aussi, j'ai voté contre la poursuite de la libéralisation du service postal que défendait le rapport et regrette que les amendements relatifs à l'harmonisation sociale et fiscale aient été rejetés.
D'autre part, je dénonce la nouvelle référence à la mise en œuvre d'un marché intérieur transatlantique sans entrave et je mets en avant la priorité de sauvegarde et de promotion du modèle social européen.
Pour moi, l'achèvement du marché intérieur doit être régulé par des garanties de protection sociale pour les citoyens européens, ce que ne propose pas le rapport Toubon, seule contribution du Parlement européen à la révision du marché unique. 
