Bruno Gollnisch # Débats - Jeudi 18 décembre 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Gianluca Susta (A6-0447/2008
) 
  Bruno Gollnisch  (NI
), 
par écrit. – 
La contrefaçon n’est pas simplement un problème de respect des droits de propriété intellectuelle. Comme le souligne le rapporteur, ce phénomène tue l’incitation à l’innovation, cause la disparition de milliers d’emplois qualifiés ou non en Europe, jette les bases d’une économie clandestine contrôlée par le crime organisé. Ces pratiques illégales peuvent également mettre en danger la sécurité et la santé des consommateurs ou occasionner de graves dommages environnementaux.
Le problème de la qualité et de la dangerosité des produits importés, dont la contrefaçon ne fait que renforcer les risques en induisant les consommateurs en erreur, est plus général. Les pays d’origine en sont clairement identifiés, au premier rang desquels la Chine. L’Union accepte même parfois d’ouvrir ses marchés à des produits ne respectant pas les normes qu’elle impose à ses propres producteurs, comme par exemple le poulet au chlore, moins cher à produire que les poulets soumis aux contrôles vétérinaires.
Dans l’arsenal de mesures proposées par le rapporteur (accords bi- ou multilatéraux, coopération avec les pays d’origine, coopération entre les services européens concernés...), il en manque à l’évidence deux:
- les sanctions commerciales contre les États qui s’accommodent de ces pratiques;
- l’établissement d’une préférence nationale et européenne généralisée! 
