Patrick Gaubert # Débats - Jeudi 10 avril 2008 - Explications du vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Proposition de résolution: situation au Tibet (B6-0133/2008
) 
  Patrick Gaubert  (PPE-DE
), 
par écrit
. – 
(FR)
 Je me félicite de l'adoption d'une résolution commune à tous les groupes politiques sur le Tibet. Ce texte condamne les récentes répressions au Tibet, à la suite des résolutions précédentes de ce Parlement appelant les deux parties au dialogue.
Nous en appelons également au gouvernement chinois et aux partisans du dalaï-lama de cesser toute violence. Le gouvernement chinois ne doit pas exploiter la tenue des Jeux olympiques de 2008 de façon abusive en arrêtant les dissidents, journalistes et militants des droits de l'homme.
Nous demandons à la Présidence en exercice de l'Union de s'efforcer de dégager une position européenne commune en ce qui concerne la présence des chefs d'État et de gouvernement et du Haut représentant de l'Union européenne à la cérémonie d'ouverture des Jeux olympiques, et de prévoir son boycottage au cas où le dialogue ne reprendrait pas entre les autorités chinoises et sa sainteté le dalaï-lama. 
