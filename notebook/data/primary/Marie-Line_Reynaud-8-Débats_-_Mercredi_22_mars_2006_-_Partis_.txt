Marie-Line Reynaud # Débats - Mercredi 22 mars 2006 - Partis politiques européens (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie-Line Reynaud (PSE
). 
 - Monsieur le Président, je tiens tout d’abord à remercier le rapporteur pour la qualité de son travail.
Les partis politiques européens sont indispensables à la formation et à l’expression d’une véritable opinion publique européenne. C’est, en effet, principalement à eux que revient la difficile tâche d’œuvrer à la participation effective des citoyens, et cela pas seulement tous les cinq ans à l’occasion des élections européennes, mais au quotidien et dans tous les aspects de la vie politique européenne.
Or, le rapport de M. Leinen propose des pistes qui permettraient de donner aux partis européens les moyens nécessaires à la réalisation de cet objectif. Je salue en particulier les points suivants: l’amélioration des règles en matière de financement par l’introduction de davantage de clarté, de souplesse, d’indépendance et de sécurité financière sur le moyen terme; le soutien indispensable aux organisations et mouvements de jeunes européens; enfin, une meilleure représentation des femmes sur les listes électorales, et surtout parmi les élus. 
