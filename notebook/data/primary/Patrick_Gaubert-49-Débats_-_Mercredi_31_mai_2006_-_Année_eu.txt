Patrick Gaubert # Débats - Mercredi 31 mai 2006 - Année européenne du dialogue interculturel (2008) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Patrick Gaubert (PPE-DE
), 
rapporteur pour avis de la commission des affaires étrangères
. - Monsieur le Président, Monsieur le Commissaire, mers chers collègues, rapporteur pour avis de la commission des affaires étrangères, je voudrais, avant tout, féliciter la rapporteure pour son excellent travail et l’importance qu’elle a accordée à notre collaboration.
Le projet européen tire sa richesse de la diversité culturelle des États membres de l’Union. Aussi le dialogue interculturel est-il aujourd’hui, plus que jamais, un processus à encourager. L’année européenne du dialogue interculturel en sera l’occasion. À cette fin, j’envisage l’échange dans un double sens.
Les citoyens européens doivent pouvoir s’ouvrir aux cultures des autres pays européens et des pays tiers, notamment les pays du voisinage. Il s’agit aussi de sensibiliser le reste du monde aux cultures et valeurs de l’Union afin de favoriser la compréhension mutuelle. De même, le dialogue interculturel se doit d’être un vecteur d’intégration sociale des immigrés.
Rapporteur fictif au sein de la commission des libertés civiles pour l’année européenne 2007, j’insiste sur l’importance de la cohésion et de la cohérence des actions qui seront menées au cours de cette année 2007 pour l’égalité des chances pour tous, comme j’insiste sur l’importance de la cohésion des actions qui seront menées dans le cadre du dialogue culturel en 2008, car ces deux années sont complémentaires. De plus, j’invite la Commission et les parties concernées - notamment la société civile comme le disait le commissaire - à saisir l’opportunité des grands événements prévus en 2008, tels que la Coupe du monde de football ou les Jeux olympiques, pour sensibiliser la population, et notamment le groupe cible des jeunes, au dialogue interculturel. 
