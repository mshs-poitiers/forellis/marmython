Jean-Claude Fruteau # Débats - Mercredi 14 mars 2007 - Relations euroméditerranéennes - Construction de la zone de libre-échange euroméditerranéenne (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Claude Fruteau (PSE
), 
rapporteur pour avis de la commission de l’agriculture et du développement rural
. - Madame la Présidente, Monsieur le Commissaire, chers collègues, je tiens tout d’abord à féliciter notre rapporteur M. Kader Arif pour la pertinence de ses analyses.
D’un point de vue agricole, si l’ouverture des marchés offre aujourd’hui de réelles perspectives de développement économique de part et d’autre de la Méditerranée, il importe que cette évolution se fonde sur l’expérience des populations locales et les acteurs de terrain. Il est indispensable que le processus soit mis en œuvre de manière mesurée, produit par produit, et selon un calendrier progressif afin de tenir compte des petites exploitations qui sont à la fois les plus fragiles, les plus nombreuses et les plus aptes à développer une agriculture multifonctionnelle respectueuse des ressources naturelles et du développement local.
Ce travail de régulation passe par le renforcement des préférences commerciales sur la base d’une relation asymétrique au profit des pays les plus vulnérables. Il passe en outre par des mesures d’accompagnement qui permettront d’aider ces derniers à moderniser leurs structures de production et qui contribueront au développement de synergies, à travers des coopérations techniques et financières entre professionnels, ainsi que par le biais de politiques de labellisation communes. 
