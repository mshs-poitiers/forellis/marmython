Martine Roure # Débats - Jeudi 15 janvier 2004 - Votes
  Roure (PSE
),
par écrit
. 
- L’intégration des migrants, soit économiques, soit humanitaires, est un défi majeur pour nos sociétés: le succès de notre politique d’immigration se mesurera en effet à l’aune du succès de nos politiques d’intégration. Nous devons construire un contrat de valeurs entre nos sociétés et les communautés ethniques qu’elles hébergent. Nous sommes tous concernés.
Nous devons accueillir les immigrants et respecter la diversité de leurs cultures, de leurs religions et de leurs traditions. Les immigrants eux-mêmes doivent, de leur côté, accepter nos valeurs traditionnelles telles qu’elles sont développées dans la Charte européenne des droits fondamentaux: respect pour la démocratie, la liberté, les droits de l’homme, l’égalité entre les hommes et les femmes, la liberté religieuse. Ces valeurs ne peuvent en aucune circonstance, être remises en cause. Le mot de passe est "la diversité dans l’unité".
Dans un souci de renforcer les politiques d’intégration, une réflexion doit être menée sur un concept de citoyenneté civique qui pourrait être développé pour les ressortissants de pays tiers. Cela comprendrait un ensemble de droits et de responsabilités: l’égalité de traitement dans le domaine social, économique et politique, et en matière de conditions de travail, ainsi que le droit de vote aux élections locales et européennes. 
