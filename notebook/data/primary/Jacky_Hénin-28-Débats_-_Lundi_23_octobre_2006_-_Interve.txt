Jacky Hénin # Débats - Lundi 23 octobre 2006 - Interventions d’une minute sur des questions politiques importantes
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jacky Henin (GUE/NGL
). 
 - Monsieur le Président, des choix stratégiques et industriels d’Airbus dépendent non seulement le sort de dizaines de milliers d’emplois mais aussi l’avenir d’une partie non négligeable de l’industrie aéronautique européenne. Il est impératif de respecter le savoir-faire, le travail, l’investissement des salariés d’Airbus et des sous-traitants en empêchant qu’ils soient réduits à néant. Les salariés d’Airbus ne doivent en aucun cas payer les erreurs et les fautes des dirigeants et actionnaires. Ils représentent, par leur savoir-faire, la véritable richesse d’Airbus. Airbus est malade du libéralisme et il convient pour le guérir de revenir à une propriété et à un financement publics d’EADS à l’échelle européenne.
Je constate d’ailleurs avec satisfaction que beaucoup de voix s’élèvent tant en Allemagne qu’en Espagne pour donner de véritables pouvoirs d’intervention aux salariés sur la gestion d’Airbus.
Que M. Mandelson fasse son travail afin que les Américains ne contestent plus le système des avances remboursables et que la Banque centrale européenne et la Commission interviennent enfin pour contrer le dollar faible. 
