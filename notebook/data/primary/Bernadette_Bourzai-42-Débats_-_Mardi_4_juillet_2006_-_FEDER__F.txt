Bernadette Bourzai # Débats - Mardi 4 juillet 2006 - FEDER, FSE, Fonds de cohésion (dispositions générales) - Institution du Fonds de cohésion - Fonds social européen - Fonds européen de développement régional - Groupement européen de coopération territoriale (GECT) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bernadette Bourzai (PSE
). 
 - Monsieur le Président, chers collègues, je tiens d’abord à saluer le travail considérable des collègues rapporteurs et députés, qui a permis d’améliorer la proposition de la Commission dans une perspective de partenariat, de non-discrimination et, surtout, d’équilibre entre les régions des anciens et des nouveaux États membres.
Il est regrettable cependant que les montants consacrés à cette politique majeure de l’Union européenne ne soient pas la hauteur de nos ambitions.
Par ailleurs, si les politiques urbaines ont été au cœur de nombreux débats, il n’en a pas été de même pour le monde rural dont l’avenir est désormais lié au deuxième pilier de la PAC, qui connaît pourtant une baisse drastique de ses crédits. Il faudra donc veiller à la bonne articulation, sur le terrain, des fonds structurels avec le nouveau fonds agricole de développement rural.
Enfin, je regrette cette décision du Conseil de flécher les crédits de la politique régionale de façon massive sur les objectifs de Lisbonne, qui plus est, sans véritable consultation du Parlement sur la classification des dépenses.
Il faudra être vigilant pour que les programmes opérationnels dans nos régions comportent assez de flexibilité pour prendre en compte les investissements structurants, encore nécessaires et la solidarité sociale.
Enfin, je rappelle que cette réforme doit, certes, contribuer aux objectifs de compétitivité, mais aussi et surtout, à la cohésion économique, sociale et territoriale. 
