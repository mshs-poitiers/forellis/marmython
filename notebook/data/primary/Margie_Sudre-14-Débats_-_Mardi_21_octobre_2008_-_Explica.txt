Margie Sudre # Débats - Mardi 21 octobre 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
– Rapport Reimer Böge (A6-0399/2008
) 
  Margie Sudre  (PPE-DE
), 
par écrit
. – Notre Parlement vient d’approuver l’aide de 12,78 millions d’euros, proposée par la Commission européenne en faveur de la Martinique et de la Guadeloupe, destinée à couvrir une partie des dépenses opérées dans l’urgence, l’été dernier, suite au passage du cyclone Dean.
L’aide financière sera la bienvenue, d’autant que la Martinique et la Guadeloupe continuent aujourd’hui encore à ressentir les effets des dommages causés par Dean, en particulier dans le domaine des habitations et des filières agricoles de la banane et de la canne à sucre.
Le Fonds de solidarité, qui intervient ici en application d’une exception aux règles générales, est particulièrement important pour les RUP, étant donné la multiplicité des menaces qui pèsent régulièrement sur leurs populations, alors que les Caraïbes ont encore été frappées la semaine dernière par le cyclone Omar.
Je me suis beaucoup engagée, depuis la création de ce Fonds en 2002, pour que les DOM puissent profiter de ce type de soutien. L’expérience acquise par le gouvernement français dans la présentation de ses demandes, et la compréhension dont font preuve la Commission, le Parlement européen et le Conseil nous rassurent dans la capacité de l’Europe à être aux côtés des populations ultramarines confrontées à des crises majeures. 
