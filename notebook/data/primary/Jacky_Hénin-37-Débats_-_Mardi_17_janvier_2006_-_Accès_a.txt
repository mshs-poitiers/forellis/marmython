Jacky Hénin # Débats - Mardi 17 janvier 2006 - Accès au marché des services portuaires
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jacky Henin (GUE/NGL
). 
 - Monsieur le Président, la directive sur les services portuaires qu’on nous propose pour la seconde fois tourne totalement le dos à l’intérêt général, à l’amélioration des conditions de vie des citoyens européens. Pire, elle ne sert que les intérêts financiers des grandes transnationales du fret au détriment de tous les acteurs de la filière portuaire, du docker au petit entrepreneur. Son seul but: casser les statuts, les protections sociales, tirer vers le bas les salaires pour enrichir une minorité et cela au détriment de la sécurité des hommes et de l’environnement. Le modèle social de cette directive, c’est la loi de la jungle; l’autoassistance c’est la renaissance de l’esclavage!
Toutes les professions technico-nautiques, tous les syndicats européens rejettent unanimement cette Bolkestein portuaire. Hier, j’ai manifesté avec les 10 000 dockers venus de toute l’Europe devant le Parlement. Je peux témoigner de leur détermination à barrer la route à cette directive scélérate. Je puis vous assurer que voter une telle directive, c’est prendre la responsabilité de bloquer par des grèves dures de nombreux ports de l’Union, avec toutes les conséquences économiques que cela comporte.
Je rappellerai, comme d’autres, à la Commission que les représentants des peuples de l’Union que nous sommes avaient déjà repoussé cette directive. La reproposer quasiment à l’identique constitue une provocation politique contre le Parlement, contre l’ensemble des autorités portuaires. Au nom de la construction d’une autre Europe recherchant un haut niveau d’intégration sociale et démocratique, nous appelons à rejeter cette directive. 
