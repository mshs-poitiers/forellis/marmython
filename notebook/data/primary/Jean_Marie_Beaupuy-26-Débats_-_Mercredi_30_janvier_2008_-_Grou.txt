Jean Marie Beaupuy # Débats - Mercredi 30 janvier 2008 - Groupement européen de coopération territoriale (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean Marie Beaupuy  (ALDE
). – 
(FR) 
Monsieur le Président, Madame la Commissaire, laissez-moi vous dire la grande joie que j'éprouve ce soir d'intervenir sur ce dossier puisque, lorsque nous avons examiné ce rapporté, présenté par Jan Olbrycht, j'ai dit ma foi dans ce rapport.
Or, il se trouve qu'hier, ce sujet a été traité à l'Assemblée nationale, en France, par le parlement, et que - fait qui mérite d'être souligné car ce n'est pas très fréquent - tous les groupes politiques ont applaudi à la création de ce GECT, et les exemples ont été multipliés pour expliquer comment, au Nord de la France, au Sud de la France, dans les Alpes, en Lorraine, il y avait matière à mettre en œuvre très vite ce GECT. Cela s'appuyait notamment, Madame la Commissaire, vous l'avez dit, sur l'exemple de Lille, qui a été signé lundi dernier pour deux millions d'habitants en Belgique et en France.
Pourquoi cela a-t-il provoqué un grand espoir? C'est parce que, pendant trois à quatre ans, nos collègues ont cherché des formules juridiques et ne les ont pas trouvées. Avec le GECT, ils ont trouvé la solution. Maintenant, nous attendons que les 70 000 Français qui travaillent tous les jours au Luxembourg, les 30 000 Français qui travaillent à Monaco et en Italie, autant en Espagne, etc., puissent, avec le GECT, nourrir vraiment de nouvelles collaborations.
Madame la Commissaire, avec mes collègues, j'attends que vous relanciez les États qui sont en retard. En plus de la contrainte réglementaire qui s'impose, vous pourrez leur dire que certains pays le mettent en œuvre avec bonheur, avec plaisir. 
