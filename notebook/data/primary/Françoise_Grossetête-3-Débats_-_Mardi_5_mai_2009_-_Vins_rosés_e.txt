Françoise Grossetête # Débats - Mardi 5 mai 2009 - Vins rosés et pratiques œnologiques autorisées (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Françoise Grossetête, 
auteure
. − 
Madame la Commissaire, tout à l’heure, vous nous avez dit qu’aucune suggestion ne vous avait été faite, que vous attendiez des propositions, etc.
Si, des suggestions vous ont été faites. Vous avez rencontré nos viticulteurs. Ils vous ont dit qu’ils ne voulaient pas de cela. Ils vous ont dit qu’ils ne voulaient pas, eux, avoir à souffrir d’être obligés d’indiquer l’appellation «vin rosé traditionnel» pour se différencier du vin rosé sur lequel on ne mettra pas «coupé», bien évidemment. Vous ne pouvez donc pas dire qu’on ne vous a rien proposé.
Et surtout, s’agissant du champagne rosé, on sait très bien que c’est un produit œnologique qui n’a rien à voir avec le coupage tel qu’il est proposé, vin blanc et vin rouge. Ayez le courage, Madame la Commissaire, de ne pas appeler le vin coupé blanc et rouge, vin rosé. Voilà ce que l’on vous demande. C’est la suggestion que l’on vous fait. Ne pénalisez pas les vrais viticulteurs.
Comment voulez-vous que nos concitoyens puissent comprendre une telle position de la part de la Commission européenne? Vous restez sourde à tous les arguments, c’est absolument incompréhensible.
