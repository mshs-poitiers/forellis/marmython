Catherine Trautmann # Débats - Mercredi 6 mai 2009 - Réseaux et services de communications électroniques (A6-0272/2009, Catherine Trautmann) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Catherine Trautmann, 
rapporteure
. −
 Madame la Présidente, je voudrais tout d’abord faire remarquer, en ce qui concerne la demande de changement sur la liste des votes, que, si le compromis – si j’ai bien compris – a été placé, par les services de la séance qui ont fait preuve de bon sens, avant l’amendement déposé par des groupes, c’est parce qu’il va plus loin que l’amendement 46. Il comporte des clauses qui ne se limitent pas à la seule restriction de l’accès à internet mais qui comprennent aussi l’ensemble des dispositifs qui peuvent gêner les droits des usagers.
Deuxièmement, je voudrais également dire aux collègues que ce compromis a été déposé à l’article 1, qui concerne le champ d’application, et qu’il a donc une portée transversale, alors que l’amendement déposé par les groupes concerne l’article 8, qui touche les objectifs des régulateurs nationaux.
Pendant tout le temps qu’ont duré les négociations sur ce compromis, j’ai travaillé dans une bonne entente et loyalement avec l’ensemble des groupes politiques. Je prends acte, Madame la Présidente, du fait que l’un des groupes a retiré, à l’instant même, sa signature à ce compromis. Je voudrais donc vous dire que, en tant que rapporteur, je continue, bien sûr, de soutenir ce compromis, et que je me suis également exprimée en faveur de l’amendement 46.
Je souhaiterais dire que, dans les conditions dans lesquelles nous débattons, il est plus sage que ce soit notre Assemblée qui se prononce sur l’ordre des votes, plutôt que vous ou le rapporteur soyez les seuls à prendre cette décision. Cependant, je voudrais vous présenter cette demande très directement, car il faut comprendre ce qui se passe ensuite s’il y a une inversion du vote.
S’il n’y a pas d’inversion du vote, la liste reste la même. S’il y a une inversion du vote, le 46 est voté s’il obtient la majorité qualifiée. Dans ce cas-là, Madame la Présidente, je vous demande également de mettre aux voix le compromis – qui va, en effet, plus loin que le 46 – après celui-ci. Si le 46 n’obtient pas la majorité qualifiée, à ce moment-là, nous voterons sur le compromis et, ainsi, notre Assemblée aura opéré son choix.
