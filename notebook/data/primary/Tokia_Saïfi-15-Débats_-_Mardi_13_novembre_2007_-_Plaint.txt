Tokia Saïfi # Débats - Mardi 13 novembre 2007 - Plainte de Boeing (US) contre Airbus (UE) dans le cadre de l'OMC (débat) 
  Tokia Saïfi  (PPE-DE
), 
par écrit. 
– 
(FR) 
Boeing et Airbus s'accusent mutuellement depuis des années de bénéficier de subventions gouvernementales illicites. Ce jeu de plaintes croisées en délibération à l'organe des différends de l'OMC, ne cesse d'alimenter le contentieux entre les deux plus importants constructeurs aéronautiques mondiaux. Ces accusations de subventions et d'aides directes, si elles sont confirmées et reconnues, constituent de graves entraves au système économique mondial et créent de fait une distorsion au libre marché avec de forts préjudices sur le système concurrentiel dicté par l'OMC. Or, à l'heure où l'on défend un système mondial libre de droits, dénoué de toute entrave, cette guerre commerciale est une entaille à la concurrence mondiale qui se veut loyale et équitable. Seul l'organe de juridiction de l'OMC sera à même de rendre des conclusions impartiales.
Néanmoins, on sait que Boeing a la volonté de se positionner comme leader mondial et il ne faudrait pas que la plainte déposée vienne comme une tentative de déstabilisation envers l'avionneur Airbus. Ce litige commercial est délicat mais ne devra pas pour autant nuire à la coopération transatlantique à l'heure où, justement, le Cycle de Doha est au point mort et où l'on espère une issue favorable pour la fin de l'année. 
