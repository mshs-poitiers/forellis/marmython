Margie Sudre # Débats - Mardi 4 juillet 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Lucas (A6-0201/2006
) 
  Margie Sudre (PPE-DE
), 
par écrit
. - Bien que l’objectif de réduction de l’impact de l’aviation sur le changement climatique, notamment la réduction des émissions de CO2, soit hautement recommandable, il me semble indispensable que notre Parlement prenne en compte la situation des régions de l’Union les plus éloignées.
La suppression de l’exemption en matière de TVA dont bénéficie le transport aérien, et l’instauration d’une taxe sur le kérosène, si elles devaient être véritablement envisagées, auraient des effets désastreux sur l’économie des régions les plus isolées, ainsi que sur la mobilité de leurs populations.
C’est le cas notamment des régions insulaires, dont les territoires ne bénéficient ni de la route ni du rail pour accéder au reste de l’Union. C’est le cas, plus encore, des régions ultrapériphériques, pour lesquelles la circulation des personnes repose exclusivement sur le transport aérien.
Je remercie les membres du Parlement européen d’avoir adopté mon amendement demandant que la législation communautaire accorde une attention particulière aux territoires les plus isolés qui dépendent fortement du transport aérien, particulièrement les îles et les régions ultrapériphériques, où les alternatives au transport aérien sont extrêmement limitées, voire totalement inexistantes. 
