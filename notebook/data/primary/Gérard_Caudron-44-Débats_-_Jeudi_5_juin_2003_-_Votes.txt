Gérard Caudron # Débats - Jeudi 5 juin 2003 - Votes
  Caudron (GUE/NGL
),
par écrit
. - 
Dans ce débat, ma position est claire.
Premièrement, la PAC actuelle n’est certes pas parfaite. Elle est coûteuse et parfois injuste. Pour autant, c’est à cette PAC qu’on doit l’existence, encore aujourd’hui, d’une agriculture en Europe.
Deuxièmement, la PAC doit sans doute être réformée pour s’adapter à la situation européenne et mondiale d’aujourd’hui. Elle doit être mieux ciblée sur des objectifs de qualité, d’environnement et de maintien de l’emploi rural.
Pour autant, ce qui nous est proposé, habilement camouflé sous des oripeaux de justice et d’écologie (au nom du fameux "découplage"), conduira inévitablement, d’ici 10 ans, à la disparition de la PAC et, dans son sillage, à la disparition d’une véritable agriculture européenne diversifiée et autosuffisante.
C’est pourquoi je ne peux accepter les propositions qui nous sont faites. 
