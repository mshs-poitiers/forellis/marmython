Marie-Arlette Carlotti # Débats - Mercredi 12 janvier 2005 - Explications de vote
  Carlotti (PSE
),
par écrit
. - 
Je me félicite des avancées portées par le Traité constitutionnel en matière de développement et de solidarité internationale, dont le principe est consacré parmi les valeurs fondamentales de l’Union.
Ce Traité intègre, pour la première fois, un chapitre distinct portant sur la coopération avec les pays tiers et l’aide humanitaire.
Il met l’accent sur les objectifs propres à cette politique que sont l’éradication de la pauvreté, la promotion de la santé ou la lutte contre les maladies infectieuses. Il accorde une priorité aux droits de l’enfant. Il porte de nombreuses avancées en faveur des droits de la femme et de leur rôle décisif dans le développement.
L’Union européenne franchit donc une étape importante en reconnaissant dans son Traité constitutionnel que la solidarité ne peut pas se limiter à son propre territoire et à ses propres citoyens, mais qu’elle doit s’étendre au-delà de ses frontières.
Bien sûr, j’ai quelques regrets, notamment l’absence de référence aux «biens publics mondiaux» sur laquelle j’avais pourtant fait des propositions.
Mais avec mes camarades socialistes, je compte poursuivre le combat pour inscrire dans l’action et les pratiques ce que nous n’avons pu inscrire dans le texte du Traité. 
