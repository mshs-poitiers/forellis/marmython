Véronique Mathieu # Débats - Mercredi 28 novembre 2007 - Contrôle de l’acquisition et de la détention d’armes (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Véronique Mathieu  (PPE-DE
). 
 – (FR) 
Monsieur le Président, Monsieur le Commissaire, chers collègues, ce texte que nous voterons demain est un compromis qui, finalement, satisfait à peu près tous les utilisateurs légaux d'armes. Le texte de départ de la Commission nous convenait parfaitement, les positions du Conseil nous convenaient aussi. Malheureusement, les positions du rapporteur étaient assez hurluberlues et nous avons dû lutter farouchement contre ses positions de départ. Je remercie d'ailleurs M. Podesta pour son véritable travail de fourmi, pour sa patience, pour la diplomatie qu'il a déployée au sein du groupe PPE et lors des nombreuses réunions de travail.
Les compromis que nous sommes parvenus à élaborer satisfont tous les utilisateurs légaux d'armes. Je dois dire que les chasseurs français sont satisfaits du maintien des quatre catégories: c'était un point très important pour la France et je suis très heureuse ce soir de dire que nous avons pu les conserver. Je suis contente aussi de dire que le fichier central me satisfait parce qu'il est logique également de pouvoir assurer la traçabilité des armes. Je pense que, pour la sécurité des citoyens, c'est un point très important. Les armuriers sont contents aussi du marquage CIP. Nous sommes contents aussi de la vente à distance. Nous sommes relativement satisfaits du texte dans son ensemble.
Cela dit, je pense que l'année de travail écoulée doit nous faire réfléchir à la question des positions de départ des rapporteurs et je dois dire qu'il faut se garder d'avoir des positions trop arrêtées sur certains points de départ. En effet, si la Commission et le Conseil, si le groupe PPE n'avaient pas défendu vigoureusement leurs positions, je pense que l'on se serait acheminé vers un texte qui aurait été inapplicable et vers des positions, vers une idéologie vertes qui auraient largement nui aux chasseurs et aux utilisateurs légaux d'armes. 
