Michel Raymond # Débats - Mardi 11 mars 2003 - Votes
  Bernié, Butel, Esclopé, Mathieu, Raymond et Saint-Josse (EDD
),
par écrit
. - Nous maintenons notre objection de principe à l'introduction d'un financement de partis politiques européens à partir des fonds communautaires.
Toutefois, dans l'hypothèse où ce projet serait tout de même imposé par les grands groupes, il nous semble grandement préférable que ces crédits restent dans le budget de la Commission qui restera responsable de leur exécution et assumera ainsi jusqu'au bout la responsabilité de son initiative, plutôt que de les déplacer vers le budget du Parlement et perpétuer la confusion entre groupes parlementaires et partis politiques.
Nous avons donc soutenu l'amendement 8, mais uniquement sur ce principe de technique budgétaire et pour la responsabilité qui en découle, mais nous restons opposés à la proposition dans son ensemble. 
