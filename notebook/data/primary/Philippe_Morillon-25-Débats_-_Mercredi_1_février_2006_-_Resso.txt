Philippe Morillon # Débats - Mercredi 1 février 2006 - Ressources halieutiques de la Méditerranée
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Philippe Morillon (ALDE
), 
auteur
. - Monsieur le Président, Monsieur le Président en exercice du Conseil, la question orale qui nous vaut le débat de ce soir et les propositions qui seront soumises demain au vote de notre Parlement reposent sur le constat que les seules mesures de gestion applicables à ce jour à la pêche en Méditerranée remontent à 1994. Or, celles-ci ne sont plus adaptées à la situation de la ressource ni aux décisions prises sur d’autres rivages de l’Union européenne pour mettre en œuvre une politique commune de la pêche qui autorise le développement durable de ce secteur de notre économie et la préservation des ressources que l’Union doit pouvoir continuer à tirer de l’exploitation de ses mers pour faire face à la nécessité d’assurer, à terme, la suffisance alimentaire de ses concitoyens.
Un tel décalage s’explique d’abord par le fait que la Méditerranée est reconnue comme l’une des régions les plus diversifiées et les plus complexes, tant du point de vue biologique que du point de vue écologique, social et économique. C’est la raison pour laquelle notre Parlement n’avait pu parvenir à un accord au cours de la législature précédente et que le rapport de notre collègue Lisi avait abouti à un constat d’échec. C’est la raison pour laquelle, aussi, ce sujet a été réinscrit en priorité à l’ordre du jour des travaux de cette nouvelle législature et qu’il a donné lieu à l’élaboration d’un très délicat compromis au terme des travaux de notre rapporteur, Mme Carmen Fraga, à l’engagement personnel de laquelle je me dois de rendre hommage ici.
Ce rapport a été adopté, en commission d’abord, puis en plénière, au mois de juin dernier. Les mesures qu’il préconise n’ont, à ce jour, fait l’objet d’aucune décision d’application par le Conseil. Nous connaissons une partie des raisons de cet attentisme, mais j’en avais, à titre personnel, informé mes collègues en tentant de faire passer un certain nombre d’amendements visant à éviter l’interdiction de l’usage de certains types de filets employés traditionnellement par des pêcheurs de la région.
Cette affaire concernait et continue de concerner 75 bateaux qui font vivre 350 familles et qui font 80% de leur chiffre d’affaires par l’emploi de filets ancrés, appelés «tenailles». Ce type de pêche avait pour inconvénient la capture accidentelle de dauphins jusqu’à ce que la profession mette au point un programme permettant de réduire de 80% ces captures accidentelles par la mise en œuvre de répulsifs auditifs et l’embarquement systématique d’observateurs.
Si j’évoque à nouveau ce sujet, c’est parce que les retombées socio-économiques des mesures préconisées méritent certainement d’être mieux prises en considération. C’est pourquoi je soutiens, et mon groupe soutiendra, la proposition de résolution déposée par le groupe des Verts, insistant, à l’article 4, pour que les pêcheurs concernés par le nouveau règlement et contraints de changer leurs méthodes de pêche reçoivent l’indemnité appropriée.
Le commissaire européen en charge de la pêche, Joe Borg, a pu mesurer, hier, lors de la réunion de notre commission, l’émotion suscitée auprès de nos amis espagnols par la perspective d’une interdiction des filets maillants pour la pêche dans certaines zones proches du littoral espagnol, ce qui mettrait en cause l’avenir d’une flottille de 80 bateaux qui font vivre 1 500 personnes. Il s’agit là d’un cas tout à fait comparable à celui des petits métiers de la pêche artisanale sur les côtes françaises de la Méditerranée.
Cela étant, Monsieur le Président, si je peux comprendre que certaines décisions proposées par le rapport Fraga méritent d’être encore discutées et peut-être complétées d’une étude d’impact, cela ne saurait justifier de geler indéfiniment l’application de l’ensemble d’un rapport dont tout le monde s’accorde à reconnaître qu’il constitue en soi un bon compromis. 
