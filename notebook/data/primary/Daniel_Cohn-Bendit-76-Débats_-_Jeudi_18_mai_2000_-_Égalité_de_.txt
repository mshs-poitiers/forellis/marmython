Daniel Cohn-Bendit # Débats - Jeudi 18 mai 2000 - Égalité de traitement entre les personnes
  Cohn-Bendit (Verts/ALE
).
- (DE)
 Monsieur Poettering, pour toute question, le droit est sujet à l'interprétation. Je pense que la position de M. Cox, qui a atteint avec vous la majorité stratégique pour élire Mme Fontaine, est un compromis qui nous permet à tous de voter la tête haute un rapport sur le racisme. En ce qui concerne l'Autriche, les avis peuvent diverger, mais là où il importe de préciser quelle position nous défendons par rapport au racisme en Europe, nous ne pouvons pas comme auparavant nous perdre dans des débats formels, nous devons plutôt faire comprendre que nous sommes des personnes clairvoyantes. Je vous le demande : acceptez le compromis de M. Cox et permettez-nous d'aborder ce débat la tête haute, s'il vous plaît, s'il vous plaît, s'il vous plaît ! 
