Dominique Vlasto # Débats - Jeudi 15 février 2007 - Augmentation du prix de l’énergie (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Dominique Vlasto (PPE-DE
), 
par écrit
. - L’augmentation du prix de l’énergie, malgré une parité euro/dollar favorable, reste un facteur d’instabilité dans l’économie européenne. Elle reflète également la trop grande dépendance de notre économie à l’égard du pétrole: les prix du pétrole ont flambé, alors même que ceux d’autres énergies sont restés stables ou ont moins augmenté.
Il faut donc nuancer une situation qui devrait amener une réponse politique articulée autour des points suivants. L’émergence de leaders européens de l’énergie, car eux seuls sont capables de négocier d’égal à égal avec nos principaux fournisseurs et ce au bénéfice des États membres. Ils doivent pouvoir continuer à conclure des contrats à long terme pour sécuriser leurs approvisionnements et amortir de lourds investissements. Enfin, il faut éviter de les affaiblir en préconisant à tout prix la séparation patrimoniale comme facteur de compétitivité du marché, ce qui est illusoire.
La promotion d’un véritable mix énergétique reposant sur les énergies renouvelables, le nucléaire, l’efficacité énergétique et l’augmentation des budgets de recherche dédiés aux énergies du futur comme ITER.
Tout en préparant l’ère de l’après-pétrole, ces politiques permettraient à l’Union de rester compétitive en assurant, pour son économie, un approvisionnement énergétique en moyenne bon marché. 
