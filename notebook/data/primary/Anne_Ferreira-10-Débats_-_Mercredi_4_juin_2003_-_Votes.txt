Anne Ferreira # Débats - Mercredi 4 juin 2003 - Votes
  Ferreira (PSE
),
par écrit
. -
 Contrairement à la décision de mon groupe politique, j’ai voté en faveur des amendements du bloc 3.
Il m’a semblé essentiel de soutenir l’amendement 1, car celui-ci introduit la référence au principe de précaution.
Même si je comprends le souhait du rapporteur, dont je tiens à souligner le travail et la grande disponibilité, de conclure cette procédure législative dès la seconde lecture, il m’est impossible de voter contre la référence au principe de précaution. Celle-ci me paraît fondamentale et nécessaire pour un thème aussi sensible, et loin d’être tranché, que les organismes génétiquement modifiés.
Le Conseil a refusé d’intégrer cette référence au principe de précaution, qui est faite dans un considérant: c’est sa décision et je la respecte. Mais j’estime que le Parlement européen n’a pas à assumer à la place du Conseil le refus de faire référence au principe de précaution, qui, de plus, est couvert par une législation communautaire. C’est conférer aux OGM une place particulière, c’est établir un précédent, que d’autres secteurs d’activité pourraient utiliser pour récuser certaines propositions et orientations politiques à l’avenir. C’est un recul par la négation d’un principe qui représente tout de même une avancée. 
