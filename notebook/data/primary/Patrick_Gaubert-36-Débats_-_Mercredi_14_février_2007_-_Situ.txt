Patrick Gaubert # Débats - Mercredi 14 février 2007 - Situation au Darfour (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Patrick Gaubert (PPE-DE
), 
par écrit.
 - Depuis le déclenchement des hostilités en février 2003, le Darfour, l’une des régions les plus pauvres du Soudan, est le théâtre d’une crise humanitaire sans précédent. Quatre années de conflits et de souffrance pour le peuple du Darfour, quatre résolutions de notre Assemblée pour marquer notre indignation, mais la situation demeure extrêmement préoccupante.
Aujourd’hui encore, notre Assemblée réitère dans le cadre d’une résolution commune sur le Darfour, ses préoccupations et demande instamment aux Nations unies, aux États membres, au Conseil et à la Commission de prendre leurs responsabilités et de déterminer clairement une date pour le déploiement d’une force de maintien de la paix, sous mandat de l’ONU, pour sécuriser les corridors humanitaires sans délai et atténuer ainsi l’immense détresse à laquelle fait face le peuple du Darfour.
Je me félicite de cette nouvelle initiative, mais face à l’urgence de la situation, l’Europe ne peut plus se contenter de réitérer des demandes déjà formulées par le passé. Il en va de notre responsabilité et de notre crédibilité d’apporter une solution adéquate à la souffrance de millions de victimes humaines que l’Europe ne peut plus se contenter d’observer, et d’adopter une résolution qui soit enfin à la hauteur de notre indignation. 
