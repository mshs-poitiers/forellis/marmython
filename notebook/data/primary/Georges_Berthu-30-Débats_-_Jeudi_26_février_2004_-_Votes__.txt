Georges Berthu # Débats - Jeudi 26 février 2004 - Votes (suite) 
  Berthu (NI
),
par écrit
. 
- J’ai voté contre le rapport Wallis "Mieux légiférer" afin de sanctionner une attitude hélas générale: proclamer comme ici de grands principes sur le nécessaire respect de la subsidiarité, et par ailleurs approuver des textes qui la violent effrontément.
Pendant cette session, par exemple, le Parlement européen vient d’adopter une résolution sur le Conseil de printemps qui demande une meilleure "gouvernance économique" au niveau européen, ce qui signifie davantage de centralisation des politiques économiques, alors que celles-ci doivent rester nationales. De même, au cours des deux sessions précédentes, notre Assemblée a demandé un "cadre juridique européen" pour les services d’intérêt général, ce qui va légitimer des ingérences injustifiées de la Commission dans la gestion des services publics nationaux et locaux.
Le rapport Wallis soutient également l’idée du mécanisme "d’alerte précoce", telle que proposée par le projet de Constitution, qui permet aux parlements nationaux d’appeler l’attention des institutions de Bruxelles sur des violations de la subsidiarité. Mais c’est un trompe-l’œil, puisqu’il ne donne aucun pouvoir réel d’opposition aux parlements nationaux. De plus, le rapport Wallis veut l’étendre aux assemblées régionales, procédure qui serait perverse car elle établirait un lien direct entre Bruxelles et les régions, par-dessus la tête des États. 
