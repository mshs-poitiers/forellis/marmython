Patrick Gaubert # Débats - Jeudi 10 juillet 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
 – Propositions de résolution: Situation en Chine après le tremblement de terre et avant les Jeux Olympiques (RC-B6-0340/2008
) 
  Patrick Gaubert  (PPE-DE
), 
par écrit
. – (FR)
 Je me félicite de l'adoption de cette résolution commune sur la situation en Chine. En effet, il est important de continuer à faire pression sur la Chine avant la tenue des Jeux olympiques dans moins d'un mois.
Nous ne pouvons pas fermer les yeux sur les carences graves qui subsistent en matière des droits de l'homme, et ce en contradiction avec les engagements pris par la Chine elle-même. Il est de la responsabilité du Parlement européen de rappeler ces promesses publiques. Il était ainsi important de mentionner les droits des minorités, l'état de droit et l'usage toujours fréquent de la peine de mort.
Enfin, je regrette que certains amendements plus sévères à l'encontre de la Chine n'aient pas été adoptés; je pense notamment aux demandes de mise en liberté de dissidents et défendeurs des droits de l'homme tels que Hu Jia et sa femme Zeng Jinyan, à la mention de la situation au Tibet qui est loin d'être normalisée ou encore aux sentences disproportionnées prononcées sans transparence à l'encontre des manifestants en lien avec les manifestations de ce printemps. 
