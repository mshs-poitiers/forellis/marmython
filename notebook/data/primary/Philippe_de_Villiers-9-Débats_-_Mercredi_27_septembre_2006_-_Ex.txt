Philippe de Villiers # Débats - Mercredi 27 septembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Eurlings (A6-0269/2006
) 
  Patrick Louis et Philippe de Villiers (IND/DEM
), 
par écrit
. - Nous avons voté contre ce texte qui adresse à la Turquie un reproche, somme toute bien injuste: celui d’être la Turquie, c’est à dire de ne pas être européenne.
A quoi bon presser ce pays de recommandations, d’exigences: les peuples d’Europe ne veulent pas de l’adhésion de la Turquie, car l’évidence s’impose à leurs yeux, la Turquie, et ce n’est pas lui faire injure, ne fait pas partie de la famille européenne. Elle a sa propre culture, ses propres valeurs, sa propre zone d’influence. C’est le sens de l’amendement que nous avons déposé au nom du groupe IND/DEM. Il faut aujourd’hui cesser le jeu hypocrite et dévastateur des négociations d’adhésion qui ne pourront se conclure que par une crise majeure, puisque l’éventuel traité d’adhésion n’a aucune chance d’être ratifié par les peuples. Notamment en France, où la procédure référendaire sera obligatoire.
Ayons aujourd’hui une pensée pour nos collègues français pris en pleine schizophrénie. Alors qu’ils prétendent s’opposer à l’adhésion de la Turquie, ils votent chaque année, ici et au parlement français, les crédits de préadhésion à la Turquie, et ils ont accueilli au sein de leur propre parti européen, le PPE, les observateurs de l’AKP, le parti islamiste de M. Erdogan. 
