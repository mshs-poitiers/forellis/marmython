Nicole Fontaine # Débats - Lundi 10 décembre 2001 - Autorité alimentaire européenne
  La Présidente.  
- 
L'ordre du jour appelle la recommandation pour la deuxième lecture (A5-0416/2001
), au nom de la commission de l'environnement, de la santé publique et de la politique des consommateurs, relative à la position commune du Conseil en vue de l'adoption du règlement du Parlement européen et du Conseil établissant les principes généraux et les prescriptions générales de la législation alimentaire, instituant l'Autorité alimentaire européenne et fixant des procédures relatives à la sécurité des denrées alimentaires (1088/1/2001 - C5-0414/2001
 - 2000/0286(COD)) (rapporteur : M. Whitehead). 
