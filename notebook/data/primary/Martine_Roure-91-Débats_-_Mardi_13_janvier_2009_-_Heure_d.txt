Martine Roure # Débats - Mardi 13 janvier 2009 - Heure des questions (questions à la Commission) 
  La Présidente. –  
J’appelle la 
question n° 32 de Johan Van Hecke 
(H-1018/08

)
Objet:	Microcrédits
En mai 2008, la Commissaire Mariann Fischer Boel a proposé de consacrer des fonds de l’Union européenne, précédemment destinés aux subventions à l’exportation, au soutien des prix et au stockage des excédents, à des microcrédits grâce auxquels les agriculteurs des pays en développement pourraient acheter des semences et des engrais. Les microcrédits sont assurément un outil important dans la lutte contre la pauvreté et un instrument au service des objectifs du Millénaire. Dans une déclaration écrite, le Parlement s’était déjà lui même prononcé, en avril 2008, en faveur d’une augmentation des fonds pour des projets de microcrédits.
Quelle suite concrète la Commission a-t-elle jusqu’à présent donnée à cette proposition?
