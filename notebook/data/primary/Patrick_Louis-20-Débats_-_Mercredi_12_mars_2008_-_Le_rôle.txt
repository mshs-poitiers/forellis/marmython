Patrick Louis # Débats - Mercredi 12 mars 2008 - Le rôle de l'Union européenne en Iraq (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Patrick Louis  (IND/DEM
). − 
(FR) 
Madame la Présidente, chers collègues, pour Fernand Braudel, l'histoire commençait à Sumer, mais aujourd'hui, la longue histoire des minorités chrétiennes d'Iraq semble finir. Nous, les nations d'Europe, ne pouvons pas cautionner cette grande injustice par notre inaction. Ces populations ont accueilli l'Islam dans leur hospitalité et, ensemble, ils ont construit ce pays qui fut prospère avant d'être ravagé par les fanatismes et les guerres.
Aujourd'hui, ces minorités doivent se résigner à fuir. L'avenir de l'Iraq ne peut se faire sans leur présence et leurs compétences. L'avenir ne passe pas par une partition du pays, mais par une reconstruction de l'économie, une reconnaissance mutuelle entre les différentes communautés et cultures, et une valorisation de l'État de droit.
Si l'Union européenne se décide fermement à intervenir en Iraq, elle doit avoir en permanence à l'esprit, dans le cadre de son projet, la réalité dramatique de toutes ces communautés minorisées. Leur devenir doit être la toise de son intervention, et la réalité du respect mutuel entre les communautés, le critère d'affectation de ses aides. Cette action sera alors l'honneur de nos nations. 
