Françoise Castex # Débats - Jeudi 10 mai 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Andria (A6-0090/2007
) 
  Françoise Castex (PSE
), 
par écrit
. - Le manque de logements décents à prix abordable influence de manière directe la vie des citoyens, limitant leur possibilité d’insertion sociale ainsi que leurs choix en matière d’étude, de formation et de développement professionnel.
Selon moi, les problèmes de logement ne se limitent pas à des questions de construction et d’aménagement du territoire proprement dites. Ils sont également hautement influencés par une mauvaise planification urbaine qui fait que certains quartiers, affectés par une dégradation de l’environnement - pollution de l’air et de l’eau, bruit, déchets, encombrements, etc. - et des dysfonctionnements au niveau des services publics, de l’accessibilité, de la sécurité, deviennent de moins en moins attractifs et sombrent dans la paupérisation.
Face aux problèmes du mal-logement, ce sont les autorités locales qui sont le plus souvent en première ligne. Cependant cette compétence n’est pas encore prise en compte suffisamment au niveau européen. Il faut donc enclencher une coopération effective entre les niveaux local et européen.
Pour l’eurodéputée socialiste française que je suis, le bon accès de tous les citoyens aux services sociaux, de santé et de formation ainsi qu’au commerce et à l’administration publique est fondamentale. C’est un droit. 
