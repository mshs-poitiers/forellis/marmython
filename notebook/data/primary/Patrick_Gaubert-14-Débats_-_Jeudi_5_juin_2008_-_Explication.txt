Patrick Gaubert # Débats - Jeudi 5 juin 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Proposition de résolution: Processus de Barcelone: Union pour la Méditerranée (RC-B6-0281/2008
) 
  Patrick Gaubert  (PPE-DE
), 
par écrit
. –
 Je me réjouis de l'adoption de la résolution commune sur le "processus de Barcelone: Union pour la Méditerranée".
Cette résolution rappelle à juste titre l'intérêt stratégique que représentent la région méditerranéenne et le Proche-Orient pour l'Union européenne. Notre politique vis-à-vis de ces pays doit en effet être fondée sur les principes de solidarité, de dialogue et de coopération.
Le processus de Barcelone mérite aujourd'hui d'être relancé pour optimiser son efficacité. À ce titre, l'initiative proposée par la Commission va dans le bon sens et présente le mérite de se concentrer sur des projets régionaux concrets dont la réalisation permettra de répondre efficacement aux besoins des citoyens de cette région.
La proposition de la Commission prévoit également de mettre en pace une coprésidence du Processus de Barcelone, un comité permanent conjoint et un secrétariat, ce qui permettra d'améliorer la coopération et le dialogue entre les différents partenaires.
En outre, elle rappelle que les grands objectifs de cette politique doivent rester la promotion de l'état de droit, de la démocratie, du respect des droits et du pluralisme politique. 
