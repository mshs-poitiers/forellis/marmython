Yves Butel # Débats - Mercredi 3 septembre 2003 - Votes
  Butel et Saint-Josse (EDD
),
par écrit
. - Le rapport de la commission juridique part du principe qu’il ne s’agit pas, par ces propositions, de "fixer directement des règles pénales ou d’aboutir à une harmonisation pénale". Ce point de départ nous semble être le bon et résulte d’un travail préparatoire et de réflexion sérieux mené par la commission juridique. Celle-ci s’est notamment appuyée sur une audition de spécialistes du droit pénal qui ont confirmé les bouleversements et difficultés majeures que susciterait une telle harmonisation.
Malheureusement, la suite du rapport nous entraîne précisément sur la voie inverse en s’appuyant sur les interprétations extensives de la Cour de justice, en appelant la Commission à déposer des plaintes en sa qualité de gardienne des traités et en demandant à la Conférence intergouvernementale de définir une compétence pénale communautaire.
Pour cette raison, les élus Chasse, pêche, nature et traditions du groupe EDD ont voté contre ce rapport qui, selon une technique souvent utilisée dans cette enceinte, nous impose en définitive de faire ce que l’on dit ne pas vouloir faire. 
