Marielle de Sarnez # Débats - Mardi 21 octobre 2008 - Réunion du Conseil européen (15-16 octobre 2008) (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marielle De Sarnez  (ALDE
). 
 –Monsieur le Président, à l’occasion de la crise financière mondiale, l’Europe a fait ses premiers pas d’acteur politique. Et sous votre présidence, sous la présidence française, les chefs d’État ou de gouvernement ont pris les bonnes décisions. Et il me semble d’ailleurs que le plan européen est mieux inspiré que le plan Paulson, et j’espère qu’il sera de nature à limiter les dégâts.
Évidemment, nous devons maintenant aller plus loin. La réforme du système financier mondial, que nous appelons tous de nos vœux, ne pourra se faire - c’est mon intime conviction - que si l’Europe est désormais en capacité de peser. Et pour cela, il faudra que nous nous dotions de moyens nouveaux. Nous devrons mettre en place une autorité européenne des marchés financiers et une commission bancaire européenne. Nous avons besoin d’un régulateur européen qui sera, à ce moment-là, à même de parler avec les régulateurs américains et nous devrons être capables de mettre fin aux paradis fiscaux sur notre propre continent si nous voulons être crédibles pour défendre cette idée au niveau mondial.
De même, pour faire face à la crise économique et sociale qui est là, il nous faudra une réponse européenne. Nous avons besoin d’un plan d’action commun au service de nos concitoyens pour investir demain, ensemble, sur des activités non délocalisables - je pense, par exemple, aux infrastructures lourdes ou à un plan de mise aux normes environnementales du bâtiment. Nous aurons besoin d’une gouvernance économique de la zone euro, il n’est que temps. Mais plus encore, nous aurons besoin demain de penser, de définir, de porter, d’inspirer un modèle de développement européen qui soit éthique, humain, social et durable à tous les sens du terme.. 
