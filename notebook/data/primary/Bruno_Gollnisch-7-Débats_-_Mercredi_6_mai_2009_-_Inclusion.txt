Bruno Gollnisch # Débats - Mercredi 6 mai 2009 - Inclusion active des personnes exclues du marché du travail (A6-0263/2009, Jean Lambert) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: José Albino Silva Peneda (A6-0241/2009
) 
  Bruno Gollnisch  (NI
), 
par écrit
. –
 Le bilan social de votre Europe est un échec retentissant. En France, des chiffres terribles viennent de tomber: augmentation de la pauvreté de 15 % en deux ans, explosion du nombre de travailleurs pauvres, croissance exponentielle corrélative du nombre de ménages surendettés, dont les ressources ne suffisent plus depuis longtemps à assurer les dépenses courantes. Et nous n’en sommes qu’au début de cette crise profonde.
Vous prônez une «ouverture des citoyens au changement» quand, pour le travailleur, le changement, c’est la perte d’emploi et la certitude de ne pas en retrouver, grâce à vos politiques. Vous parlez «social», quand la Cour de Justice foule aux pieds les droits des travailleurs au nom de la concurrence et de la libre prestation de service. Vous en rajoutez avec la flexibilité, quand celle-ci n’est que le mot européen pour dire «précarité». Vous faites même mine de porter une attention particulière aux femmes et aux mères, quand votre stupide politique de «genre» mène à la disparition de leurs droits sociaux spécifiques, comme ceux dont elles disposaient en France en matière de retraite ou de travail de nuit.
Ce n’est pas renouveler l’agenda social qui est nécessaire, c’est changer de fond en comble votre système pervers.
