Gilles Savary # Débats - Mercredi 22 avril 2009 - Un plan d'action sur la mobilité urbaine (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Gilles Savary, 
rapporteur
. −
 Madame la Présidente, mes chers collègues, dans cette discussion, je voudrais évidemment, d'emblée, rassurer Mme
 Sommer. Il y a eu ici beaucoup de législations qui ont concerné les collectivités locales sur la mise en concurrence des Stadtwerke
, par exemple, sur les obligations de service public dans les transports, sur les directives concernant les marchés publics.
Eh bien, il ne s'agit pas de cela ici. C'est encore beaucoup plus subsidiaire. Il n'est pas question de décider ici qu'une commune, qu'une municipalité ou qu'une zone urbaine fassent une « zone 30 » ou privilégient le chemin de fer. J'ai fait en sorte qu'on ne rentre pas dans ce type de débat.
Ce que je me suis posé comme question, c'est: « Quelle peut être la valeur ajoutée de l'Union européenne?». D'abord son intérêt à agir. L'Union européenne ne peut pas laisser la question urbaine de côté le même mois - décembre 2008 – où elle s'assigne, grâce à Mme
 Merkel ou à M. Sarkozy, un plan climat particulièrement ambitieux.
Comment est-ce que l'on peut se lancer dans un plan climat qui dit « trois fois vingt » et dire « je ne m'intéresse pas à la zone urbaine », qui est celle qui influe le plus sur la question climatique?
C'est une question de cohérence politique – cohérence politique européenne – puisque nous avons été d'accord les uns et les autres, et les gouvernements, pour se lancer dans le plan climat. Il y a une légitimité à s'intéresser à l'urbain et on n'y échappera pas, sur les transports comme dans d'autres domaines.
Mais oui, il faut faire en sorte que les communes décident souverainement, elles sont plus près de nous. Mais, ce que nous pouvons faire, c'est faire en sorte qu'elles se rencontrent, qu'il y ait un échange de meilleures pratiques, un échange d'informations.
Faire en sorte qu'on les incite à mettre en place des plans de développement urbain, ce que toutes n'ont pas su faire.
Faire en sorte qu'elles intègrent toutes les dimensions des déplacements: les déplacements doux, les déplacements collectifs, les déplacements par la voie d'eau – M. Blokland a raison –, les déplacements par le chemin de fer.
Faire en sorte qu'elles rendent les transports urbains plus attractifs pour l'usager.
C'est cela que nous cherchons, et c'est la raison pour laquelle nous demandons qu'il y ait un outil financier. Il y a Marco Polo qui incite au transport combiné. Il y a eu les programmes URBAN. Nous avons plusieurs programmes européens qui sont des programmes incitatifs. Nous ne les inventons pas cette fois-ci, cela fait des années que ça dure.
Eh bien, nous devrions avoir, dans les prochaines perspectives financières, sans les augmenter, une réorientation vers les transports urbains. Voilà ce que nous proposons.
Et je dis, pour en terminer – Madame la Présidente, excusez-moi, je suis le rapporteur – à M. Tajani que, si demain nous avons une très large majorité, il devrait pouvoir revenir devant la Commission et lui dire: « Je crois que nous devons faire quelque chose, parce que nous en avons la légitimité et parce que le Parlement n'a pas agi seul ».
(Applaudissements)

