Daniel Cohn-Bendit # Débats - Mercredi 17 mai 2000 - Ordre du jour
  Cohn-Bendit (Verts/ALE
). 
 - Madame la Présidente, je ne vois pas pourquoi c'est la Conférence des présidents qui devrait décider. Il nous appartient à nous, ici, en séance plénière, de décider si le vote a lieu ou non au cours de cette période de session, et non pas à la Conférence des présidents.
Nous voulons décider ici, aujourd'hui ou demain, si le vote sur cette directive et sur ce rapport doit avoir lieu cette semaine et ne pas laisser cette décision à la Conférence des présidents. 
