Élizabeth Montfort # Débats - Jeudi 10 octobre 2002 - Votes
  Montfort (NI
),
par écrit
. 
- J'ai soutenu le rapport Bushill-Matthews, car il encourage l'investissement et le soutien financiers des entreprises, dans l'esprit de la Charte européenne des petites entreprises.
Parce qu'elles participent à l'aménagement du territoire, au maillage indispensable de l'économie de nos sociétés, elles jouent un rôle essentiel sur le plan humain et social.
Cependant, alors qu'elles sont créatrices d'emplois, le soutien financier est souvent insuffisant. Je souhaite que ce rapport contribue à corriger cette lacune.
De plus, s'il est nécessaire de soutenir les PME du secteur des technologies de pointe, il ne faut surtout pas oublier les activités traditionnelles des PME, quelle que soit la taille de l'entreprise (artisanat, micro-entreprises...).
Enfin, la Commission ne semble raisonner qu'en termes d'aide à la création, mais il est indispensable d'assurer un suivi des aides post-création. 
