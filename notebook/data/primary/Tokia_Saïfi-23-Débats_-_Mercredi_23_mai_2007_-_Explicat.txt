Tokia Saïfi # Débats - Mercredi 23 mai 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Sturdy (A6-0084/2007
) 
  Tokia Saïfi (PPE-DE
), 
par écrit
. - 
La négociation sur les accords de partenariat économique entre dans une phase cruciale, le 1er janvier 2008, marquant l’expiration des accords actuels.
En raison du caractère essentiel de ces accords, j’ai voté en faveur du rapport, considérant que ces accords permettent un nouveau cadre économique et commercial favorable au développement durable des économies des pays ACP. J’insiste sur cette dimension développement: ces accords ne peuvent être réduits à de simples accords de libre-échange au sens de l’OMC et doivent être des instruments au service du développement économique et humain. Aussi, les APE seront aussi asymétriques et progressifs que possible.
J’ai voté en faveur des amendements 20 et 28 sur la nécessité de prendre en compte, dans les négociations, les spécificités des régions et territoires d’outre-mer au titre de l’article 299, paragraphe 2, du TCE. Il convient en effet d’examiner les intérêts propres de ces territoires, d’envisager des différenciations en matière d’accès au marché et d’améliorer l’articulation des modalités existantes d’accompagnement avec celles des pays ACP. Je tiens aussi à nuancer le paragraphe 13 du rapport en rappelant les conclusions adoptées par le Conseil qui prévoient des périodes transitoires dans l’offre d’accès au marché de l’UE pour certains produits faisant l’objet d’une sensibilité particulière pour l’UE. 
