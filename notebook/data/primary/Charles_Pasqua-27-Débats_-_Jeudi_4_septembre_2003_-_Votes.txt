Charles Pasqua # Débats - Jeudi 4 septembre 2003 - Votes
  Pasqua (UEN
),
par écrit
. - Cette initiative montre qu’il est illusoire et maladroit de vouloir traiter au niveau européen des questions qui doivent demeurer strictement nationales. Car, non contents d’affaiblir les États en les vidant, par le haut (intégration supranationale) et par le bas (régionalisation infranationale) de leur souveraineté, "on" s’attaque ici à l’une des composantes essentielles de l’unité nationale de certains États membres: la langue.
Le dispositif envisagé ne vise pas seulement à reconnaître objectivement la diversité linguistique, mais à la promouvoir au détriment des langues nationales. En outre, nul doute que la reconnaissance des langues minoritaires ou régionales prépare à celle des peuples qui les pratiquent…
Je ne saurais donc accepter que la Commission établisse "les critères propres à définir, dans la perspective d’un éventuel programme en faveur de la diversité linguistique, la notion de langue minoritaire ou régionale" ou, autre source d’inquiétude, la volonté de conférer une valeur contraignante à la Charte des langues régionales ou minoritaires.
Entièrement favorable à la préservation des identités culturelles et linguistiques régionales, je demeure aussi fidèle aux principes énoncés dans notre Constitution: "la France est une République indivisible", "la langue de la République est le français".
