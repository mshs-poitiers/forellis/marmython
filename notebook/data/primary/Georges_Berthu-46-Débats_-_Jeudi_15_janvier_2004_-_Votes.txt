Georges Berthu # Débats - Jeudi 15 janvier 2004 - Votes
  Berthu (NI
),
par écrit
. 
- La communication de la Commission sur l’immigration clandestine, objet du rapport Pirker, veut à juste titre intégrer dans une même démarche des documents éparpillés sur divers sujets: lutte contre l’immigration illégale, contre la traite des êtres humains, gestion des frontières extérieures, politique des retours, etc. Il n’est que temps.
Dans ce cadre, bon nombre de propositions avancées sont dignes d’être approuvées. On notera en particulier celle qui concerne l’établissement d’un système d’information sur les visas (VIS), destiné à déjouer la fraude, notamment par l’utilisation de données biométriques. Là encore, il n’est que temps. D’ailleurs ce système d’information devrait aller beaucoup plus loin, et permettre de vérifier que les visiteurs entrés sont bien repartis à l’expiration du visa.
De même, apparaissent excellentes les mesures visant à établir une aide mutuelle des pays pour soutenir ceux dont les frontières extérieures sont les plus menacées.
En revanche, l’ensemble est pollué par l’obstination de la Commission à mettre en place un système intégré de gestion des frontières extérieures, dont le projet de Constitution (article III-166) est destiné à fournir la base juridique. Au contraire, il faut le réaffirmer, les États doivent s’entraider, mais chacun doit rester maître de ses frontières. 
