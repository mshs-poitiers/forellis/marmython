Marie-Noëlle Lienemann # Débats - Lundi 11 décembre 2006 - Agence européenne des produits chimiques - Modification de la directive 67/548/CEE sur les substances dangereuses (REACH) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie-Noëlle Lienemann (PSE
). 
 - Monsieur le Président, chers collègues, mesurons le chemin parcouru avec le projet qui nous est soumis aujourd’hui. REACH va changer considérablement notre rapport à la pollution chimique: par l’inversion de la charge de la preuve, d’abord; par une information systématique tout au long de la chaîne, ensuite. J’ai entendu parler tout à l’heure du scandale de l’amiante: nous savons très bien, aujourd’hui, qu’avec le texte que nous votons, il ne serait plus possible d’avoir ce type de scandale. Il faut le dire à l’opinion publique!
Il y a tous ces acquis, certes, mais il y a aussi, bien sûr, ce qui ne nous satisfait pas, ou pas suffisamment. Les substances naturelles, d’abord: j’aurais aimé qu’on puisse être un peu plus clair, ça peut poser des problèmes. Je ne pense pas qu’une substance naturelle soit vraiment une substance chimique. Sur l’importation, ensuite: j’aurais aimé aussi que nous ayons plus de garanties que nous puissions imposer des normes comparables aux importateurs.
Ensuite, et évidemment, il y a ce grand enjeu de la substitution. Moi aussi, comme beaucoup, j’aurais aimé que la substitution soit automatique, systématique et immédiate. J’ai voté en première lecture tous les amendements qui allaient dans ce sens, mais j’ai observé que nous n’avions pas la majorité qualifiée. Et si notre collègue Sacconi n’avait pas fait cet effort de compromis, nous prenions un grand risque de nous retrouver, au vote de deuxième lecture, sans avoir la mise en œuvre de ce principe. Car, il faut le dire: dans le compromis, le principe existe! Ce qui est mis en cause, ce n’est pas le principe, c’est sa mise en œuvre, graduelle, progressive - insuffisante de mon point de vue -, mais le principe existe pour toutes les substances dangereuses. Ne négligeons donc pas l’effort que notre collègue a consenti et la victoire que nous avons remportée de ce côté-là.
Maintenant, il va falloir faire en sorte que, au-delà de ce principe, sa mise en œuvre soit à la hauteur des enjeux. Cela dépendra des moyens de l’Agence, des moyens financiers, des moyens humains, de la pression de l’opinion publique, cela dépendra aussi des crédits qui seront consacrés à la recherche et, sur ce point, nous souhaitons que la Commission soit particulièrement offensive pour donner à ce compromis tout le sens de progrès que nous espérons. 
