Margie Sudre # Débats - Mardi 4 juillet 2006 - FEDER, FSE, Fonds de cohésion (dispositions générales) - Institution du Fonds de cohésion - Fonds social européen - Fonds européen de développement régional - Groupement européen de coopération territoriale (GECT) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Margie Sudre (PPE-DE
). 
 - Le Conseil européen de décembre 2005 a jeté les bases de la programmation des Fonds structurels jusqu’en 2013. L’enveloppe dévolue aux quatre départements français d’outre-mer est maintenue par rapport à la période 2000-2006, grâce à un montant de 2,83 milliards d’euros.
Je tiens à insister sur ce chiffre quasi constant, qui s’explique par l’éligibilité des DOM à l’«Objectif de convergence», et par leur statut de RUP, alors que l’ensemble des régions françaises de métropole, ainsi que nombre de régions de l’Union, voient malheureusement leurs aides chuter considérablement.
La France, l’Espagne et le Portugal ont obtenu qu’une allocation spécifique soit créée afin de prendre en compte les surcoûts liés aux handicaps des RUP. Je regrette le manque de visibilité de cette nouvelle mesure dans le règlement général.
Je demande à la Commission européenne une certaine souplesse lors de la mise en œuvre de ces aides dont les modalités figurent dans le règlement FEDER, la définition de tels surcoûts de fonctionnement se prêtant difficilement à une quantification arithmétique!
Les DOM sont performants en matière de gestion des aides structurelles européennes, puisqu’ils consomment régulièrement et à bon escient les fonds mis à leur disposition. Il leur appartient de continuer d’en faire bon usage. 
