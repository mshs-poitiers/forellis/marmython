Martine Roure # Débats - Mercredi 6 octobre 1999 - État des relations entre la Turquie et l’Union européenne
  Roure (PSE
). 
 - Monsieur le Commissaire, Monsieur le Président, chers collègues, en introduction de mon propos, je voudrais vous rappeler la résolution du Parlement européen qui a été votée le 18 juin 1987.
Dans cette résolution, quatre points constituaient, je cite : " des obstacles incontournables à l’examen d’une éventuelle adhésion de la Turquie à la Communauté ". Je rappelle donc ces quatre points.
Premièrement, le refus du gouvernement turc de reconnaître le génocide commis autrefois contre les Arméniens ; deuxième point, sa réticence à appliquer les normes du droit international dans ses différends avec la Grèce ; troisième point, le maintien des troupes turques d’occupation à Chypre ; quatrième point, la négation du fait kurde.
Depuis, rien n’a changé. Bien sûr, la résolution que nous avons à voter aujourd’hui reprend plusieurs points, mais nous devons absolument insister sur le fait que la situation actuelle en Turquie est inacceptable pour nous et pour les critères de Copenhague, comme d’ailleurs cela est souligné dans le troisième paragraphe.
C’est à la Turquie de modifier son attitude si elle désire vraiment adhérer à l’Union européenne. C’est à elle de changer. Il est évident, pour nous, que la question chypriote est une question incontournable et que la Turquie ne peut occulter le problème kurde. Il s’agit là de questions-clés.
Nous devons rester fermes sur nos exigences si nous voulons être crédibles et si nous voulons que la Turquie change. Nous savons que la démocratie ne peut être solide dans un pays, l’histoire nous l’a encore démontré récemment, que si ce pays sait reconnaître ses minorités et sait les respecter.
(Applaudissements)

