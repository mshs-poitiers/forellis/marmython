Anne Ferreira # Débats - Jeudi 1 décembre 2005 - Explications de vote
  Anne Ferreira (PSE
), 
par écrit
. -
 Grâce à l’engagement de la Présidence britannique et des rapporteurs du Parlement européen, un accord a pu être obtenu sur ce règlement relatif à l’octroi de licences obligatoires pour les brevets de médicaments.
Cet accord permet de rééquilibrer la proposition de la Commission européenne qui représentait par rapport à la décision de l’OMC des obstacles supplémentaires à l’accès aux médicaments pour des pays confrontés à de graves problèmes de santé.
Ce compromis avec le Conseil est satisfaisant car le système de licences obligatoires est étendu à des ONG, à des organisations internationales et aux pays non membres de l’OMC; il autorise l’exportation vers les pays faisant partie d’un même ensemble régional.
Je regrette le refus du transfert technologique vers les pays du Sud et le maintien de la possibilité pour les États membres de prescrire des conditions supplémentaires.
 
Cette législation qui n’entre toutefois en vigueur que deux ans et demi après l’adoption de la décision de l’OMC, s’avère nécessaire pour un grand nombre de pays en développement confrontés à de graves problèmes de santé. Je me réjouis que cette décision intervienne lors de la journée mondiale de lutte contre le HIV / SIDA. 
