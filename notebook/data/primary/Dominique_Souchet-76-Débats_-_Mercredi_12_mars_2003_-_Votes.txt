Dominique Souchet # Débats - Mercredi 12 mars 2003 - Votes
  Souchet (NI
),
par écrit
. - Encore une fois, les pêcheurs sont en désaccord avec la Commission. En effet, le cabillaud semble servir à celle-ci de cobaye pour expérimenter le nouvel instrument qu'elle est parvenue à imposer au Conseil dans le cadre des plans de reconstitution pour les espèces menacées : la limitation de l'effort de pêche. Cet instrument est naturellement susceptible des plus larges extensions. On parle déjà du merlu et de la sole du golfe de Gascogne.
La proposition de la Commission relative au cabillaud est fondée, comme toujours, sur les avis les plus récents du Conseil international pour l'exploitation des mers, qui comporte de nombreux partenaires extraeuropéens et dont beaucoup de prises de position ont été controversées dans le passé.
Il est incontestable que certains stocks de cabillaud sont, dans plusieurs zones, en situation fragilisée. Mais en attribuant exclusivement à la pêche l'origine de cette situation, la Commission risque de condamner les pêcheurs sans reconstituer la ressource.
Si nous nous abstenons sur la résolution Stevenson, c'est uniquement parce qu'elle préconise la codécision dans le domaine de la PCP qui n'a, à notre sens, pas besoin d'un facteur supplémentaire d'incertitude et d'imprévisibilité. Pour le reste, nous soutenons bien entendu cette résolution. 
