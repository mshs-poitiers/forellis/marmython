Marielle de Sarnez # Débats - Lundi 20 octobre 2008 - Programme Erasmus Mundus (2009-2013) (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marielle De Sarnez, 
rapporteur
. − Monsieur le Président, je remercie l’ensemble des collègues qui se sont exprimés, très nombreux. Je partage tout ce qu’ils ont dit sur le fond et je suis très heureuse de constater le consensus très large qui nous réunit ici ce soir.
Un grand merci à la Commission européenne pour toute son aide dans ces travaux. Un grand merci à la commission de la culture, à sa présidente et au secrétariat de cette commission qui a été très actif. Un grand merci à l’ensemble des membres de la commission de la culture. Un grand merci encore à ceux qui – commissions du développement, affaires étrangères et femmes – se sont exprimés.
Deux mots pour vous dire que je suis tout à fait d’accord avec les objectifs qui sont les vôtres. Nous devons améliorer la participation des femmes dans ce programme et nous devons veiller à ce que les fonds utilisés le soient conformément aux objectifs de développement et de relation extérieure. Sur cette question, notre Parlement devra rester, et restera vigilant dans les années qui viennent.
Si nous arrivons proches d’un accord en première lecture qui se concrétisera, je pense, demain matin, c’est parce que chacun a joué son rôle pleinement et positivement. Nos échanges avec la Commission européenne, les amendements de nos collègues, nos discussions en commission de la culture, le travail des commissions saisies pour avis, c’est tout cela, finalement, qui a formé la qualité et qui forme la qualité de ce programme. Je vous en remercie très sincèrement. Je pense que nous aurons ainsi fait œuvre utile en montrant que l’Europe peut être, dans le même temps, porteuse de valeurs d’exigence, mais également de générosité.
