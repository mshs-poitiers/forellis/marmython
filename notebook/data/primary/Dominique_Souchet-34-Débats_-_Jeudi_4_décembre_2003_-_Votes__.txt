Dominique Souchet # Débats - Jeudi 4 décembre 2003 - Votes (suite) 
  Souchet (NI
),
par écrit
. 
- Nous avons soutenu le rapport Auroi car nous partageons la philosophie du rapporteur et sa crainte de voir proliférer de pseudo-appellations bio, susceptibles de fourvoyer les consommateurs, si les contrôles ne sont pas suffisamment rigoureux et harmonisés.
Ces contrôles doivent naturellement intervenir au stade de la production et nous sommes d’accord sur ce point avec la position du rapporteur au sujet du risque éventuel de pollution par OGM.
Mais ces contrôles doivent s’exercer également au stade de la commercialisation des produits, et donc vis-à-vis des produits importés, pour que, sur un marché qui attire des consommateurs de plus en plus nombreux, soient évités abus et impostures. 
