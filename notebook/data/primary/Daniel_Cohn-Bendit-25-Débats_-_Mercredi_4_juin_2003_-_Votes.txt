Daniel Cohn-Bendit # Débats - Mercredi 4 juin 2003 - Votes
  Cohn-Bendit (Verts/ALE
).
-
 Monsieur le Président, chers collègues, nous avons tous lu la prise de position de la Commission.
Il existe à présent deux possibilités et je ne sais pas si, pour une fois dans cette enceinte, la raison peut l’emporter.
(Mouvements divers)

Calmez-vous! C’est très drôle. Chaque fois que le Parlement prend position, on demande à la Commission d’y réfléchir sérieusement. Quand la Commission prend position, je demande qu’on réfléchisse sérieusement à ce qu’a écrit la Commission, parce que nous, nous le demandons en permanence au Conseil et à la Commission! Voilà la raison. Elle est simple.
(Applaudissements)

La Commission nous a écrit noir sur blanc que ce que nous avons décidé c’est la demande de renvoi en commission.
(Le Président invite l’orateur à en venir à la motion de procédure pour laquelle il a demandé la parole)

Ce n’est pas une motion de procédure, Monsieur le Président, c’est une demande de renvoi en commission sur la base de l’article 144.
Je continue donc, qu’on ne dise pas par la suite qu’on ne savait pas tout. Le Conseil, qui a des difficultés à se mettre d’accord, choisira exactement l’argument de la Commission pour dire non à un statut unique, parce qu’il y a des pays qui ne veulent pas d’un statut unique des députés. C’est pour cela que je demande un renvoi en commission, pour que l’on change l’article sur l’immunité, et avoir ainsi une chance de nous doter d’un statut unique. Tous ceux qui veulent présenter ce texte au Conseil, lequel ne pourra, ne peut dire que oui ou non, puisque c’est une procédure d’avis conforme du Conseil, prennent la responsabilité devant l’opinion publique européenne de ne pas vouloir un statut européen!
(Mouvements divers et applaudissements)

Donc, je demande un renvoi en commission pour réexaminer la question. 
