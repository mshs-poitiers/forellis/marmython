Carl Lang # Débats - Mardi 23 septembre 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
− Rapport: Roselyne Lefrançois (A6-0323/2008
) 
  Carl Lang  (NI
), 
par écrit.
 – Le samedi 20 septembre, un kamikaze faisait exploser un gros camion devant le grand hôtel Marriott, en plein cœur de la capitale du Pakistan, le réduisant à l’état de ruine calcinée et faisant au moins soixante morts.
Cet attentat serait attribué aux talibans pakistanais liés à Al-Qaïda.
Les dimanche 20 et lundi 21 septembre, c’est l’ETA, organisation armée basque, qui par trois fois a fait couler le sang. La préparation de ces attentats aurait été réalisée sur le territoire français.
Le terrorisme n’a pas de frontières et l’espace Schengen lui offre un berceau idéal pour le recrutement, l’endoctrinement et la préparation logistique des attentats. 
En France, la ministre de l’intérieur Michèle Alliot-Marie a déclaré à ce propos que les «prisons françaises sont un lieu de recrutement privilégié pour les islamistes radicaux»: bel aveu! C’est un fait que les causes du terrorisme sont multiples mais elles résident aujourd’hui essentiellement dans la lutte armée de l’islam radical. Curieusement, les textes législatifs visant à détecter et à empêcher les recrutements en milieu carcéral ou encore dans certains quartiers dits sensibles des banlieues n’existent pas.
L’Union européenne entend se doter d’un corpus juridique de lutte contre le terrorisme. 
(Explication de vote écourtée en application de l’article 163 du règlement)

