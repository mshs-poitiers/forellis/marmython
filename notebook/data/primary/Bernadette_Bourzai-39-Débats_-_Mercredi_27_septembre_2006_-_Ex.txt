Bernadette Bourzai # Débats - Mercredi 27 septembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Eurlings (A6-0269/2006
) 
  Bernadette Bourzai (PSE
), 
par écrit
. - Le rapport Eurlings fait le bilan, un an après l’ouverture des négociations d’adhésion avec la Turquie, des avancées et problèmes rencontrés.
Des amendements ont permis d’équilibrer ce rapport afin de prendre en compte les efforts réalisés par la Turquie tout en soulignant les questions qui restent problématiques comme la non-signature par la Turquie du protocole d’Ankara et le traitement des minorités.
Quant au génocide arménien, il doit nécessairement faire l’objet d’une reconnaissance de la Turquie. Cependant, cette reconnaissance ne peut être présentée comme une condition préalable à l’adhésion, si l’on considère les critères de Copenhague.
Quant au paragraphe sur une éventualité de collaboration privilégiée entre l’UE et la Turquie en cas d’échec des négociations, il n’est pas opportun pour l’instant. Nous sommes dans un processus en marche et nous ne pouvons, dès à présent, considérer la conclusion des négociations de manière pessimiste.
Par ce vote, je souhaite montrer les nombreux progrès que la Turquie doit accomplir pour intégrer l’Union européenne mais ne veut pas dresser de nouvelles barrières à une éventuelle adhésion de celle-ci.
Selon moi, l’entrée de la Turquie dans l’UE est à la fois une chance pour la Turquie et une chance pour l’Europe. 
