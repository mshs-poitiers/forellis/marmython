Bruno Gollnisch # Débats - Mardi 3 février 2009 - Rapatriement et réinstallation des détenus de Guantánamo - Utilisation alléguée de pays européens par la CIA pour le transport et la détention illégale de prisonniers (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bruno Gollnisch  (NI
). - 
 Monsieur le Président, je pense que nous n’avons pas à servir d’exutoire à la politique arbitraire que les États-Unis d’Amérique ont, hélas, au mépris de nos principes communs occidentaux, délibérément conduite dans cette zone de survivance coloniale, zone de non-droit cyniquement choisie pour que ne s’y applique ni le droit de Cuba – dont il est vrai, comme l’a dit mon collègue Dillen, qu’il n’est pas une protection pour les individus –, ni le droit des États-Unis, ni le droit international qui est notre héritage commun.
Le problème me paraît juridiquement clair. S’il y a des charges criminelles de droit commun contre certains des prisonniers, ils doivent être jugés dans les formes légales. S’ils sont par exemple soupçonnés d’avoir organisé les attentats du 11 septembre, ils auraient dû, depuis sept ans, être mis en présence des charges pesant contre eux, disposer d’avocats, être traduits devant les tribunaux américains. Il n’en manque pourtant pas aux États-Unis d’Amérique!
Si d’autres sont considérés comme des prisonniers de guerre après l’intervention alliée en Afghanistan, ils devaient être détenus dans les conditions prévues par le droit de la guerre jusqu’à la fin officielle des hostilités.
S’il y a des détenus qui ne rentrent dans aucune de ces deux catégories, eh bien, ils doivent être libérés et ramenés chez eux.
On me dit que certains sont potentiellement dangereux, mais si j’avais été, moi, détenu pendant sept ans dans l’isolement le plus complet, je n’aurais peut-être pas été potentiellement dangereux au départ, mais je serais certainement devenu potentiellement dangereux à l’arrivée! Je pense que c’est le cas de la plupart de ceux qui sont ici.
Si certains ne souhaitent pas rentrer, il leur appartient de demander l’asile politique à leurs geôliers. Voilà ce que je voulais dire, en remerciant au passage M. le commissaire Barrot pour ses travaux, pour ses enquêtes, qui nous apprendront sans doute dans vingt qans quel a été le fin mot de ces transferts illégaux de prisonniers. 
