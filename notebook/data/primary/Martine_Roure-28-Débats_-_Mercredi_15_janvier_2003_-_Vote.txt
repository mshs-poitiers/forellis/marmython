Martine Roure # Débats - Mercredi 15 janvier 2003 - Votes (suite) 
  Roure (PSE
),
par écrit
. - Si, à Tampere, en octobre 1999, le Conseil européen a fixé les éléments d'une politique commune en matière d'asile et d'immigration, force est de constater que nous n'avons pas beaucoup avancé et que nous devons absolument prendre les mesures nécessaires, notamment pour mettre un coup d'arrêt à l'immigration clandestine et à la traite des êtres humains.
Nous devons améliorer l'accueil que nous réservons aux réfugiés afin que les candidats à l'immigration ne soient plus tentés d'avoir recours à des voies clandestines. En juin 2002, à Séville, le Conseil européen s'est engagé à accélérer la mise en œuvre du programme adopté à Tampere ; nous demandons donc que le Conseil respecte ses engagements et cesse de faire obstacle aux progrès attendus. Nous demandons donc une mise en œuvre rapide et effective des conclusions de Séville et nous regrettons qu'une politique commune d'immigration n'ait pas encore été mise en place. Nous devons réagir rapidement car nous ne pouvons fermer les yeux sur les drames qui se jouent actuellement dans nos États membres. 
