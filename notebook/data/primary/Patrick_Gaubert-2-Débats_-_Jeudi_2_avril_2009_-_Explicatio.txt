Patrick Gaubert # Débats - Jeudi 2 avril 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
– Rapport: Kathalijne Buitenweg (A6-0149/2009
) 
  Patrick Gaubert  (PPE-DE
), 
par écrit.
 –
 La défense des droits et la protection des personnes victimes de discriminations doit être une priorité pour l’UE, mais elle ne peut être efficace et utile que si elle assure une sécurité juridique pour les personnes concernées tout en évitant une charge disproportionnée pour les acteurs économiques visés.
Dans ce domaine sensible, il était primordial de rester vigilant quant au respect de la répartition des compétences entre l’Union européenne et les États membres et de veiller à ce que le Parlement s’en tienne strictement à ce que permet la base légale.
Or le texte tel qu’adopté aujourd’hui, s’il apporte satisfaction à certains égards –notamment en ce qui concerne la lutte contre les discriminations vis-à-vis des personnes handicapées –, les notions floues qu’il contient, les incertitudes juridiques qu’il maintient, les exigences superflues qu’il introduit, le rendent juridiquement impraticable et donc inefficace dans son application.
Estimant que la surréglementation ne peut être une solution, j’ai défendu l’amendement de rejet de la proposition de la Commission dans la mesure où les textes existants en la matière ne sont pas appliqués par un certain nombre d’États membres qui font l’objet de procédures d’infraction.
Dans ces conditions, solidaire de l’objectif de cette directive mais partiellement insatisfait, j’ai préféré m’abstenir lors du vote final. 
