Véronique Mathieu # Débats - Vendredi 24 avril 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Horst Schellhardt (A6-0087/2009
) 
  Véronique Mathieu  (PPE-DE
), 
par écrit
. – 
Ce rapport permettra à l’Union européenne de se doter d’un cadre législatif plus précis afin de renforcer le niveau de sécurité tout au long de la chaîne de production et de distribution alimentaire. Ce texte a le mérite de proposer une méthode davantage fondée sur les risques et sur les contrôles, de renforcer la cohérence des dispositions sur les sous-produits animaux et la législation en matière d’hygiène tout en introduisant des règles supplémentaires sur la traçabilité des sous-produits animaux.
Je peux d’ailleurs témoigner que le précédent rapport de M. Schnellhardt sur l’hygiène des denrées alimentaires (2002) avait eu un impact très positif en responsabilisant le monde cynégétique européen. La transposition de ce règlement dans le droit national a effectivement eu des effets positifs sur le terrain, en améliorant notamment la formation des 7 millions de chasseurs européens qui, par leur action constante sur l’environnement, sont à même de détecter rapidement et efficacement les crises sanitaires qui affectent la faune sauvage.
Je soutiens donc ce rapport qui permettra à l’Union européenne de mieux prévenir et de mieux réagir en cas de crise alimentaire liée à des produits d’origine animale. 
