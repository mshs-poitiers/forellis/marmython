Guy Bono # Débats - Mercredi 9 avril 2008 - Industries culturelles en Europe (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Guy Bono, 
rapporteur
. − 
(FR)
 Monsieur le Président, le rapport que je vous propose fait suite à la demande que nous avions formulée, Mme
 Trüpel ici présente, que je salue, et moi-même, lors du rapport Culture 2007, il y a de cela maintenant trois ans, afin de voir une plus grande reconnaissance accordée aux industries culturelles qui, je le rappelle, avec 3,1 % de la population active de l’Union européenne et 2,6 % du PIB, pèsent aujourd’hui davantage que l’industrie automobile dans l’Union européenne.
J’ai donc rédigé ce rapport dans une logique de concertation maximale avec l’ensemble des acteurs du secteur. À ce titre, je souhaite remercier les spécialistes et professionnels du secteur, ainsi que les associations d’internautes et de consommateurs et, enfin, mes collègues de la commission de la culture, pour l’ensemble des échanges que nous avons pu avoir sur ces questions cruciales.
Ce rapport part du postulat suivant: la culture et l’économie sont devenues aujourd’hui inséparables. L’économie a besoin de la culture et la culture a besoin de l’économie. De ce point de vue, il est urgent de redonner à la culture la place qui est la sienne dans l’agenda de Lisbonne. Dans cette perspective, je fais un certain nombre de propositions dans ce rapport.
La première serait de créer une task force
 qui serait chargée d’explorer la relation entre la culture, la créativité et l’innovation dans le cadre des politiques communautaires.
Deuxièmement, il est indispensable d’accorder plus de place dans nos financements communautaires aux industries culturelles. Dans ce rapport, je préconise, au niveau des États membres, des méthodes de financement privé/public, ainsi que la promotion d’un cadre réglementaire et fiscal favorable aux industries culturelles et plus spécifiquement l’application de crédits d’impôt et de taux réduits de TVA à tous les produits culturels, y compris les œuvres en ligne. En outre, j’invite également la Commission à envisager la possibilité de mettre en place un programme similaire au programme MEDIA pour l’ensemble des industries culturelles.
Troisièmement, ce rapport propose que la culture soit mieux intégrée dans les politiques extérieures de l’Union européenne parce que – et j’insiste là-dessus – ce qui fait la richesse de l’Europe, c’est d’abord sa culture ou plutôt, j’allais dire, la diversité de sa culture. Ainsi, j’invite également la Commission et les États membres à augmenter le montant des aides à la traduction.
Enfin, je voudrais, si vous le permettez, Monsieur le Président, revenir sur la question du droit d’auteur, qui a fait l’objet, dans ce rapport, de très nombreux amendements. Sur ce sujet, je m’oppose fermement à la position de certains États membres, dont les mesures répressives sont des mesures dictées par des industries qui n’ont pas été capables de changer leur modèle économique face aux nécessités imposées par la société de l’information. La coupure d’un accès internet est une mesure disproportionnée au regard des objectifs. C’est une sanction aux effets puissants, qui pourrait avoir des répercussions graves dans une société où l’accès à l’internet est un droit impératif pour l’inclusion sociale.
Mes chers collègues, je pense que nous devons faire la distinction entre la piraterie de masse et les consommateurs qui n’agissent pas dans un but mercantile. Plutôt que de criminaliser des consommateurs, il nous faut mettre en place des nouveaux modèles économiques qui permettent de trouver un équilibre entre les possibilités d’accès aux activités et contenus culturels, la diversité culturelle et une véritable rémunération aux titulaires de droits. Ce n’est, me semble-t-il, qu’à cette condition que l’Europe de la culture entrera véritablement dans le 21e
 siècle. 
