Béatrice Patrie # Débats - Mardi 13 mars 2007 - Communication de la Commission - Stratégie en matière de politique des consommateurs 2007-2013
  Béatrice Patrie (PSE
). 
 - Monsieur le Président, Madame la Commissaire, je vous remercie pour votre intervention et vos réponses.
Je voudrais vous interroger sur ce que vous entendez par instaurer des meilleures procédures de recours pour les consommateurs, puisque vous nous dites que vous n’envisagez pas de recours collectif.
Le fait est que les droits des consommateurs sont régulièrement et massivement violés que ce soit, en matière de téléphonie mobile, par des prix excessifs, ou par des prélèvements de frais bancaires indus, ou par des défaillance des fournisseurs d’accès Internet, et que bien souvent, les préjudices individuels sont peu importants, mais représentent, s’ils sont additionnés les uns aux autres, des préjudices collectifs considérables, mettant en jeu des sommes extrêmement importantes.
Face à cela, les recours individuels sont inopérants et seules, en effet, les class actions 
peuvent apparaître comme une solution satisfaisante dès lors qu’elles permettent, non seulement de fournir aux consommateurs un moyen de recours et un procès unique en réparation, mais aussi et surtout de faire reculer les entreprises, comme c’est le cas, sans qu’il y ait d’ailleurs des dérives à l’américaine, notamment au Portugal.
Pourriez-vous donc nous apporter des éclaircissements sur ce que vous appelez des recours efficaces? 
