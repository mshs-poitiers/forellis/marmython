Béatrice Patrie # Débats - Mercredi 4 septembre 2002 - Votes
  Patrie (PSE
). 
 - Monsieur le Président et chers collègues, je souhaiterais déposer, au nom du groupe du parti des socialistes européens, une motion d'ordre sur la base de l'article 144 du règlement. Il s'agit d'une motion de renvoi de ce texte en commission. Il nous semble, en effet, que le rapport Beysen ne peut pas être légitimement adopté avant que soient tranchées les questions posées par la commission exécutive dans le Livre vert sur la protection des consommateurs, à savoir : faut-il harmoniser le droit européen de la consommation par la voie d'une directive-cadre ou par la voie de dispositions sectorielles du type de la proposition de règlement sur la promotion des ventes ? 
