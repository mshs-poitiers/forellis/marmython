Alain Krivine # Débats - Jeudi 10 avril 2003 - Votes
  Krivine et Vachetta (GUE/NGL
),
par écrit
. 
- 
Le rapport du général Morillon précise les ambitions d'une politique de sécurité et de défense de l'Union européenne. Après quelques précautions d'usage, comme la nécessité de faire reposer cette politique sur l'égalité, la justice, la réciprocité, les droits de la personne, le contrôle démocratique et le droit international, et de contribuer à la liberté, à la paix, etc., le rapport dévoile ses bases concrètes, avec la vision policière du monde qui le sous-tend : une instabilité qui serait produite par le terrorisme, la prolifération des armes de destruction massive, le trafic d'armes, de stupéfiants et d'êtres humains... ainsi que l'immigration illégale, bref par la méchanceté humaine générale, et non par l'oppression sociale et les inégalités Nord-Sud.
De ce fait, les solutions proposées sont le renforcement des capacités militaires disponibles dans l'Union nécessitant le développement d'une culture européenne de défense, et cela le plus rapidement possible. Il est même souhaité que la force militaire européenne soit capable d'ici 2009 de mener, dans le cadre géographique européen, une opération de niveau et d'intensité du conflit du Kosovo. Quel avenir pacifique ! Pour ces raisons, nous avons déposé un amendement préalable demandant de refuser la guerre en tant qu'instrument de règlement des controverses internationales. Face à son rejet, nous avons voté contre le rapport Morillon. 
