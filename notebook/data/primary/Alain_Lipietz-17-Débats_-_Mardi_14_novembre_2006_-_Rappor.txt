Alain Lipietz # Débats - Mardi 14 novembre 2006 - Rapport annuel 2006 sur la zone euro (vote) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Alain Lipietz (Verts/ALE
). 
 - Avec l’accord du rapporteur, les Verts sont prêts à supprimer l’amendement 7, à condition d’ajouter, à la fin de l’amendement 13, un amendement oral. Il s’agit en fait de préciser les responsabilités de chacun en matière de politique du change. Je vous lis le texte de l’amendement oral en anglais: 
«Sans préjudice de leurs compétences et responsabilités respectives afin de faire face aux déséquilibres internationaux».
