Paul-Marie Coûteaux # Débats - Mercredi 5 septembre 2007 - Lutte contre le terrorisme (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Paul Marie Coûteaux  (IND/DEM
). –  
(FR) Monsieur le Président, je ne désapprouve, certes, pas les litanies de bons sentiments, d'incantations et de vœux pieux qui dominent ce débat mais, enfin, je n'y vois guère le recul qui serait à mon avis nécessaire à la compréhension d'un phénomène qui occupe brutalement nos esprits. Si brutalement, d'ailleurs, que nous ne prenons pas la peine d'en déterminer les tenants et les aboutissants, les causes et les conséquences. Or, mes chers collègues, qu'est-ce que la politique si ce n'est précisément la détermination patiente des causes et des conséquences des phénomènes immédiats?
On installe un peu partout, dans les lieux publics et jusque dans les rues, d'effrayants systèmes de vidéosurveillance. Pourquoi? Lutte contre le terrorisme! On fiche les citoyens, on développe les services de police et, quelque fois, de police secrète. Pourquoi? Lutte contre le terrorisme! On restaure la torture. Pourquoi? Lutte contre le terrorisme! On modifie les lois et, de plus en plus souvent, on les bafoue, bafouant même les droits élémentaires des hommes qui sont supposés avoir remplacé nos anciennes bibles. Pourquoi? Lutte contre le terrorisme! On installe un peu partout des boucliers antimissiles, même dans les pays comme la République tchèque, au grand dam de sa population qui ne connaît d'ailleurs nullement le terrorisme, tout cela au nom de la lutte contre le terrorisme. Bref, on divise les nations, on installe la méfiance entre les peuples et, surtout, on les enrégimente, ce qui est le cas de nos nations européennes, hélas, à l'Est comme à l'Ouest. Pourquoi? Lutte contre le terrorisme!
Constatons, mes chers collègues, cette évidence que ce n'est pas le terrorisme qui accapare nos esprits, c'est la lutte contre le terrorisme. C'est un voile insidieux qui brouille nos regards sans même que nous nous en apercevions. Certes, je ne nie pas ce qu'ont d'odieux les attentats qui ont touché nos nations, l'Espagne ou la Grande-Bretagne. Mais, précisément, ces exemples sont parlants car le terrorisme a touché ces pays parce qu'ils avaient soutenu une opération de guerre: l'invasion d'un pays souverain. Je ne crains d'ailleurs pas de dire que ce qui s'est passé en Irak n'est pas tant la réponse au terrorisme qu'une autre forme de terrorisme séculaire et barbare qui a fait monter d'un cran la tension internationale.
En vérité, le terrorisme est lui-même la conséquence d'un monde profondément déséquilibré, dominé par un empire qui, comme tous les empires du monde, nie les frontières et nie les peuples, tendant à créer partout un monde unidimensionnel, uniforme, obsessionnellement tendu vers la valeur unique de la marchandise. Un monde si étouffant et si violent pour la singularité des peuples que la seule réponse est une autre violence, bien entendu tout aussi inadmissible: la terreur.
Ayons donc le courage de nous interroger! Et si le terrorisme n'était pas aussi, n'était pas d'abord la conséquence de ce nouvel impératif catégorique qu'impose la loi du marché et qui est inclus dans sa logique: la suppression des frontières. Non seulement l'abolition des frontières, le transfrontièrisme à la mode, abolit la diversité du monde et pousse à la fureur ceux qui s'en veulent les dépositaires, mais en plus, la disparition des frontières favorise les menées des bandes. Est-ce que ce n'est pas là, chers collègues, un point de réflexion à verser au débat de la lutte contre le terrorisme? 
