Pervenche Berès # Débats - Mercredi 12 juin 2002 - Votes (suite) 
  Berès (PSE
). 
 - Monsieur le Président, dans la conception que j'ai de la démocratie, il y a des votes que l'on doit émettre publiquement.
(Vifs applaudissements)

Le vote secret est réservé à certaines désignations, mais lorsque des démocrates ne sont pas capables de dire publiquement leur choix concernant des désignations politiques essentielles, lorsqu'ils appellent à des bulletins secrets sur des questions de procédure, alors même qu'ils prétendent défendre la transparence, je tiens à dire ici que ce n'est pas ma conception ni de la démocratie ni de la transparence ni du sens de la responsabilité politique. Je veux dire ici publiquement que je voterai contre les amendements déposés par ces avocats de la transparence : drôle de transparence !
(Vifs applaudissements)

