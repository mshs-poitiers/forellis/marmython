Hélène Flautre # Débats - Mercredi 15 novembre 2006 - Situation à Gaza (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Hélène Flautre, 
au nom du groupe des Verts/ALE
. - Monsieur le Président, j’étais avec M. Davies, Mme Morgantini et d’autres députés à Gaza il y a deux semaines, le jour où l’armée israélienne commençait le siège de Beit Hanoun: c’était le début d’une campagne d’exécutions extrajudiciaires, de massacres et de destructions de biens civils. Israël, puissance occupante dans les territoires palestiniens, ne cesse de violer les droits de l’homme et le droit humanitaire international en toute impunité.
L’emploi de la force, avec excès et sans distinction, contre les civils et les biens de caractère civil, la destruction d’ouvrages fournissant l’électricité et l’eau, la démolition à l’explosif des édifices publics, les restrictions imposées à la liberté de circulation et les conséquences de toutes ces actions pour la santé publique, l’alimentation, la vie des familles et l’état psychologique du peuple palestinien constituent une punition collective flagrante, en violation, elle aussi flagrante, de la quatrième Convention de Genève.
Le lancement incessant de roquettes Kassam contre Israël est certes sans excuses, mais rien ne peut justifier qu’un peuple tout entier fasse l’objet d’un châtiment draconien comme celui imposé par Israël. Dans de telles conditions, l’Union européenne doit arrêter d’apporter sa caution à la politique israélienne.
Elle doit notamment assumer pleinement ses responsabilités en matière de supervision du point de passage de Rafa et ne plus simplement s’incliner devant la volonté du gouvernement israélien. L’Union doit utiliser les instruments à sa disposition dans le cadre de l’accord d’association, notamment la clause des droits de l’homme, afin de s’assurer que les violations des droits de l’homme et du droit humanitaire ne restent pas impunies. Elle doit exiger le remboursement des taxes retenues illégalement par les Israéliens. Le dialogue doit être relancé au plus vite avec le futur gouvernement d’union nationale et l’aide directe aux institutions palestiniennes doit être rétablie.
Enfin, l’Union européenne et les États membres doivent tout mettre en œuvre afin de faire appliquer l’opinion de la Cour internationale de justice sur l’édification illégale du mur. Une réunion du conseil d’association UE-Israël doit être convoquée de toute urgence. La révision de l’accord doit être envisagée si les violations ne cessent pas.
Enfin, l’Union européenne doit prendre toute sa part dans l’organisation d’une conférence internationale pour la paix dans la région. 
