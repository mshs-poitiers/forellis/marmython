Alain Lipietz # Débats - Jeudi 15 février 2007 - Rapport annuel de la BEI (2005) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Alain Lipietz, 
au nom du groupe Verts/ALE
. - Monsieur le Président, c’est un très grand plaisir pour nous de procéder à nouveau à l’examen de ce rapport annuel, qui a recueilli particulièrement aisément cette année l’unanimité dans notre commission. Tous les grands groupes l’ont voté; le rapporteur, M. Montoro, s’est montré très ouvert à toutes les suggestions, y compris aux nôtres. Cette unanimité, au sein de notre commission, reflète bien la profonde satisfaction qu’inspire au Parlement européen l’attitude que la BEI a su entretenir à notre égard depuis que, en 1999, j’ai eu l’honneur d’être le premier rapporteur de ce Parlement pour les activités de la BEI.
On peut dire que nous avons su établir entre nous une interactivité permanente et assurer l’ajustement réciproque entre les desideratas des élus, relayant souvent d’ailleurs les demandes des associations et des organisations non gouvernementales, et la politique de la BEI, qui s’est progressivement alignée sur la stratégie de Göteborg-Lisbonne, sur nos recommandations.
Cette année, au-delà du constat que les objectifs fixés sont très largement suivis par la BEI, je voudrais simplement attirer l’attention sur les difficultés que, nous-mêmes, par nos propres demandes, attirons à la Banque. On vient de parler de l’oléoduc de la Baltique; il est vrai qu’à partir du moment où nous exigeons la plus grande sécurité en matière de développement durable, il faut faire des choix entre tel et tel objectif.
Il y a un autre problème, plus complexe: nous demandons à la Banque de prêter davantage aux PME, nous lui demandons donc de trouver des dispositifs opérationnels permettant de mettre en œuvre, par les petites entreprises et les banques relais, des objectifs que nous affectons à la Banque. 
