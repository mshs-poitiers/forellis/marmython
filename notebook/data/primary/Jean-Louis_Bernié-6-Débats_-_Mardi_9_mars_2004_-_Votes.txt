Jean-Louis Bernié # Débats - Mardi 9 mars 2004 - Votes
  Bernié (EDD
),
par écrit
. 
- Deux extraits représentatifs de cette résolution suffisent à démontrer que l’objectif de simplification et d’amélioration semble hors de portée.
Voilà le résultat atteint par ce troisième rapport. Le considérant A dit: "considérant que la qualité et l’intelligibilité de la législation communautaire ont une incidence directe sur le bien-être et la prospérité des citoyens et des entreprises communautaires", tandis qu’on peut lire, au paragraphe 5: "accentue le droit du Parlement de demander à la Commission de présenter une proposition d’acte législatif, dans le cadre de l’examen par celle-ci des pratiques d’autorégulation".
Il nous sera donc une fois de plus impossible d’accorder notre soutien à une telle résolution. Les propositions de la Commission restent mauvaises sur le fond. Le Parlement n’a pas voulu ou pas su s’y attaquer. Il est resté prisonnier du système et du jargon communautaire. 
