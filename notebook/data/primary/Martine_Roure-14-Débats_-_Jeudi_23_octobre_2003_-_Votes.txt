Martine Roure # Débats - Jeudi 23 octobre 2003 - Votes
  Roure (PSE
),
par écrit
. - Depuis quelques années le trafic d’organes d’origine humaine s’est beaucoup développé. La traite d’êtres humains s’est organisée afin de prélever des organes sur des donneurs vivants. Des patients citoyens de l’Union européenne achètent des organes à des donneurs dans des pays tiers, profitant ainsi de leur désarroi devant l’extrême pauvreté.
Dans ce contexte, il faut se féliciter de la proposition de décision-cadre.
Le don d’organes de personnes vivantes constitue une source réelle d’abus et signifie souvent une exploitation de la misère. Le recours à des donneurs vivants ne doit donc constituer qu’une solution de dernier recours, lorsque la transplantation d’organes de personnes décédées est impossible. J’ai donc déposé un amendement afin de punir tout prélèvement d’organe sur des donneurs vivants qui n’auraient pas de relations personnelles étroites, telles que définies par la loi. En l’absence de telles relations, ces dons pourraient être faits uniquement dans les conditions définies par la loi et après autorisation d’une instance indépendante appropriée. Le don d’organe de donneurs vivants doit être très encadré et très surveillé afin de protéger les personnes en situation de fragilité et d’éviter l’exploitation d’êtres humains pour leurs organes, ce qui est inacceptable. 
