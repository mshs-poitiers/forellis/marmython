Chantal Cauquil # Débats - Jeudi 23 octobre 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. - 
Un avenir de "paix et de dignité" au Proche-Orient exige que les deux peuples, israélien et palestinien, puissent coexister, avec les mêmes droits, en particulier celui de disposer chacun d’un État indépendant. C’est cette possibilité qui est fermée depuis des décennies par l’État d’Israël, qui mène une politique d’oppression à l’égard du peuple palestinien.
Au cours des dernières années, cette politique a pris la forme d’un terrorisme d’État, aussi abject que le terrorisme individuel qu’il a engendré du côté palestinien, mais avec de tous autres moyens.
Stopper le terrorisme d’État d’Israël afin d’arrêter le terrorisme individuel, abandonner la politique de création des colonies sont les préalables de tout processus de paix qui, outre les droits démocratiques, devra assurer aux classes populaires la possibilité de sortir de la misère.
Les États-Unis mais aussi l’Europe ont des moyens de pression sur les dirigeants d’Israël qui ont un besoin vital du soutien économique, politique, militaire, des grandes puissances. Si celles-ci le voulaient, elles pourraient obliger Israël à abandonner sa politique d’oppression et de spoliation.
Nous avons voté pour les amendements concernant l’accord de Genève, non pas spécifiquement pour leur contenu mais par solidarité vis-à-vis de ceux, Israéliens et Palestiniens réunis, qui se battent pour que cesse une situation catastrophique pour les deux peuples.
(Explication de vote écourtée en application de l’article 137, paragraphe 1, du règlement)

