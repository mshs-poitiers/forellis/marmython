Bernadette Bourzai # Débats - Mercredi 28 mars 2007 - Production biologique et étiquetage des produits biologiques (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bernadette Bourzai (PSE
). 
 - Monsieur le Président, Madame la Commissaire, chers collègues, je tiens d’abord à féliciter Marie-Hélène Aubert pour son excellent travail depuis le début du mandat, d’abord sur le plan d’action européen en matière d’alimentation et d’agriculture biologique, puis sur cette proposition de règlement. La tâche n’était pas aisée car la proposition portait atteinte à l’identité forte et crédible de l’agriculture biologique.
Nous pouvons être satisfaits des avancées obtenues en commission de l’agriculture sur plusieurs points: un encadrement plus strict de l’utilisation des produits phytopharmaceutiques, des traitements vétérinaires et des dérogations nationales; un contrôle renforcé lors de la certification, y compris sur les produits importés; une extension du champ d’application du règlement et le maintien des comités de réglementation. Je soutiens par ailleurs la double base juridique qui nous ferait passer à la codécision.
Toutefois, je reste très inquiète sur la question de la présence d’OGM, même de façon accidentelle, dans les produits biologiques. En effet, le règlement affirme qu’un produit ne peut être étiqueté «produit de l’agriculture biologique» s’il contient des OGM, mais il accepte cependant un seuil de contamination accidentelle de 0,9 % d’OGM, ce n’est pas admissible.
C’est pourquoi je sollicite votre soutien en faveur des amendements 170 et 171 déposés par le groupe PSE, qui demandent que la présence d’OGM dans les produits biologiques soit limitée exclusivement et que le terme ne soit pas utilisé. 
