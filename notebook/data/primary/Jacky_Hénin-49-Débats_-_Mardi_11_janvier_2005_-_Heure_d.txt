Jacky Hénin # Débats - Mardi 11 janvier 2005 - Heure des questions (Commission) 
  Henin (GUE/NGL
). 
 - Madame la Présidente, permettez-moi d’exprimer dans cette enceinte ma solidarité la plus sincère à l’égard des millions de salariés qui vont voir leur vie brisée, pour satisfaire, encore une fois, une minorité de nantis.
Oui, les économistes de la Fédération internationale des syndicats - si la Commission ne le sait pas, eux le savent - parlent de la destruction de trente millions d’emplois, en Europe, au Maghreb, au Sri Lanka, en Indonésie pour un million d’entre eux. Comme si ce que venaient de vivre certains de ces pays ne suffisait pas, il faut les aider à s’enfoncer un peu plus. Il est, je le dis avec force, de la responsabilité et du devoir de la Commission, du Conseil et du Parlement d’arrêter ce désastre social. 
