Marie-Anne Isler-Béguin # Débats - Mercredi 25 mars 2009 - Accord commercial intérimaire avec le Turkménistan - Accord commercial intérimaire avec le Turkménistan (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie Anne Isler Béguin  (Verts/ALE
). - 
 Monsieur le Président, Monsieur le ministre, Madame la Commissaire, je pense qu’il faut que nous sortions de l’hypocrisie sur ce dossier.
Effectivement, moi-même, j’ai fait partie, en 2006, de la délégation du Parlement européen qui s’est rendue au Turkménistan, et nous avons fait, à l’époque, un certain nombre de propositions que vous avez relevées, Madame la Commissaire.
Évidemment, j’entends bien vos arguments et ceux du Conseil, mais lorsque je compare ce qui a été proposé et le rapport que nous avons voté ici même il y a quelques mois, le 20 février 2008, sur l’Asie centrale, où nous avons réitéré nos demandes par rapport à la libération des prisonniers, par rapport à la Croix-Rouge, etc., est-ce que vraiment il y a eu une avancée? Non.
Pour ma part, lorsque je lis «rapport Turkménistan», je lis plutôt «rapport Nabucco», parce qu’en fin de compte, nous sommes intéressés par l’énergie, par le gaz de ce troisième pays producteur de gaz au niveau mondial. Et je sais – on nous l’a très bien expliqué lorsque nous étions présents au Turkménistan – que si l’Union européenne n’était pas intéressée par le gaz turkmène, ils avaient d’autres clients, et notamment la Chine. Donc évitons l’hypocrisie, affichons clairement...
(Le Président retire la parole à l’orateur)

