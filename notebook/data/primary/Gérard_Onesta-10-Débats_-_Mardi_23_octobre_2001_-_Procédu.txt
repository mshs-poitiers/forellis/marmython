Gérard Onesta # Débats - Mardi 23 octobre 2001 - Procédure budgétaire 2002
  Deprez (PPE-DE
). 
 - Monsieur le Président, je m'autorise à vous poser une question.
Je ne comprends pas pourquoi, alors que je suis officiellement rapporteur pour avis de la commission des libertés publiques, que j'ai préparé les amendements de ma commission, que je les ait fait voter par ma commission, que je les ai défendus (et, à certains moments, de manière relativement vigoureuse) à la commission des budgets -, où j'ai d'ailleurs la plupart du temps obtenu l'acquiescement de mes collègues, pourquoi, donc, je ne figure pas sur la liste des rapporteurs pour avis. Je vous demande donc de m'autoriser à prendre la parole maintenant, au nom de la commission des libertés publiques. 
