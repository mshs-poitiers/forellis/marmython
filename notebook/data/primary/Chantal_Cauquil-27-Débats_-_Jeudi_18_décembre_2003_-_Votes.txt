Chantal Cauquil # Débats - Jeudi 18 décembre 2003 - Votes
  Bordes et Cauquil (GUE/NGL
),
par écrit
. 
- Lors des précédentes lectures, nous avions voté pour ce rapport. En effet, il proposait d’accroître sensiblement les garanties offertes aux usagers du transport aérien, même si, comme nous l’avions déjà souligné, il s’efforçait de ménager les intérêts des compagnies aériennes.
Mais, au fil des conciliations et autres aménagements, ce texte n’a cessé de réduire au profit des compagnies, les avantages accordés aux passagers. Certes, ceux-ci n’ont pas complètement disparu, et c’est uniquement pour ne pas y faire obstacle que nous ne votons pas contre ce rapport. Mais, ne voulant pas cautionner les reculades successives de ce texte face aux intérêts des compagnies aériennes, nous avons choisi de nous abstenir sur le vote final. 
