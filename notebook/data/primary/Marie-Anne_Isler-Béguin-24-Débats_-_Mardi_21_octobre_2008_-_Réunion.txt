Marie-Anne Isler-Béguin # Débats - Mardi 21 octobre 2008 - Réunion du Conseil européen (15-16 octobre 2008) (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie Anne Isler Béguin  (Verts/ALE
). -  
Madame la Présidente, Monsieur le Ministre, j’étais à Tbilissi lorsque le président est venu négocier le plan de paix et, bien sûr, je lui rends hommage d’avoir agi rapidement et d’avoir su arrêter cette guerre. Mais cette guerre c’est un peu notre échec. Nous nous sommes montrés frileux pendant quatorze ans, nous avons assisté silencieux à l’escalade des provocations dans les régions séparatistes. Cette guerre, certes, a réveillé l’Europe, en la plaçant devant ses responsabilités. Mais le feu continue à couver dans la Caucase et nous devons tout mettre en œuvre pour régler définitivement les conflits gelés pour la sécurité de toute l’Europe. 
Je connais aussi les divisions, Monsieur le Ministre, des pays européens sur l’entrée de la Géorgie dans l’OTAN, et, personnellement, je suis contre. Et je vous ferai une proposition: je demande à l’Union européenne de proposer le statut de neutralité pour ces pays du Caucase. Seul le statut de neutralité pourra apaiser les tensions avec la Russie et prévenir définitivement cette sous-région de nouveaux conflits. Ce statut assurera la sécurité de ces jeunes démocraties et contribuera à notre propre sécurité. 
