Bruno Gollnisch # Débats - Mercredi 18 février 2009 - Rapport annuel 2007 sur les principaux aspects et les choix fondamentaux de la PESC – Stratégie européenne de sécurité et PESD – Le rôle de l’OTAN dans l’architecture de sécurité de l’UE (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bruno Gollnisch  (NI
). 
 – Madame la Présidente, quelle que soit l’amitié que nous éprouvons pour les rapporteurs Vatanen et von Wogau, il ne nous est pas possible d’approuver leurs rapports.
D’abord parce que l’OTAN, Organisation du traité de l’Atlantique Nord, a été créée en 1949 pour répondre à la terrible menace que faisait peser le communisme sur l’Europe occidentale. Elle a joué un rôle utile, indispensable même. Mais, aujourd’hui, cet épouvantable système communiste s’est effondré, le Pacte de Varsovie a été dissous.
Or, l’OTAN ne cesse de s’étendre. Ses activités s’étendent en dehors de son cadre géographique. L’Afghanistan, que je sache, n’est pas riverain de l’Atlantique Nord. Le Kosovo non plus, où l’on a contribué à l’épuration ethnique des Serbes dans une guerre injuste qui n’a rien résolu. L’OTAN viole donc la Charte des Nations unies.
Mes chers collègues, vous êtes complètement inconséquents. Vous prétendez créer une Europe forte et indépendante et vous absorbez la défense européenne dans un commandement à domination américaine. Comment la Russie et d’autres nations ne verraient-elles pas dans tout cela une attitude agressive?
L’OTAN nous asservit à la politique des États-Unis d’Amérique, dont nous sommes les amis, mais dont nous ne saurions être les vassaux et encore moins les valets. Il faut en finir, il faut en sortir. L’OTAN a fait son temps! 
