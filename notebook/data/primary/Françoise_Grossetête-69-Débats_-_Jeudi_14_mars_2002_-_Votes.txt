Françoise Grossetête # Débats - Jeudi 14 mars 2002 - Votes
  Grossetête (PPE-DE
),
par écrit
. - J'ai voté en faveur de ce rapport
L'Union Européenne, et notamment le Parlement européen, sont très attachés à la protection des citoyens, de leur environnement, de leur quotidien et de leur santé. Bien souvent, des textes équilibrés dans ces domaines sont approuvés au niveau communautaire. Toutefois, nous nous apercevons que la transposition d'un certain nombre de directives par les États membres accuse un retard injustifié.
La directive sur les eaux urbaines résiduaires est un exemple de cette mauvaise application. Adoptée en 1991, elle a pour objectif de protéger l'environnement contre les effets du rejet des eaux urbaines résiduaires et des eaux résiduaires biodégradables provenant de l'industrie alimentaire. Lorsqu'elle a été appliquée, la directive a conduit à une amélioration significative de la qualité des eaux. La principale obligation qui incombe aux États membres est de mettre en place des systèmes de collecte et de traitement des eaux résiduaires et de respecter un calendrier pré-établi.
Or, non seulement des États membres tardent à communiquer toutes les informations nécessaires à une évaluation objective de cette directive mais, pis encore, d'autres ne communiquent aucune information sur ce sujet. C'est totalement inacceptable !
Aussi, je demande que le retard pris par les États membres ne s'aggrave pas, que toutes les informations soient communiquées à la Commission comme le stipule cette directive adoptée en son temps par le Conseil. Je me suis prononcée en faveur de l'ouverture par la Commission de procédures d'infraction dans les cas où les critères d'identification des zones sensibles n'auront pas été respectés ou pris en compte. 
