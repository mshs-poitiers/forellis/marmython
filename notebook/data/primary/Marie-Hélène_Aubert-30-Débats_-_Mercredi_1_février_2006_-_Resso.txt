Marie-Hélène Aubert # Débats - Mercredi 1 février 2006 - Ressources halieutiques de la Méditerranée
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie-Hélène Aubert, 
au nom du groupe Verts/ALE
. - Monsieur le Président, à mon tour, et vous verrez qu’il y a vraiment consensus sur ce dossier, je voudrais insister sur l’urgence qu’il y a à débloquer cette situation car l’état de la ressource en Méditerranée est totalement alarmant. C’est en particulier le cas pour le thon rouge, mais aussi pour d’autres espèces comme ma collègue l’a indiqué. L’attentisme, les blocages, les faux-fuyants sont par conséquent vraiment désastreux pour l’avenir de la Méditerranée.
Par ailleurs, nous souhaitons préserver la pêche artisanale qui est tellement importante dans les régions concernées où elle fait vivre des milliers de gens, comme cela a été dit. Ce n’est toutefois pas en l’exonérant d’un certain nombre de mesures de bon sens qu’on y arrivera, mais bien plutôt en l’aidant prioritairement à s’adapter et en la soutenant pour qu’elle puisse poursuivre ses activités dans le respect des réglementations qui doivent être celles évoquées dans le rapport.
Je pense par conséquent qu’il faut préserver la Méditerranée: c’est un enjeu vital pour l’avenir de la pêche et c’est aussi un enjeu culturel de taille. Le Conseil doit en avoir pleinement conscience et avoir à cœur de débloquer cette situation le plus rapidement possible. 
