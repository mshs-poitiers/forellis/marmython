Pervenche Berès # Débats - Mercredi 27 février 2002 - Ordre des travaux
  Berès (PSE
). 
 - Monsieur le Président, cette modification de l'ordre du jour n'était pas anticipée par votre proposition. Elle est directement liée à l'absence de disponibilité du Conseil. L'usage, jusqu'à présent, est que ces levées d'immunité parlementaire sont toujours examinées durant les périodes de session de Strasbourg. Je comprends donc très bien que lundi, en premier point de l'ordre du jour, nous inscrivions cette question, mais il me semble que ce n'était pas ce qui était prévu, y compris après le vote de la commission juridique la semaine dernière. 
