Ari Vatanen # Débats - Jeudi 10 avril 2008 - Livre vert sur l’«Adaptation au changement climatique en Europe – les possibilités d’action de l’UE» (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Ari Vatanen  (PPE-DE
). – 
(EN)
 M. le Président, on est censé garder son calme quand on participe à un débat, mais celui-ci échappe à tout contrôle. Il a littéralement dégénéré et, avec lui, ont disparu l’honnêteté et la raison. Nous sommes censés faire preuve d’honnêteté, mais tout décrire en termes noirs a toujours favorisé une cause politique. La manœuvre a toujours marché. Il nous faut cependant rester sérieux et examiner les chiffres en gardant notre raison et notre honnêteté.
Les gens peuvent vivre à Helsinki, où la moyenne annuelle de la température est de 6 °C ou ils peuvent vivre à Dakar où il fait 30 °C. Il y a deux mille ans, Jules César a traversé le Rhin à pied parce qu’il était gelé. Il y a mille ans, les Vikings étaient dans le Groenland – c’est pourquoi la région porte ce nom. La température n’est pas une fin en soi, mais bien la prospérité de la population et c’est ce que nous oublions.
Pourquoi avons-nous des émissions? C’est le résultat de la prospérité alors que les deux tiers de la population du monde vivent encore dans la pauvreté. La famille humaine se trouve comme dans un escalier: nous, les nantis, nous sommes au sommet et en dessous de nous sont se trouvent quatre milliards de personnes qui, littéralement, veulent se faire une place au soleil parce que deux milliards d’entre eux n’ont même pas d’électricité.
Quelle est alors la réponse? Notre réponse est d’améliorer notre technologie. Pour cela, nous avons besoin d’argent et que notre économie se développe. Nous ne pouvons amasser de l’argent si nous gaspillons et endommageons notre économie en appliquant des mesures contreproductives et très coûteuses.
Le commissaire n’a même pas mentionné le mot clé de «nucléaire» dans son intervention d’ouverture. Si nous ne sommes pas honnêtes dans ce débat, alors nous ne méritons pas le titre de responsable sérieux. En résumé, nous sommes un peu comme l’homme dont le toit fuit. Au lieu de le réparer, il commence par tapisser sa cave. Ce n’est pas ainsi que nous allons nous bâtir un avenir. 
