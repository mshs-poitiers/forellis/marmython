Jean-Louis Bourlanges # Débats - Mercredi 15 septembre 1999 - Adoption du procès-verbal
  Bourlanges (PPE
). 
 - Madame la Présidente, simplement pour dire que dans le vote relatif à l’interprétation du règlement, je suis mentionné comme ayant voté contre, alors que j’ai voté pour. En réalité, j’étais en retard et j’ai voté de la place de M. Pasqua. J’imagine que c’est ce qui explique que la machine ait résisté à mon intention.
(Rires)

Mais je l’avais signalé tout de suite après. 
