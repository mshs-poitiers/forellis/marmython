Martine Roure # Débats - Vendredi 24 avril 2009 - Paiements transfrontaliers dans la Communauté - Activité des établissements de monnaie électronique (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  La Présidente. - 
 L’ordre du jour appelle la discussion commune sur:
– le rapport de Margarita Starkevičūė, au nom de la commission des affaires économiques et monétaires, sur la proposition de règlement du Parlement européen et du Conseil concernant les paiements transfrontaliers dans la Communauté (COM(2008)0640
 - C6-0352/2008
 - 2008/0194(COD)
) (A6-0053/2009
), et
– le rapport de John Purvis, au nom de la commission des affaires économiques et monétaires, sur la proposition de directive du Parlement européen et du Conseil concernant l’accès à l’activité des établissements de monnaie électronique et son exercice ainsi que la surveillance prudentielle de ces établissements, modifiant les directives 2005/60/CE et 2006/48/CE et abrogeant la directive 2000/46/CE (COM(2008)0627
 - C6-0350/2008
 - 2008/0190(COD)
) (A6-0056/2009
). 
