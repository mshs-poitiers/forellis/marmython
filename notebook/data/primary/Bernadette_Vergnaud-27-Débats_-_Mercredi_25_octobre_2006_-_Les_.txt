Bernadette Vergnaud # Débats - Mercredi 25 octobre 2006 - Les partenariats public-privé et le droit communautaire des marchés publics et des concessions (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bernadette Vergnaud (PSE
). 
 - Monsieur le Commissaire, Monsieur le Président, mes chers collègues, le rapport de Barbara Weiler offre la clarté et la lisibilité dont nous avions besoin sur les partenariats public-privé, ce qui permet de corriger les dérives du juge européen et d’améliorer la clarification juridique pour tous les acteurs concernés. Les concessions sont trop souvent confondues avec les marchés publics: ces derniers concernent l’achat de biens consommés par l’autorité locale, alors que les concessions offrent à l’autorité publique la possibilité de déléguer à un tiers l’exercice d’une partie de ses missions. Donc, oui à une législation sur ces concessions et non à une simple communication interprétative.
Même constat pour les PPP institutionnalisés. Leur création est actuellement menacée par les rigueurs de la jurisprudence qui tend à une double mise en concurrence pour attribuer les travaux, ce qui scellerait le sort des sociétés d’économie mixte. Merci donc à notre rapporteur d’avoir demandé une initiative législative sur ce sujet.
Enfin, dans l’arrêt Commission contre Espagne, la structure intercommunale est devenue un partenaire privé pour la Cour de justice. Une commune devrait donc mettre en concurrence une structure intercommunale à laquelle elle délègue des prestations de service. Mme Weiler rétablit le droit et nous offre la meilleure contribution parlementaire sur ce sujet, je l’en remercie. 
