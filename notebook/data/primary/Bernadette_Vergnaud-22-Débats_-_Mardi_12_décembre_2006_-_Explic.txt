Bernadette Vergnaud # Débats - Mardi 12 décembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Rapport: Thyssen (A6-0408/2006
) 
  Bernadette Vergnaud (PSE
), 
par écrit
. - Le programme d’action communautaire dans le domaine de la protection des consommateurs pour la période 2007-2013 vise à «compléter, à appuyer et à suivre» les politiques des États membres.
Il contribuera à protéger la santé, la sécurité ainsi que les intérêts économiques et légaux des consommateurs, de même qu’à promouvoir leur droit à l’information, à l’éducation et à l’organisation pour défendre leurs intérêts. Les actions contribueront à garantir à tous les consommateurs de l’Union un niveau de protection élevé et à assurer l’application effective des règles de protection des consommateurs.
Je me réjouis par ailleurs que ce programme ait été scindé en deux parties distinguant ainsi la santé de la protection des consommateurs.
Même si je regrette que la Commission ait réduit l’enveloppe financière de 233,46 millions d’euros - en première lecture - à 156,8 millions d’euros et que le nombre d’actions dans le domaine de la politique des consommateurs ait été revu et réduit de vingt à onze, j’ai voté en faveur du rapport de Mme Thyssen, afin que les organisations de consommateurs puissent bénéficier de ce programme dans l’intérêt de nos concitoyens européens. 
