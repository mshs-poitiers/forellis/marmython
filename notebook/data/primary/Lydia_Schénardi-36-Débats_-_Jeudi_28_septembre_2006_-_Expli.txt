Lydia Schénardi # Débats - Jeudi 28 septembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Rapport: Breyer (A6-0254/2006
) 
  Lydia Schenardi (NI
), 
par écrit
. - Je dois féliciter ma collègue, Mme Breyer, pour son rapport. En effet, je ne peux qu’adhérer à ses conclusions qui préconisent, je cite: «d’opérer un changement radical des politiques commerciales de l’Union européenne». Il aura fallu un rapport de la commission des droits de la femme pour qu’enfin les droits des femmes, mais aussi de tous les travailleurs, soient pris en compte dans la mondialisation actuelle voulue et subie par Bruxelles.
C’est presque naïvement que le rapporteur semble découvrir que la pression concurrentielle dans une économie de plus en plus mondialisée conduit à la baisse des salaires, des frais d’exploitation, au chômage, aux délocalisations et aux fermetures d’entreprises. Les chiffres sont en effet très inquiétants: 70% des 1,3 milliard de personnes qui, à l’échelle mondiale, vivent dans la pauvreté sont des femmes.
Toutefois, je dois reconnaître qu’il est utile ici de souligner que l’inégalité entre les sexes, et tout particulièrement en Asie et en Afrique, où la femme est humiliée, bafouée et considérée comme inférieure à l’homme, engendre évidemment des difficultés dans le domaine économique, commercial, social et politique. Avant toute chose, il est bien évident que c’est le statut même de la femme qu’il faut revoir immédiatement dans tous ces pays où bien souvent c’est la loi coranique qui prévaut. 
