Chantal Cauquil # Débats - Mardi 13 janvier 2004 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. 
- Dans le rapport Vidal-Quadras Roca, consacré à la gestion du combustible nucléaire irradié et des déchets radioactifs, ainsi que dans le rapport Seppänen qui traite de la sûreté des installations nucléaires, nous avons soutenu tous les amendements qui tendaient à imposer une plus grande sécurité dans le fonctionnement des installations, comme dans le traitement et le stockage des déchets radioactifs.
En revanche, nous n’avons pas soutenu les amendements qui écartaient tout échéancier pour ce renforcement de la sécurité, parfois pour des raisons techniques, mais en laissant en fin de compte les États membres agir à leur guise.
Sans nier les dangers intrinsèques du nucléaire, c’est le fait qu’on ne puisse faire confiance aux gouvernants, parce qu’ils font toujours passer les intérêts financiers des groupes privés avant les intérêts humains des populations, qui reste là encore le pire danger. 
