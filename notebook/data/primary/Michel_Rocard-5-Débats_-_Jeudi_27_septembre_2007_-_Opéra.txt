Michel Rocard # Débats - Jeudi 27 septembre 2007 - Opération PESD à l'est du Tchad et au nord de la République centrafricaine (vote) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Michel Rocard  (PSE
). – 
(FR) 
Monsieur le Président, je crois que cet amendement est inutile dans la mesure où les Nations unies ont reçu, le 11 septembre 2007, une lettre du gouvernement tchadien, dont j'ai ici photocopie, dans laquelle il donne son accord: "accueille avec satisfaction les nouvelles recommandations du Secrétaire général des Nations unies contenues dans son rapport du 10 août 2007 pour le déploiement d'une présence internationale dans l'est du Tchad, destiné etc...".
Donc, cet amendement est inutile. L'accord est écrit, et tout le monde peut l'avoir. Je suis très surpris parce que j'ai envoyé le texte à M. Gahler. Je propose donc qu'on vote contre cet amendement. 
