Charles Pasqua # Débats - Mercredi 12 mars 2003 - Votes
  Pasqua (UEN
),
par écrit
. - Fidèle à l'orthodoxie monétariste et fédéraliste, ce rapport est un plaidoyer pour une interprétation encore plus rigide du pacte - qui porte bien mal son nom - de stabilité et croissance. Il s'agit moins, ici, de tirer les leçons de la réalité observable que d'instruire le procès des États coupables de ne pas se soumettre aux obligations d'un texte qui constitue aujourd'hui un carcan insoutenable pour la relance économique.
Surtout, il apparaît clairement que l'absolutisation du pacte de stabilité n'est pas seulement motivée par des raisons d'ordre purement économique, mais qu'il s'agit désormais d'un enjeu véritablement politique.
En effet, qui ne voit pas que l'équilibre budgétaire n'est pas seulement appréhendé comme un objectif économiquement louable - ce qui relève du simple bon sens -, mais qu'il est érigé au rang d'un dogme auquel nul ne doit déroger, sous peine de remettre en cause l'ensemble d'un édifice (l'UEM) construit pour des motifs 
avant tout 
politiques (mettre en place la première structure européenne authentiquement fédéraliste) ?
Voilà pourquoi le rapporteur, plutôt que d'analyser les réalités économiques actuelles, tient à accentuer davantage la rigueur du pacte de stabilité, notamment en renforçant l'autonomie de la Commission dans la surveillance des États membres. Cette logique est inacceptable. 
