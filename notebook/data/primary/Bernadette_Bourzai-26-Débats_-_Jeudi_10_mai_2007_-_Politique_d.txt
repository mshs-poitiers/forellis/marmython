Bernadette Bourzai # Débats - Jeudi 10 mai 2007 - Politique du logement et politique régionale - Contributions de la future politique régionale à la capacité d’innovation de l’UE (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bernadette Bourzai (PSE
). 
 - Madame la Présidente, chers collègues, je souhaite en premier lieu m’exprimer sur le rapport d’initiative de M. Andria relatif à la politique de logement et de la politique régionale. Je tiens d’abord à le féliciter pour son excellent travail, ainsi que l’intergroupe sur le logement pour la réflexion qu’il a menée concernant la contribution des fonds structurels à une politique du logement dans les États membres.
Pour ma part, élue d’une circonscription rurale et de montagne en voie de désertification, je m’intéresse davantage à la problématique du logement en milieu rural. C’est pourquoi j’ai déposé deux amendements sur le sujet, qui ont été en partie repris, d’où ma satisfaction aujourd’hui, car je craignais que le logement soit seulement associé à la problématique urbaine. Il s’agissait pour moi de souligner le cumul des handicaps dans les zones rurales - faiblesse des revenus des personnes, habitats dispersés et souvent vétustes, insuffisance de logements locatifs sociaux ou privés - et les enjeux en termes de revitalisation des territoires ruraux par l’accueil de nouvelles populations. Il était important ensuite de souligner le caractère indispensable de mesures d’encouragement à l’acquisition, la réhabilitation ou la rénovation du bâti ancien, de soutenir les organismes publics et privés prodiguant des conseils et un accompagnement individualisé pour l’installation de particuliers ou de professionnels, ainsi que d’améliorer l’offre de logements sociaux, publics et privés, neufs ou rénovés.
Le rapport d’initiative souligne de façon pertinente la spécificité de la problématique du logement dans les petites villes qui assurent le maillage du territoire et ont un grand rôle dans le développement des zones rurales.
Concernant le rapport Janowski, j’interviens au nom de ma collègue Brigitte Douay, qui ne peut pas être présente ce matin. Elle tient à souligner que le rapport tel qu’adopté en commission du développement régional sur la contribution de la future politique à la capacité d’innovation de l’Union européenne le satisfait pleinement. Elle se réjouit notamment de l’adoption d’amendements sur les PME et leur rôle dans l’innovation au plan régional, ainsi que sur les caractéristiques propres des régions montagneuses et des zones rurales. Par contre, il lui semble négatif de revenir sur la rédaction du paragraphe 14, qui est issu d’un compromis assez largement partagé par les différents groupes politiques et qui ne doit pas être modifié. 
