Danielle Auroi # Débats - Lundi 2 octobre 2000 - Ordre des travaux
  Auroi (Verts/ALE
). 
 - Madame la Présidente, je veux juste expliquer en quelques secondes pourquoi nous demandons ce report. C'est un rapport controversé qui ajoute la question des OGM dans un sujet que la Commission n'abordait pas. De plus, ce rapport est proposé au débat du jeudi soir et au vote du vendredi matin. Or, il nous semblait que les rapports controversés ne devaient pas se voter le vendredi matin. 
