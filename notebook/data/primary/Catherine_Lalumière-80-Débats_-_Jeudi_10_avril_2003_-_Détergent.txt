Catherine Lalumière # Débats - Jeudi 10 avril 2003 - Détergents
  La Présidente.  
- 
L'ordre du jour appelle le rapport (A5-0105/2003
) de M. Nobilia, au nom de la commission de l'environnement, de la santé publique et de la politique des consommateurs, sur la proposition de règlement du Parlement européen et du Conseil concernant les détergents [COM(2002) 485
 - C5-0404/2002
 - 2002/0216(COD)]. 
