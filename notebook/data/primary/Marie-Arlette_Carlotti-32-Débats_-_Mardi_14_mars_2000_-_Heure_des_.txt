Marie-Arlette Carlotti # Débats - Mardi 14 mars 2000 - Heure des questions (Commission) 
  Carlotti (PSE
). 
 - Merci beaucoup, Monsieur le Commissaire, de cette réponse très complète. Je voudrais vous demander une précision. Si la ligne B7/601 voit sa légitimité renforcée, quelles dispositions pourront être prises pour garantir l'accessibilité effective des ONG au financement de l'Union ? 
