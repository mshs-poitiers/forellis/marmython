Margie Sudre # Débats - Jeudi 1 février 2007 - Explications de vote 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Rapport (A6-0325/2006
) de M. Deva
  Margie Sudre (PPE-DE
), 
par écrit
. - La pêche constitue dans l’Océan Pacifique une ressource fondamentale pour les économies locales, et des navires venus du monde entier, notamment d’Europe, y sont actifs. Cela justifie que la commission de la pêche, dont je suis membre, soit consultée sur ce rapport.
La commission du développement, rapporteur au fond, a voulu nous imposer le rapport sans possibilité d’amendement, en octobre, ce qui a été refusé. La commission de la pêche a donc pu proposer des améliorations au rapport: meilleure coopération régionale, et surtout inclusion des pays et territoires d’Outre-mer.
L’avis de la commission de la pêche a été adopté à l’unanimité de ses membres, en novembre. Or, la veille du débat en plénière, on nous annonce que notre avis ne sera finalement pas intégré, et que seuls quelques éléments, sélectionnés arbitrairement, seront proposés comme nouveaux amendements.
Devant notre indignation, il a été décidé de rajouter notre avis au rapport final comme «erratum/addendum», sans vote, ce qui n’est guère meilleur.
La stratégie en question visait notamment à renforcer le dialogue politique entre l’Union et les îles du Pacifique. Plus de dialogue entre nos commissions parlementaires, que des milliers de kilomètres ne séparent pourtant pas, serait certainement tout aussi souhaitable! 
