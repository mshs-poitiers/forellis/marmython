Georges Berthu # Débats - Jeudi 1 avril 2004 - Votes
  Berthu (NI
),
par écrit
. 
- Le Parlement européen vient de se féliciter que le dernier Conseil européen se soit engagé à parvenir, avant le 17 juin prochain, à un accord sur le projet de constitution européenne, qualifié "d’expression de la refondation politique de notre continent".
Il s’agirait bien d’une refondation politique en effet, puisqu’elle opérerait un saut décisif vers le super-État, s’éloignant ainsi du pluralisme de souverainetés qui a toujours été la marque de l’Europe.
En particulier, les projets de compromis qui circulent actuellement en vue de l’accord final laissent prévoir l’acceptation probable du principe de la "double majorité" pour la prise de décision au Conseil - c’est-à-dire en clair l’introduction d’un critère indexant le poids des votes de chaque pays sur le nombre de ses habitants. On se dirigerait ainsi vers le calcul d’une majorité globale sans distinction des frontières, comme s’il n’y avait qu’un seul peuple unifié dans l’Union européenne.
Cette démarche, qui se trouve déjà dans la citation de Thucydide placée en exergue du projet de constitution, nie la pluralité des nations d’Europe. Elle est à l’origine du sentiment d’éloignement des peuples, qui ne se reconnaissent pas dans les institutions actuelles. C’est elle qui, fondamentalement, motive mon rejet du projet de constitution. 
