Charles Pasqua # Débats - Mardi 3 juin 2003 - Votes (suite) 
  Pasqua (UEN
),
par écrit
. -
 Dans la lignée des rapports annuels, ce document ne présente, sur le fond, qu’un intérêt très relatif. Cependant, comme l’exige l’exercice, il fournit l’occasion de faire publiquement l’éloge servile du fédéralisme juridique dont le juge communautaire, sourcilleux exécuteur testamentaire du projet politique des Pères fondateurs, est une pièce maîtresse.
Passons sur les sempiternelles félicitations adressées à la Commission, sur les regrets concernant la compétence limitée de la Cour de justice en matière de justice et d’affaires intérieures, ou sur le rappel des grands principes, véritables Tables de la Loi de la pensée fédéraliste, sur lesquels repose le gouvernement des juges au niveau européen.
En revanche, on retiendra les observations sur la nécessité d’une coopération accrue entre les parlements nationaux et le Parlement européen et entre leurs députés respectifs, pour noter qu’elles ne visent pas, hélas, à favoriser une authentique participation des parlements à l’élaboration du droit communautaire, mais simplement à renforcer le contrôle de l’application dudit droit.
Voilà qui est clair: pour le Parlement européen, les parlements nationaux n’ont qu’une fonction supplétive à remplir, à savoir organiser le "flicage" sournois de leurs propres gouvernements!
Les démocrates apprécieront... 
