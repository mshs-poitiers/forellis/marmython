Brigitte Douay # Débats - Lundi 19 mai 2008 - Budget 2009: estimations du Parlement (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Brigitte Douay  (PSE
). - 
(FR) 
Monsieur le Président, une fois encore, dans ce débat budgétaire, il nous faut remercier M. Lewandowski pour son rapport très équilibré. Et une fois encore, je tiens à souligner l'importance de l'année 2009 pour la démocratie européenne,  année qui verra le renouvellement du Parlement et de la Commission et qui sera donc une année essentielle pour la communication de nos institutions envers les citoyens.
Mais pour que ceux-ci se sentent intéressés, concernés par les questions européennes et qu'ils s'impliquent massivement dans le processus électoral, encore faut-il que la communication émanant des différentes institutions soit cohérente et lisible. Dans cette perspective, et pour la meilleure efficacité, une bonne coopération entre les trois principales institutions, qui communiquent directement ou d'une manière décentralisée par l'intermédiaire de leur représentation dans les États membres, est primordiale à un an des prochaines élections.
C'est dans cet esprit que mon groupe a déposé l'amendement n° 2 au rapport de M. Lewandowski. Il s'agit, en effet, d'un enjeu citoyen auquel nous sommes tous profondément attachés dans ce Parlement. 
