Alain Lipietz # Débats - Mercredi 22 avril 2009 - Mécanisme de soutien financier à moyen terme des balances des paiements des États membres - Mécanisme de soutien financier à moyen terme des balances des paiements des États membres (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Alain Lipietz, 
au nom du groupe des Verts/ALE
. –
 Monsieur le Président, Monsieur le Commissaire, nous avons, pour la deuxième fois, à augmenter la garantie apportée par l'Union européenne vis-à-vis des difficultés de ses membres, non membres de la zone euro. C'est la deuxième fois: en décembre, nous l'avons déjà fait.
M. McCreevy nous a félicités pour la rapidité de notre action. C'était déjà le cas en décembre, et nous voudrions dire à M. McCreevy, comme l'a dit Mme
 Berès tout à l'heure, que l'ascenseur...
Oui, Monsieur McCreevy, s'il vous plaît? Please
? Monsieur le Commissaire...
... Nous souhaiterions que la réciprocité soit appliquée, c'est-à-dire que, quand le Parlement vous demande de nous proposer un projet sur l'encadrement des hedge funds
, vous nous répondiez aussitôt, et dans les mêmes délais que ceux que nous appliquons quand vous nous demandez d'accroître la ligne d'intervention pour la protection des balances des paiements.
Nous sommes effectivement dans une crise; nous devrions siéger peut-être pas tous les jours, mais, au moins, qu'on n'attende pas six mois entre le moment où le Parlement européen demande une directive sur les hedge funds
 et le moment où la Commission s'exécute!
Alors, évidemment, en ce qui concerne cette aide, nous sommes tout à fait d'accord pour accroître la ligne de crédits, et je m'étonne un peu de l'intervention de M. Becsey. Nous avons eu exactement la même discussion au mois de décembre. Le commissaire Almunia avait expliqué à M. Becsey que c'était le gouvernement hongrois lui-même qui avait demandé l'aide du FMI, sans demander l'aide de l'Union européenne, et que c'est l'Union européenne qui avait dit: « Mais on peut vous aider nous aussi ».
Il est bien évident que l'Union européenne a un devoir de solidarité vis-à-vis des pays non membres de l'euro, mais il n'y a aucune raison de refuser, par ailleurs, l'aide du FMI, à laquelle chacun de nous, la Hongrie et la Roumanie comprises, contribue.
Alors donc, dans le rapport Berès, pour lequel nous voterons en tout état de cause, il y a quand même deux choses qui nous chagrinent. Premièrement, à quoi cela rime-t-il, quand on dit, au paragraphe 4, qu'il faut s'engager à la solidarité interpays, de rappeler ensuite dans le paragraphe 11 que, en aucun cas, nous ne sommes tenus par les engagements d'un pays? Alors, c'est vrai que l'on n'est pas tenus par les engagements d'un pays, mais il ne sert à rien de le rappeler quand on dit qu'on sera solidaires.
Deuxième problème: on dit qu'il n'y a pas de base légale pour l'augmentation de cette solidarité, mais c'est à la Commission justement de fournir cette base légale. Nous sommes en crise et nous devons être dans une grande période de fourniture de la base légale. 
