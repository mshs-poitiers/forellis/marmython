Alain Krivine # Débats - Mardi 23 septembre 2003 - Votes (suite) 
  Krivine et Vachetta (GUE/NGL
),
par écrit
. -
 Nous avons voté contre la désignation de Jean-Claude Trichet à la présidence de la Banque centrale européenne, parce qu’il a incarné pendant une quinzaine d’années la doctrine dite de la "désinflation compétitive", qui a inspiré les politiques néolibérales menées dans notre pays. C’est sous son influence que l’on a bloqué les salaires pour permettre aux profits financiers de capter une part croissante de la richesse produite. Cette politique a étouffé la croissance et favorisé la montée du chômage de masse. Nous ne voudrions donc pas voir transposé au niveau communautaire ce dogmatisme funeste.
Mais nous n’en faisons pas une question de personne: nous voterions probablement contre n’importe quelle autre candidature, parce que c’est la conception même de la Banque centrale européenne que nous contestons sur deux points essentiels. Une telle institution ne devrait pas se voir imparti un objectif inutilement rigide de 2% d’inflation qui choisit aveuglement la finance au détriment de l’emploi. Et la BCE devrait être placée sous un véritable contrôle démocratique qui aille plus loin que la démocratie d’opérette consistant, comme aujourd’hui, à voter les pleins pouvoirs à un homme qui sera désormais incontrôlable. 
