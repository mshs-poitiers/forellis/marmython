Jean-Claude Martinez # Débats - Lundi 3 avril 2006 - Conférence ministérielle de l’OMC à Hong-Kong (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Claude Martinez (NI
). 
 - Monsieur le Président, Madame la Commissaire, ici tout le monde est d’accord pour le libre commerce, personne ne veut refaire l’Albanie d’Enver Hoxha. Nous sommes donc tous pour des règles multilatérales mais quelles règles et au service de quoi?
On nous dit que le libre-échange est au service de l’emploi et de la croissance. Mais les règles appliquées jusqu’ici au commerce ont-elles fait la richesse des nations? Réponse: au Mexique, quatre vingt-quatorze accords de libre-échange et ce n’est pas le bonheur; en Équateur, les indigènes des Andes sont contre l’accord de libre-échange et, en France, en ce moment, trois millions de lycéens sont dans la rue parce qu’il n’y a pas d’emplois; chez moi, les vignerons sont dans la misère, il y a des suicides et pourtant il y a le libre-échange.
L’observation montrant que les règles actuelles du libre-échange n’amènent pas la prospérité économique, de deux choses l’une. Soit le libre-échange est le nom laïque de la religion chrétienne, où les vignerons d’Europe, les paysans, les ouvriers, doivent monter sur la croix pour racheter les péchés du monde. Dans ce cas, continuons et, en échange de nos concessions, le Brésil, l’Inde n’ouvrent pas leur marché et le monde anglo-saxon ne reconnaît pas notre propriété intellectuelle sur nos appellations agricoles. Ainsi, Madame la Commissaire, nous persistons dans la pauvre voie qui, après dix cycles de négociations du GATT et de l’OMC, n’a rien amené à l’Afrique.
Soit, enfin, on renonce à l’erreur et on emprunte la voie, non pas de la technique archaïque de la réduction et de la suppression des droits de douane, mais celle de la technique moderne de la déduction des droits de douane. On fait le saut technologique avec l’invention de droits de douane déductibles parce que, sous forme de crédits d’impôts offerts aux pays exportateurs et utilisables dans le pays importateur, on a là des droits de douane qui assurent la neutralité économique et on résout les drames de la mondialisation.
Dites à Peter Mandelson que son rôle n’est pas de suivre David Ricardo mais d’avoir la capacité d’invention de John Maynard Reynes. 
