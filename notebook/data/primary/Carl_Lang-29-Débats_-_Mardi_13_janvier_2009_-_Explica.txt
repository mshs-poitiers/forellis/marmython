Carl Lang # Débats - Mardi 13 janvier 2009 - Explications de vote (suite) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Christa Klaß (A6-0443/2008
) 
  Carl Lang  (NI
), 
par écrit
. – 
Les études d’impact menées par les instituts et centres techniques français montrent que le projet de révision de la directive européenne sur les pesticides pourrait conduire à la disparition d’un grand nombre de produits aujourd’hui disponibles sur le marché.
Il est important que ce projet donne les moyens aux agriculteurs de l’Union de protéger leurs cultures. Dans le cas contraire, des baisses de production pour les filières végétales seraient alors très importantes et pourraient avoir également un impact non négligeable sur les filières animales.
Des filières végétales entières pourraient être condamnées en France et en Europe et c’est le rôle même de l’agriculture, qui est de nourrir nos concitoyens avec des produits sains et variés, qui serait ainsi menacé.
Sans remettre en cause le souci de protection des consommateurs et des utilisateurs, la nouvelle réglementation ne doit pas non plus mettre en péril la diversité des familles chimiques et l’innovation. Elle doit donc prévoir immédiatement des solutions alternatives.
C’est la seule solution pour éviter la délocalisation de nombreuses productions agricoles pourvoyeuses d’emplois et de richesses.
Face à tous ces enjeux cruciaux pour les agriculteurs, les producteurs de légumes, de fruits et de céréales, il s’agira de continuer à rester vigilants sur les réformes en cours et sur les mesures d’application nationales qui seront prises. 
