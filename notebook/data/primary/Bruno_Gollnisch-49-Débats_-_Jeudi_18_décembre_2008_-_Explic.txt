Bruno Gollnisch # Débats - Jeudi 18 décembre 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Monica Maria Iacob-Ridzi (A6-0463/2008
) 
  Bruno Gollnisch  (NI
), 
par écrit. – 
Le problème, pour le rapporteur, ne semble pas tant être la levée des obstacles juridiques ou administratifs à la mobilité professionnelle des travailleurs européens sur le territoire de l’Union européenne, mais au contraire le fait que cette mobilité ne soit pas générale et surtout obligatoire. C’est à un grand brassage de populations, accélérant la disparition des nations d’Europe, que l’on nous invite. C’est à la concurrence salariale, au dumping social et à une harmonisation par le bas des salaires que l’on pense. Avec la création d’une carte européenne de sécurité sociale aux contours bien flous, c’est à la mise en danger et au démantèlement des systèmes de protection sociale nationaux, que l’on travaille.
Demandez à ces ouvrières françaises à qui l’on a proposé, il y a quelques années, de sauvegarder leur emploi à condition qu’elles décident de tout abandonner pour partir travailler en Roumanie pour quelques centaines d’euros par mois ce qu’elles pensent de votre mobilité!
Tenter de résoudre les problèmes fiscaux ou d’acquisition de droits sociaux des travailleurs frontaliers, ou dont la carrière s’est déroulée dans plusieurs États membres, cela relève en effet des compétences de l’Union européenne. Mais pas au prix de la précarisation sociale. 
