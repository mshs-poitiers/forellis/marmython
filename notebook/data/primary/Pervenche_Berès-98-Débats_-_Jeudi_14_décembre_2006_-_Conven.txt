Pervenche Berès # Débats - Jeudi 14 décembre 2006 - Convention de La Haye sur les titres (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Pervenche Berès (PSE
). 
 - Monsieur le Président, Monsieur le Commissaire, je voudrais simplement m’assurer que, lorsque vous rendrez compte de cette discussion au commissaire McCreevy, vous aurez soin de lui dire que le débat que nous venons de mener s’adresse à lui, mais qu’il doit aussi en rendre compte et relater ce qui vient de se passer ici, cet après-midi, lorsque cette question sera à nouveau abordée au Conseil. Il faut que le Conseil ait conscience que l’issue qui sera donnée à ce dossier préoccupe le Parlement. 
