Bernadette Vergnaud # Débats - Mardi 2 septembre 2008 - Réseaux et services de communications électroniques - Autorité européenne du marché des communications électroniques - Démarche commune d’utilisation du spectre libéré par le passage au numérique - Réseaux et services de communications électroniques, protection de la vie privée et protection des consommateurs (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bernadette Vergnaud  (PSE
).  
– (FR)
 Madame la Présidente, Messieurs les Secrétaires d'État, Madame la Commissaire, chers collègues, après des mois de discussions, qui ne sont d'ailleurs toujours pas achevées, je crois pouvoir affirmer que grâce au travail de M. Harbour et du rapporteur fictif, nous sommes parvenus à des compromis tout à fait favorables aux consommateurs. Ainsi, une meilleure concurrence doit être assurée par une série de dispositions visant notamment à obliger les opérateurs à proposer des durées de contrats acceptables et à rendre effective la portabilité du numéro en cas de changement d'opérateur, dans un délai d'une journée.
En matière de protection, la réglementation générale sur la protection des consommateurs devra s'appliquer au secteur des télécom, des mécanismes de contrôle des coûts sont prévus ainsi que l'amélioration des procédures de recours extra judiciaires en cas de litige.
Un autre point très important concerne l'accès aux services d'urgence et à la localisation de l'appel qui devra être obligatoirement disponible de manière fiable, quelle que soit la technologie utilisée.
La protection de la vie privée a aussi fait partie des priorités, de même que la protection des enfants, les fournisseurs d'accès devant fournir gratuitement aux clients les logiciels de contrôle parental.
Toutes ces avancées se devaient d'être assurées pour le plus grand nombre. De nombreuses mesures concernent donc l'égal accès pour les usagers handicapés, les personnes à faibles revenus et les PME n'ont pas été oubliées. De même, le rapport insiste sur la nécessité d'élargir le champ d'application du service universel au haut débit notamment et l'inscription de ce point dans les priorités de la présidence française est une très bonne chose.
Je voudrais maintenant évoquer la question des contenus et des droits d'auteur, qui a eu tendance à éclipser le reste des améliorations contenues dans ce texte. Notre objectif a toujours été de fournir aux consommateurs une information générale sur le respect des droits d'auteur, conformément à la proposition initiale de la Commission. Jusqu'au vote final, nous travaillerons à améliorer la formulation des compromis en veillant au respect du principe de neutralité d'accès au contenu. Certains amendements adoptés dans la directive «Vie privée» sont en revanche réellement problématiques et nous veillerons à les supprimer.
Je tiens encore à remercier mes collègues et j'attends des propositions plus précises de la présidence afin d'améliorer encore ce texte d'ici la prochaine session plénière. 
