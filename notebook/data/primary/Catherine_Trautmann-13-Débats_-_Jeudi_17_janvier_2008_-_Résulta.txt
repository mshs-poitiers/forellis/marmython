Catherine Trautmann # Débats - Jeudi 17 janvier 2008 - Résultats du second forum sur la gouvernance de l'internet (Rio de Janeiro, 12 au 15 novembre 2007 (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Catherine Trautmann, 
au nom du groupe PSE
. – 
(FR) 
Madame la Présidente, Madame la Commissaire, chers collègues, le deuxième forum sur la gouvernance de l'Internet s'est déroulée à Rio du 12 au 15 novembre dernier et a attiré 2 100 participants de tous les continents qui ont représenté les parties prenantes à l'Internet de tous les milieux publics et privés.
Le forum fournit un contexte favorable à la recherche de solutions sur les problématiques futures des TIC. Par une culture commune et un partenariat, il facilite les décisions à prendre lors des prochains sommets mondiaux sur la société de l'information et aussi par nos États.
Il permet également de mener une réflexion ouverte, à la hauteur de la complexité d'Internet, voire d'anticiper ses limites tant techniques qu'éthiques et juridiques car les enjeux sont grands outre les sujets chers à l'Union: réduction de la fracture numérique, liberté d'expression sur la toile, diversité culturelle, protection des enfants – j'en citerai quelques autres – l'identification par fréquence radio, l'IRFID, c'est-à-dire in fine la constitution d'un Internet des objets, les risques de saturation des réseaux à l'horizon 2010-2012, la numérisation, en particulier des biens culturels, les conséquences engendrées pour la protection de la propriété intellectuelle et surtout le développement de l'accès aux TIC des pays moins développés.
L'Europe est en pointe pour ce qui est de l'appareil législatif. Pour nous, Européens, c'est encourageant, mais ne nous endormons pas. Nombre de ces questions juridiques ou réglementaires sont d'ailleurs remises sur le métier avec la révision du paquet Telecom.
J'en appelle ici à une nouvelle phase de travail politique et prospectif concret, en particulier au travers de la création d'un IGF européen via les parlements nationaux et les pouvoirs locaux. Forum européen mis en œuvre d'ici 2009, peut-être forum mondial en Europe grâce à la Lituanie candidate pour 2010.
En tant que chef de délégation, je veux remercier tous les membres, collègues et fonctionnaires pour leur travail et leur disponibilité. La résolution que nous allons voter constitue une feuille de route. Je suis heureuse qu'elle bénéficie du soutien de l'ensemble des groupes de ce Parlement. Davantage de commissions parlementaires sont invitées à contribuer au renforcement de cette action, et je voudrais remercier la Commission ainsi que la solliciter pour engager la collégialité de son instance en soutien de Mme Reding. Enfin, je voudrais m'adresser au Conseil afin qu'il inscrive dans son ordre du jour des relations avec l'Inde, la préparation du forum à New Dehli et que tous les États puissent également s'engager dans la coopération renforcée. 
