Lydia Schénardi # Débats - Jeudi 18 janvier 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Rapport: Záborská (A6-0478/2006
) 
  Lydia Schenardi (ITS
), 
par écrit
. - Si une approche intégrée pour l’égalité entre les hommes et les femmes, notamment dans le domaine de l’emploi, est une nécessité, il en va tout à fait différemment de sa mise en œuvre et de son application de façon autoritaire et obligatoire.
Pourtant, c’est malheureusement ce que nous propose le rapport de mon excellente collègue, Mme Zaborska, qui au nom de la dignité et de l’égalité pour les femmes, nous demande de voter en faveur d’un système obligatoire de quotas sur les listes des partis politiques. Je crois qu’il ne faut pas céder à la tentation de l’idéologie «femmes à tout prix». Cela serait une démarche contreproductive, et à terme négative, pour l’image des femmes. Celles-ci supporteraient ipso facto la présomption irréfragable de l’incompétence et de l’absence de légitimité, quelle que soit leur valeur.
Il est en effet nécessaire de favoriser l’accès de certains postes ou fonctions à des femmes qui jusqu’à présent s’en trouvent exclues ou sous-représentées, mais ne tombons pas dans l’excès et la démagogie qui ne feront que nuire à la femme. 
