Hélène Flautre # Débats - Mercredi 25 octobre 2006 - Accord euro-méditerranéen d’association UE/Syrie (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Hélène Flautre, 
au nom du groupe Verts/ALE
. - Madame la Présidente, un obstacle essentiel sur le chemin de la signature s’élève, à savoir la situation des droits de l’homme en Syrie, laquelle ne cesse de se détériorer. Les observateurs dénoncent régulièrement les arrestations arbitraires, les tortures, les discriminations flagrantes à l’égard des minorités, les entraves aux libertés d’expression et d’association, le harcèlement et l’emprisonnement des personnes qui œuvrent pacifiquement en faveur de l’amélioration des libertés en Syrie.
Je pense à Michel Kilo, à Anwar al Bunni, à Mahmoud Issa, qui sont toujours en détention pour avoir signé en mai de cette année la déclaration Beyrouth-Damas appelant à la normalisation des relations entre le Liban et la Syrie. Michel Kilo aurait dû être libéré le 19 octobre mais une nouvelle inculpation a eu pour effet de proroger sa période de détention. Quant à M. Issa, libéré le 25 septembre, il vient d’être à nouveau arrêté il y a deux jours. Ces exemples parmi bien d’autres démontrent l’absence de volonté réelle des autorités syriennes d’engager les réformes démocratiques nécessaires.
Dans ces conditions - et vous le dites, Madame De Keyser - il est impensable que l’Union européenne conclue un accord d’association avec la Syrie. Comme la résolution l’affirme, l’amélioration de la situation des droits de l’homme et le respect des valeurs démocratiques doivent être un préalable à toute conclusion d’accords. Il importe d’ailleurs également de mettre en place un mécanisme de contrôle efficace dans le cadre de l’application de la clause «droits de l’homme». Cette approche devrait par ailleurs être appliquée, entre autres, à la Russie et au Turkménistan, ainsi qu’à bien d’autres pays. Il en va de la cohérence des positions du Parlement. 
