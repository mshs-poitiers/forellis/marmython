Yasmine Boudjenah # Débats - Jeudi 10 octobre 2002 - Votes
  Boudjenah (GUE/NGL
). 
 - Monsieur le Président, c'est pour indiquer que mon groupe retire la première partie de l'amendement. Donc, il resterait : "demande à la Commission de procéder à une évaluation régulière des conséquences économiques, sociales et environnementales de la mise en œuvre de l'accord d'association".
