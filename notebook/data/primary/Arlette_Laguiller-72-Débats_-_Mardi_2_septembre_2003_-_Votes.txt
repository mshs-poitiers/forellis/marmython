Arlette Laguiller # Débats - Mardi 2 septembre 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. - Nous avons voté en faveur de ce rapport qui concerne la protection des travailleurs contre les risques liés à l’exposition à des agents cancérigènes ou mutagènes imposant au patronat des protections et des contraintes d’utilisation.
Mais la meilleure des protections serait de ne pas utiliser de produits présentant des risques aussi graves pour la santé des travailleurs et de les remplacer par des produits moins nocifs dès la mise au point de techniques et la découverte de produits de remplacement. 
