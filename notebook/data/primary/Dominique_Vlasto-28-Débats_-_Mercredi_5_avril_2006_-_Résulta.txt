Dominique Vlasto # Débats - Mercredi 5 avril 2006 - Résultats du Conseil européen - Stratégie de Lisbonne (débat) 
  Dominique Vlasto (PPE-DE
). 
 - Faire de l’Union européenne l’économie de la connaissance la plus compétitive d’ici à 2010 implique à mon sens deux priorités: investir massivement en termes de dépenses de R[amp]D et supprimer les contraintes qui pèsent sur l’entreprise et notamment les plus petites.
Le budget européen, malgré l’accord amélioré obtenu par le Parlement européen, ne permettra pas ces investissements massifs. L’appel lancé par le Conseil européen à la BEI pour renforcer son action dans la R[amp]D est, dans ce contexte, une solution innovante et pragmatique, si 30 milliards d’euros parviennent effectivement à être mobilisés par ce biais.
Cette mesure ne sera peut-être pas suffisante pour pallier le déficit chronique des dépenses européennes de R[amp]D et un gros effort reste à fournir pour atteindre l’objectif d’y consacrer 3 % du PIB.
Il est indispensable d’associer le secteur privé à cet effort, puisque nous voyons bien que les budgets publics sont insuffisants. Il faut donc aller plus loin en matière fiscale pour inciter les entreprises à investir plus facilement dans la R[amp]D.
Cette politique volontariste est ce que nous attendons de la stratégie de Lisbonne révisée, qui doit offrir un cadre règlementaire européen stimulant, efficace et surtout simplifié pour les entreprises européennes. 
