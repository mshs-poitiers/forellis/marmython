Martine Roure # Débats - Mardi 13 janvier 2009 - Heure des questions (questions à la Commission) 
  La Présidente. –  
J’appelle la 
question n° 36 de Liam Aylward 
(H-0978/08

)
Objet:	Menaces terroristes
En novembre dernier, les attaques terroristes de Bombay ont mis en danger la vie de bon nombre de citoyens européens. À la lumière des attentats de Madrid et de Londres, qui se sont produits respectivement en 2004 et 2005, il est manifeste que des attaques similaires ayant pour cible l’Union européenne sont à craindre. La Commission pourrait-elle préciser les mesures qui sont prises pour améliorer l’échange d’informations entre les forces de police des États membres en vue de faire face à ce type d’événements?
