Jean-Claude Martinez # Débats - Mardi 3 octobre 2000 - VOTES
  Martinez (TDI
),
par écrit
. -
 La comptabilité nationale est magique. En quelques agrégats, PIB, PNB, consommation, revenus, formation du capital, etc, elle donne un modèle réduit de la vie économique d'un pays. Grâce à elle, on peut ainsi calculer les pressions fiscales, les pourcentages de déficits publics ou d'endettement.
Autant dire qu'elle est un instrument majeur du pilotage de la politique économique, budgétaire et fiscale. C'est par elle que les critères de Maastricht trouvent à s'appliquer et c'est surtout par elle que la Communauté européenne trouve ses ressources TVA et surtout sa 4ème ressource, assise sur le PNB.
Encore faut-il toutefois que les procédures d'établissement de ces comptes nationaux, à la fois dans les opérations décrites et les secteurs institutionnels résumés, soient identiques de pays à pays. Sinon, évidemment, il n'y a pas de comparaison possible.
D'où la nécessité de principes communs pour enregistrer les impôts et les cotisations sociales au sein des Quinze. C'est précisément tout l'objet du règlement concerné ici.
Il rend homogène les mécaniques des comptes économiques intégrés au sein du système européen des comptes. Ce système européen des comptes, remontant à 1970, a été remodifié en 1995.
Bien des problèmes se posent. Par exemple, pour apprécier les recettes, faut-il partir du fait générateur de l'impôt ou de l'encaissement. On sait par exemple qu'en France, le chiffre des rentrées fiscales varie selon que l'on prend les statistiques de la Direction générale de la comptabilité publique ou celles de la Comptabilité nationale. On imagine alors aisément a fortiori les variations de pays à pays. D'autant que les États ont intérêt à "tricher" pour sous-estimer leur base imposable à la TVA ou au prélèvement PNB.
Les rapports de la Cour des comptes européenne ne cessent d'ailleurs de relever ces sous-estimations, de la Grèce au Portugal.
Dans le cas d'espèce de la modification du règlement de 1996 qui nous intéresse, la Commission propose d'enregistrer les impôts et les cotisations en tenant compte d'abord des recettes effectivement perçues, plutôt que des droits à recettes reconnus à partir du fait générateur.
La Commission propose aussi d'appliquer un coefficient au pourcentage de "non recouvrable" pour ne pas sous-estimer la réalité du déficit, au regard des exigences de Maastricht.
Le tout paraît de pure technique, mais les conséquences politiques sont importantes. 
