Marie-Arlette Carlotti # Débats - Lundi 13 mars 2006 - 4ème World Water Forum de Mexico City (16-22 mars 2006) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie-Arlette Carlotti (PSE
). 
 - Monsieur le Président, un milliard 200 millions de personnes n’ont pas accès à l’eau potable, si bien qu’il y a dans le monde quinze morts chaque minute, dont la moitié sont des enfants. C’est donc une véritable course contre la montre dans laquelle nous sommes engagés. C’est pourquoi le Forum mondial doit avancer des propositions de trois ordres.
La question du droit, d’abord. L’eau est un bien commun de l’humanité et l’accès à l’eau un droit fondamental de la personne humaine. Ce droit être consacré par un traité international.
La question des moyens ensuite: réduire de moitié d’ici à 2015 la proportion de la population qui n’a pas accès à l’eau est un objectif ambitieux. Reste à trouver 100 milliards de dollars supplémentaires chaque année. Pour cela, il faut une mobilisation de l’ensemble des ressources: augmentation de l’aide publique au développement (APD), augmentation des ressources inscrites au dixième FED, soutien de partenariats novateurs public/privé ou d’opérations d’échange de dettes contre investissements.
La question de la gestion, enfin. Les politiques de l’eau doivent être définies par une approche participative, démocratique, associant les usagers, les sociétés civiles et le rôle des femmes et être gérées au niveau local, avec la mise en place de politiques tarifaires qui garantissent l’accès à l’eau des plus démunis à des prix abordables. Je souhaite que la Commission défende de telles orientations au Forum mondial de Mexico. 
