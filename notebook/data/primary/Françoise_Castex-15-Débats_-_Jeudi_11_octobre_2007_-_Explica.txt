Françoise Castex # Débats - Jeudi 11 octobre 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
– Rapport: Alain Lamassoure, Adrian Severin (A6-0351/2007
) 
  Françoise Castex  (PSE
), 
par écrit
. 
– (FR) 
J'ai voté en faveur du rapport Lamassoure relatif à la distribution des sièges au Parlement européen après les élections législatives de 2009. Cette nouvelle distribution équilibre le nombre de sièges en fonction de l'évolution de la population des États membres. Je me félicite du fait que la France bénéficie de deux députés supplémentaires par rapport à ce que le traité de Nice révisé prévoyait.
Par ailleurs, j'adhère à l'idée que cette nouvelle répartition soit révisée bien avant le début de la législature 2014-2019, afin de mettre en place un système de répartition objectif et équitable au sein du Parlement européen, tenant compte des changements démographiques, ce qui permettra d'éviter les traditionnels marchandages entre États membres. 
