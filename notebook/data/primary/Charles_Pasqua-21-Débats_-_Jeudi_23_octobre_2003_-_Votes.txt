Charles Pasqua # Débats - Jeudi 23 octobre 2003 - Votes
  Pasqua (UEN
),
par écrit
. - Ce rapport a au moins un aspect positif: suggérer qu’il existe encore
 une politique industrielle européenne. Force est de constater que nombreux sont ceux, à commencer par les salariés des entreprises européennes frappées par la crise économique, qui doutent de la volonté de l’Europe de préserver notre tissu industriel.
Certes, l’industrie européenne doit effectivement se concentrer sur ses atouts: haut niveau de qualification de la main-d’œuvre, importante capacité technologique et de recherche, maîtrise des processus de production "propres", etc.
Mais dire cela est-il suffisant? Jusqu’à présent les analyses se multiplient, mais rien de concret n’émerge.
Plus grave: prises dans le carcan d’un Pacte de stabilité qui n’opère aucune distinction entre investissements productifs et dépenses improductives dans le calcul de la dette publique, soumises au contrôle aussi rigoureux qu’abstrait de la Commission en matière d’aides d’État (affaire Alstom) ou de concentrations entre entreprises, nos entreprises n’ont manifestement pas les moyens de concurrencer efficacement l’industrie américaine, qui, elle, bénéficie d’un soutien permanent de la part de son gouvernement.
Plus que de rapports, les industries européennes, les entrepreneurs ont besoin d’une volonté politique claire. Or, ce rapport ne répond pas à cette urgence. 
