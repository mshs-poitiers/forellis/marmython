Marie-Noëlle Lienemann # Débats - Jeudi 17 novembre 2005 - Explications de vote
  Marie-Noëlle Lienemann (PSE
), 
par écrit.
 -
 J’ai toujours défendu quatre exigences dans le dossier REACH.
1-	Un système d’enregistrement large et rigoureux des substances chimiques afin de les connaître, d’évaluer leur risque, d’en informer les utilisateurs et d’éviter ceux qui sont nocifs.
2-	Un système d’autorisation non laxiste, s’agissant des produits ayant une certaine nocivité, l’autorisation d’usage doit être limitée dans le temps.
3-	Le principe de substitution: L’obligation de la substitution pour les produits nocifs doit être la règle.
S’il n’existe pas de substituts connus, des recherches doivent être immédiatement engagées et l’autorisation d’usage limitée.
4-	La création d’une agence européenne forte qui s’appuie sur le réseau d’institutions nationales chargées des expertises mais qui reste l’arbitre en dernier ressort pour la mise en œuvre de cette législation.
J’ai voté contre le « compromis PSE / PPE / ALDE « concernant la procédure d’enregistrement, car il réduit considérablement le nombre de substances concernées par REACH. 
