Lydia Schénardi # Débats - Mardi 14 mars 2006 - Institut européen pour l’égalité entre les hommes et les femmes (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Lydia Schenardi (NI
). 
 - Monsieur le Président, une chose est sûre, les structures, réseaux en tous genres, ayant vocation à étudier et défendre les femmes, ne manquent pas. On peut citer l’Institut pour l’égalité entre les hommes et les femmes, les diverses ONG, comités ad hoc, agences des droits fondamentaux, les forums consultatifs sur les droits des femmes, le lobby européen des femmes, ou notre propre commission au sein du Parlement.
Alors, est-il réellement raisonnable de créer un nouvel institut européen en charge de l’égalité entre hommes et femmes? Dans cette nébuleuse de structures ne fonctionnant pas toujours en parfaite harmonie dans l’échange des informations, ce nouvel organe censé mettre en réseau toutes ces informations aura-t-il les moyens réels d’exister? En clair, peut-on nous l’assurer, aujourd’hui, et ce au-delà même de son éventuelle opérabilité, de sa totale indépendance politique et financière?
Il semble que non, la Commission n’étant pas disposée à donner trop de marge de manœuvre à ce futur institut. En effet, sa réticence à laisser le directeur de l’Institut responsable uniquement devant son conseil d’administration et non pas devant elle, est, à cet égard, très révélateur.
Pour toutes ces raisons et d’autres encore, je ne peux pas approuver cette initiative de création de ce que je me permettrais d’appeler «une énième usine à gaz».
