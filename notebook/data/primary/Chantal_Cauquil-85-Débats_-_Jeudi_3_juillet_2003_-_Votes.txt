Chantal Cauquil # Débats - Jeudi 3 juillet 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. - Nous avons évidemment voté contre la résolution commune sur l’OMC parce que nous sommes contre le système capitaliste, comme contre toutes les organisations internationales qui le représentent.
Nous avons également voté contre ou nous sommes abstenues sur la plupart des amendements parce que, même s’ils expriment de bonnes intentions pour rendre meilleur le monde capitaliste, ils ne font que participer à la tricherie générale. Le capitalisme n’est pas améliorable avec les rustines de quelques amendements du Parlement européen. 
