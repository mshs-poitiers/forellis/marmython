Nicole Fontaine # Débats - Lundi 10 décembre 2001 - Ordre des travaux
  La Présidente.  
- 
Je voudrais vous annoncer qu'à la demande de plusieurs groupes, nous procéderons, demain, à une brève mais néanmoins intense commémoration du 11 septembre. Elle aura lieu en plénière, juste avant l'allocution du président sud-coréen, c'est-à-dire vers 11h50. 
