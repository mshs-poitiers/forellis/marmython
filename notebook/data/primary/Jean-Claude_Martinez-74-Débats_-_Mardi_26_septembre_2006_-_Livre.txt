Jean-Claude Martinez # Débats - Mardi 26 septembre 2006 - Livre blanc de la Commission sur les services d’intérêt général (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Claude Martinez (NI
). 
 - Monsieur le Président, Monsieur le Président Barroso, mes chers collègues, l’école, la santé, la poste, l’eau, les transports, c’est le cœur de la vie en société. C’est cela que le service rendu au public, dans l’intérêt du public, demande forcément pour être géré: un service de régime juridique public.
C’est en Europe que des pays comme la France ont inventé cette façon solidaire de gérer les parties communes de la copropriété sociale. Or, c’est juste au moment où cette technique intelligente du service à portée générale, à portée universelle pourrait inspirer les solutions audacieuses à trouver aux problèmes planétaires de l’eau, du médicament de base, de l’instruction, et de tous les biens des parties communes de la copropriété planétaire, c’est juste à ce moment que la Commission européenne réduit, sinon détruit, cet outil de pilotage des sociétés humaines.
La raison de ce gâchis consistant à détruire ce qui marchait depuis un siècle, c’est la croyance que le marché est grand, que la connaissance est son prophète et qu’on doit privatiser tous les services, comme le veut l’OMC, y compris, ici, les services des auxiliaires de session, c’est-à-dire ce squat social que nous avons fabriqué au Parlement européen, où 300 hommes travaillent sans papiers sociaux.
Monsieur Barroso, au-delà des problèmes techniques développés par nos collègues, comme par exemple Harlem Désir tout à l’heure, le problème est un problème culturel, est un problème de choix. Ou nous gérons les sociétés humaines à partir de la loi du marché, de la jungle, ou nous les gérons à partir de la loi de la raison.
Monsieur Barroso, est-ce que vous voulez continuer à sauter de liane en liane, du FMI à l’OMC, en poussant le cri de «marché, marché», ou est-ce que vous voulez vous asseoir dans la clairière et traiter raisonnablement des problèmes de raison? 
