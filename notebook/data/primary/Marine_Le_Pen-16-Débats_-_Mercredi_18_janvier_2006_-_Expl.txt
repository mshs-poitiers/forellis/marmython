Marine Le Pen # Débats - Mercredi 18 janvier 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Jarzembowski (A6-0410/2005
) 
  Marine Le Pen (NI
), 
par écrit
. - Ce que la Commission nous propose avec cette directive sur la libéralisation des services portuaires, ce sont ni plus ni moins des ports de complaisance. Nous n'en voulons pas. Pas plus que nous ne voulons des navires à pavillons de complaisance sur lesquels des armateurs peu scrupuleux embauchent une main d'œuvre mal formée et sous-payée.
La libéralisation à tout prix voulue par Bruxelles dans ce domaine est synonyme de chômage et de misère sociale. Au nom de son idéologie ultralibérale et antinationale, la Commission nous annonce une réduction des coûts au détriment de la sécurité, de l'emploi et de la qualité des services. La démolition du monopole dont jouissent les dockers ne conduira pas seulement à un énième cimetière social, mais engendrera de l'insécurité dans les ports européens et sera in fine un frein à leur compétitivité.
En novembre 2003, grâce à une formidable mobilisation des dockers et agents portuaires de toute l'Europe, un frein avait été mis à cette directive «Bolkestein bis», empêchant ainsi l'ouverture d'une profession injustement stigmatisée au profit de personnels sous-qualifiés du Tiers-monde.
Une fois de plus, c'est la préférence étrangère à l'embauche qui est prônée et avec elle, la fin du professionnalisme, des compétences, des traditions et des acquis sociaux. 
