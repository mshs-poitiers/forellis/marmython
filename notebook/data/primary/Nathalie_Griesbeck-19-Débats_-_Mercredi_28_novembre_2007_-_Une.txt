Nathalie Griesbeck # Débats - Mercredi 28 novembre 2007 - Une nouvelle politique européenne du tourisme: renforcer le partenariat pour le tourisme en Europe (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Nathalie Griesbeck, 
au nom du groupe ALDE
. – 
(FR) 
Madame le Président, chers collègues, bien sûr le tourisme représente une part très importante de l'économie de nos régions, mais il représente aussi, d'une certaine manière, la poursuite de la construction de l'identité européenne et de notre politique de cohésion. Il contribue souvent, bien entendu, à maintenir une activité dans les régions les plus reculées et constitue même, souvent, la première ressource des régions dites périphériques.
Ce rapport très complet met en évidence les points essentiels sur lesquels l'Union peut apporter une véritable plus-value aujourd'hui, afin d'optimiser cette ressource intelligemment pour qu'elle profite à tous, aux professionnels du tourisme comme aux touristes eux-mêmes, en un mot aux Européens, en préservant sur le long terme la qualité de nos paysages et de nos écosystèmes.
Pour ma part, venant d'une région qui a la chance d'avoir trois voisins européens, je suis sensible particulièrement au tourisme frontalier et je souhaite qu'au travers des partenariats, ce type de tourisme permette la construction d'un véritable espace de vie des citoyens européens, d'une part, qui dépasse les frontières intérieures de l'Union, d'autre part.
Mais, pour nous ouvrir encore davantage au tourisme non communautaire, il faut disposer aujourd'hui d'une politique coordonnée d'attribution des visas touristiques. Je voudrais bien aussi que l'Europe élabore des outils statistiques, de même qu'une approche transversale des financements communautaires pour permettre ces fameux effets de levier sur l'innovation, sur l'emploi, sur l'amélioration de l'offre comme de la qualité des prestations. Je souhaite que nous créions des labels de qualité européens prenant en compte les critères écologiques et sociaux et que nous renforcions tout simplement l'information et la protection des consommateurs européens. 
