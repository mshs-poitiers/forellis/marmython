Ambroise Guellec # Débats - Jeudi 21 février 2008 - 4ème rapport sur la cohésion économique et sociale – l’Agenda territorial et la Charte de Leipzig (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Ambroise Guellec, 
rapporteur
. 
(FR)
 – Monsieur le Président, je serai bref. Tout d'abord, concernant les observations faites sur les amendements présentés par la commission de la pêche, je dis tout de suite, ici, que la pêche est chère à mon cœur d'élu littoral mais que, malheureusement, nous ne sommes pas parvenus à intégrer les amendements de la commission dans le texte qui va être proposé au vote tout à l'heure. Nous le regrettons, mais je pense que ceci n'enlève rien à l'attention qui doit être portée à ce secteur très important pour certaines régions de l'Union.
Je voudrais dire, d'autre part, que la cohésion est une grande politique européenne. Je crois qu'elle était très présente dans cette Assemblée ce matin également. C'est important notamment pour les travaux futurs. Mme la commissaire disait tout à l'heure que nous opérons un constat, mais ce qui nous intéresse évidemment c'est l'avenir. Elle a pu constater que l'attente est très grande sur la cohésion territoriale, ce que ça représente dès le moment où c'est dans le traité de Lisbonne. On peut dire que la balle est maintenant dans le camp de la Commission, avec notre concours et notre soutien, Madame la Commissaire, bien entendu. Il faut que ce travail soit fait et bien fait, ensemble.
Je voudrais ajouter que le fait que tout ce domaine de la politique régionale sera maintenant en codécision Conseil-Parlement, nous aidera, je pense, à avancer ensemble, d'abord dans la visibilité pour nos concitoyens – et je crois que cela a été beaucoup souligné ce matin également –, et aussi pour qu'une vraie solidarité s'exerce au bénéfice des territoires qui en ont le plus besoin, deux sujets de préoccupations évoqués très fortement ce matin.
Nous avons donc énormément de travail devant nous. Nous nous en réjouissons pour cette politique essentielle pour l'Union européenne. 
