Élizabeth Montfort # Débats - Mercredi 4 octobre 2000 - VOTES (suite) 
  Montfort (UEN
),
par écrit
. - 
Je voudrais, avec le rapporteur, me féliciter des progrès considérables que la Lettonie a accomplis ces derniers mois, afin d'être de la première vague de l'élargissement que connaîtra prochainement l'Union européenne.
Même si ce satisfecit
 recouvre une réalité quelquefois disparate - Mme Schroedter souligne dans son rapport la situation difficile des zones rurales, ou encore les lenteurs de la nécessaire adaptation de l'administration publique - il n'en demeure pas moins que la Lettonie a infléchi sa politique de manière à devenir incontestablement, un membre de la famille européenne telle qu'on l'entend habituellement dans ce Parlement.
Et davantage que de fastidieuses exégèses comptables, c'est bien ce sentiment - ou son absence - qui devra déterminer nos choix en matière d'élargissement le moment venu, car la construction européenne n'a plus de sens si elle cesse d'être avant tout politique.
Les arguties ayant pour objet de changer la grille de lecture des événements lorsque ceux-ci contrarient des dogmes technocratiques et monétaires un peu trop figés - l'incroyable indifférence de ce Parlement face au résultat du référendum danois en constitue un magnifique et inquiétant exemple - ne sauraient servir de référence à un débat de cette importance.
La Lettonie a sa place dans cette enceinte, non seulement parce que, globalement, ses progrès lui donnent droit à ce "crédit de confiance" indispensable pour mener à bien une telle négociation, mais surtout, et avant tout, parce qu'elle est chez elle en Europe.
C'est pourquoi, en partageant les constats et les remarques du rapporteur, je me félicite de ce dialogue et des perspectives qu'il fait naître. J'espère que la Lettonie aura l'occasion d'apporter toute la richesse de ses particularités à une Union européenne qui, je l'espère aussi, aura choisi dans l'intervalle de se réorganiser en un ensemble politique cohérent, respectueux des nations et de leur souveraineté, plutôt qu'en un confus comité d'actionnaires d'inégale importance ayant perdu de vue leurs objectifs premier : ceux de la construction d'une Europe forte et indépendante. 
