Françoise Grossetête # Débats - Mercredi 3 octobre 2001 - Conseil européen extraordinaire - Bruxelles, 21 septembre 2001 - Travaux préparatoires en vue de la réunion du Conseil européen à Gand
  Grossetête (PPE-DE
). 
 - Monsieur le Président, je crois que nous pourrions présenter nos excuses au commissaire Barnier, parce qu'il est intolérable que notre Parlement, sur des sujets aussi graves que ceux-là, ne se conduise pas de la manière la plus courtoise pour écouter une intervention particulièrement importante. Je le regrette profondément, et j'en appelle quand même au sens de la responsabilité de nos collègues.
(Applaudissements)

Cela s'est déjà vu dans d'autres circonstances. Mais quand il s'agit d'un débat aussi lourd, aussi grave, je crois que la moindre des choses, c'est que tous nos collègues veuillent bien se taire et respectent le commissaire ici présent, et l'écoutent. Je crois que nous avons véritablement un problème de dysfonctionnement dans ce Parlement, Monsieur le Président.
(Applaudissements)

