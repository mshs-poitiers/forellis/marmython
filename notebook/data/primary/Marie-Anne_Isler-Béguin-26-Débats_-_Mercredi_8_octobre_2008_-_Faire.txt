Marie-Anne Isler-Béguin # Débats - Mercredi 8 octobre 2008 - Faire face aux problèmes de rareté de la ressource en eau et de sécheresse dans l’Union européenne (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie Anne Isler Béguin, 
au nom du groupe Verts/ALE
. –
 Monsieur le Président, Monsieur le Commissaire, je tiens également à remercier le rapporteur pour le travail qu’il a accompli et, effectivement, je crois que nous ne pouvons que répéter ici que l’eau est devenue une ressource précieuse et rare.
Le rapporteur nous dit que 3,2 milliards d’habitants sur cette planète pourraient être confrontés à un problème de pénurie en eau. On sait également que l’Europe n’est pas épargnée. Donc, nous devons tout mettre en œuvre pour économiser cette ressource rare. L’eau est un bien commun pour l’humanité et je regrette que certains de mes amendements n’aient pas été retenus par la commission de l’environnement.
C’est pour cela que je m’adresse à la Commission directement. J’espère que, dans le cadre de la réforme de la PAC, on reverra aussi certaines méthodes de culture, et je pense notamment à des méthodes d’irrigation qui ne sont pas adaptées dans certaines zones européennes et qu’il faudra abandonner pour cesser le gaspillage de l’eau.
Je regrette également que l’amendement sur le refroidissement des centrales nucléaires n’ait pas été pris en compte parce que, dans mon pays, la France, à un certain moment de l’année, lorsque l’étiage est très bas au niveau des rivières, il faut arrêter les centrales nucléaires ou les refroidir avec un arrosoir. Cela devient ridicule, et c’est terriblement dangereux pour nos concitoyens.
