Daniel Cohn-Bendit # Débats - Mercredi 16 janvier 2002 - Élection des questeurs du Parlement européen
  Cohn-Bendit (Verts/ALE
). 
 - (EN)
 Monsieur le Président, là n'est pas la question. Si le président du deuxième groupe en ordre d'importance estime nécessaire d'utiliser l'autre système, nous ne devrions pas en discuter et procéder de la sorte. Si tel est leur souhait et s'ils se sentent plus à l'aise ainsi, nous devrions leur octroyer cette possibilité. 
