Alain Krivine # Débats - Mercredi 12 décembre 2001 - Approbation du procès-verbal de la séance précédente
  Krivine (GUE/NGL
). 
 - Monsieur le Président, j'avais demandé lundi, au moment de l'ouverture, qu'avant ce débat les députés puissent avoir connaissance de la correspondance échangée entre M. Bush et M. Prodi. Mme Fontaine nous avait assuré qu'elle ferait le nécessaire. Lors de la réunion extraordinaire de la commission des libertés de lundi, j'ai fait la même demande aux représentants de la Commission, qui semblaient étonnés que les députés n'aient pas cette correspondance. Or, sur le Net, nous avons effectivement pu trouver la lettre de M. Bush à M. Prodi, envoyée il y a déjà deux mois, mais nous n'avons toujours pas copie de la réponse de M. Prodi envoyée fin novembre.
Il semblait d'ailleurs que dans notre commission des libertés, il y avait un accord pour avoir cette lettre. Je vous demande donc si on peut avoir connaissance de cette lettre. 
