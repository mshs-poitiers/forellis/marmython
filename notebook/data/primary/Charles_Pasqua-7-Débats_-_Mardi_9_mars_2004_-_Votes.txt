Charles Pasqua # Débats - Mardi 9 mars 2004 - Votes
  Pasqua (UEN
),
par écrit
. 
- Le rapport qui nous est soumis contient les premières mesures d’application du règlement sur le statut et le financement public des partis au niveau européen, adopté en juin 2003. Ce rapport détermine, en effet, les organes du Parlement compétents pour l’attribution du financement public aux partis européens.
Bien qu’ayant approuvé ledit règlement, je ne peux cette fois-ci souscrire au contenu de ce rapport, qui confie au Bureau du Parlement l’allocation et la gestion des fonds communautaires. Le motif avancé - le Bureau est déjà chargé des questions financières concernant l’organisation interne du Parlement - n’emporte évidemment pas la conviction.
L’article 3, point c, du règlement fait du financement des partis une question éminemment politique; le versement des subsides communautaires est soumis à une obligation de loyauté politique dont l’interprétation ne comporte pas, loin s’en faut, toutes les garanties d’impartialité nécessaires.
Dès lors, il est totalement inacceptable de faire du Bureau, organe bureaucratique dans lequel seules quelques tendances politiques sont représentées, l’instance chargée de décider de la demande ou de la suspension du financement public. Celui-ci étant de nature politique, c’est à un organe ouvertement politique, en l’espèce, la Conférence des Présidents, que devraient revenir les compétences en la matière. 
