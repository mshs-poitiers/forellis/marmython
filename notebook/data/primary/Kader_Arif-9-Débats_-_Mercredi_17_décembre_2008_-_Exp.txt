Kader Arif # Débats - Mercredi 17 décembre 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Recommandation pour la deuxième lecture Cercas (A6-0440/2008
) 
  Kader Arif  (PSE
), 
par écrit
. –
 La position commune sur le temps de travail présentée par le Conseil au vote des députés constituait une véritable régression pour les droits des travailleurs et un réel danger pour notre modèle social européen. 
Réunissant derrière lui une forte majorité, le rapporteur socialiste Alejandro Cercas, que j’ai soutenu par mon vote, a réussi à faire tomber cette vision conservatrice et réactionnaire du monde du travail, digne du XIXe
 siècle. Avec l’ensemble des socialistes, j’ai soutenu une série d’amendements qui garantissent des avancées essentielles pour les droits des travailleurs. 
Ainsi, nous avons obtenu l’abrogation de la clause dite de l’opt-out
, qui permettait de s’affranchir de la limitation du temps de travail pour imposer jusqu’à 65 heures de travail hebdomadaire. De même, parce qu’on ne peut considérer que le temps de garde est un temps de repos, nous avons permis qu’il soit compté comme temps de travail. Nous avons également obtenu des garanties en matière de repos compensateur et de conciliation entre la vie familiale et la vie professionnelle.
Cette grande victoire des socialistes européens, soutenus par les syndicats, est une victoire pour l’ensemble des Européens. L’Europe qui protège, c’est l’Europe qui fait sienne des avancées sociales majeures comme celles d’aujourd’hui. 
