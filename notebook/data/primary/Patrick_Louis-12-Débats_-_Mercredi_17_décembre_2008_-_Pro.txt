Patrick Louis # Débats - Mercredi 17 décembre 2008 - Projet de budget général 2009, modifié par le Conseil (toutes sections) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Patrick Louis  (IND/DEM
). 
 – Monsieur le Président, chers collègues, pour la 14e
 année consécutive, la Cour des comptes européenne a refusé de certifier le budget de l’Union européenne.
Si les procédures comptables de la Commission ont été saluées par la Cour - ce qui est quand même la moindre des choses - on remarquera que seuls 8 % des comptes de l’Union européenne sont dûment certifiés. Nous comprenons tous qu’à ce rythme aucune entreprise privée ne pourrait subsister à une telle critique. Cela signifie que 92 % du budget européen, soit plus de 100 milliards d’euros, restent entachés par un trop haut niveau d’inégalités et d’irrégularités.
Je viens de citer les mots du rapport. Irrégularités qui s’ajoutent à de multiples irresponsabilités. Quand vous pensez, par exemple, qu’une agence de communication prévoit un budget de 15,4 millions d’euros pour envoyer une urne dans l’espace avec le slogan «On peut voter partout», les peuples peuvent légitimement penser que l’on se moque d’eux.
Dans le contexte actuel, où les ménages et les États doivent se serrer la ceinture, au moment où la France est contributeur net pour 7 milliards d’euros, il faut arrêter de prendre les contribuables français et européens pour des pères Noël de l’Union européenne, car en juin ils deviendront des pères fouettards. 
