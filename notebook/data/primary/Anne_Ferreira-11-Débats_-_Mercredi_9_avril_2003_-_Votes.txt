Anne Ferreira # Débats - Mercredi 9 avril 2003 - Votes
  Ferreira et Patrie (PSE
),
par écrit
. - C'est parce que nous sommes profondément fédéralistes et que nous souhaitons réussir l'élargissement que nous avons dit "non" aux traités d'adhésion qui viennent d'être soumis au Parlement européen pour avis conforme.
À défaut de la réalisation de plusieurs conditions préalables, l'élargissement risque d'être un beau gâchis :
Il s'agit premièrement de conditions institutionnelles : vraisemblablement, la Convention européenne ne débouchera sur aucun bouleversement majeur des institutions permettant leur démocratisation, une plus grande lisibilité et l'affirmation de la laïcité comme principe fondamental de l'Union.
Il s'agit deuxièmement de conditions sociales : à ce jour, il n'est pas envisagé que le projet de Constitution européenne soit assorti d'un traité social comportant des garanties pour faire face aux risques de délocalisation d'entreprises ainsi que des assurances en matière de services publics.
Par ailleurs, l'accord arraché à la dernière minute en violation des prérogatives budgétaires du Parlement européen inscrit l'élargissement dans un cadre financier irréaliste et discriminant pour les pays adhérents. Il compromet gravement la réussite du processus, s'agissant notamment de la PAC dont la réorientation en faveur du développement rural est, ainsi, remise en cause.
Les pays candidats vont entrer dans un ensemble intégré qui n'est qu'un marché : en tant que socialistes européennes, nous ne trouvons pas notre compte à une telle Europe. 
