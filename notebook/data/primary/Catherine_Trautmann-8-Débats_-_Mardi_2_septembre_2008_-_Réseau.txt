Catherine Trautmann # Débats - Mardi 2 septembre 2008 - Réseaux et services de communications électroniques - Autorité européenne du marché des communications électroniques - Démarche commune d’utilisation du spectre libéré par le passage au numérique - Réseaux et services de communications électroniques, protection de la vie privée et protection des consommateurs (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Catherine Trautmann, 
rapporteur
. – (FR)
 Madame la Présidente, Madame la Commissaire, Messieurs les Ministres et chers collègues, la révision du cadre que nous examinons doit, pour être justifiée, apporter des améliorations tangibles, d'une part, pour les consommateurs, en termes de prix, d'accès, de vitesse de débit et, d'autre part, pour les entreprises, en termes de perspective d'une concurrence juste et d'investissements nouveaux et de compétitivité.
Qui dit multitude d'acteurs dit intérêts divergents, voire contradictoires. J'ai pris, pour ma part, très en amont le soin d'écouter toutes les positions, d'envoyer aussi des signes rapides et fiables dans le but d'établir ou de rétablir la confiance des entreprises et celle des consommateurs.
Les communications électroniques sont une véritable opportunité pour la croissance européenne. Le secteur représente 3 % du PIB européen. Encore faut-il savoir pleinement tirer les bénéfices de ce potentiel dans l'Union au niveau des investissements et du développement des services. Cela passe par la concurrence, mais ne s'y arrête pas. Il s'agit bien de créer les conditions d'un développement durable et responsable, autrement dit un écosystème de cette économie de la connaissance que nous appelons tous de nos vœux.
Nous devons à présent considérer les TIC comme une ressource en tant que telle. C'est donc bien un enjeu public/privé qui impose de miser sur une régulation flexible et sur la responsabilité de toutes les parties prenantes au moyen d'une coopération entre régulateurs et Commission, de la même façon qu'entre opérateurs et clients par le contrat.
Je dénombre quatre piliers que je me suis attachée à renforcer: le service au consommateur, que ce soit en termes d'accès – par un déploiement territorial plus ambitieux –, de prix juste ou de qualité; une activité industrielle soutenue, pour garantir l'emploi et l'innovation, le progrès technologique étant également un facteur de baisse de prix; la compétitivité de nos entreprises, petites et grandes, qui garantit une concurrence durable dans nos territoires et les investissements nécessaires, notamment dans la fibre, pour renforcer notre rang à l'échelle mondiale; enfin, la sécurité juridique, c'est-à-dire la fiabilité du système au travers de la responsabilisation des acteurs et de leur coopération mutuelle, en particulier entre régulateurs eux-mêmes, mais aussi avec la Commission.
J'ai été heureuse de constater que sur cette base, la commission ITRE a accepté, souvent à une large majorité, mes propositions de compromis et je remercie mes collègues pour leur grande disponibilité, malgré des délais parfois courts, mais qui nous permettent aujourd'hui d'être dans les temps pour tenir l'objectif d'un renouvellement de cette régulation avant la fin de la mandature. Ce résultat est collectif.
Je crois pour ma part que les acteurs du secteur ont répondu plutôt positivement à ces orientations et j'espère qu'elles inspireront tout autant nos partenaires du Conseil. J'ai bien écouté MM. les ministres et leurs appréciations, ainsi que Madame la Commissaire, et je les remercie pour leurs avis éclairés et positifs pour la plupart.
Pour revenir sur les points qui sont encore en débat, évoquons la question des remèdes.
Sans faire injure à la compétence des régulateurs nationaux, un consensus s'est fait jour en faveur d'une application plus cohérente des remèdes à l'échelle de l'Union. Toutefois, le veto sur les remèdes que la Commission a proposés fait l'objet d'une unanimité – ou quasi-unanimité –, contre lui. Dans le mécanisme que j'ai prévu, à chaque entité sa juste place: la Commission peut soulever un doute sur un remède mais ne peut le remettre en cause totalement que si BERT émet concomitamment une opinion négative. Inversement, dans l'hypothèse de la séparation fonctionnelle, il faut un double accord de la Commission et de BERT. Ainsi encadrée, cette séparation demeure une menace tangible mais elle ne peut être dégainée à la légère.
S'agissant du spectre, nos propositions vont dans le sens d'une flexibilité accrue de la gestion de cette ressource rare mais de manière progressive et proportionnée, parallèlement à l'introduction d'éléments d'optimisation de cette gestion. La première pierre pour nous est celle d'une véritable politique européenne du spectre qui peut être posée dès l'adoption du paquet, à l'occasion d'une grande conférence au début du prochain mandat.
Les nouveaux réseaux: ils étaient absents de la proposition de la Commission, ou insuffisamment évoqués, alors que l'Europe est déjà engagée dans cette révolution technologique. Il nous a semblé essentiel de donner dès maintenant aux États membres et aux régulateurs des orientations et des outils pour encourager les investissements et encadrer, si nécessaire, le déploiement. À ce titre, nos propositions ont été validées par un panel d'experts reconnus juste avant les congés d'été.
Un dernier problème est apparu sur le tard, celui de la protection de la propriété intellectuelle. Je regrette que ce débat ait interféré à ce point dans l'examen du paquet, je ne crois pas que ce soit le lieu pour approfondir les mécanismes permettant le strict respect de la propriété intellectuelle.
Je veux simplement dire que je souhaite que nous puissions terminer l'examen de ce paquet dans la sérénité, sans se laisser perturber par cette question, certes importante, puisque les contenus créatifs font l'objet d'une communication de la Commission. Les autres points, je les aborderai dans les deux minutes qui me resteront pour conclure. 
