Élizabeth Montfort # Débats - Jeudi 5 septembre 2002 - Votes (suite) 
  Montfort (NI
),
par écrit
. 
- 
Réaliser un espace européen de l'éducation et de la formation tout au long de la vie est devenu une priorité pour le siècle qui s'ouvre et l'on ne peut qu'encourager les initiatives allant dans ce sens.
Cette priorité est double : elle sous-tend la prise de conscience de la nécessité d'une formation tout au long de la vie pour tous et l'inscription de cette formation dans le cadre de l'espace européen et non plus seulement d'un seul État. En mettant l'accent sur la mobilité, ce rapport souligne l'importance qu'il y a désormais à former les personnes en tenant compte des expériences et des méthodes différentes propres à chacun de nos États. C'est à la fois une voie d'enrichissement individuel et un élargissement du marché de l'emploi pour chacun.
Enfin, même si ce rapport ne met pas suffisamment l'accent sur l'apprentissage, il l'inscrit néanmoins dans cette prise de conscience de la nécessité de renforcer les politiques de formation professionnelle au niveau européen et conforte ma conviction de l'absolue nécessité de mettre en place, dans les années à venir, un véritable Erasmus de l'apprentissage qui permette une formation professionnelle en réseau, conjointement pointue et ouverte sur la multiplicité des expériences de chaque peuple. 
