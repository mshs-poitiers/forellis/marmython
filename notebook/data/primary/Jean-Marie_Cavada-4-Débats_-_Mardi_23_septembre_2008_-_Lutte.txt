Jean-Marie Cavada # Débats - Mardi 23 septembre 2008 - Lutte contre le terrorisme - Protection des données à caractère personnel (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Marie Cavada  (ALDE
). 
 - Monsieur le Président, beaucoup de choses on été dites, je ne m’éterniserai donc pas sur ce sujet. Je souhaiterais simplement attirer l’attention sur un fait nouveau: le terrorisme a introduit dans notre société une donnée qui, jusque là, n’existait pas. Le terrorisme a autorisé les gouvernements et appris aux États à se méfier non plus d’un envahisseur extérieur, mais de tous leurs citoyens, et c’est là où la difficulté de gouverner réside.
Assurer la sécurité et respecter les droits, il n’est pas d’exercice plus difficile. De ce point de vue-là, je voudrais dire que le rapport de Mme Lefrançois et celui de Mme Roure constituent une synthèse extrêmement équilibrée des progrès nécessaires pour veiller à la protection des citoyens et à la protection de leur liberté.
Arbitrer entre ces deux nécessités, voilà ce que les États ne savent pas faire. Ce n’est pas dans leurs traditions, et c’est ce qu’ils apprennent actuellement peu à peu à faire; c’est certainement un honneur pour le Parlement européen et pour cette enceinte de porter l’empreinte de l’équilibre dans le cadre de la recherche des progrès à accomplir dans les deux domaines en question que sont la sécurité des citoyens et la sécurité de leur liberté.
En l’état actuel de nos travaux, il me semble que la décision-cadre, corrigée par les deux rapports de Mme Roure et de Mme Lefrançois, est le fruit de plusieurs années de travail, il faut le souligner, et donc extrêmement précieuse. Cependant, il ne s’agit là que d’une étape. Les gouvernements doivent apprendre comment tendre vers un équilibre de gouvernement des citoyens et de protection de leur vie. À cet égard, nous ne serons pas de trop pour les aider à prendre les décisions qui s’imposent et qu’ils ne sont pas capables de prendre tout seuls, selon moi, ni de les appliquer dans la limite de leurs frontières nationales.
