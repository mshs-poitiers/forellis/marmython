Véronique Mathieu # Débats - Mardi 8 juillet 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport André Brie (A6-0269/2008
) 
  Véronique Mathieu  (PPE-DE
), 
par écrit
. –
 Les bons résultats de la récente conférence des donateurs de Paris montrent que l'UE et la communauté internationale ont les yeux braqués sur l'Afghanistan.
Ce rapport, qui intervient à une période charnière pour l'avenir du pays, insiste sur les défis qui attendent l'Union européenne pour contribuer efficacement à la stabilisation du pays. Il souligne les efforts de reconstruction qui ont déjà été engagés pour le renforcement des institutions et l'amélioration des conditions de vie. À ce titre, le recul net de la mortalité infantile et l'augmentation de la scolarisation sont des signaux encourageants.
Le succès en Afghanistan passera par une meilleure coordination des différentes stratégies entre donateurs. La sécurité doit bien sûr rester une priorité, mais il faut aussi que les citoyens afghans voient concrètement en quoi l'aide leur est bénéfique. Il faut construire plus d'infrastructures de base, qu'il s'agisse de routes, d'écoles, d'hôpitaux, etc.
La réussite du processus passera aussi par une «afghanisation» du processus de stabilisation: il faut donner aux Afghans les clés qui leur permettront de prendre en main leur avenir. Beaucoup d'espoirs reposent en ce sens sur la stratégie nationale de développement de l'Afghanistan (ANDS), qui doit être soutenue par la communauté internationale et l'Union européenne. 
