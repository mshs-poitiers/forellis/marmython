Carl Lang # Débats - Mercredi 22 avril 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Romeva i Rueda (A6-0253/2009
) 
  Carl Lang et Fernand Le Rachinel  (NI
), 
par écrit
. – 
(EN)
 2008, il y a eu 165 casses de chalutiers. En 2009, 225 sont déjà annoncées.
Le secteur de la pêche en France est en extrême souffrance car le gouvernement français et Bruxelles ont décidé de sacrifier les pêcheurs français au nom de la politique économique et commerciale européenne.
Ainsi, la Norvège, pays hors de l'Union européenne, mais qui a négocié avec elle des accords économiques pour le gaz, s'octroie à elle seule 80% du quota de cabillaud, soit 500 000 tonnes par an. La France, quant à elle, ne dispose que de 9 000 tonnes par an, dont seulement 700 pour la Manche et la mer du Nord.
Comment, dans ces conditions, ne pas assister avec écœurement à la liquidation des pêcheurs français? Pourquoi Bruxelles et le gouvernement français s'acharnent-ils ainsi à la disparition programmée d'un pan entier de notre économie? Les intérêts euro-mondialistes et le dogme du libre-échange en sont l'explication.
Ce n'est pas l'enveloppe de 4 millions d'euros promise par le ministre de l'agriculture et de la pêche, au titre de compensations financières liées à l'arrêt des bateaux ayant atteint leur quota de pêche, qui résoudra ce problème. Il est urgent et vital de libérer les pêcheurs français de ces quotas européens discriminants et destructeurs. 
