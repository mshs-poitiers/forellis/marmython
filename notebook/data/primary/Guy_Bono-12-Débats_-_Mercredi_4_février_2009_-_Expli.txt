Guy Bono # Débats - Mercredi 4 février 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Claudio Fava (A6-0026/2009
) 
  Guy Bono  (PSE
), 
par écrit
. – 
J’ai voté en faveur du rapport Fava sur le projet de directive qui prévoit des sanctions à l’encontre d’employeurs d’immigrés clandestins.
D’après les chiffres de la Commission, entre 4,5 et 8 millions de ressortissants des pays tiers vivent clandestinement au sein de l’Union européenne et sont donc les cibles privilégiées d’employeurs peu scrupuleux qui vivent du travail clandestin.
Il est fondamental pour nous de mettre davantage l’accent sur ces pratiques qui sont indignes d’une Europe où le respect des Droits Fondamentaux doit s’appliquer à tous. Le temps est venu de souligner enfin la responsabilité de ceux qui profitent de cette population particulièrement vulnérable. Nous devons cesser de criminaliser ces exploités en stigmatisant les immigrés illégaux. Avec les mesures que nous préconisons ici, il ne s’agit pas seulement de sanctionner des patrons abusifs mais aussi de défendre un certain nombre de droits sociaux comme celui d’être défendu par un syndicat.
Cependant, il ne faut pas crier victoire trop tôt car la menace de sanction ne suffit pas, encore faut-il disposer des instruments juridiques de contrôle qui vont avec. C’est seulement à cette condition que nous pourrons mettre en place une politique commune efficace de l’immigration. 
