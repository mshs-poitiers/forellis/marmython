Véronique Mathieu # Débats - Mercredi 20 février 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Cem Özdemir (A6-0503/2007
) 
  Véronique Mathieu  (PPE-DE
), 
par écrit
. – 
(FR) 

À l'exception de la République kirghize, l'évolution de la zone Asie centrale est alarmante à plus d'un titre.
Tout d'abord, en matière de droits de l'homme et de démocratie, les mauvais traitements infligés à de nombreuses femmes (mariages forcés, exploitation sexuelle, viols, etc.) et le travail des enfants dans la région sont proprement inacceptables.
En matière de santé ensuite, la progression des maladies infectieuses (et notamment le VIH) demeure inquiétante.
Enfin, l'Asie centrale ne pourra bénéficier d'une intégration efficace dans le système économique mondial tant que l'ensemble des cinq pays qui la composent n'auront pas accédé à l'OMC (la République kirghize en étant membre depuis 1998).
La stratégie de l'UE pour l'Asie centrale peut contribuer efficacement au développement économique et humain de cette région du monde. L'Union européenne ne doit pas délaisser l'Asie centrale, carrefour stratégique entre les continents européen et asiatique et partenaire traditionnel dans les relations commerciales et la coopération énergétique.
Je soutiens fermement ce texte dans sa volonté de voir les priorités de l'UE en Asie centrale clarifiées, les projets européens initiés dans la région renforcés et leur mise en œuvre accélérée. C'est la raison pour laquelle j'ai voté pour le rapport. 
