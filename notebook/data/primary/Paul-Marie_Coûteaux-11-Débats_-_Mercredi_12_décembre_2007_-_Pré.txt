Paul-Marie Coûteaux # Débats - Mercredi 12 décembre 2007 - Préparation du Conseil européen (Bruxelles, 13-14 décembre 2007) (débat) 
  Paul Marie Coûteaux  (IND/DEM
). - 
(FR) 
Monsieur le Président, cette année 2007, placée sous la Présidence allemande, et suivie par la Présidence portugaise, restera dans l'histoire de la construction européenne comme l'année du plus gigantesque mépris des peuples et de la démocratie.
En effet, le traité qui sera signé demain à Lisbonne n'est ni simplifié ni consensuel. Il s'agit purement et simplement du retour du traité constitutionnel rejeté par le peuple français. La plupart d'entre vous, d'ailleurs, ainsi que M. Giscard d'Estaing, s'en réjouissent bruyamment.
Je veux donc adresser ici à mes collègues français qui soutiennent cette Constitution remaquillée la plus solennelle mise en garde. Ces dispositions créent un nouvel État. Cet État s'impose à notre peuple contre son gré, il ne sera donc pas légitime. Ceci entraîne une conséquence précise mais terrible: les organes de l'Union européenne et les actes qui sortiront d'eux devront être considérés comme illégitimes. Nous incombera ainsi bientôt un devoir impérieux dicté par le droit des peuples, à savoir le devoir de désobéissance. Je n'ai rien d'autre à dire. 
