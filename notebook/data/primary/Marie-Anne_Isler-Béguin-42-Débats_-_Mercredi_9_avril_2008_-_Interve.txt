Marie-Anne Isler-Béguin # Débats - Mercredi 9 avril 2008 - Interventions d’une minute sur des questions politiques importantes
  Marie Anne Isler Béguin  (Verts/ALE
). - 
(FR)
 Madame la Présidente, le paquet énergie-climat marque l’avènement d’un nouveau chantage, le chantage au carbone. Après avoir annoncé, le 4 avril 2008, la suppression de 575 emplois à Gandrange en Moselle, Arcelor-Mittal consent au maintien de 124 emplois sur le site, à condition de bénéficier de certificats d’émissions de CO2
. Et, pour faire passer la pilule des suppressions d’emplois lorrains, Arcelor-Mittal laisse planer l’hypothétique possibilité d’un projet-pilote de captage de CO2
, alors que ce procédé n’a pas encore été validé pour sa neutralité énergétique. En fait, Gandrange en Moselle devient le cheval de Troie des industries énergivores dont le président, M. Sarkozy est le grand défenseur au Conseil européen.
Je suis très heureuse que Mme
 la commissaire soit présente parmi nous, parce que, vraiment, je voudrais qu’on refuse le chantage d’un genre nouveau, ce chantage au carbone. Pour être efficace, en effet, le paquet énergie-climat doit suivre la respiration législative européenne. Le Parlement européen doit demander au Conseil de respecter les règles que nous nous fixons et ne pas céder aux sirènes des industriels à la recherche de quotas gratuits. Et, face aux industries et grandes entreprises, le colégislateur que nous sommes doit également assurer un accès équitable aux quotas pour les PME et PMI européennes. 
