Jacques Toubon # Débats - Mercredi 14 novembre 2007 - Gestion collective transfrontalière du droit d’auteur (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jacques Toubon  (PPE-DE
), 
par écrit
. – (FR)
 La question met la Commission devant ses responsabilités: par sa recommandation de septembre 2005 et par sa décision de remettre en cause la compétence territoriale des sociétés de gestion collective, la Commission a provoqué un bouleversement dans les relations entre les ayants droit et les sociétés nationales; et cela en dehors de toute mesure législative ou d’harmonisation.
Sous prétexte de s’adapter à l’environnement numérique, la Commission introduit en fait la confusion dans le système européen du droit d’auteur et des droits dérivés. Confusion qui conduit à la concentration et au formatage, au détriment des artistes et aux bénéfices des industriels et des opérateurs.
Il est urgent que la Commission arrête de prendre des initiatives dispersées, sans études d’impact sérieuses, examine la situation de tous les secteurs de l’art et de la culture avec toutes les parties prenantes et adopte une politique globale conforme aux exigences de la diversité culturelle, aux valeurs de l’Europe et aux objectifs de la stratégie de Lisbonne fondée sur l’économie de la connaissance et de l’innovation et propose au Parlement européen et au Conseil des projets de directives cohérents et respectueux des principes rappelés ci-dessus. 
