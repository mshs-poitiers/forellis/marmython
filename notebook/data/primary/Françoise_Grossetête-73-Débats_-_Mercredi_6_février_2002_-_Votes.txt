Françoise Grossetête # Débats - Mercredi 6 février 2002 - Votes
  Grossetête (PPE-DE
),
par écrit
. - J'ai voté en faveur de ce rapport d'initiative.
L'Union Européenne doit aujourd'hui prendre ses responsabilités dans ce dossier.
J'ai pris position, lors du vote, en faveur d'une réforme de la politique agricole commune visant à prendre en compte l'objectif de la sécurité alimentaire. Il faut bien comprendre qu'il ne s'agit pas d'opposer agriculteurs et environnementalistes. Les premiers souhaitent produire en assurant la meilleure qualité possible, les seconds souhaitent retrouver confiance en des produits sans danger pour leur santé. Ceux qui remettent en cause ce principe portent une lourde responsabilité pour l'avenir.
 
L'application systématique du principe de précaution est indispensable. En effet le principe de "tolérance zéro" doit toujours prévaloir pour toute question touchant à la protection de la santé publique.
 
Ce rapport estime que les stocks existants de farines animales et d'aliments en contenant doivent être détruits et insiste sur l'interdiction de nourrir les ruminants avec des farines animales.
 
Par ailleurs, lorsqu'un cas avéré d'ESB apparaît, le retrait de la chaîne alimentaire de l'ensemble du troupeau apparaît toujours comme la solution présentant le maximum de garanties pour la sécurité de nos concitoyens. Les méthodes alternatives à la destruction de l'ensemble du troupeau ne pourraient être mises en œuvre qu'une fois leur efficacité avérée et uniquement si elles offrent le même niveau de sécurité. La plus grande prudence s'impose en raison des incertitudes scientifiques actuelles quant aux modes de contamination.
C'est pourquoi l'amélioration et le développement de la recherche sur les EST et d'autres maladies et contaminations encore inconnues me semblent nécessaires.
Il me paraît également indispensable d'assurer au mieux le suivi des sanctions et des mesures prises à l'encontre des États membres qui ne respectent pas cette législation. 
