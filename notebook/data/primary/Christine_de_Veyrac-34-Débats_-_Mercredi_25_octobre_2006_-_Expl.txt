Christine de Veyrac # Débats - Mercredi 25 octobre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Processus de paix en Espagne (B6-0526/2006
 et B6-0527/2006
) 
  Christine De Veyrac (PPE-DE
), 
par écrit
. - Sans préjuger du fond et du bien-fondé de la démarche du gouvernement de José-Luis Rodriguez Zapatero, les institutions européennes n’ont pas vocation à prendre position sur une affaire relevant par essence - le statut et l’avenir d’une province - de la politique intérieure d’un État membre. Ceci est particulièrement vrai lorsqu’un débat interne oppose avec autant d’intensité une majorité et une opposition politiques. La résolution commune, parce qu’elle demande au Conseil et à la Commission de prendre les mesures appropriées, comporte un risque évident de dérive. La logique de certains des groupes politiques signataires de la résolution, qui ont pour ambition d’internationaliser le conflit, est dangereuse et contre-productive. Laissons les Espagnols gérer et régler entre eux ce conflit.
Dans ces conditions, une résolution alternative du PPE était pleinement justifiée. J’aurais souhaité que l’argumentation se situe cependant sur le plan d’un rejet de principe de l’examen d’une affaire intérieure d’un État membre. C’est la raison pour laquelle je me suis abstenue sur les deux résolutions. 
