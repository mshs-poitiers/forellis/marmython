Philippe Morillon # Débats - Mardi 22 février 2005 - Demande d’urgence
  Morillon (ALDE
). 
 - Monsieur le Président, c’est le président de la commission de la pêche qui va vous dire combien nous apprécions cette demande de procédure d’urgence. Sur la forme, nous avons suivi les engagements du commissaire Borg dès la catastrophe et nous l’avons appuyé. Sur le fond, nous aurons, à 17h30, une réunion exceptionnelle de la commission de la pêche pour décider si telle ou telle mesure, comme l’a dit notre collègue Swoboda, va dans le sens des intérêts de la réunion. Mais sur la forme, et je crois traduire une majorité de l’opinion de mes collègues, je suis tout à fait favorable à la procédure d’urgence. 
