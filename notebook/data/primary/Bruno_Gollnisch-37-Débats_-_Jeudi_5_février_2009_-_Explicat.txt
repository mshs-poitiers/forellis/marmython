Bruno Gollnisch # Débats - Jeudi 5 février 2009 - Explications de vote 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Relazione: Martine Roure (A6-0024/2009
)
  Bruno Gollnisch  (NI
), 
par écrit
. – 
Monsieur le Président, mes chers collègues,
Madame Roure demande des conditions d'accueil particulièrement engageantes pour les demandeurs d'asile : lieux d'hébergement conviviaux, ouverts - c'est plus facile pour disparaître dans la nature -, large accès aux soins y compris psychiatriques, à une assistance juridique, à des traducteurs ou des interprètes, à une formation et même à un emploi !
Elle semble oublier l'abus fait par les immigrants eux-mêmes des demandes de protection internationale pour contourner les lois nationales sur l'entrée et le séjour des étrangers dans nos pays, alors que leurs véritables motivations sont sociales et économiques. Elle « oublie » aussi qu'ils peuvent mentir sur leur origine, leur langue, détruire leurs papiers, etc... pour ne pas être expulsés.
Elle semble également « oublier » que ce qu'elle réclame pour ces étrangers n'est souvent pas accessible à nos propres concitoyens, à commencer par un logement décent, un travail et un accès à des services publics de qualité, notamment dans des zones où, comme à Mayotte, la submersion migratoire crée d'énormes problèmes économiques et sociaux aux habitants.
Je peux comprendre la détresse et les rêves des migrants. Mais nous n'avons ni la vocation, et encore moins les moyens, d'accueillir toute la misère du monde. Ce rapport est néfaste, et ses effets seront pervers.
