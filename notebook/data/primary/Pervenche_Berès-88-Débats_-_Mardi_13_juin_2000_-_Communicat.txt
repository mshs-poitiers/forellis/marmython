Pervenche Berès # Débats - Mardi 13 juin 2000 - Communication de la Présidente
  Berès (PSE
). 
 - Madame la Présidente, j'ai lu avec attention votre déclaration �  la suite de l'intervention du ministre de l'Intérieur français. Je crois que la question de la nation est une notion trop complexe pour pouvoir être abordée comme cela, dans un hémicycle comme le nôtre, par le biais d'un rappel au règlement.
C'est un débat profond, qui anime l'ensemble des États membres de l'Union européenne, qu'on ne peut pas balayer d'un revers de la main. Je crois que l'intervention du ministre de l'Intérieur français a été très bien comprise en Allemagne et que la sagesse de la réaction des responsables de ce pays doit aussi être la nôtre. 
