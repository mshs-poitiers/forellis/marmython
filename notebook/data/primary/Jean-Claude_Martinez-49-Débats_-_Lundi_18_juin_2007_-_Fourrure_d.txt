Jean-Claude Martinez # Débats - Lundi 18 juin 2007 - Fourrure de chat et de chien (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Claude Martinez (ITS
). 
 - Madame la Présidente, nous revenons sur l’importation de peaux de 2 millions de chiens et de chats chinois abattus, de la même façon que, dans les années 1990, nous étions revenus trois fois sur l’importation des fourrures d’animaux du Grand Nord capturés par des pièges à mâchoires. Chaque fois, que ce soit pour les phoques, pour les chiens, pour les chats, pour les renards - pardonnez-moi de vous le dire -, la Commission a gagné du temps et a laissé le trafic se perpétuer au nom de la religion du marché.
N’oublions jamais qu’en mars 1996, même avec la preuve que la maladie de la vache folle était transmissible à l’homme, la Commission avait menacé de sanctions quatre Länder allemands qui refusaient d’importer des bovins suspects. La leçon que l’on peut tirer de tout cela, c’est qu’il y a une logique du marché qui veut que tout soit objet de vente, d’achat, de profit: les chiens, les chats, les reins, les yeux, les femmes, les enfants, les esclaves, les travailleurs... Voilà pourquoi la politique doit commander à l’économie: pour rétablir ces grandes valeurs qui n’ont pas de prix. 
