Carl Lang # Débats - Jeudi 9 octobre 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Rapport: Romagnoli (A6-0360/2008
) 
  Carl Lang  (NI
), 
par écrit
. – 
Le rapport de notre collègue constitue une réelle amélioration pour l’organisation et le contenu des échanges d’informations extraites du casier judiciaire entre États membres.
Il ne fait en effet de doute pour personne que des individus comme Fourniret, ignoble pédophile ayant pu sévir en France, puis en Belgique car son casier judiciaire n’avait pas été transmis d’un pays à l’autre, puissent encore exister. Ces monstres, assassins, voyous ou voleurs n’ont pas à s’abriter derrière un manque de transparence dans les différentes bases de données nationales pour échapper à la justice.
Pour ces raisons impérieuses, nous sommes favorables à la création du système européen d’information sur les casiers judiciaires dans la mesure où celui-ci garantira les libertés fondamentales propres à chacun. Je pense notamment à la nécessaire protection de la liberté d’expression et à la pénalisation idéologique du délit d’opinion dès qu’il s’agit de s’écarter des dogmes européistes et de la dictature du politiquement correct.
L’interconnexion électronique des casiers judiciaires nécessitera par ailleurs de prendre des précautions indispensables comme la garantie de l’intégrité et de l’authenticité des informations transmises ou leurs mises à jour. Nous sommes aujourd’hui dans la phase d’ébauche de ce mécanisme: restons donc en alerte et prudents. 
