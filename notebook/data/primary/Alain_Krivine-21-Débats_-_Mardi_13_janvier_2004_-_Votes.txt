Alain Krivine # Débats - Mardi 13 janvier 2004 - Votes
  Krivine et Vachetta (GUE/NGL
),
par écrit
. 
- La prolifération de déchets radioactifs constitue un problème majeur pour l’environnement et la santé publique. Or, des pays comme la France relancent leur programme de construction de centrales nucléaires qui présentent les mêmes défauts que les anciennes sur ce plan. Dans le même temps, la logique de libéralisation est porteuse de dangers sociaux autant qu’écologiques, notamment par le recours de plus en plus massif à la sous-traitance pour la maintenance des installations nucléaires.
Dans ce contexte, le rapport Vidal-Quadras Roca tel qu’amendé prétend certes contribuer à un meilleur contrôle des déchets nucléaires, mais il présente de graves carences sur deux points: il maintient la possibilité pour un État d’exporter ses déchets dans un pays tiers (même si, et c’est un mieux, il prévoit "le consentement préalable informé de cet État"), et il favorise l’enfouissement profond des déchets radioactifs, qui est fortement contesté par les populations environnantes et par de nombreux scientifiques.
C’est pourquoi nous avons voté contre ce rapport. La filière nucléaire confirme son coût prohibitif et bénéficie de conditions scandaleusement avantageuses par rapport aux filières de production d’énergie "propre". Il faut inverser cette logique. Nous nous associons ainsi à la manifestation unitaire pour sortir du nucléaire qui aura lieu samedi 17 janvier à Paris. 
