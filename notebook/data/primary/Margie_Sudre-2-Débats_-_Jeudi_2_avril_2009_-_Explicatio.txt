Margie Sudre # Débats - Jeudi 2 avril 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
– Rapport: Kathalijne Buitenweg (A6-0149/2009
) 
  Margie Sudre  (PPE-DE
), 
par écrit
. –
 La défense des droits et la protection des personnes victimes de discriminations doit être une priorité pour l’UE, mais elle ne peut être efficace et utile que si elle assure une sécurité juridique pour les personnes concernées, en évitant une charge disproportionnée pour les acteurs économiques visés.
Dans ce domaine, il est primordial de rester vigilant quant au respect de la répartition des compétences entre l’Union européenne et les États membres et de veiller à ce que le Parlement s’en tienne strictement à ce que permet la base légale.
Le texte adopté aujourd’hui apporte satisfaction à certains égards, notamment en ce qui concerne la lutte contre les discriminations vis-à-vis des personnes handicapées, mais les notions floues qu’il contient, les incertitudes juridiques qu’il maintient, les exigences qu’il introduit le rendent juridiquement impraticable et donc inefficace dans son application.
La surréglementation ne pouvant être une solution, la délégation a défendu l’amendement de rejet de la proposition de la Commission dans la mesure où les textes existants ne sont déjà pas appliqués par certains États membres qui font l’objet de procédures d’infraction.
De fait, la délégation française, solidaire de l’objectif de cette directive mais partiellement insatisfaite, a préféré s’abstenir lors du vote final. 
