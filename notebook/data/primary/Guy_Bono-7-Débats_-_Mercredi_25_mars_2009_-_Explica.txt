Guy Bono # Débats - Mercredi 25 mars 2009 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Sarah Ludford (A6-0143/2009
) 
  Guy Bono  (PSE
), 
par écrit
. – 
J’ai voté en faveur de cette recommandation, présentée par la députée britannique démocrate Sarah Ludford, concernant les instructions consulaires communes: éléments d’identification biométriques et demandes de visa.
Cet accord en deuxième lecture permet de confirmer la volonté d’introduire des données biométriques dans le système européen d’information sur les visas (VIS). Grâce aux instructions consulaires communes, on a enfin l’assurance que l’ensemble des États membres délivrent des visas aux ressortissants de près d’une centaine de pays, sur la base de critères et de caractéristiques semblables.
Ce texte a donc le mérite d’introduire des mesures fondamentales pour la protection des citoyens, ainsi que des dispositions qui assurent le respect de la vie privée et des données personnelles des ressortissants des États tiers.
