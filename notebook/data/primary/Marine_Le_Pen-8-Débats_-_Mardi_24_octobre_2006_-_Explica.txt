Marine Le Pen # Débats - Mardi 24 octobre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Kratsa-Tsagaropoulou (A6-0307/2006
) 
  Marine Le Pen (NI
), 
par écrit
. - Le délire est au rendez-vous dans ce rapport, tous les fantasmes de la gauche et des verts y sont réunis. Quelques exemples parmi tant d’autres: au point 7, les États membres sont invités à accorder aux femmes migrantes en situation irrégulière dont les enfants sont scolarisés le droit aux prestations et allocations familiales. Au point 10, c’est la simplification des procédures d’octroi des permis de séjour qui est souhaitée. Enfin, au point 17, il s’agit de faire participer plus activement les femmes immigrées à la vie sociale et politique des pays d’accueil.
Décidément, tout est fait pour transformer comme par magie ce qui dérange, l’immigration illégale, en ce qui est bien, moral et humaniste: l’immigration légale.
Pas un mot sur la mise en place de politiques de retour au pays d’origine de ces clandestins, pas un mot non plus sur la possibilité de mise en œuvre d’une politique cohérente et efficace d’aide au codéveloppement dans ces pays afin que cesse l’hémorragie croissante en provenance notamment des pays d’Afrique.
Rien, juste la volonté affirmée du début à la fin de ce rapport d’accueillir toujours plus d’immigrés et de leur donner toujours plus de droits.
Des rapports comme celui-ci ne contribuent pas à construire l’Europe: ils la tuent. 
