Jean-Claude Martinez # Débats - Jeudi 1 avril 2004 - Votes
  Martinez (NI
),
par écrit
. 
- À l’injustice faite à nos pêcheurs s’ajoute l’injustice faite à nos ostréiculteurs et conchyliculteurs du bassin de Thau ou de l’étang de Barcarès dans l’Aude. Ils supportent en effet les coûts de la pollution massive provoquée durant l’été par les milliers et milliers de camping-cars des touristes de l’Europe du Nord qui, chaque jour, vidangent leurs eaux usées, phosphatées et même chargées de bactéries pathogènes dans nos étangs languedociens et catalans. Pour la Méditerranée, pour nos pêcheurs, ostréiculteurs et conchyliculteurs, l’Europe c’est la restriction et la pollution sans indemnisation. 
