Marine Le Pen # Débats - Mardi 14 février 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Rapport Agnoletto (A6-0004/2006
) 
  Marine Le Pen (NI
), 
par écrit
. - Exiger de nos partenaires économiques et politiques le respect des droits de l’homme part d’un excellent sentiment. Cependant les institutions européennes devraient d’abord balayer devant leur porte. D’autant plus qu’à cette porte se trouve la Turquie.
Les discriminations dont sont victimes les minorités chrétiennes, l’information sur le génocide arménien censurée, l’occupation par l’armée turque du tiers du territoire chypriote, autant de violations flagrantes du droit qui n’ont pas empêché les dirigeants européens, notamment en France Chirac et Villepin, d’ouvrir les portes de l’Europe à ce pays asiatique.
La perspective d’une adhésion incite-t-elle les autorités turques à partager les valeurs de notre civilisation? On peut en douter. L’arrivée au pouvoir en 2002 des islamistes de l’AKP menace en particulier les quelques droits concédés auparavant aux femmes. Deux faits illustrent ce phénomène: la violence avec laquelle l’année dernière une manifestation pacifique de femmes a été réprimée et le retour de la polygamie.
Au moment où dans nos pays mêmes, des associations islamistes remettent en cause la liberté de la presse, l’Union européenne, en accueillant la Turquie, renierait les valeurs sur lesquelles elle prétend fonder sa politique étrangère. 
