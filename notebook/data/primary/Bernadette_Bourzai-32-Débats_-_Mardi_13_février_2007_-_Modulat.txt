Bernadette Bourzai # Débats - Mardi 13 février 2007 - Modulation facultative des paiements directs dans le cadre de la PAC (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bernadette Bourzai, 
au nom du groupe PSE
. - Madame la Présidente, nous avons rejeté, en novembre dernier, à une très large majorité, la proposition de règlement sur la modulation volontaire.
La Commission européenne n’ayant pas modifié son texte, les conditions d’un second rejet de ce texte sont encore réunies; je ne les répéterai pas, mais chacun les connaît: les coupes qui ont été effectuées dans le deuxième pilier, le non-cofinancement, le fait que ce cofinancement introduise des distorsions de concurrence entre les États et le déséquilibre que cela crée dans la structure de la politique agricole commune qui, je le rappelle, est la seule politique européenne commune et, qui, de ce fait, risque d’être renationalisée.
Pour autant, les besoins de financement de la politique de développement rural sont réels et je crains vraiment un accroissement de la désertification de nos zones rurales, si rien n’est fait en faveur de la modernisation des structures agricoles, du renouvellement des générations d’agriculteurs, de la qualité de la vie et de l’environnement, la diversification économique de nos campagnes.
C’est pourquoi je vous demande, Madame la Commissaire, de proposer à la place de la modulation facultative, une augmentation du taux de modulation obligatoire qui soit identique dans tous les États membres. Je voudrais également souligner que la modulation obligatoire s’applique pour l’instant dès qu’une exploitation reçoit plus de 5 000 euros d’aides par an, c’est-à-dire la grande majorité des exploitations agricoles.
Pour avoir un véritable outil de redistribution des aides agricoles, il faudrait prendre aussi en compte d’autres critères, comme la taille de l’exploitation, sa dépendance aux aides, la main-d’œuvre employée, la marge brute, etc. Au-delà, on pourrait d’ailleurs envisager un plafonnement des aides directes pour une meilleure répartition.
Malheureusement, même si le Parlement européen rejette une seconde fois massivement cette proposition, ce qui constitue normalement un acte législatif important, il ne s’agit que d’un avis, et je partage totalement celui de M. Goepel. Il me paraît donc nécessaire de maintenir encore la pression sur la Commission et le Conseil en conservant, pour l’instant, la réserve budgétaire de 20% des fonds de développement rural pour l’année 2007. 
