Jean-Marie Cavada # Débats - Mardi 12 décembre 2006 - Coordination de certaines dispositions des États membres relatives à la radiodiffusion télévisuelle (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Marie Cavada (ALDE
), 
rapporteur pour avis de la commission des libertés civiles, de la justice et des affaires intérieures
. - Madame la Présidente, une minute, c’est un spot de pub! Bon, on va quand même essayer d’en tirer quelque chose.
Parlons politique d’abord. Je veux remercier Mme la Commissaire. Je me souviens de Liverpool: le texte que vous nous aviez présenté constituait déjà un bon moyen terme. Je veux remercier Mme Hieronymi dont les qualités de négociation en tant que rapporteur ont permis l’intégration dans le rapport présenté en plénière de l’essentiel des amendements de la commission des libertés civiles, de la justice et des affaires intérieures.
J’ai deux restrictions à formuler. Je voudrais, d’abord, me faire le porte-voix de quelques délégations pour regretter que les compromis qui ont été approuvés en commission de la culture, lesquels dégagent un juste équilibre entre les besoins de financement des radiodiffuseurs, d’une part, et le respect des téléspectateurs, d’autre part, soient aujourd’hui remis en cause. C’est une très mauvaise idée, nuisible à l’ensemble de l’audiovisuel, parce qu’une industrie qui ne respecte pas ses consommateurs se condamne à terme. Concernant, donc, les règles d’insertion publicitaire, je crois qu’il est indispensable de respecter l’intégralité des œuvres culturelles et, en ce qui me concerne, jusqu’à plus ample informé, je m’en tiendrai au respect de tranches de quarante-cinq minutes, que l’on ne peut pas couper.
Pour ce qui est du placement de produits, la commission de la culture a su baliser très strictement l’affaire: je soutiens donc son amendement.
Enfin, Madame la Présidente, je regrette que ce rapport et les conditions politiques dans lesquelles il est présenté ne permettent pas de réfléchir à moyen terme sur l’équilibre entre l’audiovisuel public et l’audiovisuel privé: il faudra qu’on s’y attaque ensemble, à moyen terme, si on veut assurer la survie de ce rapport
