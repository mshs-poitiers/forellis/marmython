Carl Lang # Débats - Jeudi 13 décembre 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Proposition de résolution (B6-0495/2007
) 
  Carl Lang  (NI
), 
par écrit
. – (FR) 
Le secteur textile en France et en Europe depuis la fin de l'Accord multifibres a fait de certaines de nos régions des déserts économiques et sociaux. Il y règne désormais précarité et pauvreté pour des milliers de femmes et d'hommes ayant perdu leur emploi.
La destruction de ces entreprises, de ce tissu social, au nom de la mondialisation et de l'ultralibéralisme européiste, est le symbole d'un des plus grands échecs économiques de l'Union européenne.
Depuis des années, cette logique provoque des délocalisations, dans tous les autres secteurs économiques, de nos productions, même d'excellence, vers d'autres pays du monde: en Afrique du Nord et surtout en Asie. Ce rééquilibrage mondial n'aura en réalité rien apporté aux pays tiers, si ce n'est l'aggravation de l'esclavagisme économique au profit d'une petite élite au sein de l'usine Chine et l'installation durable du chômage en Europe sur fond de crise économique persistante.
Il est évident que la concurrence mondiale encouragée par l'OMC est la raison majeure de l'appauvrissement généralisé et du manque de dynamisme européen. Il est urgent que l'Union européenne arrête cette folie, pour mettre enfin en place protection et préférence communautaires. 
