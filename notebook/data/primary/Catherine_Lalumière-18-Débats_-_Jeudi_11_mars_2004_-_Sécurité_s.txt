Catherine Lalumière # Débats - Jeudi 11 mars 2004 - Sécurité sociale applicable aux travailleurs et à leur famille se déplaçant à l’intérieur de la Communauté
  La Présidente. -  
L’ordre du jour appelle le rapport (A5-0058/2004
) de Mme Gillig, au nom de la commission de l’emploi et des affaires sociales, sur la proposition de règlement du Parlement européen et du Conseil modifiant le règlement (CEE) 1408/71 du Conseil relatif à l’application des régimes de sécurité sociale aux travailleurs salariés, aux travailleurs non salariés et aux membres de leur famille qui se déplacent à l’intérieur de la Communauté, et le règlement (CEE) 574/72 du Conseil fixant les modalités d’application du règlement (CEE) 1408/71.
La parole est à M. Solbes Mira, au nom de la Commission.
Permettez-moi, Monsieur le Commissaire, en raison notamment de votre nationalité, de vous exprimer ma très profonde sympathie. 
