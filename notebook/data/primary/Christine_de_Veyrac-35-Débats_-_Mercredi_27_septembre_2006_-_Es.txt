Christine de Veyrac # Débats - Mercredi 27 septembre 2006 - Espace de liberté, de sécurité et de justice - Politique commune d’immigration (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Christine De Veyrac (PPE-DE
). 
 - Monsieur le Président, nous le constatons chaque jour davantage, une politique commune d’immigration au niveau européen est devenue indispensable. À ceux qui en doutaient, les régularisations massives d’étrangers sans papiers dans certains pays européens ont prouvé qu’elles entrainaient des arrivées massives de nouveaux migrants illégaux sur notre continent. Cette situation a des répercussions sur tous les pays de l’Union européenne puisque, chacun le sait, le migrant régularisé peut circuler librement sur une grande partie du territoire de l’Union.
Nous avons eu à travers l’exemple espagnol la démonstration que, dans l’espace sans frontières où nous vivons, un gouvernement ne peut plus décider seul, sans concertation avec ses partenaires, de régulariser tous les immigrés en situation irrégulière présents sur son territoire. Une politique commune et concertée est devenue indispensable, comme d’ailleurs le prévoyait le projet de constitution européenne qui transférait l’essentiel de cette matière vers la majorité qualifiée.
Je remarque que, malgré les efforts de la Commission, certains États semblent faire marche arrière en refusant aujourd’hui ce qu’ils avaient accepté hier. C’est regrettable. Le maintien de l’unanimité en ce domaine est un facteur de paralysie et d’inefficacité. Le sommet informel de l’Union européenne le 20 octobre prochain doit décider de mesures fortes et concrètes pour enrayer l’immigration clandestine. Il ne doit pas se contenter de grandes déclarations de bonnes intentions, comme c’est trop souvent le cas.
Enrayer l’immigration clandestine, oui, mais le problème doit être pris à la racine. Il doit être examiné en concertation avec les pays d’Afrique, dans le cadre d’une vraie stratégie de codéveloppement, à travers une véritable circulation des compétences entre pays d’immigration et pays d’émigration. Mais surtout, l’Union européenne doit amplifier et mieux contrôler sa politique d’aide au développement, savoir où vont les fonds, comment ils sont employés.
Enfin, je n’oublie pas le très court terme et les problèmes immédiats et je voudrais regretter, comme l’a fait Gérard Deprez, le comportement des États qui n’ont de cesse de parler d’entraide et de solidarité, sans jamais pourtant passer de la parole aux actes. Face à l’afflux d’immigrés illégaux aux Canaries, il est de notre devoir de venir en aide à l’Espagne et c’est aux États membres d’être au rendez-vous de l’action et de la solidarité. 
