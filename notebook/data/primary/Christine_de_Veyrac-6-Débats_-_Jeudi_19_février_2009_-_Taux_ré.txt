Christine de Veyrac # Débats - Jeudi 19 février 2009 - Taux réduits de taxe sur la valeur ajoutée (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Christine De Veyrac  (PPE-DE
), 
par écrit. 
–

 Mesdames et Messieurs, je me réjouis que nous abordions enfin dans cet hémicycle la question de l’élargissement de l’assiette du taux réduit de TVA des États-membres à toute une série de biens et services.
Le texte que la Commission européenne a présenté le 28 janvier 2009 propose d’appliquer un taux réduit de TVA à plusieurs catégories d’activités, notamment la restauration.
C’est une mesure que je souhaite et que j’appelle de mes vœux depuis plusieurs années. Après des années d’immobilisme, je souhaite que les États membres s’accordent enfin sur cette mesure.
La proposition de la Commission européenne intervient à la suite de la déclaration allemande du 20 janvier dernier, qui indiquait qu’elle ne s’opposait plus à l’instauration d’un taux réduit dans ce secteur. Cette simultanéité laisse espérer des changements importants.
En ces temps de crise, je pense qu’une telle mesure, si elle se concrétisait, permettrait d’augmenter les salaires des employés de ce secteur, et donnerait l’opportunité aux restaurateurs de recruter davantage de main-d’œuvre.
Le texte que nous votons aujourd’hui doit donner un signal fort dans ce sens.
Je vous remercie de votre attention. 
