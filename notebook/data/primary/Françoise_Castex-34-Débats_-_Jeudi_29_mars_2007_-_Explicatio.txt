Françoise Castex # Débats - Jeudi 29 mars 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Aubert (A6-0061/2007

) 
  Françoise Castex (PSE
), 
par écrit
. - J’ai voté en faveur du renvoi du rapport Aubert relatif à la production biologique et à l’étiquetage des produits biologiques en commission de l’agriculture.
Il est nécessaire que le seuil de contamination accidentelle aux OGM ne soit pas identique à celui de l’agriculture conventionnelle, c’est-à-dire de l’ordre de 0,9%, car ainsi, on admettrait de facto que l’on ne peut plus empêcher les contaminations, ni garantir qu’un produit même certifié bio est exempt d’OGM.
À cet égard, j’ai soutenu la proposition émanant des socialistes européens qui postulait que la présence d’OGM dans des produits biologiques devait être limitée exclusivement aux volumes imprévisibles et techniquement inévitables à concurrence d’une valeur maximale de 0,1% et que le terme bio ne devait pas être utilisé pour désigner les produits dont la contamination accidentelle par les OGM est supérieure au seuil détectable de 0,1%.
Enfin, je soutiens la demande d’un changement de base juridique sur cette question de l’agriculture biologique. De «consultant», le Parlement Européen entend devenir «codécideur» sur cette problématique ce qui constitue une avancée en la matière. 
