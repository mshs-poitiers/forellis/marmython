Bernadette Bourzai # Débats - Lundi 21 mai 2007 - Normes de qualité environnementale dans le domaine de l’eau (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bernadette Bourzai (PSE
), 
rapporteur pour avis de la commission de l’agriculture et du développement rural
. - Madame la Présidente, Monsieur le Commissaire, chers collègues, j’interviens en tant que rapporteur pour avis de la commission de l’agriculture et du développement rural sur cette proposition de directive-fille découlant de la directive-cadre sur l’eau.
Je tiens d’abord à féliciter Mme Laperrouze pour l’excellent travail qu’elle a effectué au sujet de ce dossier complexe et très technique. Je suis particulièrement satisfaite du vote intervenu en commission de l’environnement, car douze des vingt et un amendements proposés par la commission de l’agriculture et du développement rural ont été repris.
Brièvement, il s’agissait pour notre commission: premièrement, de rappeler les principes de précaution, d’action préventive et du pollueur-payeur; deuxièmement, de souligner la nécessité d’une exploitation rationnelle des terres dans le cadre d’une agriculture écologique; troisièmement, de définir les mesures complémentaires nationales et communautaires à mettre en œuvre, telles que l’encadrement d’autres polluants et les programmes spécifiques de surveillance pour les sédiments et les biotes; quatrièmement, de souligner la nécessité d’une évaluation formelle de la cohérence et de l’efficacité des différents actes communautaires sur la qualité de l’eau; cinquièmement, d’appeler à une coordination des programmes de veille et des inventaires nationaux lorsqu’un cours d’eau traverse plusieurs États membres; sixièmement, enfin, de démontrer la nécessité pour les États membres d’accompagner leur inventaire d’un calendrier de mise en œuvre des objectifs de diminution, voire d’arrêt, des émissions. 
