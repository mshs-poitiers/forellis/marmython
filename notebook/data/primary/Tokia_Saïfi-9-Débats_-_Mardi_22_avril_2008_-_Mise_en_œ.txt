Tokia Saïfi # Débats - Mardi 22 avril 2008 - Mise en œuvre de la programmation du 10e Fonds européen de développement (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Tokia Saïfi  (PPE-DE
), 
par écrit
. − (FR)
 Le 10e FED est le cadre pluriannuel de coopération et développement de l'UE avec les pays ACP. Il s'élève à 22,7 milliards d'euros pour la période 2008-2013 et a vocation à aider certains des pays les plus pauvres et vulnérables du monde. Pour que ce fonds, bras financier de la politique européenne de développement, soit efficace, il faut aller au delà de la contribution asymétrique des États membres et l'intégrer dans le budget général de la Communauté. La budgétisation de l'aide européenne présenterait en effet des avantages certains puisque l'aide aux ACP serait soumise aux même règles de programmation et de gestion que les autres instruments d'action extérieure de l'UE, renforçant ainsi la cohérence, la transparence, l'efficacité et le contrôle démocratique de la coopération au développement. Il faut dépasser la notion de "liens historiques" qui reliaient certains États membres avec les pays et territoires d'outre-mer et faire de l'Afrique et du développement une priorité de l'UE dans son ensemble. Si l'UE est soucieuse de l'efficience de ces politiques et programmes, alors elle doit poursuivre la modernisation de son aide extérieure en intégrant, dès 2009, le FED au budget communautaire. 
