Hélène Flautre # Débats - Mercredi 24 octobre 2007 - Sommet UE-Russie (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Hélène Flautre  (Verts/ALE
).  
– Madame la Présidente, chers collègues, soyons lucides! La décision que nous avons prise hier sera, elle est déjà interprétée par les autorités russes comme un grand succès: pas de résolution, pas de message. C'est une vraie récompense après le refus d'une délégation officielle "droits de l'homme" du Parlement européen. La société russe demeure plus que jamais otage d'une presse propagandiste et s'enferme dans un nationalisme dangereux.
Aujourd'hui, en Russie, la violence basée sur la peur fait loi en même temps que le racisme et la xénophobie font recette. Le mot "indépendant" n'a plus droit de cité. Défenseurs des droits de l'homme, journalistes ou opposants politiques "indépendants" sont étiquetés automatiquement "ennemis du régime". Adoptée en 2007, une loi leur est d'ailleurs officieusement consacrée. Sous couvert de lutte contre l'extrémisme, elle agit en fait pour donner aux autorités toute latitude dans ce combat inégal. Et comme nous l'expliquait hier Marie Mendras, cet état de violation permanente des droits de l'homme, finalement, ne mobilise pas les foules là-bas. Poutine a ainsi réussi son pari: persuader ses concitoyens qu'il existe une spécificité russe en matière de démocratie et des droits de l'homme.
Avec une telle conception, il n'y a rien d'étonnant à ce que la Russie soit encore le dernier État membre du Conseil de l'Europe à ne pas avoir ratifié le protocole 14. L'engagement de la Cour pour Poutine, c'est une aubaine, une bénédiction. Ça lui permet le non-suivi de l'application des arrêts, comme de laisser patienter les requêtes, notamment celles des torturés tchétchènes. Face à ce constat, il serait tout de même bien naïf de penser que le 2 décembre prochain verra se dérouler en Russie des élections libres et transparentes. Devant une telle parodie, est-ce qu'on n'est pas plutôt en droit de parler d'un plébiscite pour ou contre Vladimir Poutine? Sans modifier la constitution, il parviendra à garder entre ses mains le pouvoir politique, économique, financier, administratif, judiciaire, de sécurité.
À la veille du sommet UE-Russie, je vous appelle, Conseil et Commission, à inscrire les droits de l'homme au plus haut niveau politique. Il est impératif qu'ils occupent une place centrale, qu'il s'agisse de discuter du futur accord ou du Kosovo ou de l'énergie. C'est ce que nous demandent les démocrates russes, ils nous disent simplement: "Continuez à parler d'eux, continuez à dire la vérité". Nous, du moins, ce faisant, nous ne risquons pas notre peau. 
