Nathalie Griesbeck # Débats - Mardi 14 mars 2006 - Orientations pour la procédure budgétaire 2007 (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Nathalie Griesbeck (ALDE
). 
 - Monsieur le Président, Madame la Commissaire, chers collègues, à mon tour de m’associer tout d’abord au travail de notre collègue Louis Grech et à la très grande qualité de son rapport.
Dans le contexte actuel et avec les difficultés que nous rencontrons dans le cadre des négociations avec le Conseil sur les perspectives financières 2007-2013, ce rapport vient à point nommé faire le point sur les moyens financiers qu’il conviendra de mettre en œuvre en 2007 pour le financement des institutions majeures de notre Union: Parlement, Conseil, Cour de justice, etc.
Je souhaite m’associer ce soir aux demandes portant sur les principes de bonne gestion et, aussi, de recherche de valeur ajoutée, mais souhaiterais que nous élargissions ces principes à l’ensemble des institutions et, bien sûr aussi, tout particulièrement aux agences de l’Union européenne.
Nous devons faire davantage encore d’efforts dans l’optimisation des moyens de travail, de la gestion de l’outil informatique, des coûts liés à la transmission des données et, enfin, de notre politique des ressources humaines.
À mon tour, je voudrais insister sur notre politique de communication: il faut qu’elle permette un véritable accès de nos concitoyens non seulement à l’information, comme on l’expliquait à l’instant, mais aussi à toutes les expressions de l’Union. En effet, les citoyens de l’Union doivent pouvoir véritablement non seulement comprendre les décisions que nous prenons en leur nom, mais aussi s’approprier ce magnifique projet de société, que nous bâtissons pour eux, et je l’espère, avec eux.
Nous devons, en ce sens, redoubler nos efforts de communication, affecter des moyens conformes à la mise en œuvre d’une politique de communication qui soit moderne, efficace, pédagogique, en un mot, qui soit adaptée à notre temps, à travers notamment des outils tels que la web tv.

Dans le même sens, il me semble inévitable de souligner qu’il importe de renforcer aussi l’accueil des visiteurs et de la presse sur les divers lieux de session. Nous manquons indéniablement, dans la perspective de ce futur élargissement, d’infrastructures suffisantes. Il m’arrive très souvent de recevoir des délégations dans les couloirs du Parlement. 
