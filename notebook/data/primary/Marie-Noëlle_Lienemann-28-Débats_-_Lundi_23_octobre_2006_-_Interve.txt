Marie-Noëlle Lienemann # Débats - Lundi 23 octobre 2006 - Interventions d’une minute sur des questions politiques importantes
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie-Noëlle Lienemann (PSE
). 
 - Monsieur le Président, chers collègues, EADS et les projets industriels que ce groupe porte, parmi lesquels Airbus, en font un des grands fleurons de l’Europe, un fleuron industriel, symbole de notre technologie et de nos capacités pour l’avenir. Or, cette entreprise est en grande difficulté. Je n’insisterai pas sur les raisons, notamment liées à sa gestion, qui ont pu justifier ou expliquer une partie de ses difficultés actuelles, lesquelles requièrent des remèdes efficaces.
En revanche, j’insisterai sur les déclarations de la direction d’Airbus, qui nous explique que, pour être compétitive, l’entreprise devra probablement délocaliser ou installer un certain nombre de ses activités dans la zone monétaire dite «dollar», pour cette seule raison que le taux de change euro/dollar est défavorable à l’un des secteurs de pointe les plus importants pour l’Union européenne. Interrogé sur ce point, M. Trichet n’a qu’une seule réponse: «Je n’ai pas à m’expliquer sur le taux de change!».
Ne croyez-vous pas, chers collègues, Monsieur le Président, qu’il est urgent de changer la politique monétaire de l’Europe?
(Le Président retire la parole à l’orateur)

