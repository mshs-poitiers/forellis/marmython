Marie-Anne Isler-Béguin # Débats - Lundi 11 décembre 2006 - Agence européenne des produits chimiques - Modification de la directive 67/548/CEE sur les substances dangereuses (REACH) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie Anne Isler Béguin (Verts/ALE
). 
 - Monsieur le Président, avec REACH, tous ensemble, et Guido Sacconi, notre rapporteur, nous avions suscité un espoir immense: débarrasser notre environnement des substances chimiques persistantes, bio-accumulatives et toxiques qui empoisonnent notre santé et la nature. Le projet de réglementer les produits chimiques avait réveillé la conscience écologique et sociale des Européens, et même au-delà de nos frontières. Dans ce sens, un travail énorme de dialogue avec la société civile - syndicats, ONG, entreprises et industriels - aura permis de converger vers la nécessité d’améliorer la santé publique et la qualité de l’environnement et d’assurer l’information des citoyens et des travailleurs sur les produits chimiques qui nous entourent.
Hélas, malgré un message encourageant de notre commission de l’environnement, les compromis en cours affaibliront ce projet REACH. Comment expliquer à nos compatriotes que nous ne ferons pas peser la responsabilité de la dissémination des substances toxiques sur les industriels et que ce sera aux consommateurs et à l’ouvrier qui manipule des substances dangereuses d’assumer cette responsabilité? Comment expliquer que le Parlement défend la substitution des molécules cancérigènes, mutagènes et des perturbateurs endocriniens, sans en exiger l’application systématique? Et que dire du manque de transparence de l’information sur les substances les plus dangereuses? Pour nous, c’est inacceptable et incompréhensible. Alors même que nombre de PMI et PME ont intégré la valeur ajoutée d’une chimie verte, les poids lourds de l’industrie chimique européenne refusent d’évoluer. Ils continuent à influencer nos travaux, malgré le coût financier que l’augmentation des pathologies implique pour notre système de santé, ce qu’oublie d’ailleurs totalement M. Nassauer.
Aujourd’hui, notre responsabilité est grande et nous devrons assumer nos votes. Les amendements des Verts vont dans le sens d’un REACH renforcé, qui ait du sens et qui n’attende pas les calendes grecques pour la substitution des produits les plus dangereux, ce que reconnaît M. Sacconi. Pour conclure, je dirai que ce compromis est peut-être un grand pas pour l’industrie chimique, mais qu’il restera une reculade pour notre Parlement. 
