Nicole Thomas-Mauro # Débats - Mercredi 15 mars 2000 - VOTES
  Thomas-Mauro (UEN
),
par écrit
. - Étiquetage, quand tu nous tiens...
Voici un signe distinctif au niveau européen auquel nous voulons redonner ici de la vigueur. Ce label, tout comme la marque française NF Environnement, est d'application volontaire, c'est-à-dire qu'il appartient aux industriels d'en faire la demande. Il repose sur le principe d'une approche globale, qui prend en considération tout le cycle de vie du produit.
Tous les signes nationaux ne doivent cependant pas être mis en arrière plan. En effet, les consommateurs, les acheteurs ont leurs propres codes, et ont forgé leur confiance sur les logos qu'ils connaissent. La volonté de supplanter le logo national reviendrait encore une fois à troubler le citoyen habitant dans l'Union européenne, perdu entre les listes d'ingrédients, les labels qualité, les logos, les références aux régions... une forêt d'indications. On se croirait presque devant une tablette de chocolat...
Ayant à cœur de défendre les PME en matière de facilités d'accès aux éco-labels communautaires qui doivent être envisagés, nous apportons notre soutien au rapporteur. 
