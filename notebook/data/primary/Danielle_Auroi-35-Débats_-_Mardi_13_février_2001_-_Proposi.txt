Danielle Auroi # Débats - Mardi 13 février 2001 - Propositions pour la réforme du marché de la viande bovine
  Auroi (Verts/ALE
). 
 - Monsieur le Président, Monsieur le Commissaire, trois courtes questions. La première, vous avez parlé des mesures concernant l'agriculture biologique, certes, mais que pensez-vous faire en ce qui concerne tous les produits certifiés, labellisés, qui sont soumis, eux aussi, à des critères très stricts ?
Deuxièmement, concernant les veaux, que faites-vous, ou qu'entendez-vous faire, concernant l'élevage du veau sous la mère car, à l'heure actuelle, le lait tel qu'il est donné aux veaux en batterie pose aussi des problèmes que vous n'avez pas retenus.
Enfin, troisième question, qu'allez-vous faire pour éviter des mesures qui risquent de pénaliser les petits agriculteurs seulement et pas les gros ? 
