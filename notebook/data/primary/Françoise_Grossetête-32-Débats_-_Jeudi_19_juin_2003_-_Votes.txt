Françoise Grossetête # Débats - Jeudi 19 juin 2003 - Votes
  Grossetête (PPE-DE
),
par écrit
. - J’ai voté en faveur de ce rapport. Nous avons tous encore en mémoire les terribles catastrophes s’abattant sur certaines usines, notamment celle de Toulouse. Face à ces accidents, il est prématuré de vouloir remettre en cause une législation qui existe depuis 3 ans. En revanche, il convient d’améliorer les dispositifs de sécurité.
Notre priorité est de pouvoir conjuguer développement industriel et sécurité accrue. Imagine-t-on en effet une Europe sans usines? Le risque zéro n’existe pas et ce serait un mensonge d’affirmer le contraire.
L’objectif est donc de se préparer à l’inattendu. Pour cela, la notion d’information est primordiale, notamment celle sur les comportements à adopter en cas d’explosion. Ceci est fondamental pour éviter de voir dans le futur des dégâts encore plus graves.
Il ne faut pas affoler la population. Se préparer à l’inattendu, c’est aussi tout faire pour l’éviter. Aussi une évaluation juste des risques, selon des critères définis au niveau communautaire, une responsabilisation des acteurs de l’entreprise - salariés et dirigeants - avec une obligation de formation adéquate, la mise en œuvre de systèmes de gestion de sécurité avec des tests sur le terrain et le renforcement des contrôles, sont-ils le fondement des nouvelles exigences demandées.
(Explication de vote écourtée en application de l’article 137, paragraphe 1, du règlement)

