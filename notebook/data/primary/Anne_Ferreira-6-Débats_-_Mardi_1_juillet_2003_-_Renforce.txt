Anne Ferreira # Débats - Mardi 1 juillet 2003 - Renforcer l’industrie pharmaceutique européenne dans l’intérêt des patients - Propositions d’action
  Ferreira (PSE
).
-
 Messieurs les Commissaires, parmi vos nombreuses propositions, vous avez évoqué la valeur ajoutée des médicaments. Je crois que c’est un point essentiel. Actuellement arrive sur le marché un grand nombre de nouveaux médicaments qui, malheureusement, sont rarement innovants. Comment pensez-vous concrètement inciter les industries à produire des médicaments non seulement nouveaux mais aussi innovants?
Vous avez également évoqué la question du portail informatique européen. La Nouvelle-Zélande, dont je me fais le porte-parole, travaille actuellement sur un système de ce genre et se demande si nous allons y entrer par le biais des médicaments ou par celui des maladies et, dans cette dernière hypothèse, si les réponses apportées porteront sur la thérapie médicamenteuse ou si différentes thérapies de substitution seront proposées sur ces sites. 
