Fodé Sylla # Débats - Jeudi 14 juin 2001 - Situation en République centrafricaine
  Sylla (GUE/NGL
). 
 - Monsieur le Président, je n'ai pas grand-chose à ajouter, parce que M. le commissaire a répondu aux questions que je voulais poser.
Ceci dit, simplement, il est important de réinsister peut-être une dernière fois sur le fait que toutes les causes de ce qui se passe aujourd'hui dans un pays comme le Centre-Afrique sont quand même liées à ces fameuses politiques, dites d'ajustement structurel, qui sont imposées à des pays par le FMI, la Banque mondiale, et qui poussent des populations entières à vivre dans la misère et à ne pas pouvoir contrer efficacement les dictateurs à travers le monde.
Je tiens vraiment, Monsieur le Commissaire, Monsieur le Président, à ce que l'Union européenne continue à renforcer son effort pour venir en aide aux populations civiles, à la démocratie, en luttant contre ces politiques du désistement structurel, parce qu'elles sont injustes. Elles aident à renforcer les dictateurs et non pas les démocrates. 
