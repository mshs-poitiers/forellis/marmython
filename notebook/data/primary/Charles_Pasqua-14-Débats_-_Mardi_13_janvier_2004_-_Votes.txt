Charles Pasqua # Débats - Mardi 13 janvier 2004 - Votes
  Pasqua (UEN
),
par écrit
. 
- Espérons que le sursaut de ce Parlement à l’occasion du rapport Breyer ne marquera pas seulement un coup d’arrêt des visées dogmatiques des ayatollahs de l’antinucléaire, mais qu’il permettra une réflexion sereine propice à un véritable changement de cap.
En effet, seule l’énergie nucléaire - enrichie des progrès spectaculaires de la recherche rendant déjà disponible une énergie de qualité en abondance et en toute sécurité - est à même de répondre tout à la fois à la préoccupation de sauvegarde de l’environnement, à l’impératif d’indépendance énergétique et au formidable défi que représente l’essor des pays émergents.
Est-il en effet raisonnable de penser que la demande d’énergie, corollaire du développement, pourra être satisfaite par les énergies renouvelables dans le cas de pays comme l’Inde ou la Chine? Est-il responsable de condamner l’énergie nucléaire européenne à un arrêt définitif alors qu’elle seule peut contribuer, par l’expérience et l’excellence dans ce domaine, à sécuriser réellement les nouvelles unités qui, dans ces pays tiers, n’attendront pas le feu vert de l’Union pour s’implanter?
La problématique énergétique, sensible pour l’Union européenne, primordiale pour les trois quarts de la planète, ne trouvera de solution qu’au terme d’une approche dépassionnée et pragmatique, loin des chimères et fantasmes de l’intégrisme écologique. 
