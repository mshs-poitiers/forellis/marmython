Gilles Savary # Débats - Jeudi 18 janvier 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Rapport: Jarzembowski (A6-0475/2006
), Savary (A6-0480/2006
) et Sterckx (A6-0479/2006
) 
  Gilles Savary (PSE
). 
 - Monsieur le Président, je voudrais me féliciter ici que le Parlement européen ait fait preuve de beaucoup de sagesse en repoussant la libéralisation du système ferroviaire et des systèmes ferroviaires nationaux.
Je suis personnellement très favorable à l’ouverture des réseaux et je suis un des grands partisans de la construction d’une Europe du rail sans frontières, ce qui supposera effectivement la fin des monopoles nationaux. Cela dit, je considère que la façon dont la Commission européenne nous propose de libéraliser, qui se réduit à une bataille d’échéances et de dates, est inacceptable et va nous mener naturellement à la concentration de l’économie ferroviaire autour de quelques grandes compagnies, avec la disparition de nombreuses compagnies nationales, une grande bagarre sur les segments de marché les plus rentables, c’est-à-dire les lignes à grande vitesse, et probablement une très grande difficulté à financer les lignes budgétaires afférentes à l’aménagement du territoire.
Le train c’est aussi le train régional, c’est aussi le train grandes lignes, qui est souvent déficitaire, qui est financé par ce que sont aujourd’hui les grandes lignes internationales, et je suis très inquiet de voir que l’on n’a pas véritablement pris en compte cet aspect des choses. Comment financer des petites lignes ferroviaires qui sont très utiles alors que l’on va assécher le financement et les marges des compagnies ferroviaires par l’ouverture à la concurrence internationale? C’est une question à laquelle nous devons répondre. Nous avons jusqu’à 2010 et je souhaite personnellement que les financements du service public et des lignes relatives à l’aménagement du territoire figurent prochainement à l’ordre du jour de notre Assemblée. 
