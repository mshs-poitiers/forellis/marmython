Françoise Grossetête # Débats - Jeudi 5 juin 2003 - Approbation du procès-verbal de la séance précédente
  Grossetête (PPE-DE
).
-
 Monsieur le Président, dans le procès-verbal d’hier, je ne suis pas inscrite comme étant présente, alors que j’étais bien évidemment là et que j’ai participé à tous les appels nominaux. J’ai déjà fait la demande pour que ce soit rectifié. Je vous remercie. 
