Marie-Noëlle Lienemann # Débats - Mercredi 27 septembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Rapkay (A6-0275/2006
) 
  Marie-Noëlle Lienemann (PSE
), 
par écrit
. - J’ai voté contre le rapport Rapkay qui ne reconnaît pas la nécessité d’une directive-cadre pour les SIG et SIEG, laissant la porte ouverte à la poursuite de la dérégulation en cours, qui remet en cause nos services publics, ainsi que l’accès de tous et partout à ces services essentiels.
Une directive-cadre devrait garantir la péréquation entre les usagers, l’égalité des citoyens et des territoires, l’aménagement du territoire et la pérennité, dans la durée, des services rendus, ainsi qu’un niveau de qualité.
Il est urgent de mettre un coup d’arrêt à cette libéralisation généralisée qui ne donne satisfaction ni aux salariés de ces activités ni aux citoyens. 
