Marie-Anne Isler-Béguin # Débats - Jeudi 13 mars 2008 - Arménie
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie Anne Isler Béguin, 
auteur
. − 
(FR) 
Monsieur le Président, Monsieur le Commissaire, chers collègues, la tragédie qui s’est produite en Arménie à la suite des élections présidentielles du 19 février 2008 serait-elle l’aveu de l’impuissance de l’Europe à accompagner de fragiles et petites démocraties du Caucase du Sud dans leur chemin vers la démocratie?
Après la crise en Géorgie, c’est l’Arménie qui plonge dans une crise politique majeure. Alors que l’attention était vive durant la campagne électorale, la communauté internationale n’aura pas su favoriser le dialogue nécessaire pour éviter les affrontements du 1er
 mars. Après onze jours de contestations des résultats du scrutin de la part d’une opposition conduite par l’ancien chef de l’État, Ter Petrossian, la police a tenté de disperser les manifestants. La situation a dégénéré, entraînant huit morts, de nombreux blessés et l’instauration de l’état d’urgence et de restrictions à la liberté d’information, à la liberté de réunion ainsi que de restrictions imposées aux partis politiques. Depuis lors, 400 personnes ont été arrêtées. L’inquiétude est manifeste pour une population qui craint la mise en œuvre d’une politique de répression. Aujourd’hui, il est de notre devoir d’en faire écho auprès de toutes les parties du conflit politique arménien.
Mais quelle est la méthode à suggérer pour ramener chacun à la raison et à la table de discussions alors que les tensions sont à ce point exacerbées? C’est tout l’enjeu auquel nous sommes confrontés. Il faut d’abord redonner confiance aux Arméniennes et aux Arméniens dans leur jeune démocratie. Préalable à la confiance, une enquête sur le déroulement des évènements est indispensable, de même que la libération des prisonniers. Ensuite, avec la communauté internationale, notre représentant spécial pour le Caucase du Sud, nos partenaires du Conseil de l’Europe et de l’OSCE, nous devons imposer un calendrier à nos amis arméniens pour qu’ils se retrouvent autour de la table des négociations et nous devons y ramener toutes les parties en conflit, les autorités tout comme l’opposition. Les règles démocratiques passent par le dialogue et la non-violence, nous devons en être les facilitateurs.
Si vous me le permettez, Monsieur le Président, j’aimerais faire un amendement oral. Je ne sais pas comment procéder: dans le cadre de la résolution, nous avons commis une erreur. Au considérant H., nous avons parlé du territoire du Haut-Karabagh alors que nous voulions mentionner le statut du Haut-Karabagh. Donc, les collègues sont apparemment d’accord pour cet amendement oral. 
