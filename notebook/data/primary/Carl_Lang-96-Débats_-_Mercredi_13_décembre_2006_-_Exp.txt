Carl Lang # Débats - Mercredi 13 décembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Rapport Brok (A6-0436/2006
) 
  Carl Lang (NI
), 
par écrit
. - L’actuelle stratégie d’élargissement consistant à regrouper dans un même ensemble une quarantaine d’États conduira l’Europe de Bruxelles à subir le sort de la grenouille de Jean de la Fontaine qui, voulant se faire aussi grosse que le bœuf, éclata. Cela pour deux raisons.
La première tient au fait qu’aucune limite géographique ne lui étant donnée, cette Europe-là, après avoir accueilli la Turquie, n’aura aucune raison de refuser l’entrée d’autres pays asiatiques ou africains.
La seconde raison est liée à la nature idéologique du projet euro-bruxellois, visant à briser l’identité et la souveraineté des nations européennes pour créer sur les ruines de celles-ci un super État centralisé, dont l’administration sera encore plus pesante que le sont les administrations nationales.
Plutôt que de dissoudre nos peuples dans cet ensemble supranational, voué comme l’Union soviétique et la Yougoslavie à disparaître, construisons une grande Europe des nations libres et souveraines, unies par les valeurs humanistes et chrétiennes qui ont fait la grandeur de notre civilisation. 
