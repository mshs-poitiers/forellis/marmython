Pervenche Berès # Débats - Jeudi 31 mai 2001 - VOTES
  Berès et Garot (PSE
),
par écrit
. - La délégation socialiste française a toujours défendu et encouragé un renforcement des relations positives existant entre l’agriculture et l’environnement. C’est pourquoi nous soutenons les objectifs généraux du rapport Myller.
Pour autant, nous ne pouvons accepter que, sous couvert d’un amendement (150), on tente de remettre en cause les fondements mêmes de la PAC. Nous ne pouvons cautionner le remplacement des aides �  la production. La PAC d’aujourd’hui n’a plus rien �  voir avec celle d’il y a quarante ans, elle a été réformée en profondeur. Nous voulons poursuivre ce travail, mais pas de façon insidieuse. Des réorientations sont d’ores et déj�  possibles et souhaitables. Nous les soutenons. Mais nous ne pouvons entériner cet amendement qui conduirait �  remettre en cause un pan entier de la PAC sans l’intégrer dans une réorientation globale. 
