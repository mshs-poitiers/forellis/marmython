Pierre Schapira # Débats - Jeudi 30 novembre 2006 - SIDA
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Pierre Schapira (PSE
). 
 - Monsieur le Président, mes chers collègues, cette résolution va dans le bon sens. Je voudrais ajouter que la lutte contre le sida est aussi une question de gouvernance qui doit être traitée au niveau local.
Je vous rappelle qu’en 2007, c’est-à-dire aujourd’hui, 50 % de la population mondiale vit dans les villes. Dans de nombreuses villes africaines, la prévention et les soins nécessitent l’implication des autorités locales afin de les rendre plus adaptés aux besoins locaux et de lever les tabous sociaux, qui sont souvent très forts. Nous devons coopérer directement avec les villes pour les aider à gérer leurs services de santé et pour leur apporter des financements accrus.
J’aurais souhaité que notre résolution propose davantage de solutions concrètes, sur le terrain. Nous aurions pu demander à la Commission de favoriser l’offre de traitements combinés, médicaments réunissant trois types de remèdes en une seule et même gélule, ce qui simplifie grandement le traitement des patients vivant dans des zones où les hôpitaux, les médecins et les laboratoires sont peu nombreux. Nous devons privilégier leur utilisation car, mes chers collègues, nous avons un devoir impérieux: atteindre les objectifs du Millénaire. 
