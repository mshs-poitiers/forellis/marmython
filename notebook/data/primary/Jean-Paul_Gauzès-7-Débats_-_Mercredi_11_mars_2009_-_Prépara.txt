Jean-Paul Gauzès # Débats - Mercredi 11 mars 2009 - Préparation du Conseil européen (19-20 mars 2009) - Plan européen de relance économique - Lignes directrices pour les politiques de l’emploi des États membres - Politique de cohésion: investir dans l’économie réelle (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Paul Gauzès  (PPE-DE
). 
 – Madame la Présidente, Monsieur le Commissaire, mes chers collègues, nos concitoyens, en ces temps de crise, attendent beaucoup de l’Europe. L’Europe ne doit pas les décevoir.
Bien sûr, le réalisme amène à constater que les moyens financiers de l’Europe sont limités, une réflexion doit être engagée pour les améliorer. Néanmoins, l’Europe peut mieux exister, mieux réussir, avec une plus grande volonté politique.
D’abord, bien sûr, en catalysant les actions et les efforts des États membres, mais aussi par une approche coordonnée au niveau européen. Le plan de relance est essentiellement une boîte à outils pour favoriser les restructurations. Le rôle de la BEI doit être renforcé.
L’Europe doit agir et définir une stratégie économique claire et novatrice. Les agents économiques ont besoin de perspectives et de stabilité juridique. Il importe d’abord de mettre de l’ordre dans les services financiers afin que les institutions bancaires jouent leur rôle principal, qui est de financer le développement économique.
Les textes actuellement en préparation doivent y contribuer: directives sur les fonds propres des banques et des assurances, règlements sur les agences de notation. Sur ce dernier texte, nous devons tirer les conséquences du dysfonctionnement constaté.
Il est également urgent d’organiser une supervision européenne des activités financières qui sont réglementées. Le rapport du groupe de Larosière formule des propositions utiles et opportunes qu’il faut rapidement mettre en œuvre.
Il faut également donner à l’Europe une politique industrielle véritable, efficace et moderne. À cet égard, nous devons concilier les impératifs de développement durable et la nécessité d’un tissu industriel de qualité, producteur de richesses et fournisseur d’emplois.
Il convient, dans les temps de crise que nous vivons, de ne pas perturber les secteurs qui fonctionnent par la production de règles ou de réglementations dont l’efficacité n’est pas formellement établie. Par exemple, dans le secteur automobile, qui connaît aujourd’hui de graves difficultés, il est important que le règlement d’exemption de la distribution automobile, qui expire en 2010, soit prorogé.
Il faut également, par exemple, être vigilant dans la négociation de l’accord bilatéral avec la Corée, qui pourrait être très favorable à notre industrie. 
