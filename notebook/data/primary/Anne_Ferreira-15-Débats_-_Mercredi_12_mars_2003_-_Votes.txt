Anne Ferreira # Débats - Mercredi 12 mars 2003 - Votes
  Ferreira (PSE
),
par écrit
. - 
Je déplore que sur un sujet aussi déterminant pour l'avenir des citoyens européens, il faille se contenter d'une procédure de résolution. Un commissaire ne devrait parler au nom de l'Union européenne qu'après avoir négocié son mandat auprès des représentants élus. La constitution devra prendre en compte ce déficit démocratique pour réussir à rapprocher l'Europe des citoyens. Les amendements proposant une réorientation politique du texte étant rejetés, je vote contre la résolution.
Les services publics ne doivent pas faire partie des négociations, un moratoire suspensif de tout nouvel accord permettrait une véritable évaluation d'impact en ce qui concerne les autres services marchands.
Certains affirment que les pays en voie de développement sont demandeurs de ces accords. Ils n'ont d'autre possibilité que de privatiser leurs services publics, le niveau de leur dette leur interdisant tout emprunt destiné au développement de ceux-ci, notamment pour l'eau. L'OMC ne doit plus être le cheval de Troie d'un libéralisme qui a montré ses limites et son incapacité à améliorer la qualité de la vie de la majorité des citoyens du monde. Les échanges commerciaux au niveau international sont indispensables, mais ils doivent être subordonnés à l'adoption et au respect de règles environnementales et sociales. 
