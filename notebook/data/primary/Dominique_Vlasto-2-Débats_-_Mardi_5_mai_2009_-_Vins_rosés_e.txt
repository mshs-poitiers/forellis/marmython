Dominique Vlasto # Débats - Mardi 5 mai 2009 - Vins rosés et pratiques œnologiques autorisées (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Dominique Vlasto  (PPE-DE
), 
par écrit. 
– 

Sous prétexte de réviser les pratiques œnologiques autorisées, la Commission européenne s’apprêterait à autoriser le mélange de vins blanc et rouge sous l’appellation «vin rosé».
Je conteste la possibilité d’appeler «vin rosé» un mélange de différents vins. Ce n’est pas la seule couleur du vin qui doit en faire le nom: ce sont les cépages, les terroirs et le savoir-faire des viticulteurs qui font les vins et non la couleur finale de n’importe quel liquide.
Pour faire un rosé par coupage, il faudrait obligatoirement partir d’un vin blanc, à plus de 95 %, que l’on tacherait de vin rouge. Or, le vin rosé résulte de la fermentation de raisins ou de moûts majoritairement rouges. Ainsi, en autorisant le coupage des vins, la Commission européenne autoriserait une véritable contrefaçon du vin rosé: ce serait organiser une tromperie des consommateurs.
Outre l’insulte faite aux viticulteurs engagés dans une politique de qualité, notamment en Provence, il serait inadmissible de légitimer un sous-produit issu du mélange de produits finis et d’autoriser la référence trompeuse à la couleur du rosé.
Sans autre solution, il faudrait au moins imposer à ces boissons un étiquetage correspondant à leur réel contenu: «vin coupé» ou «mélange de vins». 
