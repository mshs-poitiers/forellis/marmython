Bernard Poignant # Débats - Mercredi 4 juin 2003 - Votes
  Poignant (PSE
),
par écrit
. -
 Le secteur de la pêche traverse, en apparence, une période calme. Avec la fin des négociations de la réforme de la pêche, les professionnels dressent le bilan de ces 18 mois.
Concernant les aides publiques, alors que la Commission poussait à la fin des aides publiques dès décembre 2002, les négociations ont conduit au maintien de ce régime jusqu’à décembre 2004. Si les pêcheurs sont prévenus, les difficultés ne sont pas aplanies.
En Bretagne, près de 75% de la flotte devrait être remplacée. L’usage de bateaux neufs, sécurisés, avec des conditions techniques pour faciliter le travail des marins, devrait être généralisé.
Mais, si la bataille des aides publiques est interrompue, pour l’instant, au niveau communautaire, elle va bientôt commencer au niveau international. La Commission a présenté une proposition à l’OMC en vue d’interdire toutes les subventions afin d’empêcher un excédent de capacité dans le secteur de la pêche sous le prétexte de lui conserver sa viabilité.
Concernant les plans de reconstitution, l’exemple vient du plan de reconstitution du cabillaud en Écosse. Les conséquences sont connues: faillite de certaines entreprises qui ne peuvent plus pêcher, pression halieutique accrue sur d’autres espèces et sur d’autres lieux, pression plus importante sur les marchés.
(Explication de vote écourtée en application de l’article 137, paragraphe 1, du règlement)

