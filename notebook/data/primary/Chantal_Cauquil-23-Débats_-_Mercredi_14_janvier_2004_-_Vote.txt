Chantal Cauquil # Débats - Mercredi 14 janvier 2004 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. 
- 
Malgré toutes les concessions faites par l’auteur du rapport aux forces politiques réactionnaires qui œuvrent pour la privatisation des services publics, il semble aujourd’hui débordé par ceux-là même dont il voulait obtenir le soutien. Amendé ou pas, le rapport Herzog codifie la disparition des services publics, jusqu’à leur nom, qui devient "services d’intérêt général.
Nous sommes opposées à ce rapport et, bien sûr, aux amendements qui l’aggravent. L’éducation, la santé, les transports collectifs, les services postaux, les télécommunications, l’approvisionnement en eau et en énergie, l’électricité et l’élimination des déchets, doivent rester ou redevenir des services publics indépendants des stupides et inhumaines lois du marché, qui ne sont pas destinés à rapporter du profit privé, mais à satisfaire les besoins collectifs.
En outre, le processus exprimé par le rapport se traduit par des réductions drastiques de personnel, c’est-à-dire par une catastrophe sociale. Sous prétexte d’harmonisation des pratiques entre les différents pays qui composent l’Union européenne, ses autorités se font les artisans d’une régression sociale grave. Pourtant, l’harmonisation pourrait se faire sur la base d’un développement des services publics, de leur élargissement, notamment à la construction de logements sociaux, si les institutions européennes, comme les États nationaux, étaient au service de l’intérêt collectif et non des intérêts privés.
En conséquence, nous avons voté contre ce rapport. 
