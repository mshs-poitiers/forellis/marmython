Alain Lipietz # Débats - Jeudi 15 février 2007 - Augmentation du prix de l’énergie (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Alain Lipietz, 
au nom du groupe Verts/ALE
. - Madame la Présidente, je crois que nous avons d’une part un accord fort sur le caractère structurel de la hausse de l’énergie. Là-dessus, je me félicite de la contribution de la commission ITRE qui a été tout a fait claire et qui a appuyé la commission économique et monétaire.
Nous avons un désaccord, y compris au sein de la commission économique et monétaire, sur le caractère spéculatif qui s’est ajouté, de surcroît, à ce Fonds structurel. De ce point de vue, j’approuve tout à fait l’intervention de Mme Budreikaitė et je souhaiterais qu’elle intervienne auprès de son groupe pour qu’il retire l’amendement contestant la composante spéculative de la volatilité du prix de l’énergie. Nous devrons prendre des mesures contre la volatilité; nous devons, à tout le moins, et le pouvons, éliminer la composante spéculative des mouvements du prix de l’énergie.
Ceci dit, il faut prendre conscience des conséquences particulièrement graves de la hausse structurelle du prix du pétrole. Un chiendent est en train d’envahir la planète: c’est le palmier africain, ce sont les cultures céréalières destinées à la production de biofioul ou de biodiesel, qui vont aboutir à un couplage du prix de l’alimentation et du prix de l’énergie. Ce risque est inacceptable; nous ne pouvons le courir.
Depuis des années, nous nous demandons comment éviter aux travailleurs la deuxième vague des effets du pétrole sur le coût de la vie. Si, par un malheur extraordinaire, la politique de développement des biocarburants devait concurrencer la politique de la production alimentaire, alors, oui pour le coup, nous obtiendrions ce couplage et ce serait la pire des catastrophes. 
