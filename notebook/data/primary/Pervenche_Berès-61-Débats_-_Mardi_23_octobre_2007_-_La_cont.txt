Pervenche Berès # Débats - Mardi 23 octobre 2007 - La contribution des politiques fiscale et douanière à la stratégie de Lisbonne (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Pervenche Berès  (PSE
). - 
 Monsieur le Président, Monsieur le Commissaire, chers collègues, je veux d'abord ici remercier notre rapporteure pour la qualité de son travail et les efforts qui ont été les siens pour parvenir à un texte qui, malheureusement, ne semble pas totalement la satisfaire.
La fiscalité est, manifestement, un complément indispensable à la construction d'un véritable marché intérieur si l'on veut bien penser celui-ci au-delà d'une simple zone de libre-échange où toute forme de dumping fiscal et social serait autorisée. L'achèvement du marché intérieur nécessite donc l'harmonisation de certaines règles fiscales et appelle en premier lieu la création d'une assiette commune consolidée pour l'impôt sur les sociétés. Le rapport qui nous est aujourd'hui proposé ouvre la porte en ce sens et je m'en réjouis. Je constate qu'un certain nombre de collègues sont très mobilisés contre ce projet au nom d'une concurrence qu'ils qualifient de juste.
Pour moi, une concurrence libre et non faussée - et je ne suis pas une avocate frénétique d'une telle concurrence - nécessite un certain nombre de transparences dans les règles du jeu. Et je m'étonne que des pays qui, au nom de la solidarité, ont bénéficié d'apports considérables de l'Union européenne refusent aujourd'hui cette étape qui, en termes de solidarité, de concurrence, de transparence, serait une avancée importante.
La fiscalité est également un formidable outil que l'Union doit mobiliser pour mettre en œuvre les stratégies dont elle se dote, car elle n'a pas tant d'outils que ça. Il peut avoir un impact favorable sur les énergies les plus propres ou encore pour atteindre ...
(Le Président retire la parole à l'orateur.)

