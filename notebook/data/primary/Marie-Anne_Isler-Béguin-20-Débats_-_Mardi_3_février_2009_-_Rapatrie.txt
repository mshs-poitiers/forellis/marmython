Marie-Anne Isler-Béguin # Débats - Mardi 3 février 2009 - Rapatriement et réinstallation des détenus de Guantánamo - Utilisation alléguée de pays européens par la CIA pour le transport et la détention illégale de prisonniers (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie Anne Isler Béguin  (Verts/ALE
). - 
 Monsieur le Président, nous avons en effet tous milité pour la fermeture de Guantánamo et nous nous sommes tous félicités de la décision du président Obama de fermer cette prison de la honte.
Aujourd’hui, la question d’accueillir les détenus de Guantánamo ne devrait pas se poser à nous. En tant qu’Européens, fidèles à nos valeurs de défense des droits de la personne humaine, nous devons prendre nos responsabilités car il est de notre devoir d’accueillir ces ex-détenus.
On a pu lire – et là je m’adresse au Conseil –, on a pu lire dans la presse que des États membres n’étaient pas très chauds pour cet accueil. Alors là, je voudrais vraiment lancer un appel au Conseil et en particulier aux États membres réticents pour qu’ils acceptent de recevoir ces détenus sur leurs territoires.
Et je vous dirai, chers collègues, que le pire serait que le bel élan suscité par la fermeture de Guantánamo s’enlise parce que nous n’aurions pas répondu présents lorsqu’on aura fait appel à l’Europe. 
