Kader Arif # Débats - Lundi 18 février 2008 - Réforme des instruments de protection du commerce (débat) 
  Kader Arif  (PSE
). – 
(FR) 
Monsieur le Président, Monsieur le Commissaire, si vous envisagez un nouveau projet de réforme proche de sa mouture actuelle, comment imaginez–vous pouvoir défendre à l’OMC une politique qui sanctionne le dumping quand il est pratiqué par des entreprises étrangères, mais l’accepte quand ce sont des entreprises européennes qui en bénéficient? Le problème n’est pas de savoir qui produit les marchandises qui arrivent en Europe, mais si elles sont exportées dans des conditions loyales. On ne pourrait accepter qu’une entreprise dite européenne, qui pratique le dumping, devienne inattaquable uniquement parce qu’elle est européenne selon votre nouvelle définition.
Par ailleurs, vu que ce débat devra se tenir au niveau multilatéral, pourquoi ne pas être plus visionnaire et inclure le dumping social et environnemental dans le champ des instruments de défense commerciale? Ce serait bien la place de l’Europe, et tout à son honneur, d’être à l’avant-garde dans ce combat! 
