Gilles Savary # Débats - Mercredi 22 avril 2009 - Plan d'action en faveur des systèmes de transport intelligents - Systèmes de transport intelligents dans le domaine du transport routier et interfaces avec d'autres modes de transport (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Gilles Savary  (PSE
). - 
 Madame la Présidente, je voudrais d'abord remercier Mme
 Jensen de son travail toujours remarquable, parce qu'il est très ouvert et très attentif à l'ensemble des opinions.
Je voudrais d'abord dire que, derrière les systèmes de transport intelligent, il y a quand même des crédits européens et que j'ai été un peu stupéfait de constater combien ils étaient presque essentiellement orientés vers l'automobile.
Je crois que l'automobile a, évidemment, ses vertus; c'est un secteur économique extrêmement puissant en Europe. Mais je pense que nous avons besoin de mettre de l'intelligence dans tous les modes de transport.
Je ne néglige pas le fait qu'il y ait l'ERTMS pour le chemin de fer, SESAR, GALILEO, mais il manque, à mon avis, une approche orientée vers l'usager en ce qui concerne l'information de l'usager, l'accessibilité des personnes à mobilité réduite et la façon dont on peut adapter aujourd'hui les véhicules urbains, la mise en place d'un véhicule urbain économe en matière énergétique, la sécurité dans les transports, notamment dans les transports collectifs, qui est un sujet tout à fait important, l'information de l'usager, les billetiques, qui souvent mériteraient des avancées considérables.
Donc, je pense qu'il faudrait que l'on diversifie quand même les moyens dans les années à venir. En particulier, je voudrais qu'on fasse attention au respect des données personnelles; il faut éviter que l'on ne se retrouve dans le 1984 d'Orwell, que l'on ne coure après le mirage du remplacement intégral de l'homme par la machine. On a vu ce qu’il s'est passé sur l'Hudson cet hiver: s'il n'y avait pas eu de pilote, on aurait sans doute eu une mécanique qui n'aurait pu redresser l'avion. Enfin, je pense qu'il faut que l'on privilégie également d'autres modes que les modes automobiles.
Sous ces réserves, je suis d'accord avec le rapport qui a été présenté. 
