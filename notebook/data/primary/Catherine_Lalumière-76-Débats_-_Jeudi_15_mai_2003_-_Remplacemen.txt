Catherine Lalumière # Débats - Jeudi 15 mai 2003 - Remplacement d’un membre du directoire de la BCE
  La Présidente.  
- 
L’ordre du jour appelle le rapport (A5-0153/2003
) de Mme Randzio-Plath, au nom de la commission économique et monétaire, sur la nomination de Mme Gertrude Tumpel-Gugerell comme membre du directoire de la Banque centrale européenne [8090/2003 - C5-0193/2003
 - 2003/0810(CNS)]. 
