Georges Berthu # Débats - Jeudi 26 février 2004 - Votes (suite) 
  Berthu (NI
),
par écrit
. 
- En adoptant le rapport Bigliardo sur les finances publiques dans l’UEM, le Parlement européen a finalement supprimé du texte d’origine les appels à des sanctions contre la France et l’Allemagne pour non-respect du pacte de stabilité. C’est la sagesse.
Toutefois, j’ai voté contre l’ensemble du texte, car ce qui reste n’est guère meilleur que ce qui a été supprimé.
Le rapport suggère en effet des mesures d’aménagement du pacte afin de le rendre intelligent, ce qui est impossible. Il est en effet stupide en lui-même, car il impose une règle budgétaire uniforme qui sera toujours arbitraire, même si elle devenait demain plus sophistiquée. Il faut plutôt laisser à chaque gouvernement, responsable devant son peuple, une plus grande liberté d’apprécier la politique la meilleure dans l’intérêt de sa société.
Certes, on peut soutenir que l’existence de l’euro impose une certaine coordination des comportements budgétaires. Mais cette règle n’est pas absolue, comme on le voit aujourd’hui où l’euro reste à un niveau élevé malgré la divergence complète des politiques nationales. De toute façon, si vraiment l’unification monétaire devait imposer l’uniformisation, budgétaire ou autre, celle-ci entraînerait des coûts tellement exorbitants qu’il deviendrait opportun de se demander si l’opération reste globalement avantageuse. 
