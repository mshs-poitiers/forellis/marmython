Martine Roure # Débats - Mardi 13 janvier 2009 - Heure des questions (questions à la Commission) 
  La Présidente. –  
J’appelle la 
question n° 33 de Marian Harkin 
(H-0970/08

)
Objet:	Réforme du budget
Les résultats de la consultation publique sur «Reformer le budget - changer l’Europe» ont révélé une demande à la Commission d’améliorer l’effectivité et l’efficacité de l’exécution du budget en augmentant la transparence et l’accès du public à celui-ci. De plus, la récente publication du rapport 2007 de la Cour des comptes a fait plusieurs recommandations en termes d’équilibre coûts/risques, de surveillance et de rapports, ainsi que de simplification des instruments et d’amélioration de l’information et du contrôle assurés par les États membres. La Commission peut-elle expliquer par quelles mesures elle compte répondre aux principaux enseignements de la consultation publique et du rapport de la Cour des comptes afin d’accroître les performances et de réduire des charges administratives?
