Marine Le Pen # Débats - Jeudi 1 juin 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Hennicot-Schoepges (A6-0168/2006
) 
  Marine Le Pen (NI
), 
par écrit
. - Comment peut-on avoir un échange culturel avec l’autre si l’on ne sait pas qui on est? Vous reconnaissez que «l’identité religieuse est une partie essentielle de notre identité à tous - même pour nos concitoyens laïques», mais vous avez refusé de reconnaître nos racines chrétiennes dans ce qui est, heureusement pour nous, votre défunte Constitution. De quoi parlons-nous alors? L’amendement 9 parle d’une «civilité interculturelle», mais c’est une contradiction dans les termes, ou alors la civilité ne veut plus rien dire! L’addition de beaux et grandiloquents mots finit justement souvent par ne rien vouloir dire.
Le résultat de votre démarche c’est le relativisme et la trahison: vous voulez faire la promotion de l’année européenne du dialogue interculturel aux Jeux olympiques de Pékin et ainsi servir de roue de secours à une dictature communiste (amendement 38)! Quelle honte! Vous allez parler du dialogue interculturel avec le Tibet sans doute? Cette initiative à elle seule disqualifie votre projet. 
