Bruno Gollnisch # Débats - Mercredi 28 février 2001 - Ordre du jour
  Gollnisch (TDI
). 
 - Madame la Présidente, ce n’est pas véritablement une opposition. Je trouve simplement un peu regrettable qu’il n’y ait pas de résolution prévue pour faire suite au débat sur les frappes aériennes en Irak, qui me paraît être un problème très important dans le contexte, en particulier, de la violation des principes régissant la politique étrangère et de sécurité commune. 
