Charles Pasqua # Débats - Mardi 17 décembre 2002 - Votes (suite) 
  Pasqua (UEN
),
par écrit
. - 
Compte tenu du talent dont M. Bourlanges fait preuve habituellement, on pouvait s’attendre à autre chose de sa part que ce rapport sans grand intérêt qui, surtout, ne brille guère par son originalité.
En effet, la hiérarchie des normes qu’il propose (bloc constitutionnel, lois organiques, lois ordinaires) n’est jamais qu’une fidèle copie de celle prévue par la Constitution française de la Ve République. De surcroît, dans la mesure où la hiérarchie des normes implique nécessairement une hiérarchie des fonctions et des organes, le rapporteur expose ici, ni plus ni moins, une nouvelle architecture communautaire qui reproduit au niveau européen le modèle constitutionnel étato-national.
Comme tout exercice de ce type, le mimétisme institutionnel a cependant ses limites. Est-il vraiment pertinent de vouloir ainsi transposer au niveau communautaire le modèle constitutionnel commun aux États membres ? La réponse est bien évidemment négative.
Ce qu’oublie le rapporteur lorsqu’il substitue la loi européenne à l’actuel règlement, exigeant au passage, cela va de soi, l’extension de la procédure de codécision, ou lorsqu’il évoque une réforme de la procédure de révision des Traités, c’est que, même dotée d’un système politique identique à celui des États, l’Union sera toujours dépourvue de l’essentiel : la légitimité démocratique. 
