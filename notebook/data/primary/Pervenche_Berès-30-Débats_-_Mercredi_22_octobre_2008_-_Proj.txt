Pervenche Berès # Débats - Mercredi 22 octobre 2008 - Projet de budget général 2009 (section III) - Projet de budget général 2009 (sections I, II, IV, V, VI, VII, VIII, IX) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Pervenche Berès 
, rapporteure pour avis de la commission des affaires économiques et monétaires. 
− 
Madame la Présidente, Madame la Commissaire, Monsieur le Président en exercice du Conseil, au nom de la commission économique et monétaire, je voudrais émettre une satisfaction, un regret et un espoir.
La satisfaction, c’est que la commission des budgets ait bien voulu reprendre notre proposition, à savoir que les superviseurs, qu’ils soient bancaires, d’assurance ou de valeurs mobilières, ont besoin de davantage de financement pour pouvoir mieux travailler ensemble. Je crois que c’est une évidence, et que même ceux qui ne sont pas membres de la commission économique et monétaire accepteront cela comme un impératif.
J’ai un regret. Dans la crise que nous vivons aujourd’hui, l’euro est notre pilier, c’est notre socle! Or, la Commission a coupé drastiquement les moyens de communication autour de cette merveille qu’est l’euro.
J’ai un espoir, c’est que demain la plénière prenne conscience que les moyens de l’Eurogroupe, qu’il se réunisse à quelque niveau que ce soit dorénavant, doivent être renforcés. Cela ne peut pas être une structure légère, comme cela, en l’air. C’est devenu une réalité. Pour cela, j’espère que ce Parlement acceptera demain de voter le principe de moyens renforcés pour l’Eurogroupe. 
