Daniel Cohn-Bendit # Débats - Lundi 2 septembre 2002 - Ordre des travaux
  Cohn-Bendit (Verts/ALE
). 
 - Monsieur le Président, je crois que le Tribunal pénal est défendu pratiquement par tous les groupes de ce Parlement. Cependant, tous les gouvernements, qu'ils soient de droite ou de gauche, ne défendent pas véritablement le Tribunal pénal, et je crois qu'à l'occasion de cette session, nous avons besoin d'un débat nous permettant d'affirmer que nous refusons toute proposition d'accorder l'immunité à qui que ce soit en dehors de ce Tribunal. Un tel débat donnerait, lors de la deuxième session de septembre, la possibilité de faire une résolution.
Étant donné que le 30 septembre, une réunion des ministres des Affaires étrangères est prévue afin de décider de la possibilité ou non d'exceptions, je crois que deux débats vaudront mieux qu'un pour bien montrer à tous les gouvernements que le Parlement européen ne veut pas qu'il y ait d'exception quelle qu'elle soit.
C'est pourquoi ni mon groupe ni moi-même n'estimons qu'un seul débat, le 24 septembre prochain, clos sur une résolution, soit suffisant. Ce que nous préconisons consiste à ouvrir un débat pour que l'opinion publique européenne connaisse la position ferme de tous les groupes de ce Parlement. C'est pour cette raison que nous demandons d'intégrer cela dans le programme de cette session.
(Applaudissements)

