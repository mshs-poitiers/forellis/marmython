Danielle Auroi # Débats - Mardi 13 novembre 2001 - Votes
  Auroi (Verts/ALE
). 
 - Monsieur le Président, c'est pour signaler dans la version française deux erreurs qu'il faut corriger.
Au paragraphe 10, il est dit dans la version française "Les procédures de violation du traité doivent être poursuivies contre les États membres", ce qui est un contresens. Il faut comprendre "Les procédures de poursuite pour violation du traité doivent être mises en place contre les États membres".
Au paragraphe 16, dans la version française, il est dit "invite la Commission à examiner comment promouvoir de petites tueries locales". Monsieur le Président, pour éviter de petites tueries plus graves, je propose - ça rappelle un film "Petits meurtres entre amis" -, je propose donc des "petites structures locales d'abattage".
