Hélène Flautre # Débats - Mercredi 10 octobre 2007 - Situation humanitaire dans la bande de Gaza
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Hélène Flautre  (Verts/ALE
). – 
(FR)
 Monsieur le Président, alors que les points de passage de la bande de Gaza sont fermés et contrôlés par l'armée israélienne, alors que, tous les jours, des incursions meurtrières y sont menées, alors que le nombre de colons dans les territoires occupés ne cesse de croître, alors qu'en violation du droit international la construction du mur et le quadrillage sécuritaire se poursuivent, il y a encore, dans cette enceinte, ici même, dans notre Parlement européen, des personnes pour nier le statut de puissance occupante d'Israël. C'est ahurissant !
Israël est une puissance occupante et, à ce titre, a des obligations en vertu des conventions de Genève et, en particulier, Israël ne peut en aucun cas avoir recours aux punitions collectives. Nous serions bien avisés de nous assurer qu'Israël respecte ses obligations, au lieu d'envoyer de catastrophiques signaux, comme nous l'avons fait, par exemple, en suspendant le financement de l'approvisionnement en carburant de la centrale électrique à Gaza. Les larmes de crocodile que nous avons versées sur les divisions interpalestiniennes sont bien malvenues après que l'Union européenne a été incapable de soutenir les efforts d'Abbas pour coopter l'aile pragmatique du Hamas.
Qu'est-ce qu'on pourrait encore imaginer de pire? Jouer Ramallah contre Gaza? Désigner les bons Palestiniens et les aider à éliminer les mauvais? Qui peut croire à une solution durable sans que soit garantie l'unité politique et territoriale de la Palestine? Qui peut croire que la paix, pour les Israéliens et les Palestiniens, se construira par une politique qui entraîne, de fait, la radicalisation des populations en Israël comme en Palestine ?
La situation humanitaire à Gaza viole tous les standards de dignité humaine. Il faut obtenir, et dans les plus brefs délais, la levée du blocus de la bande de Gaza. Il faut donc exercer sur Israël les pressions adéquates. Cette question ne peut plus être un tabou et je vous demande, Conseil et Commission, quelles sont les mesures que vous comptez entreprendre afin de favoriser la levée du blocus et de contraindre Israël à respecter ses obligations et engagements. Je vous demande ce que vous comptez entreprendre pour qu'Israël renonce, à un mois de la conférence internationale, à son projet de couper en deux la Cisjordanie en reliant Jéricho à Jérusalem-Est.
(Applaudissements)

