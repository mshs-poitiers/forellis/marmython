Jean-Claude Martinez # Débats - Jeudi 6 avril 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Élections au Belarus (B6-0234/2006
) 
  Jean-Claude Martinez (NI
), 
par écrit
. - Il y a eu la révolution Orange en Ukraine et la vie y est restée grise. Il y a la révolution Blanche à Moscou et le bilan est là: effondrement de l’espérance de vie de 10 ans, chute démographique, criminalité, destruction des systèmes sanitaires et pillage des biens nationaux au profit des oligarques. Encore un effort, et le livre noir du libéralisme va égaler le livre noir du communisme.
Il ne faudrait pas priver la Biélorussie de toutes les félicités engendrées par le marché. Les Biélorusses ont droit eux aussi à la liberté de l’oligarchie médiatico-militaro-politique, avec son cortège de chômage, d’euthanasie passive des quatrièmes âges, d’avilissement culturel, de stagnation économique, d’individualisme sauvage et de solitude sociale.
On comprend que le Parlement européen se réjouisse de la démocratie politico-médiatique des magnats Berlusconi, Bouygues-Lagardère ou Murdoch. Le Biélorusse a droit lui aussi à la misère libérale des nations et à la démocratie cathodique frelatée. 
