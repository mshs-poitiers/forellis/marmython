Christine de Veyrac # Débats - Mercredi 9 mai 2007 - Transports terrestres, rail et route: obligations de service public (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Christine De Veyrac (PPE-DE
). 
 - Monsieur le Président, Monsieur le Vice-président de la Commission, Monsieur le représentant du Conseil, chers collègues, comme l’ont souligné plusieurs de nos collègues, cela fait presque sept ans que ce texte est en discussion devant les institutions européennes, et c’est beaucoup trop. Nous avons besoin de trouver un accord aujourd’hui: il est essentiel pour l’organisation des transports publics de proximité dans l’Union européenne.
Permettez-moi de féliciter vivement le rapporteur, Erik Meijer, ainsi que les rapporteurs fictifs, Mathieu Grosch et Willi Piecyk, même s’il n’est pas présent ce soir. Je les félicite pour avoir négocié ces derniers mois avec les États membres et avoir réussi à trouver un accord avec le Conseil sur ce texte. Il me semble que le compromis ainsi conclu assure un bon équilibre entre une ouverture maîtrisée et progressive à la concurrence et le respect des exigences de service public. C’est pourquoi je crois que nous devons soutenir cet accord et éviter ainsi de prolonger stérilement les discussions, ce qui serait inévitable si nous allions en conciliation.
Bien sûr, cet accord n’est pas parfait, mais c’est un bon compromis qui devrait permettre d’apporter une réelle sécurité juridique à tous les acteurs du transport concernés et de fixer un cadre modernisé, favorable au développement des transports collectifs en Europe. J’espère vraiment que nous allons pouvoir l’adopter demain à une large majorité, donnant ainsi aux services publics de transport de voyageurs par rail et par route un cadre harmonisé et équilibré. 
