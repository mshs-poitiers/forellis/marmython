Bruno Gollnisch # Débats - Jeudi 20 mars 2003 - Guerre en Irak
  Gollnisch (NI
). 
 - Monsieur le Président, pour une très brève motion de procédure, si vous me le permettez. Avoir une discussion, comme vous l'avez souhaité, est excellent, mais un parlement doit exprimer sa position par un vote. Nous devons offrir aux différents groupes la possibilité de déposer des résolutions et d'exprimer la conclusion de ce débat par un vote. Sinon, notre discussion n'aura aucun effet. 
