Jean-Claude Martinez # Débats - Lundi 7 juillet 2008 - Constitution des groupes politiques (modification de l'article 29 du règlement) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Claude Martinez  (NI
). 
 – (FR) 
Monsieur le Président, il y a un marché unique, il y a une monnaie unique, il serait tout à fait normal qu'il y ait un groupe unique, ce qui serait d'ailleurs beaucoup plus efficace. Il y aurait un seul intervenant et il se partagerait plus facilement les choses. M. Corbett propose d'en monter le seuil à 30. Ça me paraît très dangereux parce que les Irlandais, les Français, les Hollandais sont capables d'en envoyer 31, et là on serait bien embêtés. Alors moi, je crois qu'il faut prévoir un seuil flottant. On le laisse à 30, mais à discrétion, le Président peut le monter à 35-40, si les députés qui veulent s'agréger ne sont pas tout à fait normaux.
Mais il y a une technique qui était pratiquée sous la quatrième république, qui s'appelait les invalidations. C'est-à-dire qu'au début de la législature, la majorité décidait de conserver le mandat de tel ou tel député ou de l'invalider. Ça, c'est une technique parfaite. M. Corbett pourrait être chargé, par exemple, d'invalider toutes les personnes qui ne pensent pas comme lui. On pourrait aussi prévoir une modulation. Ceux qui ne pensent pas comme M. Corbett verseraient leur traitement à M. Corbett ou au groupe de M. Corbett. Enfin, tout cela n'a aucune importance, les Irlandais vous l'ont montré, adoptez les règles que vous voulez, ça ne se passe déjà plus ici, Monsieur Corbett. 
