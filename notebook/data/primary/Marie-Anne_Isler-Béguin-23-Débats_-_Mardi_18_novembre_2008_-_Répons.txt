Marie-Anne Isler-Béguin # Débats - Mardi 18 novembre 2008 - Réponse de l’Union européenne à la crise financière mondiale: suivi du Conseil européen informel du 7 novembre et du Sommet du G20 du 15 novembre 2008 - Programme législatif et de travail de la Commission pour 2009 (débat)
  Marie Anne Isler Béguin  (Verts/ALE
). - 
 Madame la Présidente, je m’adresse à la Commission puisque le Conseil n’est plus là, mais je pense vraiment que les propositions que M. Barroso a faites ne vont pas régler le problème de la crise actuelle, parce que je crois que vous avez oublié un élément; vous avez oublié les responsables de cette crise. Ce sont notre système productiviste et notre développement qui détruisent et qui exploitent la planète et les hommes.
La crise n’est pas terminée, je crois qu’il faudrait s’en rendre compte. Je pense même qu’elle débute seulement et que notre société va vaciller beaucoup plus qu’elle ne vacille actuellement. Je crois que, si les responsables d’un monde globalisé ne voient pas qu’il faut changer de cap, nos concitoyens, eux, par contre, ne comprennent pas comment on peut trouver des milliards d’euros pour donner aux banques et, demain, à l’automobile, alors que le panier de la ménagère se réduit comme une peau de chagrin.
Lorsque vous proposez une relance économique, si c’est la même qui a été faite durant ces années et qui a amené au chaos, vous vous trompez. Je pense qu’il faut vraiment réduire l’empreinte écologique. C’est ça... Comment allez-vous répondre à cette question?
(La présidente retire la parole à l’oratrice)

