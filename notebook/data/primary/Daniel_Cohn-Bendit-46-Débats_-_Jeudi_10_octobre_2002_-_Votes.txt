Daniel Cohn-Bendit # Débats - Jeudi 10 octobre 2002 - Votes
  Cohn-Bendit (Verts/ALE
). 
 - Monsieur le Président, selon l'ordre des votes, nous votons d'abord l'accord d'association sur l'Algérie et plus tard la résolution. En commission des affaires étrangères, nous avons fait le contraire : nous avons d'abord voté pour la résolution et après pour l'accord d'association. Étant donné que mon groupe fait dépendre son vote sur le oui ou non à l'accord d'association du contenu de la résolution, nous demandons qu'on vote d'abord la résolution, et après l'accord d'association. 
