Marine Le Pen # Débats - Mercredi 26 septembre 2007 - Immigration - Immigration légale - Priorités politiques dans le cadre de la lutte contre l'immigration clandestine de ressortissants de pays tiers (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marine Le Pen  (NI
). 
 – (FR) 
Monsieur le Président, chers collègues, les derniers chiffres fournis par le rapport annuel consacré aux activités d'Eurodac en 2006, outil biométrique utilisé à l'échelle européenne pour les demandeurs d'asile, montre que le nombre de personnes qui ont franchi illégalement une des frontières de l'Union a augmenté de 64% par rapport à 2005! Augmentation consternante qui prouve, si besoin en était encore, que l'Europe est impuissante à maîtriser ses frontières extérieures et à endiguer la croissance exponentielle d'une immigration clandestine, en provenance d'Afrique notamment.
Seul soulagement dans ce rapport, le Parlement semble avoir pris conscience que les régularisations en masse des immigrés entrés clandestinement sur le territoire de l'Union n'était pas une solution en soi et ne résolvait pas les problèmes. Alléluia! Il aura néanmoins fallu que l'Espagne, la Belgique, la France, l'Italie et les Pays-Bas recourent à cette dangereuse politique de régularisations, produisant inévitablement un phénomène dit «d'aspiration» et influençant, par là même, les flux migratoires de leurs voisins européens, pour que cette prise de conscience minimum ait enfin lieu.
Soyons positifs. C'est déjà un début, mais, afin de lutter efficacement contre l'immigration clandestine, une seule et première mesure est à prendre d'urgence: le rétablissement des contrôles aux frontières extérieures de l'Union. Ce n'est pas le gadget de Frontex, véritable coquille vide dotée de peu de matériel, de peu d'hommes et qui, par ailleurs, n'obtient pas le soutien de certains pays européens, soucieux de préserver leur souveraineté en matière de gestion de l'immigration, qui pourra sortir l'Europe de cette spirale infernale.
C'est l'Europe elle-même qui est à l'origine de cette immigration continue et exponentielle, en participant aux criminels accords de Schengen. Qu'elle les résilie, et vite! 
