Jean-Louis Bernié # Débats - Jeudi 30 novembre 2000 - VOTES
  Bernié (EDD
),
par écrit
. 
- Les catastrophes maritimes se suivent et se ressemblent ; après le naufrage de l’Erika et du Ievoli Sun, qui ont touché dernièrement les côtes françaises, il est temps que l’Union européenne réagisse et contrôle fermement les sociétés de classification autorisant la navigation des bateaux-poubelles. Dans ce contexte, une législation européenne s’impose.
Ce rapport visant à améliorer le contrôle des navires et à mettre en place des procédures et des règles d’inspection harmonisées doit donc être soutenu, car il représente une avancée. Même si, à notre avis, le rapporteur ne va pas encore assez loin dans la démarche.
En effet, s’il prévoit une harmonisation des critères de contrôle pour l’ensemble des pays membres, rien n’est envisagé pour les navires contrôlés hors Union européenne et provenant de pays tiers. Qu’est-ce qui empêchera, par exemple, un navire maltais ou chypriote en mauvais état de naviguer près de nos côtes ? Quand l’Union européenne prendra-t-elle enfin la décision d’interdire l’accès aux eaux territoriales à tout navire non conforme aux règles qu’elle édicte ?
L’Europe, d’habitude si prompte à légiférer en matière d’environnement, continuera-t-elle encore longtemps à faire l’impasse sur la sécurité maritime, parce que d’énormes intérêts financiers sont en jeu et que les intérêts de ses membres divergent ? Quand l’Union débloquera-t-elle les moyens humains et financiers à la hauteur de la tâche à accomplir, pour que ce texte ne reste pas une simple déclaration d’intention ?
Nous voterons donc ce rapport qui va dans le bon sens, tout en déplorant qu’il ne prévoie pas : de déterminer précisément la responsabilité des pollueurs (contrôleur, armateur, affréteur, assureur) ; des mesures préventives et répressives permettant de lutter contre les dégazages intensifs responsables de l’essentiel de la pollution maritime ; d’inventorier les navires-poubelles ; d’interdire l’accès aux eaux territoriales à tout navire présentant un danger manifeste, quel que soit son âge.
Réviser la directive 94/57/CE devient urgent, d’autant que Malte et Chypre, respectivement 4ème et 6ème flotte mondiale, sont candidats à l’adhésion.
Cette action communautaire doit être doublée d’une réactivation de l’OMI qui, dotée d’un véritable pouvoir de police pour faire appliquer le code ISM, serait un instrument efficace, à l’image d’Europol.
Nous espérons que le Conseil prendra rapidement ses responsabilités. Il en va de la qualité de notre environnement. 
