Chantal Cauquil # Débats - Jeudi 29 janvier 2004 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. 
- Comme d’habitude, le Parlement européen a discuté de l’avenir d’un secteur industriel sans s’occuper des travailleurs qui font marcher ce secteur, si ce n’est accessoirement.
Pour notre part, notre problème n’est pas de savoir si tel secteur est concurrentiel ou pas par rapport à des produits venus d’ailleurs, et encore moins de donner des conseils aux patrons des entreprises concernées pour qu’elles deviennent plus concurrentielles. Que l’industrie textile soit concurrentielle ou pas, il est révoltant que 850.000 emplois aient été supprimés!
Notre opposition à ce texte signifie que ce n’est pas l’industrie textile qu’il s’agit de protéger, mais ses travailleurs. De la même façon qu’il faut protéger les travailleurs des secteurs qui, pour être modernes comme l’aéronautique ou l’informatique, n’en licencient pas moins.
Nous sommes contre l’économie de marché stupide et contre la concurrence dont les propriétaires et actionnaires des entreprises font payer les frais aux seuls travailleurs. Car, dans cette concurrence-là, sont perdants aussi bien les travailleurs des pays pauvres, à qui on impose les bas salaires de la surexploitation, que les travailleurs des pays dits riches, jetés à la rue par leurs entreprises. À plus forte raison, nous sommes contre le fait que cette concurrence soit introduite entre les travailleurs de différents pays. 
