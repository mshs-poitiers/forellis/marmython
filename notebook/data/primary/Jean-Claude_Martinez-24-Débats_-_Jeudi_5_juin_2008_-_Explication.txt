Jean-Claude Martinez # Débats - Jeudi 5 juin 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Marie-Hélène Aubert (A6-0193/2008
) 
  Jean-Claude Martinez  (NI
), 
par écrit
. –
 Les pêcheurs de France, d'Espagne et d'Italie disent leur révolte légitime, leur droit de vivre de leur travail, leur indignation. Les paysans, d'ailleurs, ne disent pas autre chose.
Depuis un an, on sait que les prix du carburant étouffent économiquement nos pêcheurs. Ce choc s'ajoute à l'étranglement bureaucratique de nos pêcheurs à coup de quotas, de pénalités, de surveillances, d'inspections qui transforment la liberté de la Haute Mer en une Union soviétique liquide.
Face à la tragédie d'une profession, que propose-t-on à ces hommes, à ces régions, à ces villages, à ces économies locales? Rien! Sinon une vague aide transitoire, comme un soin palliatif.
Pire, le jour où les pêcheurs de la Méditerranée sont à Bruxelles pour crier leur droit de vivre, nous discutons ici de sanctions pénales de plus contre la pêche illégale.
Même la voie de la TVA et des accises à alléger est refusée aux pêcheurs.
La solution, c'est un fonds européen de compensation de la dette des pêcheurs afin de ne pas leur faire supporter la rupture de l'égalité devant les charges publiques, provoquée par la politique d'importation et la domination des géants de la distribution. 
