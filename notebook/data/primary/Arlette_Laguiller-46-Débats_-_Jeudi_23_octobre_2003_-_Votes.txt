Arlette Laguiller # Débats - Jeudi 23 octobre 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. - Ce rapport se veut la justification de la mise en place d’une politique étrangère et d’une force militaire communes en Europe, avec la prétention d’assurer la sécurité européenne.

Mais les seules guerres récentes où sont impliqués un ou plusieurs pays d’Europe, en Irak, en Afghanistan, en Côte-d’Ivoire, sont toutes des guerres de brigandage contre des pays dont il est ridicule d’affirmer qu’ils ont menacé la sécurité de l’Europe. Que les puissances européennes les mènent ensemble ou séparément, ces guerres ne sont nullement destinées à défendre la population de l’Union mais à assurer les intérêts de ses classes possédantes.
Par ailleurs, le rapport peut décliner le mot "multilatéralisme" dans toutes les langues européennes, il n’en reste pas moins que ce sont des criailleries de petites puissances impérialistes à qui la principale, les États-Unis, ne laisse pas d’autre choix que de s’aligner derrière Washington ou de se réfugier dans des bouderies impuissantes.
Le quasi-éclatement de l’Union européenne à propos de la guerre en Irak n’est pas accidentel. Il montre à quel point l’Union européenne est un assemblage, mal taillé, d’États aux intérêts contradictoires dans bien des domaines.
Nous rejetons "les choix fondamentaux" décrits par ce rapport, qui ne sont pas opérés en fonction des intérêts des peuples, mais contre eux. 
