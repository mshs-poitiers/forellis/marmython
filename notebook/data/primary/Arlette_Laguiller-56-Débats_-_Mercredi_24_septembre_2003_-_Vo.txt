Arlette Laguiller # Débats - Mercredi 24 septembre 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. - Au nom de prétendus principes écologiques érigés en dogmes placés au-dessus de la vie sociale réelle, ce rapport, parlant de la taxation des produits énergétiques et de l’électricité, ne précise pas quelle est la catégorie sociale frappée par cette taxation.
Pour notre part, nous ne verrions aucun inconvénient à ce qu’on augmente les prélèvements sur les bénéfices des grands groupes industriels qui sont les principaux pollueurs. Nous sommes, en revanche, tout à fait opposées à l’idée d’augmenter, pour les particuliers, la taxation sur la consommation de gazole. Cet impôt indirect frappe pour l’essentiel les couches populaires à qui, faute de transports collectifs convenables, on impose l’usage de voitures particulières.
Il est à plus forte raison inacceptable de taxer le fioul ou l’électricité à usage domestique. Voilà le sens que nous entendons donner à notre vote négatif sur l’ensemble de ce rapport. 
