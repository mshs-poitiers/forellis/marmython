Kader Arif # Débats - Mercredi 14 novembre 2007 - Renforcer la politique européenne de voisinage - Situation en Géorgie (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Kader Arif  (PSE
) 
. – (FR) 
Monsieur le Président, je concentrerai mon propos sur la région méditerranéenne, dont les rapporteurs – je les en remercie – rappellent à juste titre l'importance pour la politique extérieure de l'Europe. C'est parce que je soutiens un engagement européen fort dans cette région du monde que je souhaite mettre en garde contre les risques de dilution de la politique méditerranéenne de l'Union dans la politique européenne de voisinage.
Il ne faudrait pas qu'une concurrence s'installe entre les pays de l'est de l'Europe et nos voisins du Sud. La politique de voisinage se veut un complément bilatéral au processus multilatéral de Barcelone, dont il ne faut pas oublier qu'il constitue depuis 1995 le cadre de référence pour organiser les relations au sein de la Méditerranée. À ce titre, ni la politique européenne de voisinage, ni aucun autre projet en direction des pays méditerranéens, ne doit occulter ou remplacer les objectifs de Barcelone axés sur les trois piliers, politique, économique et social, seuls à même de favoriser une véritable intégration régionale.
Je tiens donc à souligner deux points. D'une part, l'équilibre de la répartition financière entre les pays de l'est de l'Europe et ceux de la Méditerranée doit être respecté. La préservation d'une politique européenne forte et ambitieuse pour la région méditerranéenne en dépend. D'autre part, en référence au futur projet de zone de libre-échange euroméditerranéenne, objet de mon rapport voté au Parlement cette année, je tiens à rappeler la nécessité d'une approche concertée et graduelle permettant à ces pays de maîtriser le rythme et l'intensité de l'ouverture commerciale en fonction de leurs spécificités, et notamment de la fragilité de certains secteurs de leur économie. L'objectif doit rester celui d'un commerce au service du développement.
Je souhaite, pour conclure, que ces éléments soient intégrés dans le rapport car ils sont indispensables à la définition d'une politique méditerranéenne claire et fondée sur une vision stratégique du long terme pour le développement et la stabilisation de cette région. 
