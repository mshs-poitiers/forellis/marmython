Sylviane Ainardi # Débats - Mardi 14 janvier 2003 - Votes
  Ainardi (GUE/NGL
), 
rapporteur
. - Monsieur le Président, pour des raisons techniques malencontreuses, les amendements que j'avais faits à mon rapport n'ont pas été déposés. Je voudrais d'emblée, avant d'en venir au vote amendement par amendement, dire que, pour des raisons de cohérence de l'ensemble du paquet, j'ai décidé d'en réintroduire certains sous la forme d'amendements oraux. Il y a deux amendements qui correspondent à la nouvelle rédaction qui est proposée pour l'article 3 du rapport sur l'Agence. Il y a un considérant qui fait référence à la modification de l'article 29 du rapport sur la sécurité. Il y a encore un autre amendement, qui est l'expression de ma volonté de poser la question du financement. Je souhaite avoir l'opinion de mes collègues. 
