Francis Wurtz # Débats - Lundi 10 mars 2008 - Ordre des travaux
  Francis Wurtz  (GUE/NGL
).  
– (FR) 
Monsieur le Président, ce qui vient de se passer et qui a fait rire certains collègues, est très grave. Il s’agit, mon cher collègue Joseph Daul, d’un jeune de 23 ans condamné à mort pour avoir téléchargé un texte sur le droit des femmes dans l’Islam, dans un pays qui a des relations étroites avec l’Union européenne, où des soldats de nos pays sont en train de défendre l’Afghanistan, avec une nouvelle constitution qui, officiellement, défend les droits fondamentaux et la liberté d’expression. Avec une voix, vous venez de prendre une responsabilité que, j’espère, vous n’aurez pas à regretter durement.
(Applaudissements)

