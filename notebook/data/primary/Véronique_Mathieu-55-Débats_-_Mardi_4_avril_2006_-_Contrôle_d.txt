Véronique Mathieu # Débats - Mardi 4 avril 2006 - Contrôle de l application du droit communautaire (2003 2004) - Mieux légiférer 2004: application du principe de subsidiarité - Mise en oeuvre conséquences et impact de la législation en vigueur sur le marché intérieur - Stratégie de simplification de l environnement réglementaire (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Véronique Mathieu (PPE-DE
). 
 - La législation européenne est trop complexe et parfois superflue: une rénovation intelligente doit permettre de lutter contre cette opacité malsaine et dommageable!
D’une part, il convient d’améliorer en réduisant et simplifiant la législation existante. Dans ce travail de simplification comme pour toute nouvelle mesure adoptée, il faut respecter les principes de proportionnalité et de subsidiarité: l’Union européenne là où il le faut, quand elle est plus efficace que les États agissant seuls, autant qu’il le faut, mais pas plus qu’il ne faut.
L’application de ces principes d’apparence technique implique de juger de leur opportunité sociale, culturelle, ou autre, alors même qu’aucun mécanisme de contrôle efficace de leur application n’existe! Cette lacune était comblée par le projet de Constitution. Dans l’attente de sa ratification, il faut s’interroger sur leur bonne application.
D’autre part, il convient d’améliorer le contrôle de la transposition du droit communautaire car l’insécurité juridique sape la compétitivité de nos entreprises. L’instauration de correspondants nationaux est une mesure positive si les contrôles englobent une analyse quantitative et qualitative et intègrent une analyse d’impact sur l’environnement social, économique et écologique. Ces analyses d’impact doivent pouvoir être comparées: elles doivent donc être uniformisées. Pour atteindre cet objectif, le Parlement européen doit renforcer son pouvoir dans ce domaine. 
