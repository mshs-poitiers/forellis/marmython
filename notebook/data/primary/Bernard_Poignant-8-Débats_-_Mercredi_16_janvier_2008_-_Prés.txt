Bernard Poignant # Débats - Mercredi 16 janvier 2008 - Présentation du programme de la présidence slovène (débat) 
  Bernard Poignant  (PSE
). – 
(FR)
 Monsieur le Président, vous vous présentez devant nous avec modestie. Vous avez même évoqué d'éventuelles erreurs, confessé une possible naïveté. Dans le cadre de la troïka, je vous suggère de transmettre au Président français qui vous succédera ce sens de l'humilité.
J'ai un seul message à vous transmettre: donnez à l'Europe les meilleures leçons de votre histoire. Vous êtes des Celtes, moi aussi! Vous avez été dominés par la Bavière, aspirés par la République de Venise, intégrés dans l'empire austro-hongrois, occupés par Napoléon – acceptez mes excuses –, partagés entre l'Allemagne, l'Italie et la Hongrie, intégrés dans la Fédération yougoslave et vous êtes indépendants depuis votre referendum de 1990.
Quelle histoire! Qui mieux que vous peut parler du dialogue interculturel? Qui mieux que vous a du crédit pour le faire? Donc, mettez plus haut cette priorité. Vous l'avez mise en quatrième position, je vous suggère de lui faire monter un ou deux escaliers!
Cela dit, ne limitez pas le dialogue interculturel au dialogue interreligieux. Pensez aussi à l'apport à l'Europe de ceux qui considèrent que la religion et la politique ne doivent pas être mêlées car l'histoire a montré que quand l'une s'occupe de l'autre, cela donne toujours de mauvais résultats.
Pour conclure, je voudrais, si vous le permettez, et toujours dans l'esprit de ce que je viens de dire, lire à nos collègues un petit passage de votre hymne national, car il est beau! "Vivent tous les peuples qui aspirent à voir le jour. Là où le soleil suit son cours, la querelle du monde sera bannie. Tout citoyen sera libre enfin, et pas un ennemi, et le frontalier, le frontalier sera son voisin". C'est cela l'esprit de votre présidence, c'est cela l'esprit de l'Europe. Alors, je suis Français, je ne vais pas lâcher ma "Marseillaise", mais pendant six mois, je veux bien être Slovène!
(Applaudissements)

