Marie-Anne Isler-Béguin # Débats - Lundi 22 octobre 2007 - Directive-cadre sur l'utilisation durable des pesticides - Stratégie thématique concernant l'utilisation durable des pesticides - Mise sur le marché des produits phytopharmaceutiques (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie Anne Isler Béguin  (Verts/ALE
) 
. –Monsieur le Président, je me permets d'intervenir parce que j'ai du mal à comprendre. J'ai participé aux discussions à la commission de l'environnement sur ces différents rapports mais j'ai eu l'impression que le rapport que nous a présenté Mme Klass n'était pas le rapport qui a été voté à l'unanimité par la commission mais que c'était plutôt sa position personnelle. Donc, je voulais savoir si, effectivement, il y avait une présentation plus objective de la position de la commission de l'environnement sur ce rapport très important. 
