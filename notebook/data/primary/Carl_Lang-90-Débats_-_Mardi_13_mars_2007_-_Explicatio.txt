Carl Lang # Débats - Mardi 13 mars 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Howitt (A6-0471/2006

) 
  Carl Lang (ITS
), 
par écrit
. - La responsabilité sociale des entreprises est un concept fourre-tout qui malgré l’imagination des européistes n’a rien d’original. Ce gadget non contraignant apparaît aux États-Unis dès les années 1950. En France, la notion «d’entreprise citoyenne» fait son apparition notamment avec le rapport Sudreau en 1975. Et, en 1982, on imposera des objectifs sociaux aux entreprises du secteur concurrentiel public pour épater le peuple et les cadres du socialisme triomphant.
Vingt-cinq années plus tard, la RSE voudrait tout simplement humaniser et réguler le mondialisme. Ces vœux pieux en des temps de chômage et de précarité sociale sur fond d’instabilité internationale donnent à la RSE l’image d’une vaste fumisterie. Il faut clairement arrêter de perdre notre temps et notre argent pour des considérations ne cherchant qu’à faire passer des messages bobos et gentils alors que nous sommes à la traîne du monde.
Soyons responsables d’abord des nôtres. Respectons-nous en appliquant en Europe une préférence et une protection communautaire des personnes, des productions et des entreprises. Renforçons par exemple les droits de douanes sur les importations de produits des pays tiers qui n’auront pas été fabriqués selon des normes minimales sociales européennes. 
