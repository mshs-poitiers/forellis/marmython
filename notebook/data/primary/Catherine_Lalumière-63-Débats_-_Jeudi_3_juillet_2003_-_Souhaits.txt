Catherine Lalumière # Débats - Jeudi 3 juillet 2003 - Souhaits de bienvenue
  La Présidente.  
- Mes chers collègues, je voudrais, en mon nom personnel et en votre nom à tous, saluer la présence à la tribune officielle des membres de la délégation du parlement kazakh participant aux travaux de la quatrième réunion de la commission parlementaire de coopération Union européenne/République du Kazakhstan.
(Applaudissements)

Cette délégation est composée de huit membres et est présidée par M. le vice-président du parlement kazakh.
(Applaudissements)

