Bruno Gollnisch # Débats - Mercredi 11 mars 2009 - Préparation du Conseil européen (19-20 mars 2009) - Plan européen de relance économique - Lignes directrices pour les politiques de l’emploi des États membres - Politique de cohésion: investir dans l’économie réelle (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bruno Gollnisch  (NI
). 
 – Monsieur le Président, mes chers collègues, c’est dans cette période de crise que se révèlent la valeur et l’utilité des structures, et cette crise montre que l’Europe de Bruxelles ne sert à rien. Le plan de relance, pompeusement qualifié d’européen, est en fait l’addition des financements décidés par les États membres. La contribution du budget européen n’en représente qu’une très faible partie.
Quand on consacre 200 milliards d’euros de soutien à l’économie réelle et à l’emploi, on en donne 2 000 aux banques, sans garantie que celles-ci les utiliseront pour financer les entreprises et les particuliers. Privatisation des profits, socialisation des pertes, tel est le dernier mot de ces politiques économiques, qu’elles soient libérales ou socialistes.
La solidarité européenne ou le soutien aux États? Le sommet informel du 1er mars a refusé en bloc la conditionnalité des aides au secteur automobile, au nom du marché et de la concurrence. Aucun changement de politique, aucun changement de logique, aucune rupture avec le système qui nous a menés à la catastrophe! Nous sommes au bord du gouffre et, dans quelques jours, les chefs d’État et de gouvernement vont nous proposer de faire un grand pas en avant. 
