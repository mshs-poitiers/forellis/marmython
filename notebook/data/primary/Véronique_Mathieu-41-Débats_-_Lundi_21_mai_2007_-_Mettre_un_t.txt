Véronique Mathieu # Débats - Lundi 21 mai 2007 - Mettre un terme à l’appauvrissement de la biodiversité d’ici 2010 (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Véronique Mathieu (PPE-DE
), 
par écrit
. - La biodiversité, plus qu’une priorité, est une nécessité et il convient d’agir rapidement afin d’enrayer la diminution de la biodiversité d’ici 2010.
L’Union européenne est la première à se fixer de véritables objectifs dans ce domaine, souhaitons qu’elle donne l’exemple au reste du monde. Le développement durable, la chasse durable, ne sont pas seulement des termes qui ont le vent en poupe, mais ceux sont bien le symbole d’une évolution des pratiques industrielles, de production et du monde cynégétique.
Les chasseurs et les organisations cynégétiques n’ont d’ailleurs pas attendu l’Union européenne, les parlementaires et encore moins ce rapport pour se fixer des obligations en matière de respect des espèces et espaces; notamment grâce aux fondations de protection des habitats et de la faune sauvage qui mènent des actions efficaces depuis déjà plusieurs années.
C’est pourquoi, je tiens à ce que la chasse ne soit pas stigmatisée, mais au contraire soutenue dans ses efforts de bonne gestion environnementale.
Je ne peux, à ce titre, que regretter la formulation de l’article vingt, qui rend la chasse en partie responsable de la dégradation de la biodiversité, ne prenant ainsi pas en compte l’existence d’une chasse durable. 
