Kader Arif # Débats - Mercredi 29 novembre 2006 - Instrument financier européen pour la promotion de la démocratie et des droits de l’homme dans le monde (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Kader Arif (PSE
). 
 - Monsieur le Président, Madame le Commissaire, chers collègues, il faut tirer fierté d’être membre de l’Union européenne quand on voit son action en matière de promotion de la démocratie et des droits de l’homme. Ces valeurs communes font de l’Union un acteur primordial sur la scène internationale autour de ces questions. Il était indispensable, dans ce combat permanent, de réformer l’ancien instrument pour lui donner une plus grande efficacité. C’est grâce à la pugnacité des rapporteurs, Mme Flautre et M. McMillan-Scott, que je félicite, que nous avons devant nous un texte présentant de réelles avancées.
S’agissant d’un instrument important, il se devait d’être ambitieux. C’est le cas, précisément, des références aux droits des migrants et à la possibilité offerte de passer outre l’aval des pays tiers pour promouvoir démocratie et droits de l’homme. Je regrette cependant que les négociations n’aient pas permis d’étendre le champ d’application de cet instrument à la prévention des conflits. En outre, malgré une implication plus forte du Parlement dans le suivi, le refus du Conseil de formaliser les relations entre les deux institutions sur ce sujet est un manque.
Pour conclure, l’efficacité se jouera sur une évaluation régulière de cet instrument et sur sa révision: le Parlement devra y prendre toute sa place car je crois que c’est un combat de l’Europe des valeurs pour un monde sans peur. 
