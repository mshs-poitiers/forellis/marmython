Véronique Mathieu # Débats - Jeudi 22 mai 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
– Proposition de résolution: Hausse des prix des produits alimentaires dans l'Union européenne et dans les pays en développement (B6-0217/2008
) 
  Véronique Mathieu  (PPE-DE
), 
par écrit
. – 
À une heure où la flambée des prix des produits alimentaires est des plus préoccupantes, une résolution du Parlement européen apparaît plus que jamais nécessaire.
D'une part, la pénurie alimentaire qui sévit dans de nombreux pays du monde est proprement inacceptable. Des mesures d'urgence doivent être proposées pour mettre un terme à la famine qui affecte, en ce moment même, les populations les plus vulnérables des pays en développement. Outre ces mesures d'urgence, qui ne permettront pas d'apporter une solution à long terme au problème, l'Union européenne doit mettre en place une stratégie, en accord avec les organisations internationales (notamment la Banque mondiale, le FMI, la FAO et l'OMC). Une stratégie de fond qui devra intégrer l'ensemble des causes de la crise: évolution des habitudes alimentaires en Asie, essor rapide des cultures de biocarburants, etc.
D'autre part, au sein de l'Union européenne, la hausse inquiétante du prix du panier du consommateur offre l'occasion d'une réflexion approfondie sur la réforme de la politique agricole commune. À l'heure du bilan de santé, des propositions concrètes doivent être faites pour mettre un terme à cette inflation au sein même du marché intérieur, mais aussi pour assurer l'autosuffisance alimentaire de l'Union européenne. 
