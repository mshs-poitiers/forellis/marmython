Anne Laperrouze # Débats - Mardi 13 juin 2006 - Activités de recherche, de développement technologique et de démonstration (2007-2013, FP7) ***I - Activités de recherche et de formation en matière nucléaire (2007-2011) * (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Anne Laperrouze (ALDE
). 
 - Monsieur le Président, ma contribution au débat relatif à ce septième PCRD portera sur deux sujets: la question des cellules souches et le budget.
La recherche dans le domaine des cellules souches embryonnaires a permis à la médecine de faire un grand bond en avant. Ces recherches sont porteuses de beaucoup d’espoir, notamment pour le traitement des maladies génétiques jusqu’alors réputées incurables ainsi que du cancer ou pour la mise au point de médicaments.
Dans ce domaine, l’Union européenne doit avancer, prudemment mais sûrement. C’est pour cette raison qu’elle a mis en place le système de contrôle et d’évaluation le plus poussé au monde: analyses par des experts indépendants, analyses par des comités d’éthique nationaux et européens, accord du comité de programmation, sans oublier l’obligation de respecter le cadre légal des États membres dans lesquels est effectuée la recherche.
Le financement de la recherche sur les cellules souches embryonnaires est indispensable à plusieurs titres: pour contrôler les activités dans ce domaine, pour respecter nos valeurs éthiques, pour permettre et protéger les avancées scientifiques des chercheurs européens, pour rester en compétition avec les pays tiers. C’est pourquoi c’est l’amendement adopté par la commission de l’industrie qui doit être voté jeudi prochain.
En ce qui concerne le budget, je ne peux, comme mes prédécesseurs, qu’en déplorer la faiblesse. Néanmoins, je mets en garde contre la tentation d’aller puiser dans des thématiques telles que l’agriculture et l’énergie, qui ont besoin de financement, alors que d’autres secteurs semblent être dotés trop généreusement. À cet égard, il me semble impératif de tenir un débat sur la dotation du Centre commun de recherche.
En conclusion, tout en saluant l’excellent travail de notre rapporteur, je tiens à souligner l’importance déterminante de ce programme. Mettre en place un espace européen de la recherche, c’est contrer le risque que les auteurs de projets innovants aillent chercher ailleurs les sources de financement, c’est créer un électrochoc de croissance et de compétitivité.
Le septième PCRD constitue l’un des moyens qui permet à l’Europe d’avancer et à nos concitoyens de bénéficier, dans la vie de tous les jours, des retombées de la recherche européenne. 
