Marine Le Pen # Débats - Mercredi 14 juin 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Brepoels (A6-0124/2006
) 
  Marine Le Pen (NI
), 
par écrit
. - Depuis sa création en 1993, les principales tâches de l’Observatoire des drogues et des toxicomanies sont la collecte, l’analyse et la diffusion de données transnationales sur les problèmes de la drogue.
Treize ans plus tard, le bilan n’est pas glorieux. Si on constate une certaine amélioration de la connaissance du phénomène de la drogue, dans ses aspects les plus divers sur le territoire de l’Union européenne, en revanche, aucune incidence positive sur la consommation n’est à noter. Et bien au contraire!
Un constat est à faire. Aujourd’hui, la consommation de drogues ne se limite plus aux produits traditionnels, mais nous assistons avec inquiétude à l’émergence de nouveaux marchés pour les produits de synthèse, à la production plus facile et moins chère. Ils génèrent plus de bénéfices pour les trafiquants et sont par ailleurs encore plus nocifs pour la santé des consommateurs.
L’Observatoire des drogues et des toxicomanies en réalité ne constitue qu’une simple base de données dépourvue de toute valeur opérationnelle. En fait, tous les chiffres fournis ne servent qu’à couvrir l’échec des différentes politiques nationales en matière de lutte contre les drogues. Il est grand temps que les États prennent leur responsabilité et arrêtent de s’abriter derrière des organismes qui n’ont ni la vocation, ni la mission d’agir pour eux. 
