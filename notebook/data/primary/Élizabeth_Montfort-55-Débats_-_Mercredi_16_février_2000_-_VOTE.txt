Élizabeth Montfort # Débats - Mercredi 16 février 2000 - VOTES (suite) 
  Montfort (UEN
),
par écrit
. - Il est heureux que le Parlement européen poursuive ses initiatives 
en 
matière 
de lutte contre les discriminations et les inégalités, et les quatre domaines retenus par le programme EQUAL - la capacité d'insertion professionnelle, l'esprit d'entreprise, la capacité d'adaptation et l'égalité des chances pour les hommes et les femmes - paraissent d'une réelle pertinence.
Mais le présent rapport, probablement animé des meilleures intentions, prend ça et là une direction qui me semble fort périlleuse en ce qu'elle tend à gommer les notions de citoyenneté et de légalité 
et fait d'une multiplicité d'exceptions le droit commun sur lequel se construit la réflexion communautaire.
En effet, si l'on ne peut qu'approuver la décision d'adopter des mesures spécifiques pour éradiquer toute forme de discrimination sur le marché du travail susceptible de frapper les demandeurs d'asile, déjà - par définition - durement frappés par le destin, encore faut-il conserver de cette notion une conception légaliste : en clair, s'il y a toute raison de venir en aide aux demandeurs d'asile dûment reconnus et en situation régulière, il en va tout autrement des nombreuses catégories énumérées par le rapport - demandeurs en examen, sous protection temporaire, exclus du statut et menacés d'expulsion - pour lesquelles ce rapport prévoit une assistance identique.
Or les mesures d'aide qui sont prévues par le rapport risquent, par capillarité, de connaître une néfaste extension : demandeurs d'asile, bien sûr, puis demandeurs d'asile en instance de jugement sous protection temporaire, enfin demandeurs dont la requête a été rejetée et menaçables d'expulsion, devraient pouvoir, selon le rapport, profiter de ce train de mesures alors que leur nature même - qu'est ce qu'un demandeur d'asile dont le statut de réfugié est refusé et qui est menacé d'expulsion sinon un immigré en situation irrégulière ? - devrait, au moins pour les derniers d'entre eux, les exclure d'une politique d'intégration dans une communauté qui ne reconnaît pas comme légitime et régulière leur présence en son sein.
Un tel signal constituerait, volontairement ou pas, un formidable appel à une immigration dont la seule demande du statut de réfugié, 
accordé ou pas, suffirait à justifier une palette de mesures d'aide devant conduire à son intégration durable dans le monde du travail, dans un environnement général fait de pénurie et dans lequel les populations régulières, étrangères ou autochtones, sont cruellement confrontées au problème du chômage.
En conséquence, et malgré ses orientations générales qui ne peuvent qu'être partagées, il n'était pas envisageable que je m'associe au vote d'un rapport qui porte en lui le germe d'une nouvelle atteinte aux notions de citoyenneté et de légalité républicaine, auxquelles je suis particulièrement attachée. 
