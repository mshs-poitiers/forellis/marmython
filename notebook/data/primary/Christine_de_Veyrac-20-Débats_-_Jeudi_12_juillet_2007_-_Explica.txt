Christine de Veyrac # Débats - Jeudi 12 juillet 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Piecyk (A6-0235/2007
) 
  Christine De Veyrac (PPE-DE
), 
par écrit. - 
Je soutiens pleinement le rapport de mon collègue Willi Piecyck.
Ce texte, qui est la réponse du Parlement européen au livre vert de la Commission sur la politique maritime de l’Union européenne, aborde de nombreux points importants.
Il s’agit notamment du défi que constitue le changement climatique pour la politique maritime: la politique européenne de la mer doit jouer un rôle important dans ce domaine en s’appuyant sur la réduction des émissions de CO2 des navires, l’intégration éventuelle des navires dans le système des échanges de quotas d’émissions et la promotion des énergies renouvelables.
Trouver un équilibre durable entre la protection de l’environnement et l’exploitation commerciale des océans européens est indispensable. Par ailleurs, j’estime, comme nous l’avons voté, que la Commission doit renforcer toutes les mesures relatives à la responsabilité civile et pénale en cas d’avarie ou d’incident et redoubler d’attention quant à l’application des règles rendant la double coque obligatoire. 
