Nicole Thomas-Mauro # Débats - Mercredi 4 septembre 2002 - Votes
  Thomas-Mauro (NI
),
par écrit
. - Les États, et en particulier la France, qui ont souverainement légiféré pour réglementer les ventes à perte et les soldes, l'ont fait avec l'objectif clairement affiché de préserver l'équilibre entre les producteurs, les distributeurs et les consommateurs. C'est l'intérêt de chacun d'entre eux que les parlements nationaux ont cherché à protéger.
Aussi, la volonté de la Commission de lever les restrictions aux promotions des ventes, au motif d'un renforcement de l'harmonisation économique, menace-t-elle directement les intérêts et l'équilibre des acteurs économiques.
C'est aussi, au motif d'un intérêt du consommateur mal compris, l'image de nos produits de grandes marques qui risque de pâtir de la politique en avant de certains distributeurs peu scrupuleux des producteurs - et de leurs propres contraintes économiques - sur lesquels ils prospèrent.
Les amendements adoptés par les commissions parlementaires, dont ceux que j'ai eu l'occasion de présenter, ont heureusement permis de restaurer le respect de nos législations nationales et ce n'est qu'à la condition de leur adoption par le Parlement que je soutiendrai ce texte. 
