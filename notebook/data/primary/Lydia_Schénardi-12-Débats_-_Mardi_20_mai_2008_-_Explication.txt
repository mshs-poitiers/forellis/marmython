Lydia Schénardi # Débats - Mardi 20 mai 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Elizabeth Lynne (A6-0159/2008
) 
  Lydia Schenardi  (NI
), 
par écrit
. –
 (FR) 
Il existe, actuellement, pas moins de cinq directives relatives à l'égalité des chances et à la non-discrimination dans l'Union européenne. Vingt-huit actions en infraction sont en cours contre des États membres qui ne les ont pas transposées. On ne peut que le déplorer.
Cependant, doit-on systématiquement imposer l'égalité des genres par la contrainte et par la répression?
Je ne le crois pas, bien au contraire. Cessons de stigmatiser ce discours sur les discriminations en qualifiant de «gentils» les groupes et populations minoritaires, et notamment les immigrés, et de culpabiliser les Européens qui ne cessent de pratiquer la discrimination.
Il faut en finir avec ces refrains gauchistes qui ne servent en rien la cause des personnes en situation de discrimination, mais qui bien au contraire s'en trouvent par là même stigmatisées.
Mettons plutôt l'accent sur la responsabilité personnelle de chacun afin de faire cesser les discriminations quelles qu'elles soient et sur la nécessité, notamment pour les immigrants, de s'adapter à nos règles, à nos lois et à nos valeurs. 
