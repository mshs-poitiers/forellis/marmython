Bruno Gollnisch # Débats - Mercredi 9 juillet 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Jean Lambert (A6-0209/2008
) 
  Bruno Gollnisch  (NI
), 
par écrit
. –
 J'ai deux remarques sur le rapport Lambert, et le règlement qu'il amende.
1. Bien que le rapporteur s'en défende, le projet de règlement anticipe sur la libre circulation, et la liberté d'installation et d'accès au marché du travail des ressortissants des pays tiers sur l'ensemble du territoire de l'Union européenne, toutes choses, faut-il le rappeler, qui ne sont pas encore, heureusement, devenues des réalités. Il participe un peu plus du dépouillement des États membres de leurs prérogatives en matière de politique d'immigration, c'est-à-dire de leur droit souverain de choisir et de contrôler l'entrée, la résidence, et l'étendue des droits des étrangers sur leur territoire.
2. Il semble normal de faire bénéficier les citoyens des États membres de l'UE d'une coordination des systèmes de sécurité sociale, et de garantir que la protection sociale qu'ils sont en droit d'attendre (en raison de leur travail et de leurs cotisations) ne souffre pas de la mobilité "internationale" à laquelle on les incite. Mais vouloir à tout prix instaurer en la matière une totale égalité de traitement entre les citoyens européens et les autres, sans jamais se préoccuper de réciprocité, ne fait que renforcer l'incitation à l'immigration que représente déjà l'immense, aveugle et suicidaire générosité de nos systèmes sociaux. 
