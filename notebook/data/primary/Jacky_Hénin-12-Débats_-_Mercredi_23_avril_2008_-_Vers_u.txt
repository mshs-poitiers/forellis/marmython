Jacky Hénin # Débats - Mercredi 23 avril 2008 - Vers une réforme de l'Organisation mondiale du commerce (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jacky Hénin, 
au nom du groupe GUE/NGL
. –
 Madame la Présidente, la seule chose sur laquelle nous serons d'accord, c'est la nécessité urgente de révolutionner l'OMC.
L'OMC, comme le FMI, est une organisation illégitime, antidémocratique et dangereuse pour les intérêts des peuples. Elle a été fondée en son temps pour assurer l'hégémonie financière et industrielle des États-Unis et des grands groupes transnationaux.
L'Union s'est, bien évidemment, servilement mise au service de ce système, en espérant recueillir quelques miettes du festin du maître américain. Le libre-échange débridé s'est aujourd'hui retourné contre ses initiateurs et le centre de gravité économique de la planète a basculé de l'Occident vers l'Asie, produisant la plus terrible des crises financières et alimentaires qu'ait connue notre planète.
On voit mal des pays qui, hier, étaient classés parmi les émergents, devenus à leur tour prédateurs, renoncer à leur stratégie de prédation au nom d'on ne sait quelle bienveillance révélée, toute l'organisation même du commerce international construite autour de l'OMC les encourageant à persévérer dans cette voie. La règle connue de tous les joueurs est de s'enrichir le plus vite possible, tous les moyens étant bons, y compris en spéculant sur les médicaments ou les produits alimentaires de première nécessité.
Dans l'Union, les inégalités ont explosé et les classes populaires et moyennes sont en voie de paupérisation. La crise alimentaire qui frappe les populations les plus pauvres est une conséquence directe de la politique de l'OMC visant à détruire les cultures vivrières au profit de cultures d'exportation. Les biocarburants ne sont que le bouc émissaire facile d'un système mercantile à révolutionner au plus vite.
Je tiens, ici, à dénoncer les propos irresponsables de M. Mandelson, appelant encore à plus déréglementer les marchés agricoles au moment où le programme alimentaire mondial pointe l'envolée des prix alimentaires et déclare qu'il s'agit, je cite, d'un "tsunami silencieux qui menace d'entraîner dans la famine des dizaines de millions de personnes supplémentaires." M. Mandelson tient-il donc à entrer dans le livre d'histoire sous l'épithète infâmante d'affameur?
Oui, l'OMC doit être révolutionnée pour brider les comportements spéculatifs, favoriser les producteurs et non une minorité de profiteurs au service de la finance mondiale, pour encourager la souveraineté alimentaire et industrielle des peuples et inciter les nations à la coopération et non à la concurrence. 
