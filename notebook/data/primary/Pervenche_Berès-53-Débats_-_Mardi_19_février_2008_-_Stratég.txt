Pervenche Berès # Débats - Mardi 19 février 2008 - Stratégie de Lisbonne - Les lignes directrices intégrées pour la croissance et l'emploi (Partie: grandes orientations des politiques économiques des États membres et de la Communauté): lancement du nouveau cycle (2008-2010) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Pervenche Berès  (PSE
). – 
(FR) 
Monsieur le Président, je crois que, cette année, notre débat a une certaine importance puisque nous révisons le cycle de Lisbonne.
Je comprends, finalement, la stratégie de la Commission qui consiste à dire, d'une certaine manière, tout va très bien, Madame la Marquise, ne changeons rien puisque les fondamentaux de l'économie européenne sont bons. Vous le faites parce que, par rapport à la situation de l'économie américaine, c'est vrai, nos fondamentaux sont bien meilleurs. Pour autant, j'ajoute à cela que négocier à vingt-sept une redéfinition des lignes directrices doit être relativement difficile.
Mais il ne suffit pas de s'arrêter à ce constat. La situation de l'économie américaine aura un impact sur la situation de l'économie de l'ensemble des pays de l'Union européenne et de la zone euro, peut–être, en particulier. De la même manière, puisqu'en mars dernier, les chefs d'État et de gouvernement ont adopté de nouveaux objectifs légitimes, stratégiques, en termes d'environnement et d'énergie, nous devons en tenir compte. Enfin, chacun mesure l'impact sur les marchés financiers et la répercussion sur l'économie réelle de la situation de turbulence, pour le moins, qui existe sur les marchés financiers.
Alors, nous demandons que ces trois éléments soient actés dans une réécriture des lignes directrices. Monsieur Turk, lorsque nous vous avions vu à Ljubljana en novembre dernier, vous nous aviez dit: "Mais dites-nous ce que le Parlement européen veut". Eh bien, nous vous le disons, nous voulons que la coordination des politiques économiques, nous voulons que la question du changement climatique, nous voulons que la question de la supervision des marchés financiers soient mieux prises en compte dans ces lignes directrices. Si un effort peut encore être fait dans ce sens-là, nous le saluerons comme un progrès dans une meilleure capacité de coordination des politiques économiques, et donc d'utilisation des lignes directrices.
Mais, Monsieur le Commissaire, ne croyez pas que nous nous arrêterons là! Nous vous demanderons aussi qu'il y ait de la cohérence entre ces lignes directrices et tous les autres outils qui sont à la disposition de la Commission pour que cette stratégie, que nous définissons ensemble, puisse être mise en œuvre à travers les outils dont dispose l'Union européenne dans ce domaine. 
