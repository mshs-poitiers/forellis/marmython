Charles Pasqua # Débats - Jeudi 1 avril 2004 - Votes
  Pasqua (UEN
),
par écrit
. 
- Si le système du FED a vécu jusqu’à aujourd’hui sur un mode singulier, c’est parce que les relations tissées par certains États membres de l’Union - et par certains seulement - avec les pays en développement, et particulièrement l’Afrique, sont elles-mêmes singulières.
Plutôt que de les suspecter, l’Union européenne devrait au contraire se féliciter de cette permanence de relations privilégiées malgré le développement, ces dernières années, d’un prisme communautaire qui, pour cause d’élargissement, a focalisé ses efforts en direction de l’Est au détriment du Sud.
S’il faut rationaliser et simplifier les procédures, la réforme du système au profit d’une communautarisation et d’une budgétisation totale de l’aide fait émerger deux injustices: celle de voir le niveau global de financement baisser, en supprimant le facteur psychologique des contributions volontaires qui incitait généralement à une générosité maximale; celle aussi, en délivrant le label "européen" à une grosse part de la politique de coopération, de réduire la visibilité de l’action - et donc l’influence - des principaux donateurs sur la zone concernée.
En somme, comme toujours, le nivellement se fera par le bas, conférant aux plus retors un paravent immérité et décourageant les pays les plus généreux, condamnés à voir leurs efforts masqués par un anonymat injuste et contre-productif. 
