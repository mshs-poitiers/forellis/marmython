Jacques Toubon # Débats - Mercredi 14 novembre 2007 -  Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport de Mme  Gutiérrez-Cortines (A6-0410/2007
) 
  Jacques Toubon  (PPE-DE
), 
par écrit
. – (FR) 
Conformément au vote de la commission des affaires juridiques, je considère que la Commission outrepasse sa mission et qu'il n'est pas nécessaire que l'Union européenne impose dans ce domaine de nouvelles directives aux États membres. Cela relève de la compétence nationale. Il me paraît artificiel de vouloir appliquer des prescriptions analogues dans des pays qui sont marquées par des traditions juridiques et des situations environnementales extrêmement diverses. Il est donc nécessaire que la Commission reprenne l'examen de sa proposition et fasse apparaître, de manière détaillée, quelles sont les situations qui appellent une législation communautaire. 
