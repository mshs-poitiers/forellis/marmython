Chantal Cauquil # Débats - Mercredi 14 janvier 2004 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. 
- Nous avons voté contre ce rapport d’un cynique paternalisme à l’égard des pays africains. Comment oser, par exemple, affirmer que ces pays "doivent se donner des moyens pour mettre en place une stratégie de développement durable, équitable et viable, ouvrant la voie vers la réalisation des droits à l’alimentation, à la santé, à l’éducation, au logement et aux autres besoins des populations africaines"?
 Même dans les riches pays d’occident, la classe dominante prive de ces droits une partie importante de la population!
Mais les populations africaines ont à supporter, en plus des prélèvements de leur propre classe privilégiée, ceux autrement plus considérables des groupes industriels et financiers des pays impérialistes. Depuis le commerce des esclaves jusqu’à aujourd’hui, où ces pays sont saignés par le système bancaire mondial, en passant par le pillage colonial, le capitalisme occidental n’a jamais cessé d’appauvrir ce continent.
Et, aujourd’hui, le Parlement européen, ce parlement des pays dont la couche dirigeante est coupable d’avoir réduit les pays africains à la misère, lâche majestueusement que ces pays devraient réparer eux-mêmes les dégâts qui lui ont été causés à travers les siècles! C’est odieux. 
