Carl Lang # Débats - Jeudi 12 mars 2009 - Explications de vote (suite) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Proposition de résolution (RC-B6-0135/2009
) 
  Carl Lang  (NI
), 
par écrit
. – 
Les bons sentiments affichés par les différents groupes politiques, à l’exception notable des communistes (et pour cause!), n’est que le reflet du politiquement correct des hippies du show-business international. La cause tibétaine, dont la mainmise des bobos européens en mal de spiritualité a empoisonné le véritable combat de libération, est l’exemple même de ce qu’il ne faut pas faire en termes de politique intérieure et internationale.
Des députés veulent condamner avec force politesse les exactions communistes chinoises tout en se déclarant favorable à l’autonomie d’une région qui n’est pas le Tibet historique. L’idée d’autonomie du Tibet, la voie «Save Tibet», est la laisse agitée sous le nez d’une élite impuissante et d’un peuple assassiné, tant dans son âme que dans sa chair.
Le Tibet, comme d’autres nations opprimées, est l’exemple de ce qui arrive lorsque la dictature communiste s’instaure et que l’arme de l’immigration invasion est utilisée pour empêcher tout retour en arrière, politique, ethnique, culturel et spirituel.
Le Tibet a sans douté raté sa chance de retrouver sa souveraineté en ne maintenant pas la lutte armée depuis l’exil de son chef. La route à suivre était celle du combat pour l’indépendance, «Free Tibet», et non celle d’un esclavage perpétué au sein d’une «autonomie» sur le papier. 
