Gérard Caudron # Débats - Mercredi 25 septembre 2002 - Votes
  Caudron (GUE/NGL
),
par écrit
. - 
À l'heure du bilan, après cinq ans de mise en œuvre de la stratégie européenne pour l'emploi, et avant l'adoption finale des lignes directrices pour l'emploi pour 2003, il est important de rappeler que le chômage demeure un fléau, et qu'il est la principale cause d'exclusion sociale. C'est pourquoi plusieurs exigences s'imposent : demander à la Commission d'analyser les causes de la faible croissance économique dans l'Union et présenter des propositions en vue d'encourager une croissance réelle et significative pour accroître l'offre de postes de travail.
Au niveau de la méthode, il s'agit de renforcer le rôle des parlements nationaux dans le processus d'élaboration des plans d'action nationaux, d'accorder un rôle au Parlement européen, dans le cadre de la procédure de codécision, dans les questions relatives à l'emploi relevant de la méthode ouverte de coordination, sans oublier d'associer plus étroitement les partenaires sociaux.
Sur le fond, il convient de mettre l'accent sur le développement des marchés locaux et régionaux du travail afin de donner un nouvel élan aux initiatives destinées à attirer les investissements productifs et d'encourager l'équilibre économique, la cohésion sociale et la création d'emplois dans les zones et les régions les moins développées. Une meilleure intégration des immigrants légaux sur le marché du travail doit également être prise en considération. 
