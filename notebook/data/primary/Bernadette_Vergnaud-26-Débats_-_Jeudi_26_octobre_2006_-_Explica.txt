Bernadette Vergnaud # Débats - Jeudi 26 octobre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Schroedter (A6-0308/2006
) 
  Bernadette Vergnaud (PSE
), 
par écrit
. - C’est un constat: la directive sur le détachement des travailleurs, mal appliquée dans certains États membres, ne remplit pas ses objectifs. Cela est dû aux différences d’interprétation de certains concepts clés (le travailleur, le salaire minimum et la sous-traitance), aux difficultés de procéder au contrôle du respect de la directive et d’accéder aux informations, tant pour les travailleurs que pour les PME.
Un système efficace de coopération entre les États membres nécessite de renforcer la participation des partenaires sociaux, d’informer les travailleurs détachés de leurs droits respectifs et les entreprises, plus particulièrement les PME et les entreprises artisanales, des interlocuteurs à contacter.
Enfin, il est désormais impératif que la Commission européenne examine des solutions constructives susceptibles de prévenir et d’éliminer la concurrence déloyale, illustrée par les sociétés «boîtes aux lettres» ou encore les doubles détachements d’un État à l’autre, ainsi que le dumping social découlant du détachement abusif de travailleurs, notamment par le biais des «faux travailleurs indépendants».
Il ne s’agit pas de modifier l’acquis de la directive, mais de l’améliorer. C’est pourquoi, j’ai voté en faveur du rapport d’initiative de Madame Schroedter. 
