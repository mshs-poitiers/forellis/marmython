Joseph Daul # Débats - Mercredi 5 avril 2006 - Mesures exceptionnelles de soutien du marché (secteur avicole) (débat) 
  Joseph Daul (PPE-DE
). 
 - Monsieur le Président, je vais être très rapide ce soir pour essayer de gagner du temps. Je voudrais simplement remercier la Commission d’avoir agi rapidement la semaine dernière sur la proposition du Conseil. Le Parlement a démontré qu’il était aussi efficace et, si toutes les institutions s’y mettent, il est clair que nous pouvons prendre les bonnes décisions en huit jours, pour répondre à des catastrophes naturelles, et venir en aide à leurs victimes.
Pour le reste, je suis d’accord avec l’ensemble de mes collègues de la commission de l’agriculture et nous avons déjà beaucoup parlé de la grippe aviaire. Nous avons été, pour une fois, rapides et l’on ne peut pas dire que ce soit à cause de l’Europe que les éleveurs devront attendre pour toucher des indemnités. 
