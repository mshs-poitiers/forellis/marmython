Charles Pasqua # Débats - Jeudi 29 janvier 2004 - Votes
  Pasqua (UEN
),
par écrit
. 
- Chaque fois que le Parlement présente un rapport sur la politique extérieure de l’Union, la majorité fédéraliste de notre assemblée ne peut résister à la tentation d’introduire des considérations totalement hors propos qui, finalement, nuisent à l’ensemble du travail accompli.
Si l’on peut parfaitement rejoindre ici le rapporteur sur certains points, notamment sur la consolidation des relations entre les organes internes de l’ONU, il ne saurait être envisageable d’admettre "la participation de l’Union européenne comme membre à part entière du Conseil de sécurité et de l’Assemblée générale de l’ONU" ou, dans le même ordre d’idées, "que le ministre des Affaires étrangères de l’Union européenne, prévu dans le projet de Constitution européenne, occupe le siège proposé pour l’UE".
S’il est intéressant de constater que de telles propositions révèlent la vraie nature du projet fédéraliste puisqu’elles supposent, par définition, la transformation de l’Union en un État, il est bien évident qu’elles relèvent également du pur fantasme.
Une seule observation suffit à démontrer l’incongruité du projet: quelle aurait été le proposition du représentant de l’Union lors de la crise irakienne, sachant que l’Union était elle-même divisée comme jamais sur la question? 
