Catherine Trautmann # Débats - Mardi 13 décembre 2005 - Conservation de données traitées dans le cadre de la fourniture de services de communications électroniques accessibles au public
  Catherine Trautmann (PSE
). 
 - Madame la Présidente, trouver le point d’équilibre entre la protection de la vie privée et la rétention des données à caractère personnel pour répondre au risque terroriste est difficile, tant la tentation de transformer des mesures d’exception en règles générales existe dans nos États.
Le compromis passé associant le Parlement européen est une étape positive, mais il ne doit pas faire perdre de vue l’objectif de disposer d’une directive offrant un cadre juridique et des garanties identiques dans tous les pays, pour tous les citoyens.
J’insiste sur les recommandations de la commission de l’industrie, de la recherche et de l’énergie concernant les entreprises. Elles ne doivent pas devenir un instrument de surveillance des citoyens pour les gouvernements, ni être entravées dans leur activité, et leur collaboration doit s’accompagner de garanties strictes, notamment le plein remboursement du surcoût.
Enfin, il est indispensable que le dispositif soit le mieux adapté au fonctionnement de la toile, qu’il ne gêne ni le développement d’Internet ni l’accès aux TIC dans l’Union. L’un et l’autre ne peuvent se passer de liberté. 
