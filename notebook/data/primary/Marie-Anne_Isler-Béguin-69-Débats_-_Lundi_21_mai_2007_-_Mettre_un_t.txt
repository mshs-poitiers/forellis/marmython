Marie-Anne Isler-Béguin # Débats - Lundi 21 mai 2007 - Mettre un terme à l’appauvrissement de la biodiversité d’ici 2010 (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie Anne Isler Béguin, 
au nom du groupe Verts/ALE
. - Madame la Présidente, je voudrais, tout d’abord, également féliciter notre rapporteur pour l’excellence de son travail. Bien sûr, l’intitulé est très ambitieux: comment enrayer la diversité, la disparition des espèces végétales et animales d’ici 2010 et au-delà? On aimerait tant y croire!
Mais nous savons combien, à court terme, il est difficile de mettre en musique les différents instruments européens et multilatéraux, dont nous écrivons la partition tous les jours ici. À l’heure où l’attention des citoyens et des médias est focalisée sur le changement climatique, il est nécessaire de rappeler l’importance de la biodiversité, car, contre le changement climatique, il n’y a pas de meilleure stratégie que de favoriser l’évolution dynamique des écosystèmes: M. le commissaire l’a très bien rappelé il y a quelques instants.
Vous l’aurez compris, la diversité biologique implique une approche dynamique et quand on raisonne la diversité biologique in situ
 en Europe, on décline la préservation des espèces végétales et animales à partir des territoires dont la gestion et l’aménagement se définissent au plus près des populations locales. Dans cet esprit, la volonté politique et la capacité gouvernementale d’ouvrir le dialogue sont impératives pour développer et soutenir des réseaux d’exception environnementale, comme Natura 2000.
À l’opposé, la gestion ex situ
 de la diversité biologique évoque plutôt une pratique conservatoire des espèces animales et végétales. Au-delà des collections, poussiéreuses dirais-je, de nos muséums, la conservation ex situ
, telle que nous la concevons aujourd’hui, veut, bien sûr, éviter le pire en conservant le matériel génétique dans des centres agronomiques: on le mettrait à l’abri, prétendument. Mais pour ma part, je m’interroge sur le financement des groupes consultatifs pour la recherche agricole internationale et sur son fonctionnement, qui doit intégrer des communautés locales et autochtones.
En conclusion, je voudrais souligner, bien sûr, l’importance, pour nos populations et pour nos territoires, d’une bonne mise en œuvre des réglementations européennes, parce que nous savons que tout se tient et, par exemple, la directive-cadre sur l’eau doit être appliquée. En matière de biodiversité, l’eau est un médium qui assure depuis l’amont la dynamique des écosystèmes. Pour cette raison, il est essentiel de s’assurer de la non-détérioration des eaux douces plutôt que de se référer à une vague notion de bon état écologique. 
