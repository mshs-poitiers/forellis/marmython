Brigitte Douay # Débats - Mercredi 27 septembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Krehl (A6-0281/2006
) 
  Brigitte Douay (PSE
), 
par écrit
. - Les orientations stratégiques communautaires pour 2007-2013 ont enfin été adoptées par le Parlement européen. J’ai voté pour les recommandations de la rapporteur qui approuve ces orientations.
J’ai néanmoins attiré l’attention de la Commission au cours du débat sur l’enjeu de la coopération transfrontalière qui doit permettre la réduction des disparités entre zones frontières de l’Union, à condition que des attributions disparates de fonds structurels découlant de la nomenclature statistique n’entravent pas l’objectif de cohésion et n’accroissent pas les inégalités.
Il est essentiel d’assurer des conditions de développement économique, social et territorial équilibré de part et d’autre des frontières et d’apporter une attention toute particulière aux programmes destinés à la coopération transfrontalière. 
