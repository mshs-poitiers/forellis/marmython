Patrick Louis # Débats - Jeudi 19 janvier 2006 - Période de réflexion (structure, sujets et cadre pour une évaluation du débat sur l’Union européenne) (vote) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Patrick Louis (IND/DEM
). 
 - Monsieur le Président, je m’appuie sur l’article 138 relatif aux traductions. L’amendement 1 déposé par le groupe IND/DEM vous propose un projet alternatif de résolution qui entende et respecte le refus de nos peuples de tout super État et donc de toute constitution. Or, la traduction de cet amendement, notamment la version anglaise, a plusieurs fois trahi le sens original. Ainsi, là où nous évoquons «la poursuite de la construction européenne», expression politiquement neutre, la traduction évoque «l’intégration européenne», expression désignant le processus supranational que nous rejetons. Sans doute est-ce là un réflexe pavlovien qui contamine de nombreux bureaux de cette maison, n’admettant toujours pas qu’une autre Europe est possible. Je vous rappelle donc que seule la version originale française fait foi. 
