Carl Lang # Débats - Jeudi 1 avril 2004 - Votes
  Lang (NI
),
par écrit
. 
- Quelle remarquable hypocrisie de la part du Parlement européen qui discute aujourd’hui de l’adhésion de la Turquie comme si rien n’était déjà décidé, alors même que l’imminent élargissement de l’Europe à dix nouveaux pays précède et prépare une "seconde vague" d’adhésion, composée de la Bulgarie, de la Roumanie et de la Turquie.
Le titre du rapport en dit long sur ce que mijote la Commission avec la complicité active des "élites" politiques. "Rapport sur les progrès de la Turquie sur la voie de l’adhésion": à la seule lecture de ce libellé, il ne fait aucun doute que l’adhésion de la Turquie est déjà programmée par les instances communautaires. Nous sommes censés attendre décembre 2004 pour savoir si les négociations d’adhésion peuvent débuter. Quelle mascarade! Tous les dirigeants européens se sont déjà prononcés en faveur de cette adhésion contre nature et ce, sans même consulter leur peuple.
Premier à avoir dénoncé le scandale d’un élargissement non européen, politiquement injustifié, économiquement suicidaire et socialement dévastateur, le Front national est l’unique force politique française à véritablement s’opposer à l’entrée de la Turquie dans l’Union européenne. 
