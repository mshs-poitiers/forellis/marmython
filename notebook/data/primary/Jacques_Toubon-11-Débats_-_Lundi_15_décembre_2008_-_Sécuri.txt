Jacques Toubon # Débats - Lundi 15 décembre 2008 - Sécurité des jouets (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jacques Toubon  (PPE-DE
). - 
 Monsieur le Président, chers collègues, je voudrais simplement souligner, dans cette affaire, combien les institutions communautaires ont la capacité de réagir pour résoudre efficacement les problèmes de nos concitoyens.
C’est à l’été 2007 que se sont produits un certain nombre de scandales. Le Parlement a demandé que des mesures soient prises. La Commission a travaillé et, aujourd’hui, nous sommes capables d’adopter cette directive sur la sécurité des jouets, qui marque un progrès considérable. 
Naturellement, nous le devons, d’abord, et il faut le souligner, à notre rapporteure et au travail qui a été fait pour concilier des positions qui, au départ, étaient contradictoires. Aujourd’hui, nous avons un texte efficace et équilibré, qui, par exemple, permettra, ainsi que vient de s’y engager la Commission de réexaminer la norme applicable au livre pour enfants et qui traite, avec beaucoup de sens de l’équilibre, les substances parfumantes dans un certain nombre de jouets
C’est là l’exemple même d’une législation des Européens au service des Européens, et il faut dire que ce Parlement y a joué un rôle déterminant.
