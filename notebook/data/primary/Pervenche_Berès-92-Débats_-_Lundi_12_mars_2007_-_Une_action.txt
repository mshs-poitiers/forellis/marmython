Pervenche Berès # Débats - Lundi 12 mars 2007 - Une action, une voix - Proportionnalité entre propriété et contrôle des entreprises de l’UE (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Pervenche Berès (PSE
). 
 - Monsieur le Président, nous avons bien écouté la réponse du commissaire. Cela dit, le message qu’il est important de transmettre au commissaire McCreevy est le suivant. Nous ne pouvons certes pas préjuger de l’analyse que le commissaire fera des résultats de l’étude, mais notre inquiétude vient de l’étude elle-même, avant même que les résultats en soient connus, vu les questions qui ont été élaborées par les auteurs cette étude.
Je crois que c’est le message qu’il faut transmettre au commissaire McCreevy. 
