Bruno Gollnisch # Débats - Mardi 21 octobre 2008 - Réunion du Conseil européen (15-16 octobre 2008) (débat)
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bruno Gollnisch  (NI
). 
– 
Monsieur le Président du Conseil et de la République française, nous discutons des soins palliatifs à apporter aux malades, mais nous sommes bien discrets sur les causes de la maladie. Comment aucune institution de l’Union n’a vu venir la crise actuelle? Ni le Conseil, ni la Commission, ni la Banque centrale, ni même, mes chers collègues, notre Parlement, ni aucun des gouvernements des États membres! Crise prédite, il est vrai, par une poignée seulement d’économistes, comme le prix Nobel Maurice Allais, et de responsables politiques, essentiellement de notre famille d’esprit, comme une fois de plus, Jean-Marie Le Pen. Vox clamens in deserto
, hélas!
La crise est cependant d’évidence celle du système euromondialiste, du libre-échange sans freins, du terrifiant découplage entre la fiction financière et nos réalités économiques et industrielles déclinantes, qui pourraient demain faire l’objet de fonds souverains d’États tiers profitant de la situation actuelle. Votre activité même, Monsieur le Président, témoigne de l’inadaptation de l’Union: réunion à quatre, le samedi 4 octobre, pas à vingt-sept; réunion bilatérale avec la seule Allemagne le 11; réunion des quinze membres, seulement, de l’Eurogroupe; réunion avec le Président des États-Unis d’Amérique pour le convaincre d’organiser encore une autre réunion théoriquement refondatrice de tout le système à laquelle seraient conviés, si l’on comprend bien, six seulement des vingt-sept États de l’Union, les États-Unis, le Japon, la Russie, l’Inde et la Chine.
Je ne préjuge pas de l’utilité de ces réunions. Je dis qu’il s’agit là du retour à une diplomatie bilatérale ou multilatérale qui montre bien que privée de réactivité, empêtrée dans des normes bureaucratiques, boulimique de compétences qu’elle n’est pas capable d’exercer, l’Union est un cadre dépassé. Le compte rendu du Conseil européen en témoigne si l’on sait lire entre les lignes. Il ratifie vos initiatives, il supplie à mots couverts la Banque centrale de desserrer un peu l’étau des critères de Maastricht, mais il ne décide rien.
Vous avez évoqué la situation en Géorgie et vos efforts. Mais comment ne pas voir que la reconnaissance unilatérale de l’indépendance du Kosovo pavait la voie à celle de l’Abkhazie et de l’Ossétie du Sud? Comment justifier l’extension indéfinie de l’OTAN au moment où le pacte de Varsovie a lui-même disparu?
Monsieur le Président, la voie à suivre est autre. Elle implique une rupture radicale avec le système mondialiste, la complète remise en cause des bienfaits prétendus du brassage universel des personnes, des marchandises et des capitaux. La défense non équivoque de nos indépendances et de nos identités, ce n’est pas s’isoler, c’est, au contraire, la condition du retour de notre influence dans le monde.
