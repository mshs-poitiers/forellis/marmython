Philippe Morillon # Débats - Lundi 4 mai 2009 - Ordre des travaux 
  Philippe Morillon 
, président de la commission PECH
. – Monsieur le Président, le problème c’est, tout simplement, que nous n’aurons pas le temps. Ce n’est pas sérieux d’engager aujourd’hui une réflexion, dans les quelques heures à peine qui nous restent, sur un sujet qui n’aboutira qu’en 2012 ou 2013.
Nous en avions discuté en commission, lors de la réunion du 30 avril. De l’avis de l’ensemble de mes collègues, il faut attendre la prochaine législature pour commencer à examiner les choses.
