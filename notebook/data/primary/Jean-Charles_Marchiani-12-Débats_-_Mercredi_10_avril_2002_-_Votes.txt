Jean-Charles Marchiani # Débats - Mercredi 10 avril 2002 - Votes
  Marchiani (UEN
),
par écrit
. - Au risque de briser la belle unanimité, du reste assez factice à mes yeux, qui domine dans ces rangs, ce débat m’inspire deux réflexions.
Tout d’abord, je constate qu’une fois de plus l’Union européenne, en général, et ce Parlement, en particulier, se mêlent de ce qui ne les regarde pas.
En effet, jusqu’à preuve du contraire, la Fédération de Russie n’est pas membre de l’Union européenne. Dès lors, à quel titre nous permettons-nous d’intervenir dans les affaires intérieures de cet État souverain ? Certes, la situation en Tchétchénie est dramatique. Mais une guerre est toujours un événement déplorable, à plus forte raison lorsqu’elle est déclenchée par des factieux contre l’autorité légitime d’un État !
Quoiqu’en disent les tenants de l’interventionnisme humanitaire, les opérations conduites par les forces russes sur le territoire de la Tchétchénie - la Tchétchénie qui, rappelons-le, fait partie intégrante de la Fédération de Russie - sont le fait d’une armée régulière, placée sous l’autorité d’un Président démocratiquement élu, et relèvent à ce titre du simple maintien de l’ordre. A moins de vouloir s’ériger en gendarme moralisateur donnant des leçons au monde entier, ce Parlement n’a pas la moindre compétence pour juger le comportement des forces russes en Tchétchénie. Je le dis et je le répète, notre Assemblée commet aujourd’hui un acte d’ingérence inadmissible !
(Explication de vote écourtée en application de l'article 137, paragraphe 1, du règlement)

