Kader Arif # Débats - Lundi 23 mars 2009 - Accord de partenariat économique CE/CARIFORUM - Accord de partenariat économique d’étape CE/Côte d’Ivoire - Accord de partenariat Cariforum - CE - Accord de partenariat économique d’étape CE - Côte d’Ivoire - Accord de partenariat économique d’étape CE - Ghana - Accord de partenariat économique intérimaire CE - États du Pacifique - Accord de partenariat économique intérimaire CE - États de l’APE CDAA - Accord de partenariat économique États d’Afrique de l’Est et du Sud (AES) - CE - Accord de partenariat économique CE - États partenaires de la Communauté d’Afrique de l’Est - Accord de partenariat économique d’étape CE - Afrique centrale (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Kader Arif, 
auteur
. − Monsieur le Président, chers collègues, dans ce long débat qui nous regroupe aujourd’hui, je souhaiterais prendre quelques instants, si vous le permettez, pour regarder le chemin parcouru jusqu’ici.
Souvenons-nous des premières prises de position de certains députés de ce Parlement face à l’inquiétude croissante dans les pays ACP, face aux manifestations anti-APE, face aux signaux d’alerte envoyés par les ONG du Nord comme du Sud, lorsque nous demandions que la priorité de ces accords soit le développement, chose qui semblerait presque évidente aujourd’hui, tant la Commission ne cesse de le répéter. Mais M. Mandelson, à l’époque, osait à peine nous répondre car, pour lui, il s’agissait d’abord de stimuler le commerce comme si la simple ouverture des barrières douanières provoquerait le miracle du développement.
Alors qu’on nous traitait d’idéalistes, qu’on nous disait manipulés par les ONG, qu’on s’offusquait de nous voir réclamer des instruments de protection, de régulation, d’intervention de la puissance publique, que s’est-il passé? Eh bien, il est apparu que non, nous n’étions pas des irresponsables. Non, les gouvernements des pays ACP n’ont pas accepté que les négociations continuent à être menées sous la pression ou sous la menace. Non, les risques de l’ouverture des échanges ne sont pas une vue de l’esprit, ils sont réels et ils auront des conséquences concrètes et immédiates: diminution des budgets des États par pertes de revenus douaniers, fragilisation des industries naissantes du secteur agricole, risques pour la sécurité alimentaire des populations.
Ces craintes, nous les exprimions il y a bien longtemps, avant même que n’éclatent les émeutes de la faim ou la crise financière. Alors que dire de la situation aujourd’hui? Le FMI, la Banque mondiale, l’ONU reconnaissent que les pays en développement, contrairement à ce qui était énoncé à l’origine, seront gravement touchés par la récession mondiale.
Comme le soulignait récemment Jacques Diouf, directeur général de la FAO, oserons-nous dire à ceux que nous appelons partenaires, que nous sommes prêts à dépenser des milliards pour sauver le système bancaire mondial mais pas pour sauver leurs populations qui meurent de faim?
Je veux être tout à fait honnête, Madame la Commissaire, et je tiens à ce que ce soit clair. Si vous ne vous engagez pas fermement et précisément au nom de la Commission pour que nous ayons la garantie que ces APE seront réellement pro-développement, je ne voterai pas les avis conformes. Les mots ne suffiront pas, les déclarations d’intention non plus, on en a trop entendus. Nous voulons des engagements précis et je tiens à les citer un par un. Les APE ne seront des accords satisfaisants que s’ils promeuvent l’intégration régionale, s’ils contribuent au développement des pays ACP et à la réalisation des objectifs du Millénaire pour le développement.
Lorsque nous demandons de favoriser l’intégration régionale, cela a une traduction concrète. Typiquement, en Afrique centrale, le Cameroun a été pointé du doigt, pour ne pas dire fortement critiqué, par ses voisins, pour avoir signé cet accord intérimaire avec l’Union. Parmi les huit pays de la région, je rappelle que cinq sont des PMA, c’est-à-dire des pays qui bénéficient d’office et en pleine conformité avec les règles de l’OMC, d’un libre accès au marché européen pour les exportations sans qu’aucune contrepartie ne puisse leur être demandée. Je ne peux que comprendre leurs préoccupations lorsque la Commission leur demande de s’ouvrir à 80 % aux exportations européennes.
Alors, si la commissaire s’engage à favoriser une intégration régionale, si elle s’engage à davantage de flexibilité pour prendre en compte les différents niveaux de développement de nos partenaires, peut-elle nous indiquer pourquoi elle n’accepte pas l’offre de 71 % de libéralisation proposée par l’Afrique centrale?
Second thème essentiel sur lequel nous attendons une réponse: les sujets de Singapour. Ceux-ci ne peuvent être imposés dans les négociations contre la volonté des pays partenaires. Sur ce point, je tiens particulièrement à insister sur les marchés publics. Certes, il faut de la transparence – je la défendrai toujours – mais pouvons-nous priver nos partenaires ACP d’un instrument essentiel de leur souveraineté par le soutien à leur industrie et à leurs services locaux en imposant la libéralisation des marchés publics?
Troisième point: les services. Dans nos débats sur les APE avec le Cameroun, la Commission a souligné à maintes reprises que nos partenaires voulaient négocier sur les services. C’est peut-être une réalité. Mais, cependant, gare à ceux qui utiliseraient cet argument pour imposer la libéralisation des services à toutes les régions et à tous les pays, et surtout pour justifier la libéralisation des services publics. Madame la Commissaire, j’attends votre engagement ferme pour que les services publics restent hors du cadre des négociations et ce, dans toutes les régions. Nous le savons, les pertes de revenus douaniers vont provoquer une diminution des budgets de nos partenaires. Et en cas de baisse de revenus, les premiers secteurs à en pâtir seront les secteurs tels que l’éducation, la santé ou la recherche. Il serait donc inadmissible que dans ce contexte, les gouvernements ACP perdent le contrôle de leurs services publics et j’en appelle à Mme la commissaire pour qu’elle nous donne son engagement ferme sur ces questions.
Quatrième point, et cela a déjà été évoqué, la sécurité alimentaire doit être protégée. Ceci implique non seulement de prévoir les mesures de sauvegarde adéquates, mais aussi de permettre à nos partenaires de soutenir leurs exportations afin de rester compétitifs sur les marchés mondiaux. Je sais que des évolutions positives ont eu lieu en ce sens dans la région SADEC. La Commission est-elle prête à proposer le même genre de mesures dans les autres régions?
Dernier point. Nous savons que la mise à niveau des économies ACP exigera un engagement financier massif de l’Union tant pour protéger les industries naissantes des effets négatifs de la libéralisation que pour développer la compétitivité des économies de nos partenaires. Et malheureusement, contre les recommandations répétées de notre groupe politique, ce sont bien les fonds du FED qui seront utilisés comme première source de financement des APE. Nous savons que par le passé, la Commission n’a pas brillé par la mise en œuvre de ces fonds, j’appuie donc sur la nécessité d’utiliser ces fonds rapidement et selon les priorités de nos partenaires.
En conclusion, Madame la Commissaire, ces accords sont l’image que donnera l’Union européenne face au reste du monde, l’image que donnera l’Union européenne face aux pays les plus pauvres de la planète. 
