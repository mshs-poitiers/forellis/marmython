Chantal Cauquil # Débats - Jeudi 9 octobre 2003 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. 
- En n’envisageant l’industrie spatiale qu’en termes de lutte sur un marché en concurrence, essentiellement avec les États-Unis, ce rapport est tout un symbole. Il souhaite évidemment le recours accru aux fonds publics, c’est-à-dire à l’argent des contribuables, pour aider les entreprises européennes à préserver ou à gagner des parts de ce marché. Et pour que ce soit plus efficace encore, il préconise des commandes dans le secteur militaire, ce qui permettrait en outre aux industriels de profiter des connaissances déjà acquises en ce domaine et ce qui irait dans le sens de la mise en place d’une force militaire européenne indépendante de celle des États-Unis. Cette force militaire que certains jugent indispensable pour conduire ce qu’ils appellent des "opérations de maintien de la paix" et qui ne sont rien d’autre que des opérations de défense des intérêts des industriels et financiers européens.
Un tel rapport, par sa clarté, est comme un aveu: les institutions européennes n’ont d’autres buts que de mobiliser les connaissances et les fonds publics dans l’intérêt des capitaux privés européens, dans le secteur spatial comme dans le reste. Nous ne pouvons évidemment que voter contre un rapport aussi empreint de ces seules préoccupations. 
