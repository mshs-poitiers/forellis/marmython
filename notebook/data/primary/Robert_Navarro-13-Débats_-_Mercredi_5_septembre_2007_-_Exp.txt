Robert Navarro # Débats - Mercredi 5 septembre 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Ayala Sender (A6-0286/2007
) 
  Robert Navarro  (PSE
), 
par écrit
. –
 Si je me félicite de l'adoption du rapport d'initiative d'Inès Ayala Sender sur la logistique, qui souligne l'importance de l'intermodalité pour des transports plus propres et durables, je ne peux que regretter l'adoption du paragraphe 21, contre lequel j'avais déjà voté en commission des transports.
J'ai voté contre ce paragraphe, car les camions de 60 tonnes, à qui ce paragraphe vient d'entrouvrir la porte, sont dangereux pour l'environnement comme pour les usagers de nos routes, qui n'ont pas été conçues pour supporter de telles charges. Les arguments du lobby routier sont fallacieux: les capacités de chargement des poids lourds actuels étant déjà sous-utilisées, la réduction des émissions de gaz polluants que l'on nous fait miroiter sur le thème "moins de véhicules pour plus de marchandises transportées" est un leurre. Quant à l'impact de ces mastodontes sur nos infrastructures, il retombera in fine sur les épaules des contribuables, en faisant exploser les budgets de réfection des routes.
Enfin, concernant la sécurité routière, outre les risques posés par la dégradation de l'infrastructure, la taille même de ces monstres routiers représente un danger pour tous les autres usagers. 
