Jacky Hénin # Débats - Jeudi 2 février 2006 - Législation sociale relative aux activités de transport routier - Harmonisation de dispositions en matière sociale dans le domaine des transports par route
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jacky Henin (GUE/NGL
). 
 - Monsieur le Président, enfin nous avançons, à la vitesse d’une tortue en plein effort, mais nous avançons.
S’il est un domaine où le dumping social et fiscal fait des ravages et où il est impératif que le législateur intervienne, c’est bien celui des transports routiers. Avec les cadences infernales imposées par les flux tendus, on peut parler à juste titre de salaire de la peur. Un salaire de la peur qui est bien trop souvent un salaire horaire de misère, qui pousse à enchaîner les heures de conduite et les trajets, au mépris de la sécurité, au mépris de sa santé et de celle des autres. Je pense aussi aux petits entrepreneurs de ce secteur qui vivent dans l’angoisse quotidienne de la faillite, victimes du dogme criminel de la concurrence libre et non faussée, bien loin aujourd’hui des idéaux qui les ont amenés à devenir des entrepreneurs indépendants.
Une seule loi existe, celle qui consiste à transporter une marchandise d’un point à un autre, le plus rapidement possible et au moindre coût. Peu importent les risques, après tout les assurances existent. Cette loi porte un nom, c’est la loi de la jungle. Il est grand temps d’agir pour que nos routes ne ressemblent plus au far-west. Je regrette, malgré les efforts courageux et tenaces de notre collègue Markov, la timidité des textes finaux, mais ils constituent un premier pas dans la bonne direction.
Certains de nos collègues hurlent aux big brothers
 quand on parle d’utiliser des techniques électroniques pour contrôler le respect de la réglementation. Ils crient à l’atteinte aux libertés individuelles. Pourtant, il s’agit ici de protéger des vies. Ne croirait-on pas que ce sont les grands patrons de sociétés européennes qui sont les principaux terroristes, eux qui violent les lois sociales et encouragent le dumping fiscal et social entre nos nations, ces employeurs qui sont directement responsables, quotidiennement, de dizaines d’accidents mortels sur nos routes? Alors oui, utilisons tous les moyens à notre disposition pour assurer la sécurité et permettre que l’Union européenne vive tranquillement. 
