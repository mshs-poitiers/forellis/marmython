Daniel Cohn-Bendit # Débats - Jeudi 29 janvier 2004 - Remise du Prix Sakharov
  Cohn-Bendit (Verts/ALE
). 
 - Monsieur le Secrétaire général des Nations Unies, chers collègues, j’avoue franchement qu’en vous entendant, Monsieur le Secrétaire général, résonnait dans mes oreilles un autre grand discours, le discours de Martin Luther King qui, une fois, à New York, s’écriait: "I have a dream". Et en vous écoutant, j’avais un rêve aussi, un rêve que ce que vous avez dit ici sur l’émigration devienne le discours de tous les collègues dans cette enceinte, et non pas seulement votre discours.
(Applaudissements)

J’avais un rêve que tous les chefs de gouvernement qui disent vous admirer, admirent ce que vous dites, la lucidité de votre discours sur l’immigration. J’avais un rêve qu’enfin les peuples vous écoutant admirent le discours et la lucidité et l’humanisme sur l’émigration. Car, chers collègues, rien ne sert d’applaudir debout M. Kofi Annan si, dans le quotidien, dans nos résolutions, dans les lois, dans nos pays, nous faisons exactement le contraire de ce qu’il nous demande.
(Vifs applaudissements)

Et c’est pour cela qu’avec admiration, avec bonheur - même si, je l’avoue, notre groupe était sceptique sur ce prix Sakharov, parce que nous le voulions pour un homme, une femme d’Iran ou de Tchétchénie qui se battent pour leur liberté -, et au nom de cette lucidité dont vous avez fait preuve ici, je vous demande, en tant que prix Sakharov, faites en sorte que le gouvernement turc libère Leila Zana, faites en sorte que le gouvernement cubain laisse sortir ce prix Sakharov et, surtout, comme on vous le demande, faites en sorte que l’ONU prenne une initiative pour que le peuple juif, qui a dû émigrer d’Europe parce qu’on a voulu le liquider, puisse vivre en paix, et que le peuple palestinien, qui a le droit à un État, puisse vivre en paix. C’est au nom de cette idée, de la force et de votre lucidité que je vous salue en tant que prix Sakharov.
(Vifs applaudissements)

