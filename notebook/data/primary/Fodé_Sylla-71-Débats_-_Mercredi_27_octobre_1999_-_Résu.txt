Fodé Sylla # Débats - Mercredi 27 octobre 1999 - Résultats du Conseil européen des 15 et 16 octobre à Tampere
  Sylla (GUE/NGL
). 
 - Monsieur le Président, chers collègues, Tampere a décidé l’élaboration d’une Charte européenne des droits fondamentaux.
On peut considérer comme une avancée significative le fait de consulter le HCR en matière de politique d’immigration et d’asile. Je regrette que les associations de défense des droits de l’homme et antiracistes ainsi que les partenaires sociaux aient été d’ores et déjà écartés de l’élaboration de cette charte. Je voudrais, pour ma part, rendre hommage ici, à ces hommes et ces femmes. Leur dévouement et leur travail de terrain a permis de récréer un maillage social et fait reculer au quotidien le racisme, la haine et le mépris.
Associer ces forces vives permettrait, lorsque l’on aborde les questions d’immigration et d’asile, de sortir d’un débat tronqué. L’amalgame se fait trop souvent entre chômage, insécurité et immigration. Entendre dire aujourd’hui qu’on va à travers Eurodac relever les empreintes digitales des demandeurs d’asile et des mineurs est inacceptable, et justifier cela par le fait que c’est plus facile que de relever leur ADN est à la limite de l’entendement.
Il faut, au contraire, donner l’égalité des droits et traiter les immigrés comme des citoyens à part entière. Cela passe par le regroupement familial, la lutte contre les discriminations en matière de logement, de travail, de loisirs, le droit du sol intégral. Et face aux humiliations quotidiennes faites aux immigrés, il est juste et normal de leur donner le droit de vote. Enfin, régulariser les sans-papiers et supprimer la double peine donnerait tout son sens à cette charte.
Une phrase extrêmement importante, si vous permettez, a été dite à Tampere. Cette charte garantira, face aux poussées de l’extrémisme….
(Le président retire la parole à l’orateur
) 
