Bruno Gollnisch # Débats - Mardi 2 septembre 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Renate Weber (A6-0293/2008
) 
  Bruno Gollnisch  (NI
), 
par écrit
. 
– (FR)
 Ce texte a pour mission principale, sous couvert de renforcement des capacités opérationnelles d'Eurojust dans la lutte contre différentes formes de criminalité, de satisfaire l'obsession du politiquement correct des tenants de la police de la pensée.
L'objectif de contrôle de tous les propos sous menace de pénalisation, qu'ils soient écrits ou proférés en réunion, est à peine voilé. En effet, déjà différentes voix se sont élevées au sein du Parlement européen afin d'adopter une directive-cadre tendant à pénaliser le prétendu racisme et la xénophobie et à créer pour sa rapide transposition, un parquet européen unique, c'est-à-dire un nouveau Torquemada du «politiquement correct» de l'Union.
Malheureusement, plus le Parlement européen, institution qui s'autoproclame temple de la démocratie, acquiert de pouvoir décisionnel, plus les libertés fondamentales, notamment les libertés de recherche, d'opinion et d'expression sont bafouées. En réalité, cette Europe totalitaire est bien plus dangereuse que les soi-disant monstres qu'elle prétend combattre. Il s'agit avant tout pour les tenants de l'idéologie euro-mondialiste et immigrationiste d'éliminer leurs gênants adversaires, par l'adoption de législations européennes pénales répressives.
Nous ne l’acceptons pas. 
