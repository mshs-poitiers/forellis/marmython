Kader Arif # Débats - Mardi 5 septembre 2006 - Suspension des négociations concernant l’Agenda de Doha pour le développement (ADD) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Kader Arif (PSE
). 
 - Monsieur le Président, chers Collègues, Monsieur le Commissaire, pour tous ceux qui sont attachés à la structure multilatérale, comme cela vient d’être rappelé, ce coup d’arrêt dans la mise en œuvre du programme de travail de Doha en faveur du développement, qui devait permettre de mettre les règles du commerce au service du développement, est une bien mauvaise nouvelle. En cas d’échec définitif, nous savons ce qui nous attend: une prolifération d’accords bilatéraux régionaux, toujours au détriment du plus faible. En somme, tout le contraire de nos objectifs, qui visent le rééquilibrage des règles du commerce international en faveur des pays en développement.
Ce cycle devait nous permettre d’assurer un partage plus équitable des bénéfices de la mondialisation et de prendre en compte les différences de niveau du développement, il devait offrir aux pays en développement un meilleur accès aux marchés sans forcer à tout prix l’ouverture de leur propre marché. Ces négociations présentaient, certes, des insuffisances mais elles assuraient également des avancées, qui ont été rappelées. Ces avancées, ces acquis méritent d’être sauvés et je me réjouis, à cet égard, de la volonté exprimée par le commissaire Mandelson. Cette volonté est indispensable, la relance des négociations est primordiale mais avancer, c’est aussi réformer, et la question de la réforme de l’OMC est aujourd’hui posée. 
