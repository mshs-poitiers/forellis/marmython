Chantal Cauquil # Débats - Mercredi 14 janvier 2004 - Votes
  Bordes, Cauquil et Laguiller (GUE/NGL
),
par écrit
. 
- Protéger les espèces animales sauvages dont certaines sont menacées de disparition, qui ne serait d’accord avec cette idée générale? Mais le rapport trace les limites de ses propres propositions en affirmant, à juste raison, que "la question de la chasse et de la menace qu’elle représente pour les espèces en voie d’extinction, notamment les grands singes, doit être traitée dans le cadre de développement et de lutte contre la pauvreté".
Mais, sur ce terrain, aucune proposition n’est faite. Et, d’ailleurs, comment en faire sans remettre en cause le système économique et social existant, un système qui affame des millions d’êtres humains pour lesquels la consommation de la viande de brousse est souvent la seule source de protéines animales, notamment dans les zones tropicales?
Dans ces conditions, les mesures de contrôle ou de rétorsion proposées seront totalement inefficaces dans la plupart des cas, notamment contre les bandes organisées qui pratiquent des chasses massives, souvent pour les marchés occidentaux, et ne se retourneront que contre quelques affamés.
Nous nous sommes abstenues sur ce rapport. 
