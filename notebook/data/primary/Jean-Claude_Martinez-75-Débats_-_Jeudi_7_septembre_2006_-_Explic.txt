Jean-Claude Martinez # Débats - Jeudi 7 septembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Suspension des négociations concernant l’Agenda de Doha pour le développement (ADD) (B6-0465/2006
) 
  Jean-Claude Martinez (NI
), 
par écrit
. - À la session de Genève en juin 2006, l’Europe s’est montrée flexible jusqu’à la soumission lors des négociations commerciales de l’OMC. Après avoir accepté, le 18 décembre 2005 à Hong Kong, l’abandon de nos restitutions aux aides à l’exportation agricole, à partir de 2013, Peter Mandelson, notre commissaire, chef de la négociation, a fait le cadeau royal à Genève pour qu’un accord planétaire intervienne: la baisse jusqu’à 50 % de la protection douanière de notre marché agricole.
Mais le Brésil, qui a des élections présidentielles, et les USA, qui ont des élections au Congrès, n’ont pas pris le moindre risque: baisser leurs aides internes pour les USA, ouvrir leur marché industriel pour le Brésil.
Les négociations finiront par reprendre mais, déjà, on sait que la Commission européenne a cédé sur les aides aux exportations et sur la protection de notre agriculture et de notre viticulture contre le dumping social des pays tiers, où des multinationales produisent sans droit du travail et sans droit social.
La solution pour 2007 et sortir de l’impasse, c’est d’avoir l’imagination d’inventer des droits de douane modulables, remboursables et bonifiables, c’est-à-dire des droits de douane déductibles. 
