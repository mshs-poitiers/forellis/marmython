Bruno Gollnisch # Débats - Jeudi 5 février 2009 - Explications de vote 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Relazione: Christina Muscardini (A6-0001/2009
)
  Bruno Gollnisch  (NI
), 
par écrit
. – 
Nous avons voté le rapport de Mme Muscardini sur les PME, qui est une critique sévère de la politique commerciale de l'Union européenne, même s'il est rédigé dans le langage édulcoré et technocratique cher à cette maison.
Tout y passe: politiques focalisées sur les besoins des grandes entreprises, faiblesse des actions en faveur de l'accès aux marchés étrangers et de la défense de l'application de la réciprocité par les pays tiers, difficultés d'accès aux instruments de défense commerciale pour les petites entreprises, mollesse de la protection contre les contrefaçons et l'usage illégitime ou frauduleux des indications d'origine géographiques...
Il est temps en effet que l'Union européenne cesse de pratiquer l'immolation de ses entreprises et de ses travailleurs sur l'autel d'une concurrence et d'une ouverture des marchés qu'elle est seule au monde à pratiquer. Il est temps de soutenir les PME à l'exportation, de les défendre vraiment contre la concurrence déloyale, de protéger raisonnablement nos marchés.
Or, en restant attaché à l'internationalisation des entreprises comme une fin en soi, le rapporteur continue de promouvoir un système fondé sur la liberté absolue de circulation des biens, des services, des capitaux et des hommes, un système qui nous a menés à une profonde crise économique, financière et sociale, un système avec lequel l'Union européenne doit rompre absolument.
