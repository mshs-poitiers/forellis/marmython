Françoise Grossetête # Débats - Mardi 12 juin 2001 - VOTES
  Grossetête (PPE-DE
),
par écrit
. -
 J'ai voté pour ce rapport.
Par principe, je ne suis pas favorable à la création d'une agence. Ces derniers temps, nous constatons une multiplication de ces nouvelles entités : Agence pour la sécurité maritime, Agence européenne de la sécurité aérienne, etc. Une nouvelle bureaucratie communautaire se dessine. Je le déplore. Pour autant, certaines circonstances peuvent nous amener à réfléchir sur l'opportunité de création d'une Autorité spéciale. C'est le cas ici.
L'existence de ce rapport découle en effet d'un triste constat : la Commission européenne n'a pas été en mesure de réagir de manière efficace aux différentes crises alimentaires.
Aussi, pour pallier les carences de la Commission, pour répondre à l'attente des consommateurs et des agriculteurs, et pour leur redonner confiance, la constitution d'une Autorité spécifique pour la sécurité alimentaire peut être une alternative efficace.
Deux axes essentiels doivent guider l'action de l'Autorité alimentaire : il faut lui confier une mission claire et établir une séparation entre ce qui relève, d'une part, de la responsabilité scientifique et, d'autre part, de la responsabilité politique. L'Autorité doit se concentrer sur l'identification et l'évaluation des risques, coordonner ses activités avec celles des agences nationales et fournir toutes les informations nécessaires. En aucun cas, l'Autorité alimentaire ne doit avoir pour vocation de gérer le système d'alerte rapide. Celui-ci est un instrument au service du consommateur et la décision sur les mesures à prendre doit relever intégralement et exclusivement du secteur politique.
Enfin, concernant la localisation de cette Autorité, j'ai voté en faveur de la totalité de l'amendement 188. La France est un grand pays fondateur de l'Union. Elle doit montrer à ses partenaires son attachement à l'Europe en transposant, dans les meilleurs délais, les directives européennes. Or, jusqu'ici, mon pays reste parmi les plus mauvais élèves.
Par ce vote, je veux simplement adresser un signal fort aux responsables politiques de la France pour que cette situation s'inverse. La France rehaussera ainsi son prestige et pèsera d'un poids plus important lors de prochaines négociations. 
