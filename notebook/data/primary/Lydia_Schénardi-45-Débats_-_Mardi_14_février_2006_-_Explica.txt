Lydia Schénardi # Débats - Mardi 14 février 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Rapport Rothe (A6-0020/2006
) 
  Lydia Schenardi (NI
), 
par écrit
. - L’un des défis les plus importants pour les pays européens réside dans leur politique énergétique et dans leur capacité à utiliser les énergies renouvelables pour diminuer leur dépendance aux approvisionnements en pétrole et en gaz et aux nombreuses fluctuations de prix de ceux-ci.
Ces énergies renouvelables devraient prendre une place prépondérante dans le domaine du chauffage et de la réfrigération, domaine qui actuellement représente près de 70 % des dépenses d’énergie en Europe.
Nous soutiendrons donc ce qui pourrait encourager à l’utilisation de ces énergies renouvelables: information des utilisateurs, élimination des obstacles administratifs, aides à la mise en place, encouragement de la recherche, etc.
Mais nous serons particulièrement vigilants sur le respect de la souveraineté des États en matière d’approvisionnement énergétique, ainsi que sur les aspects fiscaux des propositions qui pourraient être faites.
Nous n’avons pas encore mesuré toutes les conséquences de la libéralisation des marchés de l’électricité et du gaz sur les bilans énergétiques nationaux. Et il n’est pas question de donner à la Commission, par le biais de propositions fiscales, des pouvoirs supplémentaires en matière de politique énergétique, que les traités ne lui confèrent pas mais qu’elle s’est déjà montrée habile à s’approprier par des voies détournées. 
