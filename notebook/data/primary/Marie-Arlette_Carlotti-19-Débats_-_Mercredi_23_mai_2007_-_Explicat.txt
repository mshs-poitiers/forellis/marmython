Marie-Arlette Carlotti # Débats - Mercredi 23 mai 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport: Panayotopoulos-Cassiotou (A6-0068/2007
) 
  Marie-Arlette Carlotti (PSE
), 
par écrit
. - 
Ce rapport du Parlement européen est un pas dans la bonne direction.
Il fait du travail décent un outil de la politique européenne de développement, d’abord en cofinançant, avec l’OIT, un programme de développement pour le travail décent, ensuite en créant un label européen et en dressant une liste noire des entreprises qui violent les normes fondamentales du travail, et enfin en mettant en place des sanctions commerciales contre les pays qui portent gravement atteinte aux droits sociaux fondamentaux.
Il porte des avancées pour le travail décent en Europe, où il reste aussi beaucoup à faire: en poussant les États membres à ratifier les conventions de l’OIT sur la sécurité et la santé des travailleurs, la protection de la maternité ou les travailleurs migrants; en demandant l’introduction d’un salaire minimum en tant que filet de sécurité pour empêcher toute exploitation des travailleurs; en améliorant l’accès à l’apprentissage tout au long de la vie et en plaidant pour une meilleure harmonisation des régimes de pension.
Ce sont les bases de cette Europe sociale que les socialistes européens veulent construire. Je voterai donc en faveur de ce rapport. 
