Carl Lang # Débats - Mercredi 15 mars 2000 - VOTES
  Lang (TDI
),
par écrit
. - On marche sur la tête. Dans une Europe qui clame sa volonté de protéger le consommateur, qui parle labels de qualité et authenticité, nous voilà face à une position commune du Conseil qui préfère dénaturer un produit jusqu’ici de qualité, le chocolat, plutôt que de froisser les multinationales de l’agroalimentaire.
Doit-on s’en étonner d’ailleurs, puisque cette même Europe produit des vaches folles, des moutons tremblants, des poulets à la dioxine, importe de la viande aux hormones et des céréales génétiquement modifiées.
Certes, on "informe" le consommateur que le produit qu’il achète est frelaté. Mais quelle hypocrisie ! Quelle information ! En caractères minuscules, dans un langage incompréhensible, au dos des emballages.
Vous avez l’habitude de faire n’importe quoi au nom de vos dogmes, de la libre circulation, de la libre concurrence, du libre échange. De grâce, ne faites plus n’importe quoi avec nos traditions gastronomiques, nos produits de qualité ou du terroir. Ne jouez plus avec l’alimentation. Cela, ce sera un véritable progrès.
Vous l’aurez compris : mes collègues de la coordination des droites européennes et moi-même voterons pour "l’option zéro", zéro matières grasses végétales autres que le beurre de cacao dans le chocolat. 
