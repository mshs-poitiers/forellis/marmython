Dominique Vlasto # Débats - Mercredi 24 octobre 2007 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Proposition de résolution RC-B6-0376/2007

  Dominique Vlasto  (PPE-DE
), 
par écrit
. – Je me suis abstenue sur la résolution sur les relations UE-Turquie pour marquer mon opposition aux négociations d'adhésion en cours. Deux événements récents devraient faire comprendre les risques liés à cette hypothétique adhésion. D'abord, la crise politique que le pays a traversée avant la désignation de son président de la République: elle a illustré les tensions au sein de la société turque mais aussi la fragilité institutionnelle de ce pays. Je pense également aux tensions à la frontière iraquienne et au risque de déstabilisation d'une des rares zones où la guerre était contenue. La décision du Parlement turc d'autoriser l'armée à faire des incursions militaires en Irak n'est pas acceptable. La Turquie joue un jeu régional dangereux et l'UE ne devrait pas soutenir les dérives populistes et belliqueuses actuelles.
Tout ceci conforte ma conviction: si nous élargissions l'Union jusqu'aux frontières iraquiennes, je ne vois pas ce qui lui resterait d'européenne! Je pense que la Turquie n'est toujours pas apte à adhérer à l'UE. Il nous revient de lui proposer des formules alternatives: dans ce contexte, l'Union de la Méditerranée proposée par Nicolas Sarkozy est assurément une chance à saisir pour l'UE et la Turquie. 
