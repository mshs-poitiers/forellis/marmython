Bernadette Vergnaud # Débats - Mardi 14 février 2006 - Services dans le marché intérieur (suite du débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Bernadette Vergnaud (PSE
). 
 - Monsieur le Commissaire, Monsieur le Président, chers collègues, je tiens tout d’abord à rendre hommage aux travaux de notre rapporteur, Evelyne Gebhardt, qui a, en réalité, accompli un travail complet de réécriture du texte.
Comment faut-il vous dire, Monsieur le Commissaire, que les citoyens européens ne veulent plus de cette Europe du moins-disant social? En langue du pays d’origine, peut-être? Malgré de grandes avancées comme l’exclusion des soins de santé, des services d’intérêt général, la préservation de la directive «Détachement des travailleurs», je regrette que les SIEG et les services sociaux ne soient toujours pas exclus. Je me refuse à voir ces missions de services publics subordonnées aux règles du marché concurrentiel.
De même, l’exclusion du droit du travail dans l’article 2 serait très souhaitable afin de ne pas générer un contentieux aléatoire. Enfin, même si le pays d’origine n’est plus mentionné, le compromis proposé abandonnera le pouvoir effectif du législateur au juge communautaire qui déterminera l’orientation de la politique sociale européenne. Appliquer le principe du pays de destination à l’exercice des activités de services et le principe du pays d’origine au seul droit d’accès eut été préférable.
Quant à ceux qui ont prétendu de façon mensongère que voter non au projet de Constitution européenne signait la mort de la directive Bolkestein, l’actualité leur démontre au contraire que cette directive est bien vivante et qu’elle exige de notre part une extrême vigilance et un combat soutenu. 
