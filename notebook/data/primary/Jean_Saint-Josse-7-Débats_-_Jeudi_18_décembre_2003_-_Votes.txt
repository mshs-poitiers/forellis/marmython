Jean Saint-Josse # Débats - Jeudi 18 décembre 2003 - Votes
  Saint-Josse (EDD
),
par écrit
. 
- La délégation EDD/CPNT déplore, comme l’a justement relevé ma collègue Véronique Mathieu, que le budget de la PAC ne soit pas à la hauteur des besoins du monde agricole, et ce tant dans l’intérêt de l’ensemble des populations à se voir offrir une production de qualité qu’en matière d’aménagement des territoires.
Les fonds alloués aux actions structurelles sont mal utilisés ou sous-employés et confirment les carences et les vices de cette politique encadrée par Bruxelles. Ces phénomènes sont aggravés par un élargissement mal pensé qui pénalisera les États membres actuels sans répondre aux besoins des nouveaux États membres. Parallèlement, les dépenses de fonctionnement de la bureaucratie (personnel et immobilier) sont en augmentation sensible.
De plus, l’exercice 2004 démontre une nouvelle fois qu’un certain nombre d’associations et d’organisations, en phase avec les vues de la Commission sur l’avenir de l’Union européenne ou plus spécifiquement sur les perspectives d’une politique comme celle de l’environnement, bénéficient d’exercice en exercice de dotations propres ou au titre de certains programmes conçus à dessein.
Ces pratiques ne sont pas à l’abri de dérapages et font que chaque euro supplémentaire du budget européen est susceptible de devenir une source de préoccupation pour la commission du contrôle budgétaire. Pour l’ensemble de ces raisons, le budget pour 2004 ne répond pas aux priorités des députés EDD/CPNT, qui s’y sont de fait opposés lors du vote. 
