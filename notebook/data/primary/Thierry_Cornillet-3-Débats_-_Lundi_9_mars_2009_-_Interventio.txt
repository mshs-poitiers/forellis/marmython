Thierry Cornillet # Débats - Lundi 9 mars 2009 - Interventions d’une minute sur des questions politiques importantes
  Thierry Cornillet  (ALDE
). - 
 Madame la Présidente, en tant que rapporteur permanent de notre Parlement pour l’action humanitaire, je rentre du Kivu en République démocratique du Congo et j’avais l’espoir de vous délivrer un message optimiste quant à la résolution de cette situation et au retour des déplacés chez eux.
Hélas, mon optimisme a été douché par la décision du président El Béchir concernant la plus grande crise humanitaire, celle du Darfour. Certes, le président El Béchir, on a raison de le déférer, car c’est en fait une lutte contre l’impunité, et c’est tout à fait heureux de faire respecter le droit international humanitaire.
Mais le président El Béchir vient de rajouter à son cas puisqu’il vient de prendre une décision doublement irréfléchie: d’une part parce qu’elle s’ajoute aux griefs qui lui sont déjà reprochés et, d’autre part, parce qu’elle est, à elle seule, passible d’être déférée au CPI, les conséquences au Darfour étant extraordinairement graves en matière humanitaire.
Donc, le monde ne l’oubliera pas mais nos seules paroles n’auront que peu d’effet, Madame la Présidente. J’ai apprécié que nous changions le point mais, au-delà des paroles, faisons des actes. 
