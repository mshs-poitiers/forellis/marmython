Gérard Caudron # Débats - Mercredi 4 septembre 2002 - Votes
  Caudron (GUE/NGL
),
par écrit
. - Je veux, après avoir félicité notre collègue Luis Berenguer Fuster pour son travail et son rapport, dire le grand intérêt d'une résolution du Parlement européen qui vise à encourager l'emploi et qui reconnaît la nécessité des aides d'État. On y dit que le chômage est trop élevé. J'ajouterai que c'est un drame et un véritable cancer qui doit mobiliser toutes les énergies ! Les aides d'État sont parfaitement légitimes pour épauler un bon nombre de dispositifs de lutte contre le chômage et pour l'emploi.
Au demeurant, il était parfaitement inacceptable de lier ces aides à un accroissement du marché du travail ; les droites l'ayant fait voter, j'ai donc dû, avec mes collègues de la Délégation de la Gauche Républicaine, Radicale et Citoyenne, voter contre le rapport. 
