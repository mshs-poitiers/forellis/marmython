Brigitte Douay # Débats - Mardi 12 décembre 2006 - Création du Fonds européen d’ajustement à la mondialisation (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Brigitte Douay (PSE
). 
 - Monsieur le Président, je me réjouis, comme mes collègues, de la création de ce Fonds, lequel suscite beaucoup d’espoir chez les salariés inquiets des délocalisations et a suscité de nombreux débats dans nos commissions respectives, en particulier sur son montant, sur les critères d’attribution, et donc sur les risques d’iniquité de traitement entre régions qui pourraient en résulter. Mais il est important pour les travailleurs européens de savoir que l’Union a, enfin, décidé de prendre en compte les destructions d’emplois qu’une mondialisation mal maîtrisée peut entraîner dans chacun de nos pays, en particulier dans des régions d’industrie traditionnelle.
Je m’associe aux félicitations adressées à Mme Bachelot et à l’ensemble des rapporteurs pour leur travail approfondi et me réjouis que le Fonds soit opérationnel dès le 1er janvier 2007. Comme beaucoup, je regrette cependant que le seuil d’octroi du Fonds n’ait pas été abaissé, que sa dotation ne soit pas plus importante et définitivement affectée et qu’il ne soit pas mobilisable pour des délocalisations à l’intérieur de l’Union européenne, les plus nombreuses.
Mais ce Fonds existe et il faudra très vite le faire fonctionner car il y a urgence, comme l’actualité nous le montre fréquemment, puis en évaluer la portée et l’utilité, pour le corriger et le réalimenter si nécessaire. Je suis satisfaite également qu’il y ait obligation d’informer les travailleurs et les régions qui en bénéficieront, car tout ce qui permet de renforcer auprès de nos concitoyens, souvent sceptiques, le sentiment de la valeur ajoutée apportée par l’Union européenne en matière de cohésion et de solidarité est le bienvenu. 
