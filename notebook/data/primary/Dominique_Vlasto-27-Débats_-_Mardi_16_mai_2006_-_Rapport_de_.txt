Dominique Vlasto # Débats - Mardi 16 mai 2006 - Rapport de progrès sur la voie de l’adhésion de la Bulgarie et de la Roumanie (débat) 
  Dominique Vlasto (PPE-DE
). 
 - Dans sa communication du 25 octobre 2008, la Commission européenne relevait «des lacunes particulièrement préoccupantes dans la préparation de l’adhésion».
J’en citerais deux principales: la corruption qui demeure un problème sérieux pouvant menacer le marché intérieur et les difficultés substantielles pour mettre en place un dispositif efficace de gestion et de contrôle financiers permettant l’exécution des fonds structurels.
On ne peut nier les progrès accomplis et la volonté d’adhérer de la Bulgarie et de la Roumanie. Toutefois, je doute que les lacunes particulièrement préoccupantes relevées par la Commission européenne puissent être comblées d’ici à la date prévue de leur adhésion, le 1er janvier 2007.
Avec l’arrivée des dix nouveaux États membres, les disparités socio-économiques ont été multipliées par deux dans l’Union élargie, alors que les Quinze ne stimulent pas la croissance européenne, leurs performances économiques restant plutôt moyennes. Par conséquent, ne devrions-nous pas d’abord songer à consolider l’Union à 25 États membres avant d’envisager l’adhésion de nouveaux pays?
Je pense ainsi que ni l’Union européenne, ni la Bulgarie, ni la Roumanie ne seront prêtes pour 2007. C’est pourquoi je demande au Conseil de ne pas confondre vitesse et précipitation dans la perspective de leur adhésion. 
