Marine Le Pen # Débats - Mercredi 7 septembre 2005 - Explications de vote
  Marine Le Pen (NI
),
par écrit
. 
- Ce rapport qui part d’un bon sentiment devient vite un invraisemblable fourre-tout. L’exposé des motifs commençait pourtant bien et l’intérêt était évidemment d’envisager une protection accrue des mineurs face aux fléaux que sont la pornographie, la pédophilie, la violence des images et des contenus mais aussi et cela a été oublié, la provocation à l’usage des drogues ou des comportements dangereux, le recrutement et l’enrôlement dans des entreprises fanatiques et terroristes.
Le sujet était assez vaste et nos politiques hélas assez défaillantes pour se pencher plus sérieusement sur ces faits gravissimes et tenter d’y répondre afin de protéger nos enfants. Au lieu de cela, le rapport s’égare, s’éparpille et mélange tout et n’importe quoi, la pornographie certes, mais aussi la lutte contre les discriminations, la formation des enseignants et enfin le droit de réponse dont on ne voit vraiment pas en quoi il peut être d’une aide quelconque aux mineurs.
La technique de ce rapport, habituelle dans cette enceinte, consiste à noyer le «poisson» politique dans une mer de bonnes intentions. Elle rend ce texte une fois de plus obscur, abscons, idéologique et donc totalement inefficace. 
