Jean-Louis Bourlanges # Débats - Mardi 26 octobre 1999 - Budgets CE et CECA "2000"
  Bourlanges (PPE
) 
, rapporteur
. - Monsieur le Président, je voudrais remercier Mme Schreyer des propos qu’elle vient de tenir. Nous apprécierons sa proposition une fois qu’elle aura été formulée, ce qui nous aidera à la comprendre.
Pour l’instant, je me limiterai à une seule petite observation, que Mme Schreyer comprendra parfaitement, au sujet des crédits de la recherche. J’ai apprécié les termes des engagements qui ont été publiquement pris par Mme le Commissaire concernant la gestion des lignes relatives à la recherche. J’attache une importance particulière - ainsi, je crois, que l’ensemble de la commission des budgets - à ce que ces engagements reçoivent une concrétisation écrite au-delà de la lettre fort intéressante que Mme Schreyer nous a envoyée, à laquelle le président Wynn a répondu et qui, je crois, appelle des précisions complémentaires.
Donc, je vous demanderai, Madame la Commissaire, de bien vouloir, avec vos collègues et dans les termes que vous estimerez appropriés, concrétiser les engagements que vous venez de prendre, afin que nous puissions voter jeudi en toute connaissance de cause et sur la base d’engagements clairs excluant tout malentendu entre nous. 
