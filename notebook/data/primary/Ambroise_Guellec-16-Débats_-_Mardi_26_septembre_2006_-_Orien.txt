Ambroise Guellec # Débats - Mardi 26 septembre 2006 - Orientations stratégiques en matière de cohésion (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Ambroise Guellec (PPE-DE
). 
 - Monsieur le Président, Madame la Commissaire, nous n’avons guère de doute sur l’issue du vote de demain sur ces orientations stratégiques qui recueilleront vraisemblablement la quasi-unanimité de notre Assemblée; je pense, comme beaucoup ici, que le travail de notre rapporteur y est pour quelque chose.
Il est cependant, comme chacun sait, bien tard pour en parler, ici et maintenant. L’élaboration des programmes opérationnels est déjà bien avancée dans la plupart des pays. Évidemment, c’est la suite qui nous intéresse. À quoi servira réellement la politique régionale de l’Union, qui représente presque 40% du budget communautaire: à la stratégie de Lisbonne, à la cohésion territoriale, ou bien aux deux? C’est ce que nous espérons.
Il m’apparaît en tous les cas que le principe sur lequel le Parlement n’a pas été consulté, l’earmarking
, qui concerne surtout les États membres anciens, est l’un des plus technocratiques et, à mon modeste point de vue, l’un des moins intelligents sans doute jamais imaginés en Europe. C’est la conjugaison de deux technocraties, celle de la Commission, dont la science en la matière est bien réputée, celle des États membres, qui est aussi variée que développée. Qu’en sortira-t-il? Il est encore permis d’espérer le meilleur. Il faudra pour cela attendre l’examen par la Commission, qui va durer des mois, des programmes opérationnels présentés par les États membres.
Je forme le vœu qu’ils répondent d’abord et avant tout aux besoins et à la volonté des populations urbaines, et des populations rurales, auxquels ils s’appliquent. Nous vous savons attentive, Madame la Commissaire, à ces préoccupations de bon sens. Puissiez-vous en convaincre vos interlocuteurs et vos collaborateurs. 
