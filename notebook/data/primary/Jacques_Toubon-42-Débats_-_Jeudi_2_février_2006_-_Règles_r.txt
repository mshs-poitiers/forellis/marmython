Jacques Toubon # Débats - Jeudi 2 février 2006 - Règles relatives aux quantités nominales des produits en préemballages (vote) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jacques Toubon (PPE-DE
), 
rapporteur
. - Monsieur le Président, je voudrais simplement indiquer que la proposition de la Commission consistant à maintenir des gammes obligatoires d’emballages pour un certain nombre de produits de grande consommation, dans l’intérêt des consommateurs, est une proposition cohérente. En revanche, la position qui consiste à accepter que la Commission maintienne certains secteurs obligatoires, tout en refusant la proposition de la Commission, elle, n’est pas cohérente.
Je recommande donc, dans l’intérêt des consommateurs et conformément à l’étude indépendante qui a été commanditée par le Parlement européen pour la première fois dans son histoire, de voter pour l’ensemble des amendements proposés par la commission du marché intérieur qui les a adoptés à vingt-huit voix avec une abstention. 
