Danielle Darras # Débats - Mercredi 13 mars 2002 - Votes (suite) 
  Darras (PSE
),
par écrit
. -
 Avant toute chose, je tiens à remercier à la fois notre rapporteur au sein de la commission de la politique régionale, du transport et du tourisme, M. Markus Ferber, et l'ensemble de cette commission pour le travail qu'ils nous ont proposé.
L'amendement de compromis du rapporteur Ferber est insatisfaisant, voire même inacceptable.
Insatisfaisant, car cet amendement demande une évaluation unilatérale, réalisée par la seule Commission et non, comme je l'aurais souhaité, contradictoire et faite par tous les acteurs.
Inacceptable, car en refusant la proposition initiale du Parlement de limiter à 150g et 4 fois le prix la prochaine étape de la libéralisation, aucune garantie n'est donnée pour maintenir un niveau élevé de cohésion sociale.
Inacceptable encore, car cet amendement refuse de poser le problème des conditions d'accès aux réseaux des nouveaux opérateurs.
Inacceptable enfin, car cet amendement refuse de poser le problème du financement du service universel ; or, s'il est une demande légitime, c'est bien celle du financement : l'on est en effet en droit de savoir comment le service universel sera financé, c'est-à-dire comment le service universel pourra être garanti en tant que tel.
Néanmoins, je voterai la position commune par défaut, car tenter à nouveau d'amender le texte, c'est risquer de voir tomber la position commune ; c'est donc courir le risque qu'aucune directive n'existe sur les services postaux et, par suite, qu'une libéralisation complète de ce secteur puisse intervenir d'ici à 2 ans.
C'est donc davantage un souci de sécurité qu'un réel enthousiasme dans ce texte à mon avis bien trop cyniquement libéral qui me conduit à voter cette position commune. 
