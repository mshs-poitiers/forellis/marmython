Lydia Schénardi # Débats - Mercredi 17 décembre 2008 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
- Rapport Turmes (A6-0369/2008
) 
  Lydia Schenardi  (NI
), 
par écrit
. –
 Nous avons eu l’occasion de dire à plusieurs reprises ici que le simple objectif de réduire la dépendance de l’Union européenne vis-à-vis des importations de gaz ou d’hydrocarbures pouvait en soi justifier la promotion des énergies renouvelables.
Le compromis présenté aujourd’hui, qui s’insère dans le «paquet énergie-climat», est comme tous les compromis: ni totalement mauvais, ni totalement satisfaisant.
Il n’est, notamment, pas totalement satisfaisant sur les biocarburants, de deuxième génération ou non: garantie insuffisante quant à la concurrence avec les productions alimentaires, flou quant aux modifications éventuelles de l’affectation des sols, silence sur le bilan carbone réel de ces sources d’énergie...
Il n’est pas totalement convaincant sur la «garantie d’origine» censée identifier l’électricité verte notamment, quant on connaît la réalité de ce qu’est l’approvisionnement en électricité, les publicités douteuses sur le sujet et le surcoût important pour les consommateurs.
Il n’est enfin, pas satisfaisant du tout quant aux conséquences sociales: nous aimerions avoir la certitude, comme d’ailleurs pour l’ensemble de ce paquet législatif adopté au moment où débute une crise mondiale qui promet d’être profonde et longue, que si la situation économique l’exige, les intérêts des citoyens et des travailleurs européens primeront sur toute autre considération. 
