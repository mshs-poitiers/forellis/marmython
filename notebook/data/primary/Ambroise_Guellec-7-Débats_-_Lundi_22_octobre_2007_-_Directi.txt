Ambroise Guellec # Débats - Lundi 22 octobre 2007 - Directive-cadre sur l'utilisation durable des pesticides - Stratégie thématique concernant l'utilisation durable des pesticides - Mise sur le marché des produits phytopharmaceutiques (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Ambroise Guellec, 
au nom du groupe PPE-DE
. –
  Monsieur le Président, c'est un dossier important, complexe et difficile dont nous débattons aujourd'hui. En effet, la lutte contre les prédateurs et ennemis des cultures représente un enjeu essentiel, tant en termes de sécurité alimentaire que de protection et d'amélioration de notre environnement.
À cet égard, l'usage raisonné de pesticides reste incontournable. Il est cependant indispensable de faire une juste évaluation des risques, de leur utilisation, notamment en termes de santé humaine, et de définir les voies de la réduction programmée de leur usage. Le projet de résolution sur la stratégie thématique à suivre dans ce domaine répond bien, à mon sens, aux questions essentielles qui se posent à nous, et je voudrais rendre hommage à notre rapporteure, Mme Belohorská, et dire également la très bonne collaboration que nous avons eue sur ce texte.
Quelques observations: le problème central de la gestion des risques ne peut être séparé de celui des quantités de pesticides utilisées. L'éradication des substances toxiques ou les plus dangereuses doit être posée en principe incontournable. Pour les autres pesticides, la subsidiarité doit s'appliquer. À partir d'un cadre communautaire commun, il appartient aux États membres de définir leurs objectifs propres, leurs calendrier et critères nationaux de réduction des quantités utilisées. Les plans d'action nationaux permettront également l'adaptation aux conditions spécifiques de chaque pays. La protection du milieu aquatique est également un sujet tout à fait important et elle doit être rigoureusement assurée par la délimitation de zones tampons faite en fonction des conditions locales.
Je voudrais dire, en conclusion, que le dispositif que nous voulons mettre en œuvre se situe dans la continuité et en cohérence avec la directive REACH. Et je résumerais le débat dans une formule: "aussi peu que possible de pesticides, autant que nécessaire". 
