Margie Sudre # Débats - Mercredi 4 juin 2008 - Système communautaire contre la pêche illicite, non déclarée et non réglementée (INN) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Margie Sudre  (PPE-DE
), 
par écrit. 
–
 Le Parlement européen porte un nouveau coup à la pêche illicite, non-déclarée et non-réglementée, véritable fléau international.
Les mesures adoptées complètent efficacement le dispositif existant. Je retiens particulièrement la mise en place, par l'État du port, d'un régime de contrôle des navires y faisant escale, impliquant la délivrance d'un certificat attestant de la légalité des captures et l'interdiction d'accepter des navires pirates. De même, je salue l'interdiction d'importer du poisson provenant de la pêche illicite ainsi que la publication d'une liste répertoriant les navires voyous.
La valeur ajoutée de ces nouvelles règles européennes découle également du développement d'un système d'alerte communautaire se déclenchant en cas de soupçon de pêche pirate. Le système de sanctions a par ailleurs été renforcé, incluant l'interdiction pour les navires clandestins de bénéficier de subventions publiques et l'obligation de les rembourser, le cas échéant.
Surtout, je me suis battue avec succès pour que le Parlement européen porte aux régions ultrapériphériques une attention particulière dans le cadre de la lutte contre la pêche illicite, en raison de l'exceptionnelle fragilité de leurs écosystèmes. C'est un signal fort adressé aux flottes sans scrupules qui permettra de mettre un frein réel à la concurrence déloyale étouffant à petit feu nos pêcheurs. 
