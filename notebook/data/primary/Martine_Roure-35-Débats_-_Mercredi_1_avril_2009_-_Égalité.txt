Martine Roure # Débats - Mercredi 1 avril 2009 - Égalité de traitement entre les personnes sans distinction de religion ou de convictions, de handicap, d’âge ou d’orientation sexuelle (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Martine Roure  (PSE
). 
 – Monsieur le Président, tout d’abord je souhaite remercier particulièrement notre rapporteure du travail effectué et du résultat qui est enfin obtenu.
L’article 13 du traité est notre socle, et je souhaite insister pour dire que les États membres peuvent garantir un niveau plus élevé de protection. Il ne s’agit là que de normes minimales et – soyons clairs – il est impossible d’abaisser le niveau actuel de protection de chaque État membre en invoquant cette nouvelle directive. Parce que – soyons encore plus précis – certains États membres ont un niveau de protection très élevé, ça existe.
Ne pas être victime de discriminations est un droit fondamental pour toute personne qui vit sur le territoire de l’Union. Pourtant, à partir de l’apparence extérieure, ou simplement du patronyme d’une personne, on se rend compte qu’il y a trop souvent discrimination.
S’agissant des personnes handicapées, nous devons nous assurer que cessent pour elles les discriminations liées à un fauteuil roulant car l’accès à de nombreux lieux est trop souvent difficile. L’amélioration de la législation européenne est une condition indispensable pour lutter contre les discriminations – et je le répète – c’est une condition indispensable. Nous avons besoin de cette législation.
Très jeunes, nos enfants subissent des discriminations qui les traumatisent et ils portent tout au long de leur vie le poids de ces discriminations. J’attire notamment l’attention sur les discriminations multiples. La Commission, d’ailleurs, avait omis de les inclure dans sa proposition. Nous proposons donc une définition précise de ces discriminations.
Nous devons absolument renforcer la législation afin de rendre effective l’égalité de traitement, quelles que soient les différences. À cet égard, nous demandons que les États membres prennent des mesures visant à promouvoir l’égalité de traitement et l’égalité des chances quelles que soient la religion, le handicap, l’âge ou encore l’orientation sexuelle.
Et, pour conclure, je tiens à ajouter que nous espérons, pour 2010, une proposition de la Commission visant à mettre sur le même pied la discrimination fondée sur le sexe, ce qui mettrait fin à toute hiérarchie des droits. 
