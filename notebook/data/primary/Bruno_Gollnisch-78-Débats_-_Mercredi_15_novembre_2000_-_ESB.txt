Bruno Gollnisch # Débats - Mercredi 15 novembre 2000 - ESB et interdiction des farines animales dans l'alimentation de tous les animaux
  Gollnisch (TDI
). 
 - Madame la Présidente, je voudrais présenter un très bref fait personnel, puisque notre collègue, Graefe zu Baringdorf a incriminé le non-participation des membres de mon groupe à la commission d'enquête sur l'ESB. Je souhaiterais juste apporter la précision suivante : il y a eu deux commissions d'enquête au sein de ce Parlement. M. Martinez a participé à la première et je tiens, ici même, à la disposition de notre collègue, son opinion minoritaire qui a été formulée par écrit lors de la première commission, la commission Böge.
Nous n'avons pas participé à la deuxième commission parce que tout simplement nous en avons été exclus par le mécanisme de la loi d'Hondt et par les règlements ou le manque de règlements, je ne sais pas, de ce Parlement.
La meilleure façon de ne pas nous entendre, c'est de ne pas nous inviter. En tout état de cause, M. Martinez avait formulé son avertissement solennel en 1990, soit 6 ans avant la première commission d'enquête. 
