Nathalie Griesbeck # Débats - Mardi 23 octobre 2007 - Projet de budget général 2008 (section III) – Projet de budget général 2008 (sections I, II, IV, V, VI, VII, VIII, IX) (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Nathalie Griesbeck  (ALDE
). - 
 Monsieur le Président, mes chers collègues, une minute et demie pour s'exprimer sur le projet de budget 2008 est un exercice redoutable, mais je souhaite néanmoins tout d'abord prendre le temps, pardon, de remercier nos rapporteurs, Kyösti Virrankoski et Ville Itälä, avec qui nous avons dû procéder avec grande difficulté aux arbitrages que la commission des budgets vous propose aujourd'hui. Bien sûr, le cadre budgétaire pluriannuel 2007-2013 nous engonce dans une rigueur qui ne nous permet malheureusement pas de poursuivre, ni aussi fermement ni surtout aussi vite, les objectifs que nous nous sommes donnés pour l'Europe et dont les politiques constituent, à mes yeux, le seul salut pour nos régions européennes en étant capables de donner à nos concitoyens le confort de vie auquel ils aspirent dans un environnement mondial métamorphosé qui bouge et qui évolue sans cesse. J'invite donc notre Parlement à soutenir les initiatives de notre commission, surtout dans la perspective de la renégociation à mi-parcours du cadre pluriannuel et de la définition également d'un nouveau mécanisme de ressources propres.
Concernant ce budget et en raison du peu de temps dont je dispose, je souhaite ne m'exprimer finalement que sur un seul point. J'ai choisi, en qualité de rapporteur permanent pour les Fonds structurels dans la COBU, d'interpeller les membres de la Commission européenne sur les raisons de la mise en réserve de 30 % des dépenses administratives relatives à ces fonds. L'étude que nous avons récemment présentée avec Kyösti, portant sur la mise en œuvre des Fonds structurels, met en évidence le retard considérable que la Commission a pris dans la validation des cadres nationaux et des programmes opérationnels, avec un reste à liquider record de trois années; et avec cette prise de position ferme sur la réserve, nous avons voulu tirer la sonnette d'alarme. Les politiques régionales, tout comme le soutien à la recherche et à l'innovation, constituent des politiques capitales pour l'avenir de notre économie, donc de nos emplois, donc de la qualité de vie des Européens. Nous voulons faire vite, mais nous voulons faire bien, ce qui explique le niveau exigeant des crédits que nous proposons au Conseil de soutenir. 
