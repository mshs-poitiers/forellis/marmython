Jean-Claude Martinez # Débats - Mercredi 14 novembre 2007 - L'intérêt européen: réussir le défi de la mondialisation (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean-Claude Martinez  (ITS
). - 
(FR)
 Monsieur le Président, mes chers collègues, globalisation, mondialisation, ou selon le jésuite Teilhard de Chardin, planétisation, le constat est banal: nous sommes en face d'une deuxième mondialisation, plus complète que celle des années 1900, puisqu'elle est à la fois financière, économique, linguistique, migratoire et idéologique, avec un modèle dominant, le marché.
Cette mondialisation, autre banalité, entraîne des effets négatifs au Sud avec la surexploitation, en Inde et en Chine, des hommes des sols, des forêts, des mers, des eaux, et la mise en danger des droits de l'homme. Au Nord, ce sont les délocalisations, les pertes d'emplois, la déstabilisation financière de nos systèmes sociaux et, du fait du vieillissement et des coûts liés au quatrième âge, le risque pour l'Europe de se transformer en Rwanda gériatrique, avec ce que cela implique de mépris de la vie et, partant, d'atteintes aux droits de l'homme.
Face à ces réalités, à ces banalités, que fait-on? On fait de l'incantatoire, du dérisoire et du divinatoire. Pour ce qui est de l'incantatoire, par exemple, cela consiste, en ce moment, à faire des débats, des résolutions. L'incantatoire politique consiste à invoquer la stratégie de Lisbonne, l'économie la plus compétitive. On croirait Khrouchtchev à l'ONU dans les années 60, qui voulait rattraper le système capitaliste. C'est Harry Potter face à la mondialisation.
On fait aussi du dérisoire. L'exemple parfait, c'est le fonds budgétaire sur la mondialisation, avec quelques sucettes financières. Faute de maîtriser les choses, on s'en remet à Dieu et on fait du divinatoire. Au nom du Père Adam Smith, du Fils Ricardo et du Saint-Esprit du marché, dans le grand temple planétaire de la libre-échangéologie, on réduit les droits de douane pour finir par les supprimer.
Eh bien cela, c'est de la pensée magique. Or, le génie de l'Europe, c'est d'avoir inventé, il y a 2 500 ans, la pensée logique, c'est-à-dire la raison. Or, la raison, c'est de dire: le libre commerce est nécessaire, mais les protections sociales, culturelles sont aussi nécessaires. Il faut donc concilier la liberté des échanges et la sécurité des hommes.
Pour cela, il existe une nouvelle technologie douanière, la technologie des droits de douane déductibles, c'est-à-dire des droits de douane supportés, certes, par l'exportateur, mais qui lui ouvrent un crédit douanier égal au montant des droits de douane qu'il a supportés et qui peuvent être déduits de ses achats dans le pays de l'importateur. Grâce à ces droits de douane de nouvelle génération modulables, remboursables, négociables et bonifiables, on résoudra le problème banal des asymétries économiques, sociales, environnementales dans les échanges internationaux entre le Nord et le Sud. 
