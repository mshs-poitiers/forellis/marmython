Martine Roure # Débats - Mardi 9 mars 2004 - Votes
  Roure (PSE
),
par écrit
. 
- La situation dans les prisons en Europe est alarmante! Le rapport sur lequel nous nous prononçons aujourd’hui revient sur l’inflation de la population carcérale, le surpeuplement, l’augmentation du nombre de détenus étrangers et de ceux en attente de condamnation définitive.
Un rapport du Conseil de l’Europe rappelle aussi la persistance de mauvais traitements, l’inadaptation ou l’insuffisance des structures pénitentiaires, des activités prévues et des soins proposés, le développement de la toxicomanie.
Je soutiens l’élaboration d’une charte pénitentiaire européenne contenant des règles contraignantes pour les États. Nous invitons les États à débloquer des fonds pour la modernisation des prisons et pour une formation plus adaptée de la police et du personnel pénitentiaire. Nous proposons une application plus étendue des peines de substitution à l’incarcération et la possibilité pour les députés européens de visiter les prisons.
Mes amendements déposés en commission ont tous été adoptés et visent à renforcer le rapport sur plusieurs points: nous devons garantir l’accès à des structures de préparation à la réinsertion pour réduire les récidives. Il est essentiel de renforcer la protection contre la violence des codétenus et la prévention du suicide. Les mineurs et les femmes, en particulier les femmes enceintes ou mères de jeunes enfants, doivent particulièrement être protégées. 
