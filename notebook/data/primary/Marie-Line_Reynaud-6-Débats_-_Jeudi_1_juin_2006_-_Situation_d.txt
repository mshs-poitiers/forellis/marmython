Marie-Line Reynaud # Débats - Jeudi 1 juin 2006 - Situation des femmes Roms dans l’Union européenne (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Marie-Line Reynaud (PSE
). 
 - Monsieur le Président, je tiens à remercier Mme Járóka pour la qualité de son rapport et le sérieux avec lequel elle a considéré les amendements déposés. Je suis également très satisfaite des résultats du vote au sein de la commission des droits de la femme et de l’égalité des genres.
J’avais en effet insisté dans mes amendements sur quelques points qui me semblent fondamentaux dès lors qu’on s’intéresse à la situation des femmes roms en particulier: la nécessité pour les enfants roms de l’apprentissage de la lecture et de l’écriture, l’importance, également, de la mixité sociale en matière de logements ou encore le besoin d’aménager des aires d’accueil pour les Roms non sédentaires.
L’accès à l’éducation semble toujours poser problème à Mme le rapporteur, mais mes collègues de la commission des droits de la femme ont majoritairement choisi de soutenir mes propositions et je les en remercie. J’espère qu’il en sera de même en plénière et, plus généralement, que cet excellent rapport sera très largement approuvé, car ce sont des initiatives comme celles-ci qui contribuent à informer les citoyens européens et à améliorer le quotidien des Roms et leur intégration. 
