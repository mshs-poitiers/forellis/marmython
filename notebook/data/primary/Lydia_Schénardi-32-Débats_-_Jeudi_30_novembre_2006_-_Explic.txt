Lydia Schénardi # Débats - Jeudi 30 novembre 2006 - Explications de vote
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Espace de liberté, de sécurité et de justice (B6-0625/2006
) 
  Lydia Schenardi (NI
). 
 - Monsieur le Président, après avoir ouvert les vannes de l’immigration incontrôlée en supprimant les contrôles aux frontières de l’Union par les accords de Schengen et communautarisé des pans entiers de la législation en matière d’asile avec le traité d’Amsterdam, on nous propose, aujourd’hui, d’aller plus loin dans la délégation de compétences à l’Union en abandonnant les décisions à l’unanimité au Conseil pour les questions de coopération judiciaire et policière.
Décidément, nos eurocrates ne tirent de leçons de rien. Après s’être alarmés du déferlement sur les côtes espagnoles et italiennes de dizaines de milliers de clandestins profitant de l’absence de contrôle aux frontières pour pénétrer sur le territoire de l’Europe, après avoir amèrement constaté l’impuissance de l’Europe à gérer de telles situations, voilà que M. Cavada, reprenant le discours de M. Sarkozy, nous propose de réduire encore un peu plus le pouvoir de décision des États membres en matière de gestion de leurs flux migratoires en supprimant la règle de l’unanimité au Conseil par l’activation de la clause passerelle.
Ce n’est pas une Europe de type fédéral qui pourra enrayer les criminalités transfrontalières, bien au contraire, et, à l’instar de l’exemple suisse, il faut que les peuples européens reprennent en mains leur destin et puissent se protéger efficacement contre l’invasion migratoire. 
