Patrick Gaubert # Débats - Mercredi 14 juin 2006 - Montée des violences racistes et homophobes en Europe (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Patrick Gaubert, 
au nom du groupe PPE-DE
. - Monsieur le Président, mes chers collègues, l’Union européenne est fondée sur une communauté de valeurs indivisibles et universelles de dignité humaine, de liberté, d’égalité et de solidarité.
En s’unissant, nos pays ont décidé d’adhérer à ces valeurs et de les promouvoir. Chaque groupe politique a décidé de déposer une résolution pour faire suite aux déclarations que nous venons d’entendre; j’en ai moi-même rédigé une au nom du PPE-DE.
Nous constatons presque quotidiennement que la lutte contre les intolérances est bien loin d’être achevée. J’aurais préféré aborder ce sujet différemment. Il est affligeant de devoir rappeler publiquement que le racisme est intolérable dans nos sociétés. Récemment, dans nos pays, de nombreuses agressions racistes, xénophobes, antisémites et homophobes ont eu lieu, ce qui est intolérable et inacceptable.
Si en tant que citoyens, nous devons rester vigilants, en tant qu’élus nous devons être fermes et condamner avec énergie ces comportements; se taire, c’est les accepter. Nous devons aussi adopter des lois pour protéger nos concitoyens - la volonté politique est primordiale dans ce domaine -, et les pays non pourvus de législation antiraciste ou antidiscriminatoire doivent légiférer.
Je répète à nouveau ce que j’ai dit hier dans cet hémicycle et depuis de nombreux mois en commission. Le Conseil doit immédiatement cesser de bloquer la décision-cadre contre le racisme et la xénophobie, sinon les grandes déclarations de bonnes intentions ne servent à rien. Nos gouvernements respectifs doivent montrer l’exemple et avancer dans ce combat pour l’égalité, le respect de l’autre et la tolérance.
En ce qui concerne l’actualité, on assiste à une montée des partis d’extrême-droite dans bon nombre de nos pays, comme il est malheureusement nécessaire de le rappeler aujourd’hui. Bien que personnellement très engagé dans ce combat, je comprends les raisons qui ont conduit mon groupe à refuser de signer le texte commun: ce texte s’enferme dans des postures idéologiques qui ont fait faillite. Ce n’est pas en stigmatisant tel ou tel degré de l’actualité que l’on fera avancer ces pays sur le chemin du respect strict des valeurs de l’Union.
Il est inacceptable de confondre les cas d’agression individuelle commis dans des États luttant contre le racisme et l’homophobie avec des positions extrêmes prises ouvertement par certains gouvernements. Ces situations doivent être différenciées. Il est dangereux de faire des amalgames. Nous devons extraire cette question du débat relevant des calculs de circonstance. La lutte contre le racisme, contre la xénophobie, contre l’homophobie n’est ni de gauche, ni de droite; nous devons nous en convaincre. Voilà pourquoi la résolution commune qui sera mise aux voix demain me semble constituer un compromis équilibré.
Je trouve très regrettable - et je conclurai par là - que sur ce sujet, le Parlement hésite à parler d’une seule voix. Il s’agit là d’une occasion manquée, alors que je sais qu’ici au Parlement, c’est un combat que nous partageons tous.
(Applaudissements)

