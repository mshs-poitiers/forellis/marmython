Jean Marie Beaupuy # Débats - Lundi 9 mars 2009 - Statut de la société privée européenne - Transfert transfrontalier du siège social d’une société - Small Business Act - Participation des travailleurs dans les sociétés dotées d’un statut européen (débat) 
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
Textes déposés :
Débats :
Votes :
Textes adoptés :
  Jean Marie Beaupuy, 
au nom du groupe ALDE
. –
 Monsieur le Président, Monsieur le Commissaire, je voudrais bien sûr, moi aussi, me joindre aux compliments à notre collègue, Mme
 Herczog, pour l’excellent rapport en 90 points concernant le Small Business Act.
Mais le travail du Parlement européen ne trouvera, Monsieur le Commissaire, véritablement son efficacité qu’avec une mise en œuvre rapide et efficace dans chacun de nos États.
Vous avez dit, Monsieur le Commissaire, en commençant, que, en vous appuyant sur ce rapport, vous alliez élaborer une stratégie globale, mais vous n’ignorez pas qu’il y a actuellement une crise financière, une crise économique, une crise climatique qui nous imposent des réponses rapides.
Je prendrai donc deux exemples. Premier exemple: je demande à ce que les délais des règlements en faveur des entreprises soient rapportés à 30 jours. Cela est indiqué au point 87 pour le fonds de cohésion. Je demande à ce que la Commission et les États membres prennent des dispositions pour que tous les marchés publics puissent régler les entreprises à 30 jours.
Lorsque l’on sait, d’ailleurs, que 20 % des dépôts de bilan sont dus à des retards de paiement des collectivités publiques, nous mesurons là combien cette action des gouvernements et de la Commission permettra de réduire le nombre de chômeurs qui actuellement s’accroît de jour en jour.
Deuxième exemple: les formalités administratives. Au point 72, nous demandons de réduire d’au moins 25 % les formalités administratives actuelles. Je puis vous dire, moi, en tant que chef d’entreprise, qu’il s’agit là d’une disposition concrète que nous attendons depuis des années. Les chefs d’entreprise ne sont pas faits pour remplir des papiers à longueur de journée, ils sont faits pour apporter des produits et des services à nos concitoyens.
Si vous entendez cet appel, Monsieur le Commissaire, non seulement au-delà de la stratégie globale que vous voulez élaborer, mais en termes de réponse concrète des gouvernements et de la Commission, alors, dans le cadre des plans de relance actuels, il y aura des dispositifs qui apporteront des solutions à nos concitoyens, dès maintenant. 
