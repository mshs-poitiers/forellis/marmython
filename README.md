# Marmython

Support de formation : Algorithme + Programmation orienté gestion de <b>corpus numérique en SHS</b>


# Table des matières
* Le diaporama
	* [odp](marmython.odp)
	* [pdf](marmython.pdf)
	
* Le notebook
	* [Consulter](https://nbviewer.jupyter.org/urls/gitlab.huma-num.fr/mshs-poitiers/forellis/marmython/raw/master/notebook/marmython.ipynb?flush_cache=true)
	* [Executer](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmshs-poitiers%2Fforellis%2Fmarmython/16bf1829efb37cee925155b2e7c953113c67dc37?filepath=notebook%2Fmarmython.ipynb)

		
